/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.hl7.impl;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import org.junit.Before;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.contact.BasicAddressFormatter;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.hl7.patient.PatientContext;
import org.openvpms.hl7.patient.PatientContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Base class for HL7 message tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractMessageTest extends ArchetypeServiceTest {

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The medical record rules.
     */
    @Autowired
    private MedicalRecordRules medicalRecordRules;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The patient context.
     */
    private PatientContext context;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        LookupService lookups = getLookupService();
        IArchetypeService service = getArchetypeService();
        CustomerRules customerRules = new CustomerRules(service, lookups, new BasicAddressFormatter(service, lookups));
        Party owner = customerFactory.newCustomer()
                .firstName("Foo")
                .lastName("Bar")
                .newLocation()
                .address("123 Broadwater Avenue")
                .suburb("CAPE_WOOLAMAI_3058", "Cape Woolamai")
                .state("VIC", "Victoria")
                .postcode("3058")
                .purposes("HOME")
                .add()
                .newPhone().areaCode("03").phone("12345678").purposes("HOME").add()
                .newPhone().areaCode("03").phone("98765432").purposes("WORK").add()
                .build();
        Party patient = patientFactory.newPatient()
                .owner(owner)
                .name("Fido")
                .species("CANINE")
                .breed("KELPIE")
                .male()
                .dateOfBirth(getDate("2014-07-01"))
                .build();

        Party location = practiceFactory.newLocation().name("Main Clinic").build();

        Act visit = patientFactory.newVisit()
                .patient(patient)
                .startTime("2014-08-25 08:55:00")
                .build();

        patientFactory.newWeight()
                .patient(patient)
                .weight(BigDecimal.TEN, WeightUnits.KILOGRAMS)
                .startTime("2014-08-25 08:57:00")
                .build();

        createAllergy(patient, "Penicillin", "Respiratory distress");
        createAllergy(patient, "Pollen", "Produces hives");

        User clinician = userFactory.createClinician("Joe", "Blogs");
        PatientContextFactory factory = new PatientContextFactory(patientRules, customerRules, medicalRecordRules,
                                                                  getArchetypeService(), lookups);
        context = factory.createContext(patient, owner, visit, location, clinician);
        context = Mockito.spy(context);
        Mockito.when(context.getVisitId()).thenReturn(3001L);
    }

    /**
     * Returns the patient context.
     *
     * @return the patient context
     */
    protected PatientContext getContext() {
        return context;
    }

    /**
     * Helper to create a {@code Calendar} from a date-time string in {@code java.sql.Date} format.
     *
     * @param datetime the date-time
     * @return a new {@code Calendar}
     */
    protected Calendar getDatetime(String datetime) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(TestHelper.getDatetime(datetime));
        calendar.setTimeZone(TimeZone.getTimeZone("GMT+10:00"));
        return calendar;
    }

    /**
     * Logs a message.
     *
     * @param text    text
     * @param message the message
     */
    protected void log(String text, Message message) {
        String log;
        try {
            log = message.encode();
            log = log.replaceAll("\\r", "\n");
        } catch (HL7Exception exception) {
            log = exception.getMessage();
        }

        System.out.println(text + " " + log);
    }

    /**
     * Helper to create an allergy record for a patient.
     *
     * @param patient the patient
     * @param reason  the reason
     * @param notes   the allergy notes
     */
    private void createAllergy(Party patient, String reason, String notes) {
        Entity alertType = patientFactory.newAlertType()
                .name("Allergy")
                .alertType("ALLERGY")
                .build();
        patientFactory.newAlert()
                .patient(patient)
                .alertType(alertType)
                .reason(reason)
                .notes(notes)
                .build();
    }
}
