/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.hl7.impl;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.user.User;
import org.openvpms.hl7.io.MessageDispatcher;
import org.openvpms.hl7.io.MessageService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Test implementation of the {@link MessageDispatcher}.
 *
 * @author Tim Anderson
 */
public class TestMessageDispatcher extends MessageDispatcherImpl {

    /**
     * The queued messages.
     */
    private final List<Message> messages = new ArrayList<>();

    /**
     * The processed messages.
     */
    private final List<DocumentAct> acts = new ArrayList<>();

    /**
     * Optional timestamp to assign to messages.
     */
    private Date timestamp;

    /**
     * Optional message control ID to assign to messages.
     */
    private long messageControlID = -1;

    /**
     * Used to wait for message send attempts.
     */
    private final Semaphore dispatchSemaphore = new Semaphore(0);

    /**
     * Used to wait for messages to be sent.
     */
    private final Semaphore sentSemaphore = new Semaphore(0);

    /**
     * If {@code true}, generate an exception on send.
     */
    private boolean exceptionOnSend;

    /**
     * The acknowledgment code to return in ACK messages.
     */
    private AcknowledgmentCode acknowledgmentCode;

    /**
     * The exception to populate ACK messages with.
     */
    private HL7Exception acknowledgmentException;

    /**
     * Determines if sending is simulated or not.
     */
    private boolean simulateSend = true;

    /**
     * Constructs an {@link TestMessageDispatcher} using mock connectors.
     *
     * @param messageService the message service
     * @param service        the archetype service
     * @param user           the user to initialise the security context in the dispatch thread
     */
    public TestMessageDispatcher(MessageService messageService, IArchetypeService service, User user) {
        this(messageService, Mockito.mock(ConnectorsImpl.class),
             getPracticeService(service, user));
    }

    /**
     * Constructs a {@link TestMessageDispatcher}.
     *
     * @param service         the message service
     * @param connectors      the connectors
     * @param practiceService the practice service
     */
    public TestMessageDispatcher(MessageService service, ConnectorsImpl connectors, PracticeService practiceService) {
        super(service, connectors, practiceService);
    }

    /**
     * Constructs a {@link TestMessageDispatcher}.
     *
     * @param messageService the message service
     * @param service        the archetype service
     * @param connectors     the connectors
     * @param user           the user to initialise the security context in the dispatch thread
     * @param context        the message context
     */
    public TestMessageDispatcher(MessageService messageService, IArchetypeService service, ConnectorsImpl connectors,
                                 User user, HapiContext context) {
        super(messageService, connectors, getPracticeService(service, user), context);
    }

    /**
     * Determines if message sends are simulated or not.
     * <p/>
     * Defaults to {@code true}
     *
     * @param simulate if {@code true} simulate sends, otherwise send them via TCP/IP
     */
    public void setSimulateSend(boolean simulate) {
        simulateSend = simulate;
    }

    /**
     * Waits at most 30 seconds for a send attempt.
     *
     * @return {@code true} if an attempt was made to send a message
     */
    public boolean waitForDispatch() {
        return tryAcquire(1, dispatchSemaphore);
    }

    /**
     * Waits at most 30 seconds for a message to be sent.
     *
     * @return {@code true} if a message was sent
     */
    public boolean waitForMessage() {
        return waitForMessages(1);
    }

    /**
     * Waits at most 30 seconds for message to be sent.
     *
     * @param count the no. of messages to wait for
     * @return {@code true} if a message was sent
     */
    public boolean waitForMessages(int count) {
        return tryAcquire(count, sentSemaphore);
    }

    /**
     * Returns the sent messages.
     *
     * @return the sent messages
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * Returns the processed acts.
     *
     * @return the acts
     */
    public List<DocumentAct> getProcessed() {
        return acts;
    }

    /**
     * Overrides the default message timestamp.
     *
     * @param timestamp the timestamp, or {@code null} to use the default
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Overrides the message control ID.
     *
     * @param id the message control ID, or {@code -1} to use the default
     */
    public void setMessageControlID(long id) {
        this.messageControlID = id;
    }

    /**
     * Determines if an exception should be simulated on send.
     *
     * @param exception if {@code true}, throw an exception on send
     */
    public void setExceptionOnSend(boolean exception) {
        exceptionOnSend = exception;
    }

    /**
     * Sets the acknowledgment code to return in ACK messages.
     *
     * @param code the code. May be {@code null} to indicate the default
     */
    public void setAcknowledgmentCode(AcknowledgmentCode code) {
        acknowledgmentCode = code;
    }

    /**
     * Sets the exception to return in negative-ACK messages.
     *
     * @param exception the exception. May be {@code null}
     */
    public void setAcknowledgmentException(HL7Exception exception) {
        acknowledgmentException = exception;
    }

    /**
     * Sends a message, and returns the response.
     *
     * @param message the message to send
     * @param sender  the sender configuration
     * @param queue   the message queue
     * @return the response
     * @throws HL7Exception if the connection can not be initialised for any reason
     * @throws LLPException for any LLP error
     * @throws IOException  for any I/O error
     */
    @Override
    protected Message send(Message message, MLLPSender sender, MessageQueue queue) throws HL7Exception, LLPException,
                                                                                          IOException {
        acts.add(queue.peekFirstAct());
        messages.add(message);

        if (exceptionOnSend) {
            throw new IOException("simulated send exception");
        }
        Message response;
        if (!simulateSend) {
            response = super.send(message, sender, queue);
        } else {
            response = (acknowledgmentCode == null) ? message.generateACK()
                                                    : message.generateACK(acknowledgmentCode, acknowledgmentException);
        }
        return response;
    }


    /**
     * Processes an object.
     *
     * @param object the object to process
     * @param queue  the queue
     * @throws Exception for any error
     */
    @Override
    protected void process(Message object, MessageQueue queue) throws Exception {
        try {
            super.process(object, queue);
        } finally {
            dispatchSemaphore.release();
            if (queue.getErrorTimestamp() == null) {
                sentSemaphore.release();
            }
        }
    }

    /**
     * Creates a new timestamp for MSH-7.
     *
     * @return a new timestamp
     */
    @Override
    protected Date createMessageTimestamp() {
        return timestamp != null ? timestamp : super.createMessageTimestamp();
    }

    /**
     * Creates a new message control ID, for MSH-10.
     *
     * @return a new ID
     * @throws IOException if the ID cannot be generated
     */
    @Override
    protected String createMessageControlID() throws IOException {
        return messageControlID != -1 ? Long.toString(messageControlID) : super.createMessageControlID();
    }

    /**
     * Returns a {@link PracticeService} with a pre-configured service user.
     *
     * @param service the archetype service
     * @param user    the service user
     * @return a new {@link PracticeService}
     */
    private static PracticeService getPracticeService(IArchetypeService service, User user) {
        return new PracticeService(service, new PracticeRules(service, null), null) {
            @Override
            public User getServiceUser() {
                return user;
            }
        };
    }

    /**
     * Waits at most 30 seconds for a semaphore to be released.
     *
     * @param count     the no. of permits to acquire
     * @param semaphore the semaphore
     * @return if the permits were acquired
     */
    private boolean tryAcquire(int count, Semaphore semaphore) {
        boolean result = false;
        try {
            result = semaphore.tryAcquire(count, 30, TimeUnit.SECONDS);
        } catch (InterruptedException ignore) {
            // do nothing
        }
        return result;

    }

}
