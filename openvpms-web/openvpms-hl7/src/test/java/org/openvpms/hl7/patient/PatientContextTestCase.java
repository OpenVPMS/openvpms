/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.hl7.patient;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.contact.BasicAddressFormatter;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.lookup.LookupService;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link PatientContext} class.
 *
 * @author Tim Anderson
 */
public class PatientContextTestCase extends ArchetypeServiceTest {

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The medical record rules.
     */
    @Autowired
    private MedicalRecordRules medicalRecordRules;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The patient context factory.
     */
    private PatientContextFactory factory;


    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        IArchetypeService service = getArchetypeService();
        LookupService lookups = getLookupService();
        CustomerRules customerRules = new CustomerRules(service, lookups, new BasicAddressFormatter(service, lookups));
        factory = new PatientContextFactory(patientRules, customerRules, medicalRecordRules, service, lookups);
    }

    /**
     * Tests the {@link PatientContext#getClinicianFirstName()} and {@link PatientContext#getClinicianLastName()}
     * methods.
     */
    @Test
    public void testClinicianName() {
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        Act visit = patientFactory.createVisit(patient);
        Party location = practiceFactory.createLocation();

        User clinician1 = userFactory.createClinician("J", "Smith"); // firstName, lastName populated
        PatientContext context1 = factory.createContext(patient, customer, visit, location, clinician1);
        assertEquals("J", context1.getClinicianFirstName());
        assertEquals("Smith", context1.getClinicianLastName());

        // the following represents the original behaviour, where no firstName nor lastName is provided,
        // and the first and last names are derived by splitting the name

        User clinician2 = userFactory.newUser().name("J Smith").clinician().build();
        PatientContext context2 = factory.createContext(patient, customer, visit, location, clinician2);
        assertEquals("J", context2.getClinicianFirstName());
        assertEquals("Smith", context2.getClinicianLastName());

        User clinician3 = userFactory.newUser().name("Dr J Smith").clinician().build();
        PatientContext context3 = factory.createContext(patient, customer, visit, location, clinician3);
        assertEquals("Dr", context3.getClinicianFirstName());
        assertEquals("J Smith", context3.getClinicianLastName());

        User clinician4 = userFactory.newUser().name("Admin").clinician().build();
        PatientContext context4 = factory.createContext(patient, customer, visit, location, clinician4);
        assertNull(context4.getClinicianFirstName());
        assertEquals("Admin", context4.getClinicianLastName());
    }
}