#
# Version: 1.0
#
# The contents of this file are subject to the OpenVPMS License Version
# 1.0 (the 'License'); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
# http://www.openvpms.org/license/
#
# Software distributed under the License is distributed on an 'AS IS' basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# for the specific language governing rights and limitations under the
# License.
#
# Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
#

ApplicationContentPane.separatorPosition             = 30
MainPane.separatorPosition                           = 17 * $font.size

MainPane.Menu.height                                 = max($font.size + 6, 18)
# the 18 takes into account icon heights

HelpDialog.header.separatorPosition                  = 6 * $font.size
HelpDialog.content.separatorPosition                 = 20 * $font.size
HelpDialog.content.size                              = 15 * $font.size
HelpDialog.footer.separatorPosition                  = 6 * $font.size

HelpBrowser.features                                 = concat('width=', 0.50 * $width, ',height=', 0.70 * $height, ',resizable=yes,left=', 0.40 * $width,',top=',0.15 * $height,',scrollbars=yes,location=yes,toolbar=yes')

PatientRecordWorkspace.Layout.separatorPosition      = 20 * $font.size

AbstractViewWorkspace.Layout.separatorPosition       = 5 * $font.size
BrowserCRUDWorkspace.Layout.separatorPosition        = 19 * $font.size

EditDialog.width                                     = 0.70 * $width
EditDialog.height                                    = 0.80 * $height
ChildEditDialog.width                                = 0.60 * $width
ChildEditDialog.height                               = 0.60 * $height

BrowserDialog.width                                  = 0.70 * $width
BrowserDialog.height                                 = 0.75 * $height

AppointmentBrowser.separatorPosition                 = 12 * $font.size
ScheduleBrowser.separatorPosition                    = 7 * $font.size

Calendar.separatorPosition                           = 9 * $font.size
Calendar.View.separatorPosition                      = 3 * $font.size
Roster.separatorPosition                             = 3 * $font.size

ExportPrintDialog.width                              = 50 * $font.size

MailDialog.width                                     = 70 * $font.size
MailDialog.height                                    = 50 * $font.size
MailEditor.message.height                            = 26 * $font.size
MailEditor.separatorPosition                         = 14 * $font.size
MailEditor.gridSeparatorPosition                     = 48 * $font.size
CheckInDialog.size                                   = '70x58'

ReminderGenerationDialog.width                       = 50 * $font.size
ReminderGenerationDialog.large.height                = 55 * $font.size
ReminderGenerationDialog.small.height                = 25 * $font.size

SMSConfigEmail.separatorPosition                     = 19 * $font.size
SMSConfigEmailGeneric.separatorPosition              = 25 * $font.size

SMSDialog.size                                       = '35x30'
SMSViewerDialog.size                                 = '50x30'
SMSViewer.message.background                         = '#d2e5f4'
SMSViewer.reply.background                           = '#f0f0f2'

AppointmentSMSDialog.size                            = '35x33'

SummaryDialog.width                                  = 25 * $font.size
SummaryDialog.height                                 = 40 * $font.size

PatientClinicalNote.note.height                      = 30 * $font.size
PatientClinicalNote.separatorPosition                = 37 * $font.size
PatientClinicalNote.NoteError.size                   = '60x40'
PatientVisitNote.note.height                         = 19 * $font.size
PatientVisitNote.separatorPosition                   = 30 * $font.size
PatientVisitNoteDialog.size                          = '80x50'

UserMessage.message.height                           = 28 * $font.size
SystemMessage.message.height                         = 10 * $font.size

InformationDialog.Compact.width                      = 35 * $font.size
InformationDialog.Compact.height                     = 10 * $font.size

AlertDialog.size                                     = '55x38'

ChargeRemovalConfirmationDialog.size                 = '80x30'
InvestigationOrderDialog.size                        = '50x30'

ExternalDocumentsDialog.size                         = '50x45'
EFTPaymentDialog.size                                = '40x30'

FlowSheetReportsDialog.size                          = '40x30'

GapPaymentPrompt.size                                = '55x40'

ProductOnOrderDialog.size                            = '45x28'

VisitEditorDialog.size                               = '100x60'
VisitEditor.TabbedPane.height                        = (60 * $font.size) - 8 - ($padding.small * 2 + $font.h4.size) - ($padding.medium * 2) - (8 + $font.size) - ($padding.tiny * 2 + $padding.smaller * 2 + 2 + $font.size) - 8 - (3*3) -10
# Need to set the tab pane height, to avoid scrollbars. Calculation of this is tedious, and involves subtracting the
# various component heights from that of the dialog height (must correspond to that of VisitEditorDialog.size)
#
# 60 * $font.size = dialog height
# 8 = title offset
# $padding.small * 2 + font.h4.size = WindowPane titleInsets
# $padding.medium * 2 = InsetY
# 7 + $font.size = tab button
# $padding.tiny * 2 + $padding.smaller * 2 + 2
# 32 is the size of buttonrowfill.png
# 8 = bottom offset
# 3 * 3 = 3 pixels per font instance. Firefox seems to stick a 3 pixel padding around labels.
# 10 - hack constant that gets rid of scrollbars.

LogViewer.separatorPosition                          = 5 * $font.size

dialog.title.height                                  = max($font.size + 2 + $padding.small * 2, 19)
# the 19 takes into account icon heights

dialog.margins                                       = 22
# left + right/top + bottom dialog margin

dialog.large.width                                   = 0.80 * $width
dialog.large.height                                  = 0.80 * $height
dialog.medium.width                                  = 0.50 * $width
dialog.medium.height                                 = 0.50 * $height
dialog.small.width                                   = 0.30 * $width
dialog.small.height                                  = $font.size * 25
dialog.closeicon                                     = 'closebutton-17x17.gif'

splitpane-buttonrow.separatorPosition                = min($font.size + 4 + $padding.smaller * 2 + $padding.tiny * 4, 32)
# 32 is the size of buttonrowfill.png

# font sizes were calculated using the following defaults from Firefox
# h1 = 32px, h2 = 24px, h3 = 18.7167px, h4 = 16px, h5 = 13.2833px, 10pt=16.6667px, 9pt = 15px, 8pt=	13.3333px, 7pt=11.6667px
font.typeface                                        = 'arial,sans-serif'
font.size                                            = 12
font.h1.size                                         = $font.size * 2.667
font.h2.size                                         = $font.size * 2
font.h3.size                                         = $font.size * 1.56
font.h4.size                                         = $font.size * 1.333
font.h5.size                                         = $font.size * 1.107
font.small.size                                      = $font.size

padding.large                                        = ($font.size div 3) * 4
padding.medium                                       = $font.size * 0.4
padding.small                                        = $font.size div 4
padding.smaller                                      = max($font.size div 5, 2)
padding.tiny                                         = 2

#
# Label alignment.
# This specifies the horizontal label alignment when:
# . editing and viewing objects
# . inputting report parameters
# Possible values are 'left' and 'right'
label.align                                          = 'left'

#
# Default theme. These act as the fallback if no custom theme is selected, or the custom theme doesn't
# provide some properties.
#
theme.background                                     = '#339933'
theme.foreground                                     = '#fdf5e6'
theme.title.background                               = '#ffffff'
theme.button.background                              = '#99cc66'
theme.button.foreground                              = '#000000'
theme.button.border                                  = '#7b68ee'
theme.button.pressedBackground                       = '#191970'
theme.button.pressedForeground                       = '#ffb6c1'
theme.button.pressedBorder                           = '#000000'
theme.button.rolloverBackground                      = '#99cc66'
theme.button.rolloverForeground                      = '#ffb6c1'
theme.button.disabledBackground                      = '#b0c391'
theme.button.disabledForeground                      = '#858879'
theme.button.disabledBorder                          = '#b0c391'
theme.selection.colour                               = '#85c1ff'

#
# Patient history colours
#
history.note.colour                                  = '#ffffff'
history.communication.colour                         = '#ececfc'
history.problem.colour                               = '#ffb1c6'
history.medication.colour                            = '#ffecb1'
history.weight.colour                                = '#d1ffb1'
history.attachment.colour                            = '#ffffff'
history.image.colour                                 = '#ffffff'
history.form.colour                                  = '#add8e6'
history.investigation.colour                         = '#90ee90'
history.letter.colour                                = '#add8e6'
history.invoice.colour                               = '#ffffff'

#
# Patient history field widths.
#
history.date.width                                   = $font.size * 10
history.type.width                                   = $font.size * 12.5
history.clinician.width                              = $font.size * 12.5

#
# Reminder colours. Default values are green, yellow and red respectively.
#
reminder.notdue.colour                               = '#00ff00'
reminder.due.colour                                  = '#ffff00'
reminder.overdue.colour                              = '#ff4040'

#
# Schedule status colours
#
schedule.pending.colour                              = '#ffffff'
schedule.confirmed.colour                            = '#ffffff'
schedule.checkedin.colour                            = '#dbf714'
schedule.inprogress.colour                           = '#ffcccc'
schedule.completed.colour                            = '#ccffff'
schedule.cancelled.colour                            = '#cc99ff'
schedule.noshow.colour                               = '#cc99ff'
schedule.billed.colour                               = '#ccffcc'
schedule.admitted.colour                             = '#ffcc99'

#
# Calendar event colours
#
calendar.event.colour                                = '#afafef'

# Selector field width
#
selector.width                                       = $font.size * 20

query.height                                         = $font.size * 2.8
Browser.separatorPosition                            = $font.size * 7
PagedIMTable.separatorPosition                       = $font.size * 2.4
TabbedBrowser.height                                 = $font.size * 3

SelectionDialog.list.height                          = $font.size * 15

#
# Edit dialog sizes for specific archetypes
#

#
# Contact
#
EditDialog.size.contact.phoneNumber                  = '50x30'
EditDialog.size.contact.email                        = '50x30'

#
# Customer
#
EditDialog.size.act.customerAccountChargesCounter    = '100x60'
EditDialog.size.act.customerAccountChargesCredit     = '100x60'
EditDialog.size.act.customerAccountChargesInvoice    = '100x60'
EditDialog.size.act.customerAlert                    = '82x35'

#
# Patient
#
EditDialog.size.act.patientAlert                     = '84x35'
EditDialog.size.act.patientClinicalEvent             = '70x25'
EditDialog.size.act.patientClinicalLink              = '70x25'
EditDialog.size.act.patientClinicalProblem           = '70x25'
EditDialog.size.act.patientDocumentAttachment        = '70x55'
EditDialog.size.act.patientDocumentAttachmentVersion = '70x25'
EditDialog.size.act.patientDocumentForm              = '65x25'
EditDialog.size.act.patientDocumentLetter            = '70x55'
EditDialog.size.act.patientDocumentLetterVersion     = '70x25'
EditDialog.size.act.patientDocumentImage             = '70x55'
EditDialog.size.act.patientDocumentImageVersion      = '70x25'
EditDialog.size.act.patientInsuranceClaim            = '100x55'
EditDialog.size.act.patientInsurancePolicy           = '60x25'
EditDialog.size.act.patientInvestigation             = '75x55'
EditDialog.size.act.patientInvestigationVersion      = '70x25'
EditDialog.size.act.patientMedication                = '80x40'
EditDialog.size.act.patientPrescription              = '80x40'
EditDialog.size.act.patientReminder                  = '70x55'
EditDialog.size.act.patientWeight                    = '45x25'

VetCheckDialog.size                                  = '64x48'

#
# Scheduling
#
EditDialog.size.act.calendarBlock                    = '70x30'
EditDialog.size.act.calendarEvent                    = '70x25'
EditDialog.size.act.customerAppointment              = '75x55'
EditDialog.size.act.customerTask                     = '70x25'
EditDialog.size.act.rosterEvent                      = '60x15'

#
# Products
#
EditDialog.size.product.medication                   = '100x55'
EditDialog.size.product.merchandise                  = '100x55'
EditDialog.size.product.service                      = '100x55'
EditDialog.size.product.template                     = '100x55'
EditDialog.size.product.priceTemplate                = '100x55'

#
# Batches
#
EditDialog.size.entity.productBatch                  = '70x55'

#
# Stock
#
EditDialog.size.act.stockAdjust                      = '60x58'
EditDialog.size.act.stockTransfer                    = '60x58'

#
# Organisation
#
EditDialog.size.party.organisationLocation           = '85x55'
EditDialog.size.party.organisationPractice           = '85x55'

#
# Lookup
#
EditDialog.size.lookup.oauth2ClientRegistration      = '75x25'

#
# General
#
PasswordDialog.size                                  = '35x20'
TOTPConfigurationDialog.size                         = '45x32'

#
# Admin
#
ChangePrinterDialog.size                             = '50x32'
SettingsDialog.size                                  = '60x40'
CacheDialog.size                                     = '70x30'
SynchronisationChangesDialog.size                    = '50x47'
ManagedTerminalRegistrationDialog.size               = '40x25'
MappingEditDialog.size                               = '70x58'
EditDialog.size.entity.globalSettingsFirewall        = '45x50'
EditDialog.size.entity.globalSettingsOpenOffice      = '45x20'
EditDialog.size.entity.globalSettingsPassword        = '60x20'
EditDialog.size.entity.globalSettingsQuery           = '45x20'
EditDialog.size.entity.globalSettingsReport          = '45x20'
LoggingDialog.size                                   = '70x40'

#
# BrowserDialog sizes
#

BrowserDialog.size.Mapping.Target                    = '60x50'

PreferencesDialog.size                               = '50x25'

#
# Length of numeric fields
#
numeric.length                                       = 14

#
# Maximum length for text in tables that support text abbreviation.
#
table.text.maxlength                                 = 30

#
# Maximum length for phone numbers and email addresses in the customer summary
#
customer.summary.contact.maxlength                   = 22

thumbnail.size                                       = 5 * $font.size