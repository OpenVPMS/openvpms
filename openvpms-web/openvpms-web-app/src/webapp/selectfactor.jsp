<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.openvpms.version.Version" %>
<%@ page import="org.openvpms.web.security.login.LoginStatus" %>
<%@ page import="org.openvpms.web.component.subscription.SubscriptionHelper" %>
<%@ page import="org.openvpms.web.resource.i18n.Messages" %>
<%@ page import="org.openvpms.web.system.ServiceHelper" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.openvpms.web.security.user.LoginUserDetails" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.openvpms.web.echo.button.ShortcutHelper" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  --%>

<!DOCTYPE html>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setHeader("Expires", "0"); // Proxies.
%>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="css/login.css" rel="stylesheet" media="screen"/>
    <title><%= Messages.get("login.title")%>
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
    <div class="modal-dialog">
        <div class="modal-content background modal-content-mobile">
            <div class="banner">
                <img alt="logo" class="logo" src="logo"/>
            </div>
            <div class="modal-body">
                <span class="description"><%= Messages.get("login.mfa.message") %></span>
                <%!
                    String email = null;
                    boolean totp = false;
                    String abbreviatedEmail = null;
                    String checked = "";
                    %>
                <%
                    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                    if (authentication.getPrincipal() instanceof LoginUserDetails) {
                        LoginUserDetails userDetails = (LoginUserDetails) authentication.getPrincipal();
                        email = userDetails.getEmail();
                        totp = userDetails.isTOTPConfigured();
                        if (email != null && email.length() >= 5) {
                            // only present an obfuscated email, for security
                            StringBuilder builder = new StringBuilder(email);
                            int start = 1;
                            int end = email.lastIndexOf('.');
                            if (end == -1) {
                                end = email.length() - 3;
                            }
                            abbreviatedEmail = builder.replace(start, end, StringUtils.repeat('*', end - start))
                                    .toString();
                        }
                        if (!totp) {
                            checked = "checked";
                        }
                    }
                %>

                <form action="loginfactorselected" method="POST">
                    <% if (totp) { %>
                        <input type="radio" id="factor1" name="factor" value="TOTP" required checked>
                        <label for="factor1"><%= Messages.get("login.mfa.totp") %></label><br>
                    <% } %>
                    <% if (abbreviatedEmail != null) { %>
                        <input type="radio" id="factor2" name="factor" value="EMAIL" required <%= checked %> >
                        <label for="factor2"><%= Messages.get("login.mfa.email") %> <%= abbreviatedEmail  %> </label><br>
                    <% } %>
                    <input name="submitButton" type="submit"
                           value="<%= Messages.get("login.mfa.next") %>"
                           class="btn btn-primary submitButton" aria-label="submit"/>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
