<%@ page import="org.openvpms.web.resource.i18n.Messages" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page errorPage="login.jsp" %>

<%--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  --%>

<!DOCTYPE html>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setHeader("Expires", "0"); // Proxies.
%>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="css/login.css" rel="stylesheet" media="screen"/>
    <title><%= Messages.get("login.title") %></title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <%@include file="disableDuplicateSubmit.html" %>
</head>
<body>
<div class="container">
    <div class="modal-dialog">
        <div class="modal-content background modal-content-mobile">
            <div class="banner">
                <img alt="logo" class="logo" src="logo"/>
            </div>
            <div class="modal-body">
                <h2><%= Messages.get("login.forgotpassword") %></h2>
                <div>
                    <label for="username" class="inputFieldLabel">
                        <%= Messages.get("login.reset.enterusername") %>
                    </label>

                    <form action="confirmforgotpassword" onsubmit="onSubmit(this);" method="POST">
                        <input id="username" name="username" type="text"
                               class="form-control inputField"
                               placeholder="<%= Messages.get("login.username") %>" autocapitalize="none" required
                               autofocus>

                        <button name="reset_password" type="submit"
                                class="btn btn-primary submitButton"><%= Messages.get("login.reset") %>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>