<%@ page import="org.openvpms.web.resource.i18n.Messages" %>
<%@ page import="org.openvpms.archetype.i18n.ArchetypeMessages" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  --%>

<%-- NOTE: this borrows heavily from the Amazon Cognito login pages --%>

<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setHeader("Expires", "0"); // Proxies.
%>
<!DOCTYPE html>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="css/login.css" rel="stylesheet" media="screen"/>
    <script src="js/jquery-3.6.0.min.js"></script>
    <title><%= Messages.get("login.title") %>
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <%@include file="disableDuplicateSubmit.html" %>

    <script>
        <%-- clear autocompleted password fields on page load. This is necessary for the oldPassword field in Firefox,
          -- which ignores autocomplete="off"
          --%>
        $(document).ready(function () {
            $('input[type="password"]').val('');
        });

        <%--
          -- Determines if the password is valid.
          --%>
        function isValid() {
            var valid = false;
            var oldPassword = $('#oldPassword').val()
            var newPassword = $('#newPassword').val()
            var confirmPassword = $('#confirmPassword').val();
            if (oldPassword && (newPassword || confirmPassword)) {
                if (oldPassword === newPassword) {
                    var notSameMessage = "<%= ArchetypeMessages.newPasswordMustBeDifferentToOldPassword() %>";
                    showPasswordCheck(".checkNotSame", false, notSameMessage);
                } else {
                    hidePasswordCheck(".checkNotSame");
                    valid = true;
                }
                if (valid) {
                    valid = checkPassword(newPassword) && checkPasswordsMatch(newPassword, confirmPassword);
                }
            }
            $('button[name="resetPassword"]').prop("disabled", !valid);
        }

        <jsp:include page="passwordValidation.jsp"/>

    </script>
</head>
<body>

<div class="container">
    <div class="modal-dialog">
        <div class="modal-content background modal-content-mobile">
            <div class="banner"><img alt="logo" class="logo" src="logo"/>
            </div>
            <div class="modal-body">
                <c:if test="${requestScope.passwordmismatch}">
                    <p class="errorMessage">
                        <%= Messages.get("login.change.passwordmismatch") %>
                    </p>
                </c:if>
                <form action="confirmchangepassword" onsubmit="onSubmit(this);" method="POST">
                    <span class="description"><%= Messages.get("login.change.description")%></span>
                    <label for="oldPassword"><%= Messages.get("login.oldpassword") %></label>
                    <%-- NOTE: want users to be forced to enter the old password rather than using autocomplete
                      -- This ensures that other users can't rely on a saved browser password to change the password
                      --%>
                    <input id="oldPassword" onkeyup="isValid()" class="form-control inputField" type="password"
                           name="oldPassword" autocomplete="off" required>
                    <label for="newPassword"><%= Messages.get("login.newpassword")%></label>
                    <input id="newPassword" onkeyup="isValid()" class="form-control inputField"
                           type="password" name="newPassword" autocomplete="new-password" required>
                    <label for="confirmPassword"><%= Messages.get("login.confirmpassword") %>
                    </label>
                    <input id="confirmPassword" onkeyup="isValid()" class="form-control inputField"
                           type="password" name="confirmPassword" autocomplete="new-password" required>

                    <div class="checkNotSame">
                        <span aria-hidden="true" class="checkNotSameFlag"></span>
                        <span class="checkNotSameText"></span>
                    </div>
                    <%@include file="passwordValidationLayout.html" %>

                    <button name="resetPassword" type="submit"
                            class="btn btn-primary submitButton"><%= Messages.get("login.reset.change") %>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>

</html>