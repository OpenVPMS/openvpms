<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.openvpms.version.Version" %>
<%@ page import="org.openvpms.web.security.login.LoginStatus" %>
<%@ page import="org.openvpms.web.component.subscription.SubscriptionHelper" %>
<%@ page import="org.openvpms.web.resource.i18n.Messages" %>
<%@ page import="org.openvpms.web.system.ServiceHelper" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  --%>

<%-- NOTE: this borrows heavily from the Amazon Cognito login pages --%>

<!DOCTYPE html>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="css/login.css" rel="stylesheet" media="screen"/>
    <title><%= Messages.get("login.title")%>
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
    <div class="modal-dialog">
        <div class="modal-content background modal-content-mobile">
            <div class="banner">
                <img alt="logo" class="logo" src="logo"/>
            </div>
            <div class="modal-body">
                <span class="description"><%= Messages.get("login.message") %></span>
                <form action="login" method="POST">
                    <c:if test="${param.status == LoginStatus.ERROR}">
                        <p class="errorMessage"><%= Messages.get("login.error")%>
                        </p>
                    </c:if>
                    <c:if test="${param.status == LoginStatus.EXPIRED}">
                        <%-- from confirmresetpassword, when the code has expired --%>
                        <p class="errorMessage"><%= Messages.get("login.codeexpired") %>
                        </p>
                    </c:if>
                    <c:if test="${param.status == LoginStatus.NO_ACCESS_FROM_HOST}">
                        <p class="errorMessage"><%= Messages.get("login.noaccess") %>
                        </p>
                    </c:if>
                    <label for="username" class="inputFieldLabel"><%= Messages.get("login.username")%>
                    </label>
                    <input id="username" name="username" type="text" class="form-control inputField"
                           placeholder="<%= Messages.get("login.username")%>" autocapitalize="none" required autofocus>

                    <label for="password" class="inputFieldLabel"><%= Messages.get("login.password")%>
                    </label>
                    <input id="password" name="password" type="password" class="form-control inputField"
                           placeholder="<%= Messages.get("login.password")%>" required>
                    <a class="link" href="forgotpassword"><%= Messages.get("login.forgotpassword")%>
                    </a>
                    <input name="submitButton" type="submit" value="<%= Messages.get("login.submit")%>"
                           class="btn btn-primary submitButton" aria-label="submit"/>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- suppress the footer on screens < 768px -->
<footer class="footer navbar-fixed-bottom hidden-xs">
    <div style="white-space:pre"><%= SubscriptionHelper.formatSubscription(ServiceHelper.getArchetypeService()) %>
    </div>
    <div class="version"><%= Messages.format("openvpms.version", Version.VERSION, Version.REVISION) %>
    </div>
</footer>

</body>
</html>
