<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.openvpms.version.Version" %>
<%@ page import="org.openvpms.web.security.login.LoginStatus" %>
<%@ page import="org.openvpms.web.component.subscription.SubscriptionHelper" %>
<%@ page import="org.openvpms.web.resource.i18n.Messages" %>
<%@ page import="org.openvpms.web.system.ServiceHelper" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  --%>

<!DOCTYPE html>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setHeader("Expires", "0"); // Proxies.
%>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="css/login.css" rel="stylesheet" media="screen"/>
    <title><%= Messages.get("login.title")%>
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<div class="container">
    <div class="modal-dialog">
        <div class="modal-content background modal-content-mobile">
            <div class="banner">
                <img alt="logo" class="logo" src="logo"/>
            </div>
            <div class="modal-body">
                <c:if test="${requestScope.codemismatch}">
                    <p class="errorMessage">
                        <%= Messages.get("login.mfa.codemismatch") %>
                    </p>
                </c:if>
                    <c:set var="label">
                        <c:choose>
                            <c:when test="${requestScope.factor == 'TOTP'}">
                                <%= Messages.get("login.mfa.entertotp")%>
                            </c:when>
                            <c:otherwise>
                                <%= Messages.get("login.mfa.entercode")%>
                            </c:otherwise>
                        </c:choose>
                    </c:set>
                    <form action="login2code" method="POST">
                        <label for="code" class="inputFieldLabel"><c:out value="${label}"/></label>
                        <input id="code" name="code" type="text" class="form-control inputField"
                               placeholder="<%= Messages.get("login.mfa.code")%>" autocapitalize="none" required
                               autofocus>
                        <a class="link" href="selectfactor"><%= Messages.get("login.mfa.tryanotherfactor")%>
                        </a>
                        <input id="factor" type="hidden" name="factor" value="${requestScope.factor}">
                        <input id="id" type="hidden" name="id" value="${requestScope.id}">
                        <input name="submitButton" type="submit" value="<%= Messages.get("login.submit")%>"
                               class="btn btn-primary submitButton" aria-label="submit"/>
                        <br/>
                    </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
