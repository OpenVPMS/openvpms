<%@ page import="org.openvpms.archetype.rules.user.PasswordPolicy" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="org.openvpms.web.resource.i18n.Messages" %>
<%@ page import="org.openvpms.archetype.i18n.ArchetypeMessages" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  --%>

<%-- NOTE: this borrows heavily from the Amazon Cognito login pages --%>

<!DOCTYPE html>
<%
    response.setHeader("Cache-Control","no-cache");
    response.setHeader("Pragma","no-cache");
    response.setDateHeader ("Expires", -1);
%>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="css/login.css" rel="stylesheet" media="screen"/>
    <script src="js/jquery-3.6.0.min.js"></script>
    <title><%= Messages.get("login.title") %>
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <%@include file="disableDuplicateSubmit.html" %>

    <script>
        <%-- clear autocompleted password fields on page load. This is necessary for the code field in Firefox,
          -- which ignores autocomplete="off"
          --%>
        $(document).ready(function () {
            $('input[type="password"]').val('');
        });

        <%--
          -- Determines if the password is valid.
          --%>
        function isValid() {
            var password = $('#password').val()
            var confirmPassword = $('#confirmPassword').val();
            var valid = checkPassword(password) && checkPasswordsMatch(password, confirmPassword);
            $('button[name="resetPassword"]').prop("disabled", !valid);
        }

        <jsp:include page="passwordValidation.jsp"/>

    </script>
<body>

<div class="container">
    <div class="modal-dialog">
        <div class="modal-content background modal-content-mobile">
            <div class="banner"><img alt="logo" class="logo" src="logo"/>
            </div>
            <div class="modal-body">
                <c:if test="${requestScope.codemismatch}">
                    <p class="errorMessage">
                        <%= Messages.get("login.reset.codemismatch") %>
                    </p>
                </c:if>
                <c:if test="${empty requestScope.id}">
                    <%-- if the page is refreshed, the id is lost. Need to return to the login page. --%>
                    <% session.invalidate(); %>
                    <p class="errorMessage">
                        <%= Messages.get("login.cantrefresh") %>
                    </p>
                    <a class="link" href="login"><%= Messages.get("login.returntologin")%>
                    </a>
                </c:if>
                <c:if test="${not empty requestScope.id}">
                    <form action="confirmresetpassword" onsubmit="onSubmit(this);" method="POST">
                        <span class="description"><%= Messages.get("login.reset.entercode")%></span>
                        <label for="code"><%= Messages.get("login.reset.code") %>
                        </label>
                        <input id="code" class="form-control inputField" type="password" name="code" autocomplete="off"
                               required autofocus>
                        <label for="password"><%= Messages.get("login.newpassword")%>
                        </label>
                        <input id="password" onkeyup="isValid()" class="form-control inputField"
                               type="password" name="password" autocomplete="new-password" required>
                        <label for="confirmPassword"><%= Messages.get("login.confirmpassword") %>
                        </label>
                        <input id="confirmPassword" onkeyup="isValid()" class="form-control inputField"
                               type="password" name="confirmPassword" autocomplete="new-password" required>
                        <input id="id" type="hidden" name="id" value="${requestScope.id}">

                        <%@include file="passwordValidationLayout.html"%>

                        <button name="resetPassword" type="submit"
                                class="btn btn-primary submitButton"><%= Messages.get("login.reset.change") %>
                        </button>
                    </form>
                </c:if>
            </div>
        </div>
    </div>
</div>

</body>

</html>