<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">

    <display-name>OpenVPMS</display-name>
    <description>OpenVPMS</description>
    <!-- Needed by spring if you want to deploy app more than once as different name. Change value
      -  to something unique -->
    <context-param>
        <param-name>webAppRootKey</param-name>
        <param-value>openvpms</param-value>
    </context-param>

    <!-- Acegi/Spring config -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>
            /WEB-INF/applicationContext.xml
        </param-value>
    </context-param>

    <!-- this filter is used to rewrite through the /s/* filter to add caching headers. see: urlrewrite.xml -->
    <filter>
        <filter-name>UrlRewriteFilter</filter-name>
        <filter-class>org.tuckey.web.filters.urlrewrite.UrlRewriteFilter</filter-class>
    </filter>

    <!-- Setup the Spring Security Filter Chain-->
    <filter>
        <filter-name>springSecurityFilterChain</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>
    <filter>
        <filter-name>log4JMDCUserFilter</filter-name>
        <filter-class>org.openvpms.web.echo.servlet.Log4JMDCUserFilter</filter-class>
    </filter>
    <filter-mapping>
        <!-- used by plugins to serve static resources -->
        <filter-name>UrlRewriteFilter</filter-name>
        <url-pattern>/s/*</url-pattern>
    </filter-mapping>
    <filter-mapping>
        <filter-name>springSecurityFilterChain</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <filter-mapping>
        <filter-name>log4JMDCUserFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <!-- uncomment the following to log oauth2 flows -->
    <!-- filter>
        <filter-name>requestLoggingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CommonsRequestLoggingFilter</filter-class>
        <init-param>
            <param-name>includeClientInfo</param-name>
            <param-value>true</param-value>
        </init-param>
        <init-param>
            <param-name>includePayload</param-name>
            <param-value>true</param-value>
        </init-param>
        <init-param>
            <param-name>includeQueryString</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>requestLoggingFilter</filter-name>
        <url-pattern>/oauth2/*</url-pattern>
    </filter-mapping-->

    <!--
     - Loads the root application context of this web app at startup,
     - by default from "/WEB-INF/applicationContext.xml".
     - Use WebApplicationContextUtils.getWebApplicationContext(servletContext)
     - to access it anywhere in the web application, outside of the framework.
     -->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>

    <!--  Setup spring security to subscribe to http session events in the web context -->
    <listener>
        <listener-class>org.springframework.security.web.session.HttpSessionEventPublisher</listener-class>
    </listener>

    <!--Support the scoping of beans at the request, session, and global session levels (web-scoped beans) -->
    <listener>
        <listener-class>org.springframework.web.context.request.RequestContextListener</listener-class>
    </listener>

    <listener>
        <listener-class>org.openvpms.web.echo.servlet.SessionListener</listener-class>
    </listener>

    <servlet>
        <servlet-name>LoginServlet</servlet-name>
        <jsp-file>/login.jsp</jsp-file>
    </servlet>

    <servlet>
        <servlet-name>SelectFactorServlet</servlet-name>
        <jsp-file>/selectfactor.jsp</jsp-file>
    </servlet>

    <servlet>
        <servlet-name>LoginFactorSelectedServlet</servlet-name>
        <servlet-class>org.openvpms.web.security.login.LoginFactorSelectedServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>Login2Servlet</servlet-name>
        <jsp-file>/login2.jsp</jsp-file>
    </servlet>

    <servlet>
        <servlet-name>Login2CodeServlet</servlet-name>
        <servlet-class>org.openvpms.web.security.login.Login2CodeServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>ForgotPasswordServlet</servlet-name>
        <jsp-file>/forgotpassword.jsp</jsp-file>
    </servlet>

    <servlet>
        <servlet-name>ConfirmForgotPasswordServlet</servlet-name>
        <servlet-class>org.openvpms.web.security.login.ConfirmForgotPasswordServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>ResetPasswordServlet</servlet-name>
        <jsp-file>/resetpassword.jsp</jsp-file>
    </servlet>

    <servlet>
        <servlet-name>ConfirmResetPasswordServlet</servlet-name>
        <servlet-class>org.openvpms.web.security.login.ConfirmResetPasswordServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>ChangePasswordServlet</servlet-name>
        <jsp-file>/changepassword.jsp</jsp-file>
    </servlet>

    <servlet>
        <servlet-name>ConfirmChangePasswordServlet</servlet-name>
        <servlet-class>org.openvpms.web.security.login.ConfirmChangePasswordServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>AppServlet</servlet-name>
        <servlet-class>org.openvpms.web.echo.servlet.SpringWebContainerServlet</servlet-class>
        <init-param>
            <param-name>app-name</param-name>
            <param-value>openVPMSApp</param-value>
        </init-param>
    </servlet>

    <servlet>
        <servlet-name>DownloadServlet</servlet-name>
        <servlet-class>org.openvpms.web.echo.servlet.DownloadServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>LogoutServlet</servlet-name>
        <servlet-class>org.openvpms.web.echo.servlet.LogoutServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>WebDAVServlet</servlet-name>
        <servlet-class>org.openvpms.web.webdav.servlet.WebDAVServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>JnlpDownloadServlet</servlet-name>
        <servlet-class>jnlp.sample.servlet.JnlpDownloadServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>WebDAVLaunchServlet</servlet-name>
        <servlet-class>org.openvpms.web.webdav.servlet.WebDAVEditorLauncherServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>BookingServletV1</servlet-name>
        <servlet-class>org.glassfish.jersey.servlet.ServletContainer</servlet-class>
        <init-param>
            <param-name>javax.ws.rs.Application</param-name>
            <param-value>org.openvpms.booking.impl.v1.BookingApplicationV1</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet>
        <servlet-name>BookingServlet</servlet-name>
        <servlet-class>org.glassfish.jersey.servlet.ServletContainer</servlet-class>
        <init-param>
            <param-name>javax.ws.rs.Application</param-name>
            <param-value>org.openvpms.booking.impl.BookingApplication</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet>
        <servlet-name>FileServerServlet</servlet-name>
        <servlet-class>org.openvpms.web.echo.servlet.file.FileServerServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>plugins</servlet-name>
        <servlet-class>com.atlassian.plugin.servlet.ServletModuleContainerServlet</servlet-class>
    </servlet>

    <servlet>
        <servlet-name>logo</servlet-name>
        <servlet-class>org.openvpms.web.component.service.logo.LogoServlet</servlet-class>
    </servlet>

    <servlet-mapping>
        <servlet-name>LoginServlet</servlet-name>
        <url-pattern>/login</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>SelectFactorServlet</servlet-name>
        <url-pattern>/selectfactor</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>Login2Servlet</servlet-name>
        <url-pattern>/login2</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>Login2CodeServlet</servlet-name>
        <url-pattern>/login2code</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>LoginFactorSelectedServlet</servlet-name>
        <url-pattern>/loginfactorselected</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>ResetPasswordServlet</servlet-name>
        <url-pattern>/resetpassword</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>ConfirmResetPasswordServlet</servlet-name>
        <url-pattern>/confirmresetpassword</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>ForgotPasswordServlet</servlet-name>
        <url-pattern>/forgotpassword</url-pattern>
        <url-pattern>/forgotpassword/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>ConfirmForgotPasswordServlet</servlet-name>
        <url-pattern>/confirmforgotpassword</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>ChangePasswordServlet</servlet-name>
        <url-pattern>/changepassword</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>ConfirmChangePasswordServlet</servlet-name>
        <url-pattern>/confirmchangepassword</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>AppServlet</servlet-name>
        <url-pattern>/app</url-pattern>
        <url-pattern>/app/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>DownloadServlet</servlet-name>
        <url-pattern>/document</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>FileServerServlet</servlet-name>
        <url-pattern>/download/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>LogoutServlet</servlet-name>
        <url-pattern>/logout</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>WebDAVServlet</servlet-name>
        <url-pattern>/webdav</url-pattern>
        <url-pattern>/webdav/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>WebDAVLaunchServlet</servlet-name>
        <url-pattern>/externaledit</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>JnlpDownloadServlet</servlet-name>
        <url-pattern>/webstart/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>BookingServletV1</servlet-name>
        <url-pattern>/ws/booking/v1/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>BookingServlet</servlet-name>
        <url-pattern>/ws/booking/v2/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>plugins</servlet-name>
        <url-pattern>/plugins/servlet/*</url-pattern>
    </servlet-mapping>

    <servlet>
        <servlet-name>OAuth2CodeGrantCompleteServlet</servlet-name>
        <servlet-class>org.openvpms.web.security.oauth.OAuth2CodeGrantCompleteServlet</servlet-class>
    </servlet>

    <servlet-mapping>
        <servlet-name>OAuth2CodeGrantCompleteServlet</servlet-name>
        <url-pattern>/oauth2/code/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>logo</servlet-name>
        <url-pattern>/logo</url-pattern>
    </servlet-mapping>

    <session-config>
        <session-timeout>120</session-timeout>
        <!-- 2 hours -->
    </session-config>

</web-app>
