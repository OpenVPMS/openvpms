<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="org.openvpms.archetype.rules.user.PasswordPolicy" %>
<%@ page import="org.openvpms.archetype.i18n.ArchetypeMessages" %>
<%@ page import="org.openvpms.web.resource.i18n.Messages" %>

<%--
 * Verifies if a password is valid according to the PasswordPolicy.
 * Note that passwordValidationLayout.html needs to be imported using <%include> in the form in order to display
 * validation checks.
 *
 * @param password the password
 * @returns {boolean}
--%>
function checkPassword(password) {
<%
        WebApplicationContext applicationContext
            = WebApplicationContextUtils.getRequiredWebApplicationContext(application);
    PasswordPolicy policy = applicationContext.getBean(PasswordPolicy.class);
%>

    var minLength = <%= policy.getMinLength() %>;
    var maxLength = <%= policy.getMaxLength() %>;
    var charValid = false;
    var lowercaseValid = false;
    var uppercaseValid = false;
    var numberValid = false;
    var specialValid = false;
    var lengthValid = false;

    if (password || password === "") {
        var invalidCharMatch = /[^<%=policy.getValidCharactersEscaped()%>]/.exec(password);
        charValid = invalidCharMatch == null;
        if (!charValid) {
            var invalidCharMessage = "<%= ArchetypeMessages.passwordCharacterInvalidSuffix() %>";
            showPasswordCheck(".checkValidChar", false, "'" + invalidCharMatch[0] + "' " + invalidCharMessage);
        } else {
            hidePasswordCheck(".checkValidChar");
        }

        if (<%= policy.lowercaseRequired() %>) {
            lowercaseValid = /[a-z]/.test(password);
            showPasswordCheck(".checkLowercase", lowercaseValid,
                              "<%= ArchetypeMessages.passwordMustContainLowercaseCharacter() %>");
        } else {
            lowercaseValid = true;
        }
        if (<%= policy.uppercaseRequired() %>) {
            uppercaseValid = /[A-Z]/.test(password);
            showPasswordCheck(".checkUppercase", uppercaseValid,
                              "<%= ArchetypeMessages.passwordMustContainUppercaseCharacter() %>");
        } else {
            uppercaseValid = true;
        }
        if (<%= policy.numberRequired() %>) {
            numberValid = /[0-9]/.test(password);
            showPasswordCheck(".checkNumber", numberValid, "<%= ArchetypeMessages.passwordMustContainNumber() %>");
        } else {
            numberValid = true;
        }
        if (<%= policy.specialRequired() %>) {
            specialValid = /[<%=policy.getSpecialCharactersEscaped()%>]/.test(password);
            showPasswordCheck(".checkSpecial", specialValid,
                              "<%= ArchetypeMessages.passwordMustContainSpecialCharacter() %>");
        } else {
            specialValid = true;
        }

        if (password.length < minLength) {
            showPasswordCheck(".checkLength", false,
                              "<%= ArchetypeMessages.passwordLessThanMinimumLength(policy.getMinLength()) %>");
        } else if (password.length > maxLength) {
            showPasswordCheck(".checkLength", false,
                              "<%= ArchetypeMessages.passwordGreaterThanMaximumLength(policy.getMaxLength()) %>");
        } else {
            showPasswordCheck(".checkLength", true,
                              "<%= ArchetypeMessages.passwordLessThanMinimumLength(policy.getMinLength()) %>");
            lengthValid = true;
        }
    }

    return charValid && lowercaseValid && uppercaseValid && numberValid && specialValid && lengthValid;
}

<%--
 * Determines if the passwords match.
 *
 * @param password the password
 * @param confirmPassword the confirmation password
 * @returns true if they match, otherwise false
--%>
function checkPasswordsMatch(password, confirmPassword) {
    var valid = (password === confirmPassword);
    showPasswordCheck(".checkMatch", valid, "<%= Messages.get("login.reset.passwordmismatch")%>")
    return valid;
}

<%--
 * Displays a password check.
 *
 * @param checkClass the class of the check div to show
 * @param valid determines if the check is valid or not
 * @param text the check text
--%>
function showPasswordCheck(checkClass, valid, text) {
    var flag = valid ? "&#10003;" : "&#10006;"; // i.e. check mark or heavy cross
    $(checkClass + "Flag").html(flag);
    $(checkClass + "Text").html(text);
    var addClass = valid ? "passwordValid" : "passwordInvalid";
    var removeClass = valid ? "passwordInvalid" : "passwordValid";
    $(checkClass).addClass(addClass).removeClass(removeClass);
}

<%--
 * Hide s a password check.
 *
 * @param checkClass the class of the check div to hide
--%>
function hidePasswordCheck(checkClass) {
    $(checkClass + "Flag").html("");
    $(checkClass + "Text").html("");
}