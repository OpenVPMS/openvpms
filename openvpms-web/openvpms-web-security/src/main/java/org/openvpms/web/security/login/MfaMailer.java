/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.openvpms.web.resource.i18n.Messages;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;

/**
 * Emails multifactor authentication codes.
 *
 * @author Tim Anderson
 * @see MfaService
 */
public class MfaMailer extends SecurityMailer {

    /**
     * Constructs a {@link MfaMailer}.
     *
     * @param mailSender the mail sender
     */
    public MfaMailer(JavaMailSender mailSender) {
        super(mailSender);
    }

    /**
     * Sends a password reset code email.
     *
     * @param from the from-address
     * @param to   the to-address
     * @param code the reset code
     * @throws MessagingException for any messaging error
     */
    public void sendCode(String from, String to, String code) throws MessagingException {
        String subject = Messages.get("login.mfa.code.subject");
        String text = Messages.format("login.mfa.code.message", code);
        send(from, to, subject, text);
    }

}
