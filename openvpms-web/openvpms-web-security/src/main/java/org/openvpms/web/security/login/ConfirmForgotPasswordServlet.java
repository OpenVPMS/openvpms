/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * A servlet that generates an email to a user containing a {@link SecurityCode#getCode() code} in order to reset
 * their password.
 * <br/>
 * On completion, it forwards to the {@code resetpassword} servlet, supplying the {@link SecurityCode#getId() reset id}.
 * <br/>
 * Both the code and id must be submitted in order for the password to be reset.
 *
 * @author Tim Anderson
 * @see PasswordService
 */
public class ConfirmForgotPasswordServlet extends HttpServlet {

    /**
     * The password reset service.
     */
    private PasswordService passwordResetService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ConfirmForgotPasswordServlet.class);

    /**
     * Initialises the servlet.
     *
     * @throws ServletException for any error
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        passwordResetService = context.getBean(PasswordService.class);
    }

    /**
     * Handles a POST request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException      if an input or output error is detected when the servlet handles the GET request
     * @throws ServletException if the {@code resetpassword} servlet cannot be forwarded to
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String id = sendReset(request);
        if (id == null) {
            // no message was sent. Sleep from 1 to 3 seconds to limit bots guessing valid account names, as sending
            // an email for a valid account can take a few seconds
            try {
                Thread.sleep(RandomUtils.nextInt(1000, 3001));
            } catch (InterruptedException ignore) {
                // do nothing
            }
            id = UUID.randomUUID().toString(); // generate a dummy id
        }

        // forward to the resetpassword page even if an email can't be sent, to limit bots guessing the existence of
        // accounts
        request.setAttribute("id", id);
        request.getRequestDispatcher("resetpassword").forward(request, response);
    }

    /**
     * Sends a reset mail to the user identified by the request.
     *
     * @param request the request
     * @return a UUID corresponding to the reset which must be supplied when resetting the password, or {@code null}
     * if no reset was sent
     */
    private String sendReset(HttpServletRequest request) {
        String result = null;
        String username = request.getParameter("username");
        if (username == null) {
            log.error("No username in request");
        } else {
            result = passwordResetService.sendResetCode(username);
        }
        return result;
    }
}
