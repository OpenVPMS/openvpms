/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

/**
 * Status codes for the login page.
 *
 * @author Tim Anderson
 */
public class LoginStatus {

    /**
     * Status indicating that a username or password is incorrect.
     */
    public static final String ERROR = "error";

    /**
     * Status indicating a reset code has expired.
     */
    public static final String EXPIRED = "expired";

    /**
     * Status indicating that the host the user is connecting from is not authorised.
     */
    public static final String NO_ACCESS_FROM_HOST = "noaccess";
}