/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.openvpms.component.model.user.User;
import org.openvpms.web.security.login.SecurityCodeService.Factor;

import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

/**
 * Code for password resets and multifactor authentication.
 * <p/>
 * A code consists of a UUID, which is not seen by the user, and a 6-digit code, which the
 * user will be sent. Both must be supplied in order for the code to be valid.
 *
 * @author Tim Anderson
 */
class SecurityCode {

    /**
     * The id.
     */
    private final String id;

    /**
     * The code.
     */
    private final String code;

    /**
     * The id of the user the code applies to.
     */
    private final long userId;

    /**
     * The factor used.
     */
    private final Factor factor;

    /**
     * The no. of attempts to match this code.
     */
    private int attempts;

    /**
     * User code generator.
     */
    private static final Random random = new SecureRandom();

    /**
     * Constructs a {@link SecurityCode}.
     *
     * @param user   the user the code is for
     * @param factor the factor
     */
    public SecurityCode(User user, Factor factor) {
        this(user.getId(), factor);
    }

    /**
     * Constructs a {@link SecurityCode}.
     *
     * @param userId the id of the user the reset code is for
     * @param factor the factor
     */
    public SecurityCode(long userId, Factor factor) {
        id = UUID.randomUUID().toString();
        code = String.format("%06d", random.nextInt(1_000_000)); // i.e. a 6 digit code between 0 and 999999
        this.userId = userId;
        this.factor = factor;
    }

    /**
     * Returns the identifier for this reset code.
     *
     * @return the identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the user code.
     * <p/>
     * This is shorter than the id, to aid manual entry.
     * <p/>
     * When the factor is {@link Factor#TOTP}, this is not used.
     *
     * @return the user code
     */
    public String getCode() {
        return code;
    }

    /**
     * Returns the identifier for the user that this reset code applies to.
     *
     * @return the user identifier
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Returns the factor.
     *
     * @return the factor
     */
    public Factor getFactor() {
        return factor;
    }

    /**
     * Increments and returns the no. of attempts to use this reset code.
     *
     * @return the number of attempts
     */
    public synchronized int incAttempts() {
        return ++attempts;
    }

}