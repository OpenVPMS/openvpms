/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Helper for sending security emails.
 *
 * @author Tim Anderson
 */
public class SecurityMailer {

    /**
     * The mail sender.
     */
    private final JavaMailSender mailSender;

    /**
     * Constructs a {@link SecurityMailer}.
     *
     * @param mailSender the mail sender
     */
    public SecurityMailer(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Sends an email.
     *
     * @param from    the from-address
     * @param to      the to-address
     * @param subject the subject
     * @param text    the message text
     * @throws MessagingException for any messaging error
     */
    protected void send(String from, String to, String subject, String text) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setValidateAddresses(true);
        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(text);
        mailSender.send(message);
    }
}