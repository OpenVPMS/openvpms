/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.springframework.security.core.AuthenticationException;

/**
 * Exception thrown when a user is not authorised to connect to OpenVPMS from a host.
 *
 * @author Tim Anderson
 */
public class NoAccessFromHostException extends AuthenticationException {

    /**
     * Constructs a {@link  NoAccessFromHostException}.
     *
     * @param msg the detail message
     */
    public NoAccessFromHostException(String msg) {
        super(msg);
    }
}