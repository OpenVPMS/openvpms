/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.archetype.rules.settings.SettingsCache;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Manages the firewall configuration.
 * <p/>
 * This uses settings obtained from the {@link SettingsCache}, but periodically reloads them in order to pick up
 * external changes.
 *
 * @author Tim Anderson
 */
class FirewallConfigManager {

    /**
     * The settings cache.
     */
    private final SettingsCache settings;

    /**
     * The time to use cached data in minutes, before reloading it from the database, in milliseconds.
     */
    private final long cachePeriod;

    /**
     * The current firewall settings.
     */
    private volatile FirewallConfig config;

    /**
     * The time when the config was last updated.
     */
    private long updated = -1;

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(FirewallConfigManager.class);

    /**
     * Constructs a {@link FirewallConfigManager}.
     *
     * @param settings    the settings cache
     * @param cachePeriod time to use cached data in minutes, before reloading it from the database, in minutes
     */
    public FirewallConfigManager(SettingsCache settings, int cachePeriod) {
        this.settings = settings;
        this.cachePeriod = TimeUnit.MINUTES.toMillis(cachePeriod); // use millis for calcs
    }

    /**
     * Returns the current firewall configuration.
     *
     * @return the current firewall configuration
     */
    public FirewallConfig getConfig() {
        IMObjectBean bean = settings.getGroup(SettingsArchetypes.FIREWALL_SETTINGS, useCachedSettings());
        if (needsUpdate(bean)) {
            synchronized (this) {
                if (needsUpdate(bean)) {
                    loadConfig(bean);
                }
            }
        }
        return config;
    }

    /**
     * Determines if cached settings can be used.
     * <p/>
     * Settings expire after {@link #cachePeriod} milliseconds, in order to pick up external changes from the db.
     *
     * @return {@code true} if cached settings can be used, {@code false} if they should be reloaded
     */
    protected boolean useCachedSettings() {
        long time = updated;
        return time == -1 || (System.currentTimeMillis() - time) < cachePeriod;
    }

    /**
     * Determines if settings need to be updated.
     *
     * @param bean the settings bean
     * @return {@code true} if settings need to be updated, otherwise {@code false}
     */
    protected boolean needsUpdate(IMObjectBean bean) {
        FirewallConfig stored = config;
        return stored == null || stored.needsUpdate(bean.getObject(Entity.class));
    }

    /**
     * Loads the configuration.
     *
     * @param bean the firewall settings
     */
    private void loadConfig(IMObjectBean bean) {
        boolean first = config == null;
        FirewallSettings settings = new FirewallSettings(bean);
        List<String> active = getActiveAddresses(settings);
        List<IpAddressMatcher> matchers = getIpAddressMatchers(active);
        config = new FirewallConfig(settings, matchers);
        updated = System.currentTimeMillis();
        if (!first) {
            log.info("Firewall configuration updated");
        }
        if (log.isInfoEnabled()) {
            log.info("Access={}, allowed={}", settings.getAccessType(), StringUtils.join(active, ","));
        }
    }

    /**
     * Returns active allowed addresses.
     *
     * @param settings the firewall settings
     * @return the active addresses
     */
    private List<String> getActiveAddresses(FirewallSettings settings) {
        return settings.getAllowedAddresses().stream()
                .filter(FirewallEntry::isActive)
                .map(FirewallEntry::getAddress)
                .collect(Collectors.toList());
    }

    /**
     * Returns IP address matchers for a list of addresses.
     *
     * @param addresses the addresses
     * @return the corresponding matchers
     */
    private List<IpAddressMatcher> getIpAddressMatchers(List<String> addresses) {
        return addresses.stream()
                .map(IpAddressMatcher::new)
                .collect(Collectors.toList());
    }
}