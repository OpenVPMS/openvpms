/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.openvpms.component.model.user.User;
import org.openvpms.web.security.login.SecurityCodeService.Factor;
import org.openvpms.web.security.login.SecurityCodeService.Status;
import org.openvpms.web.security.user.LoginUserDetails;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet to handle the second step of multifactor login.
 *
 * @author Tim Anderson
 */
public class Login2CodeServlet extends HttpServlet {

    /**
     * The muiti-factor authentication service.
     */
    private MfaService mfaService;

    /**
     * Initialises the servlet.
     *
     * @throws ServletException for any error
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        mfaService = context.getBean(MfaService.class);
    }

    /**
     * Handles a POST request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof LoginUserDetails) {
            LoginUserDetails userDetails = (LoginUserDetails) authentication.getPrincipal();
            User user = userDetails.getUser();
            Factor factor = Factor.fromString(request.getParameter("factor"));
            String code = request.getParameter("code");
            String id = request.getParameter("id");
            Status status = mfaService.verifyCode(id, code, factor, user);
            if (status == Status.SUCCESS) {
                loggedIn(userDetails, response, authentication);
            } else if (status == Status.CODE_MISMATCH) {
                request.setAttribute("codemismatch", true);
                request.setAttribute("id", id);
                request.setAttribute("factor", factor.toString());
                request.getRequestDispatcher("login2").forward(request, response);
            } else {
                loginError(request, response);
            }
        } else {
            loginError(request, response);
        }
    }

    /**
     * Invoked when the user passes the second authentication factor.
     * <p/>
     * This updates the security context, and redirects to the app.
     *
     * @param userDetails    the user details
     * @param response       the servlet response
     * @param authentication the authentication
     * @throws IOException for any I/O error
     */
    private void loggedIn(LoginUserDetails userDetails, HttpServletResponse response, Authentication authentication)
            throws IOException {
        UserDetails details = userDetails.getUserDetails();
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(details, authentication.getCredentials(),
                                                        userDetails.getAuthorities()));
        // userDetails.getAuthorities() should include the ROLE_USER role
        response.sendRedirect("app");
    }

    /**
     * Invoked when the user fails the second authentication factor.
     * <p/>
     * Redirects back to the login page.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException for any I/O error
     */
    private void loginError(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        response.sendRedirect("login?status=error");
    }
}