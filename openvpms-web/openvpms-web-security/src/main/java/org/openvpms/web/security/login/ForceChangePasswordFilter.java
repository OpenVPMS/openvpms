/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * A filter that redirects to the changepassword page if the logged in user is trying to access the app and
 * must change their password.
 * <p/>
 * If the changepassword servlet is accessed without the user requiring a password change, invalidates the session
 * and redirects to the login page.
 *
 * @author Tim Anderson
 */
public class ForceChangePasswordFilter extends OncePerRequestFilter {

    /**
     * The authentication context.
     */
    private final AuthenticationContext context;

    /**
     * Constructs a {@link ForceChangePasswordFilter}.
     *
     * @param context the authentication context
     */
    public ForceChangePasswordFilter(AuthenticationContext context) {
        this.context = context;
    }

    /**
     * Filters the request.
     *
     * @param request     the request

     * @param response    the response
     * @param filterChain the filter change
     * @throws IOException      for any I/O error
     * @throws ServletException for any servlet error
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        String path = request.getServletPath();
        boolean redirected = false;
        if ("/app".equals(path)) {
            if (userMustChangePassword()) {
                response.sendRedirect("changepassword");
                redirected = true;
            }
        } else if ("/changepassword".equals(path)) {
            if (!userMustChangePassword()) {
                // prevent accessing the servlet directly
                HttpSession session = request.getSession(false);
                if (session != null) {
                    session.invalidate();
                }
                response.sendRedirect("login");
                redirected = true;
            }
        }
        if (!redirected) {
            filterChain.doFilter(request, response);
        }
    }

    /**
     * Determines if there is an authenticated user that needs a password change.
     *
     * @return {@code true} if the user must change their password, otherwise {@code false}
     */
    private boolean userMustChangePassword() {
        User user = (User) context.getUser();
        return user != null && user.getChangePassword();
    }
}