/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.springframework.security.web.session.InvalidSessionStrategy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * An {@link InvalidSessionStrategy} that redirects to the login page.
 * <p/>
 * This is to handle the case where a session expires or is invalidated and the user needs to log in again.
 * <p/>
 * If the session was invalidated (e.g. via the {@link ConfirmChangePasswordServlet}, this also ensures that the
 * parameters to the login page are preserved.
 *
 * @author Tim Anderson
 */
public class RedirectToLoginInvalidSessionStrategy implements InvalidSessionStrategy {

    /**
     * Invoked when an invalid session is directed.
     * <p/>
     * This redirects to the login page.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException      for any I/O error
     * @throws ServletException for any servlet error
     */
    @Override
    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String path = request.getServletPath();
        if ("/login".equals(path)) {
            // the login page was requested, so just continue with the request
            request.getRequestDispatcher(path).forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/login");
        }
    }
}
