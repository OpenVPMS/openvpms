/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.jboss.aerogear.security.otp.Totp;
import org.jboss.aerogear.security.otp.api.Base32;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;

/**
 * Multifactor authentication service.
 *
 * @author Tim Anderson
 */
public class MfaService extends SecurityCodeService {

    /**
     * The mailer.
     */
    private final MfaMailer mailer;

    /**
     * The password encryptor.
     */
    private final PasswordEncryptor encryptor;

    private static final String APP_NAME = "OpenVPMS";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(MfaService.class);

    /**
     * The TOTP secret node name.
     */
    private static final String TOTP_SECRET = "totpSecret";

    /**
     * Constructs a {@link MfaService}.
     *
     * @param service         the archetype service
     * @param practiceService the practice service
     * @param mailer          the code mailer
     * @param encryptor       the password encryptor
     */
    public MfaService(ArchetypeService service, PracticeService practiceService, MfaMailer mailer,
                      PasswordEncryptor encryptor) {
        super(service, practiceService);
        this.mailer = mailer;
        this.encryptor = encryptor;
    }

    /**
     * Sends a code to a user via email.
     *
     * @param user the user
     * @param to   the to-address
     * @return a UUID corresponding to the code which must be supplied, or {@code null} if no code was sent
     */
    @Override
    public String sendCode(User user, String to) {
        return super.sendCode(user, to);
    }

    /**
     * Generates a TOTP {@link SecurityCode} for a user.
     * <p/>
     * This enables the user to log in using TOTP if they supply the returned {@link SecurityCode} identifier
     * along with the TOTP.
     * <p/>
     * The {@link SecurityCode} is active for 10 minutes.
     *
     * @param user the user
     * @return the {@link SecurityCode} identifier
     */
    public String generateTOTPCode(User user) {
        SecurityCode securityCode = generateCode(user, Factor.TOTP);
        return securityCode.getId();
    }

    /**
     * Verifies a code.
     *
     * @param id     the {@link SecurityCode} identifier. Must refer to an unexpired {@link SecurityCode}
     * @param code   the code to verify
     * @param factor the factor used. May be {@code null}
     * @param user   the user
     * @return the verification status
     */
    public Status verifyCode(String id, String code, Factor factor, User user) {
        Status result;
        SecurityCode securityCode = getCode(id);
        if (securityCode != null && factor != null) {
            if (securityCode.getFactor() == Factor.TOTP) {
                result = verifyTOTP(code, user, securityCode);
            } else {
                result = verifyCode(id, code, factor, securityCode);
            }
        } else {
            result = Status.EXPIRED;
        }
        return result;
    }

    /**
     * Determines if a user has TOTP configured.
     *
     * @param user the user
     * @return {@code true} if the user has TOTP configured, otherwise {@code false}
     */
    public boolean hasTOTP(User user) {
        IMObjectBean bean = getArchetypeService().getBean(user);
        return bean.getString(TOTP_SECRET) != null;
    }

    /**
     * Configures TOTP for a user.
     * <p/>
     * This generates a TOTP secret and stores it with the user.
     *
     * @param user the user
     */
    public void configureTOTP(User user) {
        IMObjectBean bean = getArchetypeService().getBean(user);
        String encrypted = encryptor.encrypt(Base32.random());
        bean.setValue(TOTP_SECRET, encrypted);
        save(user);
    }

    /**
     * Removes the TOTP secret for a user.
     * <p/>
     * This prevents the user from signing in using a QR code.
     *
     * @param user the user
     */
    public void removeTOTP(User user) {
        if (hasTOTP(user)) {
            IMObjectBean bean = getArchetypeService().getBean(user);
            bean.setValue(TOTP_SECRET, null);
            save(user);
        }
    }

    /**
     * Returns a URL to display a TOTP QRCode.
     *
     * @param user the user
     * @return the URL, or {@code null} if the user doesn't have a TOTP secret configured
     */
    public String getQRCodeURL(User user) {
        String url = null;
        String secret = getTOTPSecret(user);
        if (secret != null) {
            String account = user.getName();
            if (account == null) {
                account = user.getUsername();
            }
            Party practice = getPracticeService().getPractice();
            String issuer = (practice != null) ? practice.getName() : APP_NAME;
            issuer = issuer.replaceAll("[:&]", " ");
            try {
                URLEncoder.encode(url = String.format("otpauth://totp/%s:%s?secret=%s&issuer=%s", issuer, account,
                                                      secret, issuer), "utf8");
            } catch (Exception exception) {
                log.error("Failed to encode QRCode url for user {}: {}", user.getUsername(), exception.getMessage(),
                          exception);
            }
        }
        return url;
    }

    /**
     * Sends a code to a user.
     *
     * @param user the user
     * @param code the code to send
     * @param from the from-address
     * @param to   the to-address
     * @return {@code true} if the code was sent successfully, otherwise {@code false}
     */
    @Override
    protected boolean sendCode(User user, String code, String from, String to) {
        boolean result = false;
        try {
            mailer.sendCode(from, to, code);
            result = true;
        } catch (Exception exception) {
            log.error("Failed to send code to user={}, email={}: {}", user.getUsername(), to, exception.getMessage(),
                      exception);
        }
        return result;
    }

    /**
     * Verifies a TOTP.
     *
     * @param code         the code to verify
     * @param user         the user
     * @param securityCode the corresponding security code
     * @return the verification status
     */
    private Status verifyTOTP(String code, User user, SecurityCode securityCode) {
        Status result;
        String secret = getTOTPSecret(user);
        if (secret != null) {
            Totp totp = new Totp(secret);
            if (!totp.verify(code)) {
                result = codeMismatch(securityCode);
            } else {
                result = Status.SUCCESS;
            }
        } else {
            result = Status.ERROR;
        }
        return result;
    }

    /**
     * Returns the TOTP secret for a user.
     *
     * @param user the user
     * @return the TOTP secret, or {@code null} if none is configured
     */
    private String getTOTPSecret(User user) {
        String result = null;
        try {
            String encrypted = getArchetypeService().getBean(user).getString(TOTP_SECRET);
            result = encrypted != null ? encryptor.decrypt(encrypted) : null;
        } catch (Exception exception) {
            log.error("Failed to decrypt TOTP secret for user {}: {}", user.getUsername(), exception.getMessage(),
                      exception);
        }
        return result;
    }
}
