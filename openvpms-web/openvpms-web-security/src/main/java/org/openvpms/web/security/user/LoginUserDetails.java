/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.user;

import org.openvpms.component.business.domain.im.security.User;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Login user details.
 *
 * @author Tim Anderson
 */
public interface LoginUserDetails extends UserDetails {

    /**
     * Returns the underlying user details.
     *
     * @return the underlying user details
     */
    UserDetails getUserDetails();

    /**
     * Returns the underlying user.
     *
     * @return the user
     */
    User getUser();

    /**
     * Determines if multifactor authentication is required.
     *
     * @return {@code true} if multifactor authentication is required, {@code false} if only username & password
     * authentication is required
     */
    boolean isMultiFactorAuthenticationRequired();

    /**
     * Returns the user's preferred email address.
     *
     * @return the email address. May be {@code null}
     */
    String getEmail();

    /**
     * Determines if the user has configured TOTP.
     *
     * @return {@code true} if TOTP is configured, otherwise {@code false}
     */
    boolean isTOTPConfigured();
}
