/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.apache.commons.lang3.math.NumberUtils;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Changes the password for the logged-in user.
 *
 * @author Tim Anderson
 */
public class ConfirmChangePasswordServlet extends HttpServlet {

    /**
     * The authentication context.
     */
    private AuthenticationContext authenticationContext;

    /**
     * The password reset service.
     */
    private PasswordService passwordResetService;

    /**
     * Session attribute name for the no. of times the password has attempted to be changed.
     */
    private static final String CHANGE_PASSWORD_ATTEMPTS = "_change_password_attempts";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ConfirmChangePasswordServlet.class);

    /**
     * The maximum no. of times a user can incorrectly enter the old password, before being logged out.
     */
    private static final int MAX_ATTEMPTS = 5;

    /**
     * Initialises the servlet.
     *
     * @throws ServletException for any error
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        passwordResetService = context.getBean(PasswordService.class);
        authenticationContext = context.getBean(AuthenticationContext.class);
    }

    /**
     * Handles a POST request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        String oldPassword = request.getParameter("oldPassword");
        String newPassword = request.getParameter("newPassword");
        User user = (User) authenticationContext.getUser();
        int attempts = getAttempts(session);
        if (session != null && user != null && oldPassword != null && newPassword != null && attempts < MAX_ATTEMPTS) {
            // should only be able to change the password for the logged-in user, which requires an active session
            PasswordService.Status status = changePassword(user, oldPassword, newPassword);
            if (status == PasswordService.Status.SUCCESS) {
                // log the user out, so they can log in with the new password
                login(response, session, null);
            } else if (status == PasswordService.Status.DATA_MISMATCH) {
                request.setAttribute("passwordmismatch", true);
                // increment the no. of attempts that the user has made entering the old password
                session.setAttribute(CHANGE_PASSWORD_ATTEMPTS, attempts + 1);

                // redirect to the changepassword page to enable the user to try again.
                request.getRequestDispatcher("changepassword").forward(request, response);
            } else {
                login(response, session, LoginStatus.ERROR);
            }
        } else {
            // If there have been too many attempts entering the old password, this will terminate the session.
            // If the session has been co-opted, this limits brute force attempts on the password.
            login(response, session, LoginStatus.ERROR);
        }
    }

    /**
     * Redirects the login page.
     * <p/>
     * This invalidates the current session, if any.
     *
     * @param response the response
     * @param session  the session. May be {@code null}
     * @param status   the status
     * @throws IOException for any I/O error
     */
    private void login(HttpServletResponse response, HttpSession session, String status) throws IOException {
        if (session != null) {
            session.invalidate();
        }
        String path = "login";
        if (status != null) {
            path += "?status=" + status;
        }
        response.sendRedirect(path);
    }

    /**
     * Changes the password of the specified user.
     *
     * @param user        the user
     * @param oldPassword the old password. The hashed version of this must match the user's existing password
     * @param newPassword the new password
     * @return the change status
     */
    private PasswordService.Status changePassword(User user, String oldPassword, String newPassword) {
        PasswordService.Status status;
        try {
            status = passwordResetService.changePassword(user, oldPassword, newPassword);
        } catch (Exception exception) {
            log.error("Failed to change password for user {}: {}", user.getUsername(), exception.getMessage(),
                      exception);
            status = PasswordService.Status.ERROR;
        }
        return status;
    }

    /**
     * Returns the current no. of attempts at changing the password.
     *
     * @param session the session. May be {@code null}
     * @return the no. of attempts
     */
    private int getAttempts(HttpSession session) {
        int attempts = 0;
        if (session != null) {
            Object attribute = session.getAttribute(CHANGE_PASSWORD_ATTEMPTS);
            if (attribute != null) {
                attempts = NumberUtils.toInt(attribute.toString(), 0);
            }
        }
        return attempts;
    }
}