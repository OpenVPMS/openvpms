/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.web.security.user.LoginUserDetailsImpl;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;

/**
 * An {@link AuthenticationProvider} that authenticates users logging into the UI, according to the remote address
 * they are connecting from.
 * <p/>
 * This supports expired passwords and multifactor authentication.
 *
 * @author Tim Anderson
 */
public class FirewalledGuiUserAuthenticationProvider extends FirewalledUserAuthenticationProvider {

    /**
     * Constructs a {@link FirewalledGuiUserAuthenticationProvider}.
     *
     * @param userService     the user service
     * @param passwordEncoder the password encoder
     * @param firewallService the firewall service
     */
    public FirewalledGuiUserAuthenticationProvider(LoginUserDetailsService userService, PasswordEncoder passwordEncoder,
                                                   FirewallService firewallService) {
        super(userService, passwordEncoder, false, true, firewallService);
    }

    /**
     * Returns the user associated with a {@link UserDetails}.
     *
     * @param userDetails the user details
     * @return the associated user, or {@code null} if none exists
     */
    @Override
    protected User getUser(UserDetails userDetails) {
        User result;
        if ((userDetails instanceof LoginUserDetailsImpl)) {
            result = ((LoginUserDetailsImpl) userDetails).getUser();
        } else {
            result = super.getUser(userDetails);
        }
        return result;
    }

    /**
     * Verifies that a user is allowed with multifactor authentication.
     * <p/>
     * This is only supported when the user details are {@link LoginUserDetailsImpl}, provided by the
     * {@link LoginUserDetailsService}.
     *
     * @param request     the current HTTP servlet request
     * @param userDetails the user details associated with the request
     */
    @Override
    protected void checkAllowedWithMFA(HttpServletRequest request, UserDetails userDetails) {
        if (userDetails instanceof LoginUserDetailsImpl) {
            LoginUserDetailsImpl details = (LoginUserDetailsImpl) userDetails;
            if (details.getEmail() == null && !details.isTOTPConfigured()) {
                // cannot perform MFA as the user doesn't have a registered email address and hasn't configured TOTP
                throw new NoAccessFromHostException(request.getRemoteAddr());
            }
            details.setMultiFactorAuthenticationRequired();
        } else {
            super.checkAllowedWithMFA(request, userDetails);
        }
    }
}