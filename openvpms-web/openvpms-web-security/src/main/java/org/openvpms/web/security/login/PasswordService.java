/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.user.PasswordValidator;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.business.dao.im.security.IUserDAO;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

/**
 * Password service.
 * <p/>
 * This supports:
 * <ul>
 *     <li>sending a reset code via email to a user</li>
 *     <li>resetting the password for a user, given a reset code and id</li>
 *     <li>changing the password for the logged in user, given the existing password</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class PasswordService extends SecurityCodeService {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The users service.
     */
    private final IUserDAO users;

    /**
     * The password encoder.
     */
    private final PasswordEncoder encoder;

    /**
     * The password validator.
     */
    private final PasswordValidator validator;

    /**
     * The mailer.
     */
    private final PasswordMailer mailer;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PasswordService.class);

    /**
     * /**
     * Constructs a {@link PasswordService}
     *
     * @param service         the archetype service
     * @param users           the user service
     * @param encoder         the password encoder
     * @param validator       the password validator
     * @param practiceService the practice service
     * @param mailer          the password reset mailer
     */
    public PasswordService(ArchetypeService service, IUserDAO users, PasswordEncoder encoder,
                           PasswordValidator validator, PracticeService practiceService,
                           PasswordMailer mailer) {
        super(service, practiceService);
        this.service = service;
        this.users = users;
        this.encoder = encoder;
        this.validator = validator;
        this.mailer = mailer;
    }

    /**
     * Sends a password reset code to a user.
     *
     * @param username the username
     * @return a UUID corresponding to the reset which must be supplied when resetting the password, or {@code null}
     * if no code was sent
     */
    public String sendResetCode(String username) {
        String result = null;
        User user = getUser(username);
        if (user == null) {
            log.error("Cannot send reset code. No active user with username {}", username);
        } else {
            result = sendCode(user);
        }
        return result;
    }

    /**
     * Resets the password for a user.
     *
     * @param id       the reset code identifier
     * @param code     the reset code
     * @param password the new password
     * @return the reset status
     */
    public Status resetPassword(String id, String code, String password) {
        return applyCode(id, code, userId -> {
            Status result;
            if (!validate(password)) {
                // invalid password should have been detected before submission
                result = Status.ERROR;
            } else {
                User user = (User) service.get(UserArchetypes.USER, userId, true);
                if (user != null) {
                    setPassword(user, password);
                    sendNotification(user);
                    result = Status.SUCCESS;
                } else {
                    log.warn("Cannot reset password for user id={}. No active user exists", userId);
                    result = Status.ERROR;
                }
            }
            return result;
        });
    }

    /**
     * Changes the password for a user.
     *
     * @param user        the user
     * @param oldPassword the old password. Must match the user's hashed password
     * @param newPassword the new password. Must meet the password policy
     * @return the change status
     */
    public Status changePassword(User user, String oldPassword, String newPassword) {
        Status result;
        String existing = ((org.openvpms.component.business.domain.im.security.User) user).getPassword();
        if (encoder.matches(oldPassword, existing)) {
            if (!oldPassword.equals(newPassword) && validate(newPassword)) {
                setPassword(user, newPassword);
                sendNotification(user);
                result = Status.SUCCESS;
            } else {
                result = Status.ERROR;
            }
        } else {
            result = Status.DATA_MISMATCH;
        }
        return result;
    }

    /**
     * Sends a code to a user.
     *
     * @param user the user
     * @param code the code to send
     * @param from the from-address
     * @param to   the to-address
     * @return {@code true} if the code was sent successfully, otherwise {@code false}
     */
    @Override
    protected boolean sendCode(User user, String code, String from, String to) {
        boolean result = false;
        try {
            mailer.sendResetCode(from, to, code);
            result = true;
        } catch (Exception exception) {
            log.error("Failed to send reset code to {}: {}", to, exception.getMessage());
        }
        return result;
    }

    /**
     * Sets the password for the user.
     * <p/>
     * NOTE: this uses the DAO directly, as the user may not have permissions to save security.user instances.
     *
     * @param user     the user
     * @param password the new password
     */
    private void setPassword(User user, String password) {
        IMObjectBean bean = service.getBean(user);
        bean.setValue("password", encoder.encode(password));
        bean.setValue("changePassword", false);
        save(user);
        log.warn("Password changed for user id={}, username={}", user.getId(), user.getUsername());
    }

    /**
     * Returns a user by username.
     *
     * @param username the username
     * @return the corresponding user, if it is active, or {@code null} if it is inactive or doesn't exist
     */
    private User getUser(String username) {
        User user = users.getUser(username);
        return user != null && user.isActive() ? user : null;
    }

    /**
     * Sends a password changed notification email to the user.
     *
     * @param user the user
     */
    private void sendNotification(User user) {
        String from = getFromAddress();
        String to = getToAddress(user);
        if (from != null && to != null) {
            try {
                mailer.sendPasswordChanged(from, to);
            } catch (Exception exception) {
                log.error("Failed to send notification to {}: {}", to, exception.getMessage());
            }
        }
    }

    /**
     * Validates a password.
     *
     * @param password the password
     * @return {@code true} if the password is valid, otherwise {@code false}
     */
    private boolean validate(String password) {
        List<String> errors = validator.validate(password);
        if (!errors.isEmpty()) {
            log.error("Password doesn't match policy: {}", StringUtils.join(errors, ","));
        }
        return errors.isEmpty();
    }
}
