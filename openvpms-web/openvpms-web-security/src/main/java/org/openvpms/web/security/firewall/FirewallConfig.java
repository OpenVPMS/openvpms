/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.security.FirewallSettings.AccessType;
import org.openvpms.component.model.entity.Entity;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import java.util.List;

/**
 * Firewall configuration.
 *
 * @author Tim Anderson
 */
class FirewallConfig {

    /**
     * The allow-list.
     */
    private final List<IpAddressMatcher> allowed;

    /**
     * The access type.
     */
    private final AccessType accessType;

    /**
     * Determines if multifactor authentication is enabled.
     */
    private final boolean mfaEnabled;

    /**
     * The version where config.
     */
    private final String version;

    /**
     * Constructs a {@link FirewallConfig}.
     *
     * @param settings the settings
     * @param allowed  the allowed addresses
     */
    public FirewallConfig(FirewallSettings settings, List<IpAddressMatcher> allowed) {
        this.allowed = allowed;
        accessType = settings.getAccessType();
        mfaEnabled = settings.isMultifactorAuthenticationEnabled();
        version = getVersion(settings.getSettings());
    }

    /**
     * Determines where users can connect from.
     *
     * @return where users can connect from
     */
    public AccessType getAccessType() {
        return accessType;
    }

    /**
     * Determines if multifactor authentication is enabled.
     *
     * @return {@code true} if it is enabled, otherwise {@code false}
     */
    public boolean isMultifactorAuthenticationEnabled() {
        return mfaEnabled;
    }

    /**
     * Returns the allowed addresses that users can connect from.
     *
     * @return the allowed address. If empty, users can connect from any address
     */
    public List<IpAddressMatcher> getAllowedAddresses() {
        return allowed;
    }

    /**
     * Determines if the configuration needs to be updated.
     *
     * @param config the underlying configuration
     * @return {@code true} if the configuration needs to be updated, otherwise {@code false}
     */
    public boolean needsUpdate(Entity config) {
        return !version.equals(getVersion(config));
    }

    /**
     * Generates a string of the version of the config used.
     *
     * @param config the config
     * @return a version string
     */
    private String getVersion(Entity config) {
        return config.getId() + ":" + config.getVersion();
    }
}