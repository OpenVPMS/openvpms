/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.archetype.rules.security.FirewallSettings.AccessType;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A filter to filter requests by IP address.
 * <p/>
 * If a request is disallowed by {@link FirewallService#isAllowed(HttpServletRequest)}, sends
 * {@link HttpServletResponse#SC_UNAUTHORIZED}.
 *
 * @author Tim Anderson
 * @see FirewallService
 */
public class IpFilter extends OncePerRequestFilter {

    /**
     * The firewall service
     */
    private final FirewallService firewallService;

    /**
     * If {@code true}, treat the firewall {@link AccessType#ALLOWED_USER ALLOWED_USER} access type as
     * {@link AccessType#ALLOWED_ONLY ALLOWED_ONLY}.
     */
    private final boolean limitAllowedUser;

    /**
     * Constructs an {@link IpFilter}.
     *
     * @param firewallService the firewall service
     */
    public IpFilter(FirewallService firewallService) {
        this(firewallService, false);
    }

    /**
     * Constructs an {@link IpFilter}.
     *
     * @param firewallService  the firewall service
     * @param limitAllowedUser if {@code true}, treat the firewall {@link AccessType#ALLOWED_USER ALLOWED_USER}
     *                         access type as {@link AccessType#ALLOWED_ONLY ALLOWED_ONLY} to restrict connections
     *                         from allowed addresses only
     */
    public IpFilter(FirewallService firewallService, boolean limitAllowedUser) {
        this.firewallService = firewallService;
        this.limitAllowedUser = limitAllowedUser;
    }

    /**
     * Filters a request.
     *
     * @param request     the request
     * @param response    the response
     * @param filterChain the filter chain
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (firewallService.isAllowed(request, limitAllowedUser)) {
            filterChain.doFilter(request, response);
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}