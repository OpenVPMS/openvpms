/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.security.FirewallSettings.AccessType;
import org.openvpms.archetype.rules.settings.SettingsCache;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * A service to determine if HTTP requests should be allowed or blocked.
 * <p/>
 * The firewall is configured via {@link FirewallSettings}, which determines the level of access:
 * <ul>
 *     <li><em>ALL</em> - users can connect from anywhere</li>
 *     <li><em>ALLOWED_ONLY</em> - users can only connect from addresses in the allowed list</li>
 *     <li><em>ALLOWED_USER</em> - users can only connect from addresses in the allowed list, unless they have
 *         'connectFromAnywhere' enabled</li>
 * </ul>
 * Of the above:
 * <ul>
 *     <li><em>ALL</em> is the least secure. It is effectively a no-op.</li>
 *     <li><em>ALLOWED_ONLY</em> is the most secure, as requests are filtered by remote address</li>
 *     <li><em>ALLOWED_USER</em> requires an authenticated user before it can determine if a request should be allowed.
 *     For unauthenticated APIs it <em>cannot perform any filtering.</em></li>
 * </ul>
 *
 * @author Tim Anderson
 * @see FirewalledUserAuthenticationProvider
 * @see FirewallInitialiser
 */
public class FirewallService {

    public enum AccessStatus {
        ALLOWED,
        ALLOWED_WITH_MFA,
        DENIED
    }

    /**
     * The time to use cached data in minutes, before reloading it from the database.
     */
    private final int cachePeriod;

    /**
     * The firewall configuration manager.
     */
    private FirewallConfigManager manager;

    /**
     * The archetype service.
     */
    private ArchetypeService service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(FirewallService.class);

    /**
     * Constructs a {@link FirewallService}.
     *
     * @param cachePeriod the time to use cached data in minutes, before reloading it from the database.
     *                    This is to enable external changes to be detected.
     */
    public FirewallService(int cachePeriod) {
        this.cachePeriod = cachePeriod;
    }

    /**
     * Initialises the service.
     * <p/>
     * This is required due to cyclic dependencies between the various services, preventing constructor wiring.
     *
     * @param settings the settings
     * @param service  the archetype service
     */
    public void initialise(SettingsCache settings, ArchetypeService service) {
        this.manager = new FirewallConfigManager(settings, cachePeriod);
        this.service = service;
    }

    /**
     * Determines if a request is allowed.
     * <p/>
     * If the access type is {@link AccessType#ALLOWED_USER ALLOWED_USER}, this returns {@code true}, as it can only be
     * determined after the user is authenticated using {@link #getAccessStatus(HttpServletRequest, User, boolean)}.
     *
     * @param request the request
     * @return {@code true} if the request is allowed, otherwise {@code false}
     */
    public boolean isAllowed(HttpServletRequest request) {
        return isAllowed(request, false);
    }

    /**
     * Determines if a request is allowed.
     * <p/>
     * If the access type is {@link AccessType#ALLOWED_USER ALLOWED_USER} and:
     * <ul>
     *     <li>{@code limitAllowedUser = false}, this returns {@code true}, and a subsequent check is required
     *      after the user is authenticated using {@link #getAccessStatus(HttpServletRequest, User, boolean)}.</li>
     *      <li>{@code limitAllowedUser = true}, the access type is treated as {@link AccessType#ALLOWED_ONLY}.<br/>
     *      <br/>
     *      This should be used for endpoints that should not be accessible to anyone outside the allowed addresses.
     *      </li>
     * </ul>
     *
     * @param request          the request
     * @param limitAllowedUser if {@code true}, treat the firewall {@link AccessType#ALLOWED_USER ALLOWED_USER}
     *                         access type as {@link AccessType#ALLOWED_ONLY ALLOWED_ONLY}
     * @return {@code true} if the request is allowed, otherwise {@code false}
     */
    public boolean isAllowed(HttpServletRequest request, boolean limitAllowedUser) {
        boolean result = false;
        try {
            FirewallConfig config = manager.getConfig();
            AccessType type = config.getAccessType();
            if (type == AccessType.UNRESTRICTED || (type == AccessType.ALLOWED_USER && !limitAllowedUser)) {
                result = true;
            } else {
                result = checkAllowed(request, config);
            }
        } catch (Exception exception) {
            log.error("Failed to get firewall configuration: {}", exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Determines if a request associated with a user is allowed.
     * <p/>
     * This functions the same as {@link #isAllowed(HttpServletRequest)}, except that if the request address is not in
     * the allow-list, and the firewall configuration enables users with 'connect from anywhere' enabled to connect,
     * the request is allowed if the user also has 'connect from anywhere' enabled.
     *
     * @param request      the request
     * @param user         the user
     * @param mfaSupported if {@code true}, the endpoint supports multifactor authentication
     * @return the access status for this request
     */
    public AccessStatus getAccessStatus(HttpServletRequest request, User user, boolean mfaSupported) {
        AccessStatus result = AccessStatus.DENIED;
        try {
            FirewallConfig config = manager.getConfig();
            AccessType accessType = config.getAccessType();
            if (accessType == AccessType.UNRESTRICTED) {
                result = AccessStatus.ALLOWED;
            } else if (!checkAllowed(request, config)) {
                if (accessType == AccessType.ALLOWED_USER) {
                    if (userCanConnectFromAnywhere(user)) {
                        if (mfaSupported && config.isMultifactorAuthenticationEnabled()) {
                            result = AccessStatus.ALLOWED_WITH_MFA;
                        } else {
                            result = AccessStatus.ALLOWED;
                        }
                    }
                }
            } else {
                result = AccessStatus.ALLOWED;
            }
        } catch (Exception exception) {
            log.error("Failed to get firewall configuration: {}", exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Determines if multifactor authentication is enabled.
     *
     * @return {@code true} if multifactor authentication is enabled, otherwise {@code false}
     */
    public boolean isMultifactorAuthenticationEnabled() {
        FirewallConfig config = manager.getConfig();
        return config.isMultifactorAuthenticationEnabled();
    }

    /**
     * Determines if multifactor authentication is enabled for a user.
     *
     * @param user the user
     * @return {@code true} if multifactor authentication is enabled, otherwise {@code false}
     */
    public boolean isMultifactorAuthenticationEnabled(User user) {
        return isMultifactorAuthenticationEnabled() && userCanConnectFromAnywhere(user);
    }

    /**
     * Determines if the user can connect from anywhere.
     *
     * @param user the user
     * @return {@code true} if the user can connect from anywhere, {@code false} if they can only connect
     * from allowed addresses
     */
    private boolean userCanConnectFromAnywhere(User user) {
        IMObjectBean bean = service.getBean(user);
        return bean.getBoolean("connectFromAnywhere");
    }

    /**
     * Determines if a request comes from an allowed address.
     *
     * @param request the request
     * @return {@code true if the address is allowed, otherwise {@code false}
     */
    private boolean checkAllowed(HttpServletRequest request, FirewallConfig config) {
        boolean result = false;
        List<IpAddressMatcher> allowed = config.getAllowedAddresses();
        for (IpAddressMatcher matcher : allowed) {
            if (matcher.matches(request)) {
                result = true;
                break;
            }
        }
        return result;
    }
}