/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.component.business.service.security.UserService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * An {@link AuthenticationProvider} that authenticates users performing API requests, according to the remote address
 * they are connecting from.
 * <p/>
 * This doesn't support multifactor authentication, and will throw an exception if the user is required to change
 * their password.
 *
 * @author Tim Anderson
 */
public class FirewalledApiUserAuthenticationProvider extends FirewalledUserAuthenticationProvider {

    /**
     * Constructs a {@link FirewalledApiUserAuthenticationProvider}.
     *
     * @param userService     the user service
     * @param passwordEncoder the password encoder
     * @param firewallService the firewall service
     */
    public FirewalledApiUserAuthenticationProvider(UserService userService, PasswordEncoder passwordEncoder,
                                                   FirewallService firewallService) {
        super(userService, passwordEncoder, true, false, firewallService);
    }
}
