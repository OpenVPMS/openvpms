/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Manages sending and verifying {@link SecurityCode}s.
 *
 * @author Tim Anderson
 */
public abstract class SecurityCodeService {

    /**
     * The factor used.
     */
    public enum Factor {
        EMAIL,   // the code is sent via email
        TOTP;    // the code is a time-based one time password

        /**
         * Converts a string to a factor.
         *
         * @param value the string to convert. May be {@code null}
         * @return the corresponding factor, or {@code null} if none is found
         */
        public static Factor fromString(String value) {
            return (value != null) ? Arrays.stream(values())
                    .filter(factor -> factor.name().equals(value))
                    .findFirst()
                    .orElse(null) : null;
        }
    }

    /**
     * Security code status.
     */
    public enum Status {
        EXPIRED,             // the code has expired
        CODE_MISMATCH,       // the code doesn't match that expected
        DATA_MISMATCH,       // the data doesn't match that expected
        ERROR,               // the data was invalid
        SUCCESS              // the operation was successful
    }

    /**
     * The maximum no. of attempts that can be made with a code, before it is removed to avoid brute-force attacks.
     */
    protected static final int MAX_ATTEMPTS = 3;

    /**
     * Cache of codes that expire after 10 minutes.
     */
    private final Map<String, SecurityCode> codes
            = Collections.synchronizedMap(new PassiveExpiringMap<>(10, TimeUnit.MINUTES));

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The contact rules.
     */
    private final Contacts contacts;

    /**
     * Used to ensure missing practice email addresses are only logged once.
     */
    private volatile boolean missingEmailLogged;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SecurityCodeService.class);

    /**
     * Constructs a {@link SecurityCodeService}.
     *
     * @param service         the archetype service
     * @param practiceService the practice service
     */
    public SecurityCodeService(ArchetypeService service, PracticeService practiceService) {
        this.service = service;
        this.practiceService = practiceService;
        this.contacts = new Contacts(service);
    }

    /**
     * Sends a code to a user.
     *
     * @param user the user
     * @return an id corresponding to the code which must be supplied, or {@code null} if no code was sent
     */
    protected String sendCode(User user) {
        String result = null;
        String to = getToAddress(user);
        if (to != null) {
            result = sendCode(user, to);
        }
        return result;
    }

    /**
     * Sends a code to a user via email.
     *
     * @param user the user
     * @param to   the to-address
     * @return an id corresponding to the code which must be supplied, or {@code null} if no code was sent
     */
    protected String sendCode(User user, String to) {
        String result = null;
        String from = getFromAddress();
        if (from != null) {
            SecurityCode code = generateCode(user, Factor.EMAIL);
            if (sendCode(user, code.getCode(), from, to)) {
                result = code.getId();
            }
        }
        return result;
    }

    /**
     * Sends a code to a user.
     *
     * @param user the user
     * @param code the code to send
     * @param from the from-address
     * @param to   the to-address
     * @return {@code true} if the code was sent successfully, otherwise {@code false}
     */
    protected abstract boolean sendCode(User user, String code, String from, String to);

    /**
     * Applies an operation, if the supplied code identifier and code are valid.
     * <p/>
     * The operation will be supplied with the identifier of the user that the code corresponds to.
     *
     * @param id        the code identifier
     * @param code      the code
     * @param operation the operation to apply
     * @return the status of the operation
     */
    protected Status applyCode(String id, String code, Function<Long, Status> operation) {
        SecurityCode securityCode = getCode(id);
        Status result = verifyCode(id, code, Factor.EMAIL, securityCode);
        if (result == Status.SUCCESS) {
            result = operation.apply(securityCode.getUserId());
        }
        return result;
    }

    /**
     * Returns the practice from-address.
     *
     * @return the from-address or {@code null} if none is configured
     */
    protected String getFromAddress() {
        String from = getEmail(practiceService.getPractice());
        if (from == null) {
            if (!missingEmailLogged) {
                log.error("Cannot send security mails as the practice has no email address configured");
                missingEmailLogged = true;
            }
        } else {
            missingEmailLogged = false;
        }
        return from;
    }

    /**
     * Returns the user's to-address
     *
     * @param user the user
     * @return the to-address or {@code null} if none is configured
     */
    protected String getToAddress(User user) {
        String to = getEmail(user);
        if (to == null) {
            log.error("Cannot send security mails to {} as the user has no email address configured",
                      user.getUsername());
        }
        return to;
    }

    /**
     * Returns the code corresponding to an id.
     *
     * @param id the id
     * @return the corresponding code, {@code null} if the code doesn't exist
     */
    protected SecurityCode getCode(String id) {
        return codes.get(id);
    }

    /**
     * Generates a code for a user.
     * <p/>
     * This produces a pair containing a UUID and code, both of which must be supplied in order for the code to be
     * valid.
     * <br/>
     * The UUID is intended to be submitted via a hidden input field in a reset page, whilst the code must be entered
     * by the user.
     *
     * @param user   the user
     * @param factor the factor
     * @return a uuid and code pair
     */
    protected SecurityCode generateCode(User user, Factor factor) {
        SecurityCode code = new SecurityCode(user, factor);
        codes.put(code.getId(), code);
        return code;
    }

    /**
     * Verifies a code matches that expected.
     *
     * @param id           the id
     * @param code         the code to verify
     * @param factor       the factor used
     * @param securityCode the security code corresponding to the id. May be {@code null}
     * @return the verification status
     */
    protected Status verifyCode(String id, String code, Factor factor, SecurityCode securityCode) {
        Status result;
        if (securityCode == null || factor != securityCode.getFactor()) {
            result = Status.EXPIRED;
        } else if (!Objects.equals(securityCode.getCode(), code)) {
            result = codeMismatch(securityCode);
        } else {
            result = Status.SUCCESS;
            removeCode(id);
        }
        return result;
    }

    /**
     * Handle a code mismatch between what the user entered, and what was expected.
     * <p/>
     * If the number of tries is {@code < MAX_ATTEMPTS}, this returns {@link Status#CODE_MISMATCH},
     * otherwise it treats it as {@link Status#EXPIRED}, and removes the code.
     *
     * @param securityCode the security code
     * @return the status
     */
    protected Status codeMismatch(SecurityCode securityCode) {
        Status result;
        if (securityCode.incAttempts() < MAX_ATTEMPTS) {
            result = Status.CODE_MISMATCH;
        } else {
            // codes mismatched, and no more attempts are allowed to prevent brute-forcing the code
            result = Status.EXPIRED;
            removeCode(securityCode.getId());
        }
        return result;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getArchetypeService() {
        return service;
    }

    /**
     * Returns the practice service.
     *
     * @return the practice service
     */
    protected PracticeService getPracticeService() {
        return practiceService;
    }

    /**
     * Returns the preferred email for a party.
     *
     * @param party the party. May be {@code null}
     * @return the email address, or {@code null} if no email contact is present
     */
    protected String getEmail(Party party) {
        return party != null ? contacts.getEmail(party) : null;
    }

    /**
     * Saves a user.
     * <p/>
     * This allows users to be saved without the caller having explicit permission to save users.
     * <p/>
     * This is necessary to enable users to update passwords etc., without having general permission to save users.
     * <p/>
     * TODO - this should be done via specific update methods on IUserDAO to ensure only specific user details can be
     * updated.
     *
     * @param user the user to save
     */
    protected void save(User user) {
        ArchetypeService unrestricted = null;
        if (service instanceof Advised) {
            try {
                unrestricted = (ArchetypeService) ((Advised) service).getTargetSource().getTarget();
            } catch (Exception exception) {
                // do nothing
            }
        }
        if (unrestricted == null) {
            unrestricted = service;
        }
        unrestricted.save(user);
    }

    /**
     * Removes a code corresponding to an id.
     *
     * @param id the id
     */
    private void removeCode(String id) {
        codes.remove(id);
    }
}
