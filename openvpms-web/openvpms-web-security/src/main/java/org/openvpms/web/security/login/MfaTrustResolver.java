/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.security.login;

import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;

/**
 * A {@link AuthenticationTrustResolver} for multifactor authentication.
 * <p/>
 * This treats {@link MfaAuthenticationToken} as anonymous.
 *
 * @author Tim Anderson
 * @see MfaAuthenticationToken
 */
public class MfaTrustResolver implements AuthenticationTrustResolver {

    /**
     * The trust resolver to delegate to.
     */
    private final AuthenticationTrustResolver delegate = new AuthenticationTrustResolverImpl();

    /**
     * Indicates whether the passed {@code Authentication} token represents an
     * anonymous user. Typically the framework will call this method if it is trying to
     * decide whether an {@code AccessDeniedException} should result in a final
     * rejection (i.e. as would be the case if the principal was non-anonymous/fully
     * authenticated) or direct the principal to attempt actual authentication (i.e. as
     * would be the case if the {@code Authentication} was merely anonymous).
     *
     * @param authentication to test (may be {@code null} in which case the method will always return {@code false})
     * @return {@code true} the passed authentication token represented an anonymous
     * principal, {@code false} otherwise
     */
    @Override
    public boolean isAnonymous(Authentication authentication) {
        return delegate.isAnonymous(authentication) || authentication instanceof MfaAuthenticationToken;
    }

    /**
     * Indicates whether the passed {@code Authentication} token represents user that
     * has been remembered (i.e. not a user that has been fully authenticated).
     * <p>
     * The method is provided to assist with custom {@code AccessDecisionVoter}s and
     * the like that you might develop. Of course, you don't need to use this method
     * either and can develop your own "trust level" hierarchy instead.
     *
     * @param authentication to test (may be {@code null} in which case the method will always return {@code false})
     * @return {@code true} the passed authentication token represented a principal authenticated using a
     * remember-me token, {@code false} otherwise
     */
    @Override
    public boolean isRememberMe(Authentication authentication) {
        return delegate.isRememberMe(authentication);
    }
}
