/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.security.login;

import org.openvpms.web.security.user.LoginUserDetails;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * An authentication handler that determines if multifactor authentication is required.
 *
 * @author Tim Anderson
 */
public class LoginAuthenticationHandler implements AuthenticationSuccessHandler {

    /**
     * Handler used when no multifactor authentication is required.
     */
    private final SimpleUrlAuthenticationSuccessHandler appHandler;

    /**
     * Handler used when multifactor authentication is required.
     */
    private final SimpleUrlAuthenticationSuccessHandler login2Handler;

    /**
     * Constructs a {@link LoginAuthenticationHandler}.
     */
    public LoginAuthenticationHandler() {
        appHandler = new SimpleUrlAuthenticationSuccessHandler("/app");
        appHandler.setAlwaysUseDefaultTargetUrl(true);
        login2Handler = new SimpleUrlAuthenticationSuccessHandler("/selectfactor");
        login2Handler.setAlwaysUseDefaultTargetUrl(true);
    }

    /**
     * Invoked on successful authentication.
     *
     * @param request        the request
     * @param response       the response
     * @param authentication the authentication
     * @throws IOException      for any I/O error
     * @throws ServletException for any servlet error
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        LoginUserDetails details = (LoginUserDetails) authentication.getPrincipal();
        if (details.isMultiFactorAuthenticationRequired()) {
            saveAuthentication(new MfaAuthenticationToken(authentication));
            login2Handler.onAuthenticationSuccess(request, response, authentication);
        } else {
            // unwrap the details, but preserve the authorities, which have a pseudo ROLE_ authority
            authentication = new UsernamePasswordAuthenticationToken(details.getUserDetails(),
                                                                     authentication.getCredentials(),
                                                                     details.getAuthorities());
            saveAuthentication(authentication);
            appHandler.onAuthenticationSuccess(request, response, authentication);
        }
    }

    /**
     * Updates the security context with the supplied authentication.
     *
     * @param authentication the authentication
     */
    private void saveAuthentication(Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}