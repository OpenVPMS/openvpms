/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.archetype.rules.settings.SettingsCache;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Helper to initialise the {@link FirewallService} once the {@link FirewalledUserAuthenticationProvider}s are
 * constructed, due to circular dependencies between them.
 *
 * @author Tim Anderson
 */
public class FirewallInitialiser {

    /**
     * Constructs a {@link FirewallInitialiser}.
     *
     * @param firewallService the firewall service
     * @param settings        the settings cache
     * @param service         the archetype service
     * @param apiProvider     the API authentication provider
     * @param guiProvider     the GUI authentication provider
     */
    @SuppressWarnings("unused")
    public FirewallInitialiser(FirewallService firewallService, SettingsCache settings, ArchetypeService service,
                               LoginUserDetailsService userDetailsService,
                               FirewalledUserAuthenticationProvider apiProvider,
                               FirewalledUserAuthenticationProvider guiProvider) {
        // NOTE: the apiProvider and guiProvider are only required to ensure the initialiser is only constructed
        // after all dependencies are available
        firewallService.initialise(settings, service);
        userDetailsService.setArchetypeService(service);
    }
}