/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.security.login;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

/**
 * An authentication token for multifactor authentication.
 *
 * @author Tim Anderson
 * @see MfaAuthenticationProvider
 */
public class MfaAuthenticationToken extends UsernamePasswordAuthenticationToken {

    /**
     * Constructs a {@link MfaAuthenticationToken}.
     *
     * @param authentication the authentication
     */
    public MfaAuthenticationToken(Authentication authentication) {
        super(authentication.getPrincipal(), authentication.getCredentials(), authentication.getAuthorities());
    }

    /**
     * Determines if this is authenticated.
     *
     * @return {@code false}
     */
    @Override
    public boolean isAuthenticated() {
        return false;
    }
}
