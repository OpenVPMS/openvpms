/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.openvpms.component.model.user.User;
import org.openvpms.web.security.login.SecurityCodeService.Factor;
import org.openvpms.web.security.user.LoginUserDetails;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet invoked once the user selects the factor to use for multifactor authentication.
 * <p/>
 * This forwards to the login2 page. If email is selected, a code will first be emailed to their selected email address.
 *
 * @author Tim Anderson
 */
public class LoginFactorSelectedServlet extends HttpServlet {

    /**
     * The multifactor authentication service.
     */
    private MfaService service;

    /**
     * Initialises the servlet.
     *
     * @throws ServletException for any error
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        service = context.getBean(MfaService.class);
    }

    /**
     * Handles a POST request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof LoginUserDetails) {
            LoginUserDetails userDetails = (LoginUserDetails) authentication.getPrincipal();
            Factor factor = Factor.fromString(request.getParameter("factor"));
            if (Factor.EMAIL.equals(factor)) {
                String id = sendCode(userDetails);
                if (id != null) {
                    forwardToLogin2(factor, id, request, response);
                } else {
                    loginError(request, response);
                }
            } else if (Factor.TOTP.equals(factor)) {
                String id = service.generateTOTPCode((User) userDetails.getUserDetails());
                forwardToLogin2(factor, id, request, response);
            } else {
                loginError(request, response);
            }
        } else {
            loginError(request, response);
        }
    }

    /**
     * Sends a code to the user via email.
     *
     * @param userDetails the user details
     * @return the security code identifier that must be submitted with the user code
     */
    private String sendCode(LoginUserDetails userDetails) {
        String id = null;
        String email = userDetails.getEmail();
        if (email != null) {
            id = service.sendCode((User) userDetails.getUserDetails(), email);
        }
        return id;
    }

    /**
     * Forwards to the login2 page.
     *
     * @param factor   the factor to use
     * @param id       the security identifier that must be submitted along with the entered code
     * @param request  the servlet request
     * @param response the servlet response
     * @throws IOException      for any I/O error
     * @throws ServletException for any servlet error
     */
    private void forwardToLogin2(Factor factor, String id, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("factor", factor.toString());
        request.setAttribute("id", id);
        request.getRequestDispatcher("login2").forward(request, response);
    }

    /**
     * Invoked when multifactor authentication fails.
     * <p/>
     * Redirects to the login page.
     *
     * @param request  the servlet request
     * @param response the servlet response
     * @throws IOException for any I/O error
     */
    private void loginError(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        response.sendRedirect("login?status=error");
    }
}