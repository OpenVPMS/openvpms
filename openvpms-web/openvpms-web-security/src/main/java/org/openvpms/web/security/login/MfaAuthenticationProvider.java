/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * An {@link AuthenticationProvider} for {@link MfaAuthenticationToken}.
 *
 * @author Tim Anderson
 * @see LoginAuthenticationHandler
 */
public class MfaAuthenticationProvider implements AuthenticationProvider {

    /**
     * Performs authentication with the same contract as
     * {@link AuthenticationManager#authenticate(Authentication)}.
     *
     * @param authentication the authentication request object.
     * @return a fully authenticated object including credentials. May return
     * {@code null} if the {@code AuthenticationProvider} is unable to support
     * authentication of the passed {@code Authentication} object. In such a case,
     * the next {@code AuthenticationProvider} that supports the presented
     * {@code Authentication} class will be tried.
     * @throws AuthenticationException if authentication fails.
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (!supports(authentication.getClass())) {
            return null;
        }
        return authentication;
    }

    /**
     * Returns {@code true} if this {@code AuthenticationProvider} supports the
     * indicated {@code Authentication} object.
     *
     * @param authentication the authentication
     * @return {@code true} if the implementation can more closely evaluate the
     * {@code Authentication} class presented
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return MfaAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
