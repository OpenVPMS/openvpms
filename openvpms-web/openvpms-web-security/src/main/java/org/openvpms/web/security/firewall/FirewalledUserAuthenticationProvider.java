/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.security.UserAuthenticationProvider;
import org.openvpms.web.security.login.MfaAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * An {@link AuthenticationProvider} that authenticates users according to the remote address they are connecting
 * from.
 *
 * @author Tim Anderson
 * @see FirewallService
 */
public abstract class FirewalledUserAuthenticationProvider extends UserAuthenticationProvider {

    /**
     * The firewall service.
     */
    private final FirewallService firewallService;

    /**
     * Determines if multifactor authentication should be enabled.
     */
    private final boolean enableMultifactorAuthentication;

    /**
     * Constructs a {@link UserAuthenticationProvider}.
     *
     * @param userService                     the user service
     * @param passwordEncoder                 the password encoder
     * @param expireOnChangePassword          if {@code true}, an exception will be thrown if the user is required to
     *                                        change password
     * @param enableMultifactorAuthentication determines if multifactor authentication should be enabled
     * @param firewallService                 the firewall service
     */
    public FirewalledUserAuthenticationProvider(UserDetailsService userService, PasswordEncoder passwordEncoder,
                                                boolean expireOnChangePassword, boolean enableMultifactorAuthentication,
                                                FirewallService firewallService) {
        super(userService, passwordEncoder, expireOnChangePassword);
        this.firewallService = firewallService;
        this.enableMultifactorAuthentication = enableMultifactorAuthentication;
    }

    /**
     * Returns {@code true} if this {@code AuthenticationProvider} supports the indicated {@code Authentication} object.
     *
     * @param authentication the authentication
     * @return {@code true} if the implementation can more closely evaluate the {@code Authentication} class presented
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return super.supports(authentication) && !MfaAuthenticationToken.class.isAssignableFrom(authentication);
    }

    /**
     * Checks a user's authentication.
     *
     * @param userDetails the user details
     * @throws AuthenticationException if the user cannot be authenticated
     */
    @Override
    protected void checkAuthentication(UserDetails userDetails) {
        checkAllowed(userDetails);
        super.checkAuthentication(userDetails);
    }

    /**
     * Verifies that a user is allowed with multifactor authentication.
     * <p/>
     * This implementation throws {@link NoAccessFromHostException}.
     *
     * @param request     the current HTTP servlet request
     * @param userDetails the user details associated with the request
     */
    protected void checkAllowedWithMFA(HttpServletRequest request, UserDetails userDetails) {
        throw new NoAccessFromHostException(request.getRemoteAddr());
    }

    /**
     * Determines if a user is allowed.
     *
     * @param userDetails the user details
     * @throws NoAccessFromHostException if the user cannot access from the remote host
     * @throws IllegalStateException     if there is no current HTTP request
     */
    private void checkAllowed(UserDetails userDetails) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            User user = getUser(userDetails);
            if (user != null) {
                checkAllowed(request, user, userDetails);
            } else {
                throw new NoAccessFromHostException(request.getRemoteAddr());
            }
        } else {
            throw new IllegalStateException("Not called from within a web request");
        }
    }

    /**
     * Determines if a user is allowed.
     *
     * @param request     the current HTTP servlet request
     * @param user        the user
     * @param userDetails the user details
     * @throws NoAccessFromHostException if the user cannot access from the remote host
     * @throws IllegalStateException     if there is no current HTTP request
     */
    private void checkAllowed(HttpServletRequest request, User user, UserDetails userDetails) {
        FirewallService.AccessStatus status = firewallService.getAccessStatus(request, user,
                                                                              enableMultifactorAuthentication);
        if (status == FirewallService.AccessStatus.DENIED) {
            throw new NoAccessFromHostException(request.getRemoteAddr());
        } else if (status == FirewallService.AccessStatus.ALLOWED_WITH_MFA) {
            checkAllowedWithMFA(request, userDetails);
        }
    }
}
