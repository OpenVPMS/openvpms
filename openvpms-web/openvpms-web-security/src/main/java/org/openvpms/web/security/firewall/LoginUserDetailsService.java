/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.security.UserService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.security.user.LoginUserDetails;
import org.openvpms.web.security.user.LoginUserDetailsImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * A {@link UserDetailsService} that returns {@link LoginUserDetails}.
 *
 * @author Tim Anderson
 */
public class LoginUserDetailsService implements UserDetailsService {

    /**
     * The user service.
     */
    private final UserService userService;

    /**
     * The archetype service.
     */
    private ArchetypeService archetypeService;

    /**
     * The contacts helper.
     */
    private Contacts contacts;

    /**
     * Constructs a {@link LoginUserDetailsService}.
     *
     * @param userService the user service
     */
    public LoginUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    /**
     * Sets the archetype service.
     * <p/>
     * This can't be passed at construction due to cyclic dependency issues.
     *
     * @param archetypeService the archetype service
     */
    public void setArchetypeService(ArchetypeService archetypeService) {
        this.archetypeService = archetypeService;
        contacts = new Contacts(archetypeService);
    }

    /**
     * Locates the user based on the username.
     *
     * @param username the username
     * @return the corresponding user
     * @throws UsernameNotFoundException if no user is found with the username
     */
    @Override
    public LoginUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails details = userService.loadUserByUsername(username);
        User user = (User) details;
        String email = contacts.getEmail(user);
        IMObjectBean bean = archetypeService.getBean(user);
        String secret = bean.getString("totpSecret");
        return new LoginUserDetailsImpl(user, email, secret != null);
    }
}
