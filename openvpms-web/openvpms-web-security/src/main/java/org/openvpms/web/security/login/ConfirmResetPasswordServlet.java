/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet to perform a password reset.
 * <p/>
 * This requires the reset id and code corresponding to a current {@link SecurityCode}, and the new password to be
 * supplied in the request.
 *
 * @author Tim Anderson
 * @see PasswordService
 */
public class ConfirmResetPasswordServlet extends HttpServlet {

    /**
     * The password reset service.
     */
    private PasswordService passwordResetService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ConfirmForgotPasswordServlet.class);

    /**
     * Initialises the servlet.
     *
     * @throws ServletException for any error
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        passwordResetService = context.getBean(PasswordService.class);
    }

    /**
     * Handles a POST request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String id = request.getParameter("id");
        String code = request.getParameter("code");
        String password = request.getParameter("password");
        if (id != null && code != null && password != null) {
            PasswordService.Status status = reset(id, code, password);
            if (status == PasswordService.Status.SUCCESS) {
                login(response, null);
            } else if (status == PasswordService.Status.EXPIRED) {
                login(response, LoginStatus.EXPIRED);
            } else if (status == PasswordService.Status.CODE_MISMATCH){
                // Forward to the resetpassword page to enable the user to try again
                request.setAttribute("id", id);
                request.setAttribute("codemismatch", true);
                request.getRequestDispatcher("resetpassword").forward(request, response);
            } else {
                login(response, LoginStatus.ERROR);
            }
        } else {
            login(response, LoginStatus.ERROR);
        }
    }

    /**
     * Redirects to the login page.
     *
     * @param response the response
     * @param status   the login status
     * @throws IOException for any I/O error
     */
    private void login(HttpServletResponse response, String status) throws IOException {
        String url = "login";
        if (status != null) {
            url = url + "?status=" + status;
        }
        response.sendRedirect(url);
    }

    /**
     * Resets a password.
     *
     * @param id       the reset id
     * @param code     the reset code
     * @param password the new password
     * @return the status of the reset
     */
    private PasswordService.Status reset(String id, String code, String password) {
        PasswordService.Status status;
        try {
            status = passwordResetService.resetPassword(id, code, password);
        } catch (Exception exception) {
            log.error("Failed to reset password for id={}, code={}: {}", id, code, exception.getMessage(), exception);
            status = PasswordService.Status.ERROR;
        }
        return status;
    }
}