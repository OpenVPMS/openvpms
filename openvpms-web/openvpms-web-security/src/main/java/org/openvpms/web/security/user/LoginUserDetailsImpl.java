/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.user;

import org.openvpms.component.business.domain.im.security.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Default implementation of {@link LoginUserDetails}.
 *
 * @author Tim Anderson
 */
public class LoginUserDetailsImpl implements LoginUserDetails {

    /**
     * The user.
     */
    private final org.openvpms.component.business.domain.im.security.User user;

    /**
     * The user's preferred email address. May be {@code null}.
     */
    private final String email;

    /**
     * Determines if TOTP is configured.
     */
    private final boolean isTOTPConfigured;

    /**
     * The user authorities.
     */
    private final Set<GrantedAuthority> authorities;

    /**
     * Determines if multifactor authentication is required.
     */
    private boolean mfaRequired;

    /**
     * Constructs a {@link LoginUserDetailsImpl}.
     *
     * @param user             the underlying user
     * @param email            the user's preferred email address. May be {@code null}
     * @param isTOTPConfigured determines if the time-based one time password (TOTP) secret is configured
     */
    public LoginUserDetailsImpl(org.openvpms.component.business.domain.im.security.User user, String email,
                                boolean isTOTPConfigured) {
        this(user, email, isTOTPConfigured, "ROLE_USER");
    }

    /**
     * Constructs a {@link LoginUserDetailsImpl}.
     *
     * @param user             the underlying user
     * @param email            the user's preferred email address. May be {@code null}
     * @param isTOTPConfigured determines if the time-based one time password (TOTP) secret is configured
     * @param role             the role for this user
     */
    public LoginUserDetailsImpl(org.openvpms.component.business.domain.im.security.User user, String email,
                                boolean isTOTPConfigured, String role) {
        this.user = user;
        this.email = email;
        this.isTOTPConfigured = isTOTPConfigured;
        authorities = new TreeSet<>(Comparator.comparing(GrantedAuthority::getAuthority));
        authorities.addAll(user.getAuthorities());
        authorities.add(new SimpleGrantedAuthority(role));
    }

    /**
     * Returns the underlying user details.
     *
     * @return the underlying user details
     */
    @Override
    public UserDetails getUserDetails() {
        return user;
    }

    /**
     * Returns the underlying user.
     *
     * @return the user
     */
    @Override
    public User getUser() {
        return user;
    }

    /**
     * Returns the authorities granted to the user. Cannot return {@code null}.
     *
     * @return the authorities, sorted by natural key (never {@code null})
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * @return the password
     */
    @Override
    public String getPassword() {
        return user.getPassword();
    }

    /**
     * Returns the username used to authenticate the user. Cannot return
     * {@code null}.
     *
     * @return the username (never {@code null})
     */
    @Override
    public String getUsername() {
        return user.getUsername();
    }

    /**
     * Indicates whether the user's account has expired. An expired account cannot be
     * authenticated.
     *
     * @return {@code true} if the user's account is valid (ie non-expired),
     * {@code false} if no longer valid (ie expired)
     */
    @Override
    public boolean isAccountNonExpired() {
        return user.isAccountNonExpired();
    }

    /**
     * Indicates whether the user is locked or unlocked. A locked user cannot be
     * authenticated.
     *
     * @return {@code true} if the user is not locked, {@code false} otherwise
     */
    @Override
    public boolean isAccountNonLocked() {
        return user.isAccountNonLocked();
    }

    /**
     * Indicates whether the user's credentials (password) has expired. Expired
     * credentials prevent authentication.
     *
     * @return {@code true} if the user's credentials are valid (ie non-expired),
     * {@code false} if no longer valid (ie expired)
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return user.isCredentialsNonExpired();
    }

    /**
     * Indicates whether the user is enabled or disabled. A disabled user cannot be
     * authenticated.
     *
     * @return {@code true} if the user is enabled, {@code false} otherwise
     */
    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    /**
     * Indicates that multifactor authentication is required.
     */
    public void setMultiFactorAuthenticationRequired() {
        mfaRequired = true;
    }

    /**
     * Determines if multifactor authentication is required.
     *
     * @return {@code true} if multifactor authentication is required, {@code false} if only username & password
     * authentication is required
     */
    @Override
    public boolean isMultiFactorAuthenticationRequired() {
        return mfaRequired;
    }

    /**
     * Returns the user's preferred email address.
     *
     * @return the email address. May be {@code null}
     */
    @Override
    public String getEmail() {
        return email;
    }

    /**
     * Determines if the user has a time-based one time password (TOTP) secret configured.
     *
     * @return {@code true} if the user has TOTP configured, otherwise {@code false}
     */
    @Override
    public boolean isTOTPConfigured() {
        return isTOTPConfigured;
    }
}
