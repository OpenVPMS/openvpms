/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.junit.Test;
import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.security.FirewallSettings.AccessType;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link FirewallConfigManager}.
 *
 * @author Tim Anderson
 */
public class FirewallConfigManagerTestCase extends AbstractFirewallTestCase {

    /**
     * Tests {@link FirewallConfigManager#getConfig()}.
     */
    @Test
    public void testGetConfig() {
        FirewallSettings settings = getSettings();
        settings.setAccessType(AccessType.UNRESTRICTED);
        settings.setAllowedAddresses(Collections.emptyList());
        save(settings);

        FirewallConfigManager manager = new FirewallConfigManager(settingsCache, 5) {
            @Override
            protected boolean useCachedSettings() {
                return false;
            }
        };
        FirewallConfig config1 = manager.getConfig();
        assertEquals(AccessType.UNRESTRICTED, config1.getAccessType());
        assertEquals(0, config1.getAllowedAddresses().size());

        settings.setAccessType(AccessType.ALLOWED_ONLY);
        settings.setAllowedAddresses(Arrays.asList(new FirewallEntry("127.0.0.1", true, null),
                                                   new FirewallEntry("192.168.1.1", false, null)));
        save(settings);

        FirewallConfig config2 = manager.getConfig();
        assertEquals(AccessType.ALLOWED_ONLY, config2.getAccessType());
        assertEquals(1, config2.getAllowedAddresses().size());
    }
}