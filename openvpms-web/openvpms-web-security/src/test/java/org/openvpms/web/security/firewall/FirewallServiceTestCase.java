/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.security.FirewallSettings.AccessType;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.security.firewall.FirewallService.AccessStatus.ALLOWED;
import static org.openvpms.web.security.firewall.FirewallService.AccessStatus.ALLOWED_WITH_MFA;
import static org.openvpms.web.security.firewall.FirewallService.AccessStatus.DENIED;

/**
 * Tests the {@link FirewallService} class.
 *
 * @author Tim Anderson
 */
public class FirewallServiceTestCase extends AbstractFirewallTestCase {

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Tests the {@link FirewallService#isAllowed(HttpServletRequest)} method.
     */
    @Test
    public void testIsAllowed() {
        FirewallSettings settings = getSettings();
        settings.setAccessType(AccessType.UNRESTRICTED);
        settings.setAllowedAddresses(Collections.emptyList());
        save(settings);

        FirewallService service = new FirewallService(5);
        service.initialise(settingsCache, getArchetypeService());

        HttpServletRequest request1 = createRequest("127.0.0.1");
        HttpServletRequest request2 = createRequest("192.168.1.10");
        HttpServletRequest request3 = createRequest("192.168.10.0");
        HttpServletRequest request4 = createRequest("192.168.10.3");
        HttpServletRequest request5 = createRequest("192.168.10.5");
        assertTrue(service.isAllowed(request1));
        assertTrue(service.isAllowed(request2));
        assertTrue(service.isAllowed(request3));
        assertTrue(service.isAllowed(request4));
        assertTrue(service.isAllowed(request5));

        // set the access type to ALLOWED_ONLY. As there are no allowed addresses, requests should be disallowed
        settings.setAccessType(AccessType.ALLOWED_ONLY);
        save(settings);
        assertFalse(service.isAllowed(request1));
        assertFalse(service.isAllowed(request2));
        assertFalse(service.isAllowed(request3));
        assertFalse(service.isAllowed(request4));
        assertFalse(service.isAllowed(request5));

        // configure the firewall to allow specific addresses
        FirewallEntry entry1 = new FirewallEntry("127.0.0.1");
        FirewallEntry entry2 = new FirewallEntry("192.168.1.1");
        FirewallEntry entry3 = new FirewallEntry("192.168.10.0/30"); // i.e. 192.168.10.0 - 192.168.10.3
        settings.setAllowedAddresses(Arrays.asList(entry1, entry2, entry3));
        save(settings);
        assertTrue(service.isAllowed(request1));
        assertFalse(service.isAllowed(request2));
        assertTrue(service.isAllowed(request3));
        assertTrue(service.isAllowed(request4));
        assertFalse(service.isAllowed(request5));

        // when ALLOWED_USER is set, the firewall cannot block access without knowing the user, hence
        // isAllowed(request) always returns true. Need isAllowed(request, User) to determine if a request is
        // allowed/blocked.
        settings.setAccessType(AccessType.ALLOWED_USER);
        save(settings);
        assertTrue(service.isAllowed(request1));
        assertTrue(service.isAllowed(request2));
        assertTrue(service.isAllowed(request3));
        assertTrue(service.isAllowed(request4));
        assertTrue(service.isAllowed(request5));
    }

    /**
     * Tests the {@link FirewallService#isAllowed(HttpServletRequest, boolean)} method.
     */
    @Test
    public void testIsAllowedWithLimitAllowedUser() {
        FirewallSettings settings = getSettings();
        settings.setAccessType(AccessType.UNRESTRICTED);
        settings.setAllowedAddresses(Collections.emptyList());
        save(settings);

        FirewallService service = new FirewallService(5);
        service.initialise(settingsCache, getArchetypeService());

        HttpServletRequest request1 = createRequest("127.0.0.1");
        HttpServletRequest request2 = createRequest("192.168.1.10");
        HttpServletRequest request3 = createRequest("192.168.10.0");
        HttpServletRequest request4 = createRequest("192.168.10.3");
        HttpServletRequest request5 = createRequest("192.168.10.5");
        assertTrue(service.isAllowed(request1, false));
        assertTrue(service.isAllowed(request2, false));
        assertTrue(service.isAllowed(request3, false));
        assertTrue(service.isAllowed(request4, false));
        assertTrue(service.isAllowed(request5, false));
        assertTrue(service.isAllowed(request1, true));
        assertTrue(service.isAllowed(request2, true));
        assertTrue(service.isAllowed(request3, true));
        assertTrue(service.isAllowed(request4, true));
        assertTrue(service.isAllowed(request5, true));

        // set the access type to ALLOWED_ONLY. As there are no allowed addresses, requests should be disallowed
        settings.setAccessType(AccessType.ALLOWED_ONLY);
        save(settings);
        assertFalse(service.isAllowed(request1, false));
        assertFalse(service.isAllowed(request2, false));
        assertFalse(service.isAllowed(request3, false));
        assertFalse(service.isAllowed(request4, false));
        assertFalse(service.isAllowed(request5, false));
        assertFalse(service.isAllowed(request1, true));
        assertFalse(service.isAllowed(request2, true));
        assertFalse(service.isAllowed(request3, true));
        assertFalse(service.isAllowed(request4, true));
        assertFalse(service.isAllowed(request5, true));

        // configure the firewall to allow specific addresses
        FirewallEntry entry1 = new FirewallEntry("127.0.0.1");
        FirewallEntry entry2 = new FirewallEntry("192.168.1.1");
        FirewallEntry entry3 = new FirewallEntry("192.168.10.0/30"); // i.e. 192.168.10.0 - 192.168.10.3
        settings.setAllowedAddresses(Arrays.asList(entry1, entry2, entry3));
        save(settings);
        assertTrue(service.isAllowed(request1, false));
        assertFalse(service.isAllowed(request2, false));
        assertTrue(service.isAllowed(request3, false));
        assertTrue(service.isAllowed(request4, false));
        assertFalse(service.isAllowed(request5, false));
        assertTrue(service.isAllowed(request1, true));
        assertFalse(service.isAllowed(request2, true));
        assertTrue(service.isAllowed(request3, true));
        assertTrue(service.isAllowed(request4, true));
        assertFalse(service.isAllowed(request5, true));

        // set the access type to ALLOWED_USER. This is treated as ALLOWED_ONLY when limitAllowedUser=true
        settings.setAccessType(AccessType.ALLOWED_USER);
        save(settings);
        assertTrue(service.isAllowed(request1, false));
        assertTrue(service.isAllowed(request2, false));
        assertTrue(service.isAllowed(request3, false));
        assertTrue(service.isAllowed(request4, false));
        assertTrue(service.isAllowed(request5, false));
        assertTrue(service.isAllowed(request1, true));
        assertFalse(service.isAllowed(request2, true));
        assertTrue(service.isAllowed(request3, true));
        assertTrue(service.isAllowed(request4, true));
        assertFalse(service.isAllowed(request5, true));
    }

    /**
     * Tests the {@link FirewallService#getAccessStatus(HttpServletRequest, User, boolean)} method.
     */
    @Test
    public void testGetStatus() {
        // create a user that can only connect from designated addresses, when the firewall is enabled
        User user1 = userFactory.newUser()
                .connectFromAnywhere(false)
                .build(false);

        // create a user that can connect from any address
        User user2 = userFactory.newUser()
                .connectFromAnywhere(true)
                .build(false);

        FirewallSettings settings = getSettings();
        settings.setAccessType(AccessType.UNRESTRICTED);
        settings.setAllowedAddresses(Collections.emptyList());
        settings.setMultifactorAuthenticationEnabled(false); // NOTE: ignored for UNRESTRICTED, ALLOWED AccessTypes
        save(settings);

        FirewallService service = new FirewallService(5);
        service.initialise(settingsCache, getArchetypeService());

        HttpServletRequest request1 = createRequest("127.0.0.1");
        HttpServletRequest request2 = createRequest("192.168.1.10");
        HttpServletRequest request3 = createRequest("192.168.10.0");
        HttpServletRequest request4 = createRequest("192.168.10.3");
        HttpServletRequest request5 = createRequest("192.168.10.5");
        assertEquals(ALLOWED, service.getAccessStatus(request1, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request2, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request3, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request4, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request5, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request1, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request2, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request3, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request4, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request5, user2, true));

        settings.setAccessType(AccessType.ALLOWED_ONLY);
        save(settings);
        assertEquals(DENIED, service.getAccessStatus(request1, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request2, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request3, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request4, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request5, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request1, user2, true));
        assertEquals(DENIED, service.getAccessStatus(request2, user2, true));
        assertEquals(DENIED, service.getAccessStatus(request3, user2, true));
        assertEquals(DENIED, service.getAccessStatus(request4, user2, true));
        assertEquals(DENIED, service.getAccessStatus(request5, user2, true));

        // configure the firewall to allow specific addresses
        FirewallEntry entry1 = new FirewallEntry("127.0.0.1");
        FirewallEntry entry2 = new FirewallEntry("192.168.1.1");
        FirewallEntry entry3 = new FirewallEntry("192.168.10.0/30"); // i.e. 192.168.10.0 - 192.168.10.3
        settings.setAllowedAddresses(Arrays.asList(entry1, entry2, entry3));
        save(settings);
        assertEquals(ALLOWED, service.getAccessStatus(request1, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request2, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request3, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request4, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request5, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request1, user2, true));
        assertEquals(DENIED, service.getAccessStatus(request2, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request3, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request4, user2, true));
        assertEquals(DENIED, service.getAccessStatus(request5, user2, true));

        settings.setAccessType(AccessType.ALLOWED_USER);
        save(settings);
        // user1 can only connect from allowed addresses
        assertEquals(ALLOWED, service.getAccessStatus(request1, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request2, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request3, user1, true));
        assertEquals(ALLOWED, service.getAccessStatus(request4, user1, true));
        assertEquals(DENIED, service.getAccessStatus(request5, user1, true));

        // user2 can connect from anywhere, so all requests allowed
        assertEquals(ALLOWED, service.getAccessStatus(request1, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request2, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request3, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request4, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request5, user2, true));

        // when multifactor authentication is enabled, verify that connect from anywhere users require MFA if
        // they are outside the allowed addresses
        settings.setMultifactorAuthenticationEnabled(true);
        save(settings);

        // user2 can connect from anywhere, so all requests allowed. When the user
        // is not connecting from an allowed address and MFA is:
        // . enabled - ALLOWED_WITH_MFA is returned
        // . disabled - ALLOWED is returned
        assertEquals(ALLOWED, service.getAccessStatus(request1, user2, true));
        assertEquals(ALLOWED_WITH_MFA, service.getAccessStatus(request2, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request2, user2, false)); // MFA disabled
        assertEquals(ALLOWED, service.getAccessStatus(request3, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request4, user2, true));
        assertEquals(ALLOWED_WITH_MFA, service.getAccessStatus(request5, user2, true));
        assertEquals(ALLOWED, service.getAccessStatus(request5, user2, false)); // MFA disabled
    }

    /**
     * Creates a request.
     *
     * @param address the remote address
     * @return a new request
     */
    private HttpServletRequest createRequest(String address) {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRemoteAddr()).thenReturn(address);
        return request;
    }
}