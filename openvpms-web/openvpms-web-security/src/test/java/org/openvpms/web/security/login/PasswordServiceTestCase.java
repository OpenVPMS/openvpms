/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.login;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.user.PasswordValidator;
import org.openvpms.archetype.rules.user.TestPasswordPolicy;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.dao.im.common.IMObjectDAO;
import org.openvpms.component.business.dao.im.security.IUserDAO;
import org.openvpms.component.model.user.User;
import org.openvpms.web.security.login.SecurityCodeService.Factor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.mail.MessagingException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.security.login.SecurityCodeService.Status.CODE_MISMATCH;
import static org.openvpms.web.security.login.SecurityCodeService.Status.ERROR;
import static org.openvpms.web.security.login.SecurityCodeService.Status.EXPIRED;
import static org.openvpms.web.security.login.SecurityCodeService.Status.SUCCESS;

/**
 * Tests the {@link PasswordService}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PasswordServiceTestCase extends ArchetypeServiceTest {

    /**
     * The password encoder.
     */
    private final PasswordEncoder encoder = new BCryptPasswordEncoder();

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The user DAO.
     */
    @Autowired
    private IUserDAO userDAO;

    /**
     * The IMObject DAO.
     */
    @Autowired
    private IMObjectDAO dao;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The password reset service.
     */
    private PasswordService service;

    /**
     * The password mailer.
     */
    private TestPasswordMailer mailer;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        practiceFactory.newPractice()
                .addEmail("admin@vetsrus.com")
                .build();
        mailer = new TestPasswordMailer();
        TestPasswordPolicy policy = new TestPasswordPolicy(6, 10, false, false, false, false);
        PasswordValidator validator = new PasswordValidator(policy);
        service = new PasswordService(getArchetypeService(), userDAO,
                                      encoder, validator, practiceService, mailer);
    }

    /**
     * Tests resetting a password.
     */
    @Test
    public void testReset() {
        User user = userFactory.newUser()
                .addEmail("foo@bar.com")
                .changePassword(true)  // not required for resetting a password, but should be cleared on success
                .build();

        String id = service.sendResetCode(user.getUsername());
        String code = mailer.getCode();
        assertNotNull(id);
        assertNotNull(code);
        assertEquals(1, mailer.getSent());
        assertEquals(6, code.length());
        mailer.verify("admin@vetsrus.com", "foo@bar.com", "Your OpenVPMS verification code",
                      "Your OpenVPMS verification code is " + code);

        assertEquals(SUCCESS, service.resetPassword(id, code, "abc123"));
        mailer.verify("admin@vetsrus.com", "foo@bar.com", "Your OpenVPMS password has been changed",
                      "Your OpenVPMS password has been changed.\n" +
                      "If you didn't authorise this change, contact your administrator.");
        checkUser(user, "abc123");

        // verify can't use the same id and code to reset again
        assertEquals(EXPIRED, service.resetPassword(id, code, "abc123"));
    }

    /**
     * Verifies that a user only gets 3 attempts on entering a code before it expires.
     */
    @Test
    public void testCodeMismatch() {
        User user = userFactory.newUser()
                .addEmail("foo@bar.com")
                .build();

        // simulate entering the incorrect code 3 times. On the 3rd attempt, the code will expire
        String id1 = service.sendResetCode(user.getUsername());
        assertEquals(CODE_MISMATCH, service.resetPassword(id1, "a", "abc1234"));
        assertEquals(CODE_MISMATCH, service.resetPassword(id1, "b", "abc1234"));
        assertEquals(EXPIRED, service.resetPassword(id1, "c", "abc1234"));

        // attempts after the 3rd should be expired, even if the code is correct
        assertEquals(EXPIRED, service.resetPassword(id1, mailer.getCode(), "abc1234"));

        // only one mail should have been sent, containing the code
        assertEquals(1, mailer.getSent());

        // now simulate entering the correct code on the 3rd attempt
        String id2 = service.sendResetCode(user.getUsername());
        assertEquals(CODE_MISMATCH, service.resetPassword(id2, "a", "abc1234"));
        assertEquals(CODE_MISMATCH, service.resetPassword(id2, "b", "abc1234"));

        assertEquals(2, mailer.getSent());
        assertEquals(SUCCESS, service.resetPassword(id2, mailer.getCode(), "abc1234"));

        assertEquals(3, mailer.getSent());
        mailer.verify("admin@vetsrus.com", "foo@bar.com", "Your OpenVPMS password has been changed",
                      "Your OpenVPMS password has been changed.\n" +
                      "If you didn't authorise this change, contact your administrator.");
    }

    /**
     * Verifies an error is returned if a user is deactivated after the code is reset.
     */
    @Test
    public void testUserDeactivatedAfterReset() {
        User user = userFactory.newUser()
                .addEmail("foo@bar.com")
                .build();

        String id = service.sendResetCode(user.getUsername());
        String code = mailer.getCode();
        assertNotNull(id);
        assertNotNull(code);

        // deactivate the user and verify the reset fails
        user.setActive(false);
        save(user);
        assertEquals(ERROR, service.resetPassword(id, code, "abc1234"));

        // reactivate the user, and verify the code has expired
        user.setActive(true);
        save(user);
        assertEquals(EXPIRED, service.resetPassword(id, code, "abc1234"));
    }

    /**
     * Verifies that a reset code isn't generated if a user doesn't have an email.
     */
    @Test
    public void testUserNoEmail() {
        User user = userFactory.createUser();
        String id = service.sendResetCode(user.getUsername());
        assertNull(id);
        assertEquals(0, mailer.getSent());
    }

    /**
     * Verifies that a reset code isn't generated if a user is inactive.
     */
    @Test
    public void testInactiveUser() {
        User user = userFactory.newUser().active(false).build();
        String id = service.sendResetCode(user.getUsername());
        assertNull(id);
        assertEquals(0, mailer.getSent());
    }

    /**
     * Verifies that a reset code isn't generated if a user doesn't exist.
     */
    @Test
    public void testInvalidUser() {
        String id = service.sendResetCode(TestHelper.randomName("_user"));
        assertNull(id);
        assertEquals(0, mailer.getSent());
    }

    /**
     * Verifies an error is returned if the password is invalid.
     */
    @Test
    public void testInvalidPassword() {
        User user = userFactory.newUser()
                .addEmail("foo@bar.com")
                .build();

        String id = service.sendResetCode(user.getUsername());
        String code = mailer.getCode();
        assertNotNull(id);
        assertNotNull(code);
        assertEquals(ERROR, service.resetPassword(id, code, "abc"));

        // subsequent calls should fail as the code should have expired. Passwords should be valid on submit
        assertEquals(EXPIRED, service.resetPassword(id, code, "abc123"));
    }

    /**
     * Verifies that {@link PasswordService.Status#EXPIRED} is returned if the id passed to
     * {@link PasswordService#resetPassword(String, String, String)} doesn't exist.
     */
    @Test
    public void testResetForInvalidId() {
        SecurityCode code = new SecurityCode(1, Factor.EMAIL);
        assertEquals(EXPIRED, service.resetPassword(code.getId(), code.getCode(), "abc123"));
    }

    /**
     * Tests the {@link PasswordService#changePassword(User, String, String)} method.
     */
    @Test
    public void testChangePassword() {
        String oldPassword = "ab1234";
        User user = userFactory.newUser()
                .addEmail("foo@bar.com")
                .password(encoder.encode(oldPassword))
                .changePassword(true)
                .build();
        assertEquals(ERROR, service.changePassword(user, oldPassword, oldPassword)); // cannot set same password

        assertEquals(ERROR, service.changePassword(user, oldPassword, "a")); // password too short

        String newPassword = "xyz123";
        assertEquals(SUCCESS, service.changePassword(user, oldPassword, newPassword));
        checkUser(user, newPassword);

        assertEquals(1, mailer.getSent());
        mailer.verify("admin@vetsrus.com", "foo@bar.com", "Your OpenVPMS password has been changed",
                      "Your OpenVPMS password has been changed.\n" +
                      "If you didn't authorise this change, contact your administrator.");
    }

    /**
     * Verifies a user's password matches that expected, and the changePassword flag is cleared.
     *
     * @param user     the user
     * @param password the expected password
     */
    private void checkUser(User user, String password) {
        org.openvpms.component.business.domain.im.security.User impl
                = (org.openvpms.component.business.domain.im.security.User) get(user);
        String actual = impl.getPassword();
        assertTrue(encoder.matches(password, actual));
        assertFalse(impl.getChangePassword());
    }

    private static class TestPasswordMailer extends PasswordMailer {

        private String from;

        private String to;

        private String code;

        private String subject;

        private String text;

        private int sent;

        /**
         * Constructs a {@link TestPasswordMailer}.
         */
        public TestPasswordMailer() {
            super(null);
        }

        /**
         * Sends a password reset code email.
         *
         * @param from the from-address
         * @param to   the to-address
         * @param code the reset code
         * @throws MessagingException for any messaging error
         */
        @Override
        public void sendResetCode(String from, String to, String code) throws MessagingException {
            super.sendResetCode(from, to, code);
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public int getSent() {
            return sent;
        }

        public void verify(String from, String to, String subject, String text) {
            assertEquals(from, this.from);
            assertEquals(to, this.to);
            assertEquals(subject, this.subject);
            assertEquals(text, this.text);
        }

        /**
         * Sends an email.
         *
         * @param from    the from-address
         * @param to      the to-address
         * @param subject the subject
         * @param text    the message text
         */
        @Override
        protected void send(String from, String to, String subject, String text) {
            this.from = from;
            this.to = to;
            this.subject = subject;
            this.text = text;
            sent++;
        }
    }
}
