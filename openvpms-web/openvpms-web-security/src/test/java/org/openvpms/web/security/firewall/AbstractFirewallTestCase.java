/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.firewall;

import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.archetype.rules.settings.SettingsCache;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.bean.IMObjectBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base class for firewall tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractFirewallTestCase extends ArchetypeServiceTest {

    /**
     * The settings cache.
     */
    @Autowired
    protected SettingsCache settingsCache;

    /**
     * Returns the firewall settings.
     *
     * @return the firewall settings
     */
    protected FirewallSettings getSettings() {
        IMObjectBean bean = settingsCache.getGroup(SettingsArchetypes.FIREWALL_SETTINGS);
        return new FirewallSettings(bean);
    }

    /**
     * Saves the firewall settings.
     *
     * @param settings the settings
     */
    protected void save(FirewallSettings settings) {
        save(settings.getSettings());
    }
}