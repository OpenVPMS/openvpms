This module performs integration tests on openvpms.war using Jetty.

The pom.xml installs Jetty into target/jetty, using a customised jetty.xml in order to handle X-Forwarded-For headers.

Specifically, the following has been added to **httpConfig**:

    <Call name="addCustomizer">
        <Arg><New class="org.eclipse.jetty.server.ForwardedRequestCustomizer"/></Arg>
    </Call>

When upgrading Jetty, ensure that the jetty.xml in src/test/resources reflects that of the jetty-home distribution. 

# Debugging notes

1. Cargo logs to target/cargo.log
2. Jetty logs to target/jetty/logs/*.jetty.log
