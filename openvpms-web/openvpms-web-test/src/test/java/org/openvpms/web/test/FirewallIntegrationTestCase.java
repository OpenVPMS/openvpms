/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.test;

import org.apache.commons.lang3.math.NumberUtils;
import org.codehaus.cargo.container.InstalledLocalContainer;
import org.codehaus.cargo.container.configuration.LocalConfiguration;
import org.codehaus.cargo.container.deployable.WAR;
import org.codehaus.cargo.container.jetty.Jetty9xExistingLocalConfiguration;
import org.codehaus.cargo.container.jetty.Jetty9xInstalledLocalContainer;
import org.codehaus.cargo.container.property.GeneralPropertySet;
import org.codehaus.cargo.container.property.LoggingLevel;
import org.codehaus.cargo.container.property.ServletPropertySet;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.security.FirewallSettings.AccessType;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.security.TestFirewallBuilder;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.booking.domain.Booking;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.ws.util.ErrorResponseFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Tests openvpms.war with different firewall configurations in order to verify the Spring Security filters.
 * <p/>
 * This launches the war using Jetty, which must be installed in target/jetty. <br/>
 * In order to simulate users connecting from different hosts, Jetty must be configured to use the X-Forwarded-For
 * header for {@link HttpServletRequest#getRemoteAddr()}.This is done by adding the following to jetty.xml:<br/>
 * <pre>
 *     &lt;New id="httpConfig" class="org.eclipse.jetty.server.HttpConfiguration"&gt;
 *         ...
 *         &lt;Call name="addCustomizer"&gt;
 *             &lt;Arg&gt;&lt;New class="org.eclipse.jetty.server.ForwardedRequestCustomizer"/&gt;&lt;/Arg&gt;
 *         &lt;/Call&gt;
 * </pre>
 * Two system properties required, which will be passed to Jetty. These override the values embedded in the war:
 * <ul>
 *     <li>db.url - the test database URL</li>
 *     <li>openvpms.key - the encryption key</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class FirewallIntegrationTestCase extends ArchetypeServiceTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The plugin DAO.
     */
    @Autowired
    private PluginDAO pluginDAO;

    /**
     * The port Jetty is running on.
     */
    private int port;

    /**
     * User with {@code connectFromAnywhere=true}.
     */
    private User user1;

    /**
     * User with {@code connectFromAnywhere=false}.
     */
    private User user2;

    /**
     * User with {@code changePassword=true}
     */
    private User user3;

    /**
     * The Jetty container.
     */
    private InstalledLocalContainer container;

    /**
     * Firewall configuration builder.
     */
    private TestFirewallBuilder firewallBuilder;

    /**
     * Sets up the test case.
     * <p/>
     * In general, set up all persistent objects prior to starting Jetty, to avoid caching issues.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        practiceFactory.newPractice()
                .enablePlugins()
                .build();

        firewallBuilder = new TestFirewallBuilder(getArchetypeService());

        installPlugins();
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        ArchetypeAwareGrantedAuthority createAll = userFactory.createAuthority("create", "*.*");
        ArchetypeAwareGrantedAuthority saveAll = userFactory.createAuthority("save", "*.*");

        SecurityRole role = userFactory.createRole(createAll, saveAll);
        user1 = userFactory.newUser()
                .connectFromAnywhere(true)
                .password(encoder.encode("password1"))
                .addEmail("foo@bar.com")
                .onlineBooking()
                .addRoles(role)
                .build();
        user2 = userFactory.newUser()
                .connectFromAnywhere(false)
                .password(encoder.encode("password2"))
                .addEmail("bar@foo.com")
                .onlineBooking()
                .addRoles(role)
                .build();
        user3 = userFactory.newUser()
                .connectFromAnywhere(true)
                .changePassword(true)
                .password(encoder.encode("password3"))
                .addEmail("gum@ball.com")
                .onlineBooking()
                .addRoles(role)
                .build();
    }

    /**
     * Cleans up after the tests.
     */
    @After
    public void tearDown() {
        if (container != null) {
            container.stop();
        }
    }

    /**
     * Verifies that users can connect when the firewall access type is set to {@link AccessType#UNRESTRICTED}.
     */
    @Test
    public void testUnrestrictedAccess() {
        String address1 = "127.0.0.1";
        String address2 = "192.168.1.1";

        firewallBuilder.accessType(AccessType.UNRESTRICTED)
                .allowedAddresses()
                .build();

        startJetty();
        checkUnrestrictedAccess(address1, address2, user1, user2, user3);
    }

    /**
     * Verifies that when the firewall access type is set to {@link AccessType#ALLOWED_ONLY}, only users
     * from the listed addresses can connect.
     */
    @Test
    public void testAllowedOnly() {
        String address1 = "127.0.0.1";
        String address2 = "192.168.1.1";
        firewallBuilder.accessType(AccessType.ALLOWED_ONLY)
                .allowedAddresses(address1)
                .build();

        startJetty();
        checkAllowedOnly(address1, address2, user1, user2, user3);
    }

    /**
     * Verifies that when the firewall access type is set to {@link AccessType#ALLOWED_USER}, only users
     * from the listed addresses can connect, unless they have their <em>connectFromAnywhere</em> flag set.
     * <p/>
     * For this access type, IP filtering can only occur after a user is authenticated, so no filtering is performed
     * for unauthenticated endpoints.
     */
    @Test
    public void testAllowedUser() {
        String address1 = "127.0.0.1";
        String address2 = "192.168.1.1";
        firewallBuilder.accessType(AccessType.ALLOWED_USER)
                .allowedAddresses(address1)
                .enableMultifactorAuthentication(true)
                .build();

        startJetty();
        checkAllowedUser(address1, address2, user1, user2, user3);
    }

    /**
     * Verifies that online bookings can be made, and the resulting appointment has the correct createdBy user
     * assigned.
     */
    @Test
    public void testOnlineBookingWithUnrestrictedAccess() {
        firewallBuilder.accessType(AccessType.UNRESTRICTED)
                .build();

        // create some bookings
        Booking booking1 = createBooking(9);
        Booking booking2 = createBooking(10);
        Booking booking3 = createBooking(11);
        Booking booking4 = createBooking(12);
        Booking booking5 = createBooking(13);

        startJetty();

        // verify bookings can be submitted from both addresses by each user
        String address1 = "127.0.0.1";
        String address2 = "192.168.1.1";
        checkSubmitBooking(booking1, user1, "password1", address1);
        checkSubmitBooking(booking2, user1, "password1", address2);
        checkSubmitBooking(booking3, user2, "password2", address1);
        checkSubmitBooking(booking4, user2, "password2", address2);

        // changePassword=true so submission should fail
        checkSubmitBookingFails(booking5, user3, "password3", address1);
    }

    /**
     * Verifies that when the firewall access type is set to {@link AccessType#ALLOWED_ONLY}, only users
     * from the listed addresses can submit online bookings.
     */
    @Test
    public void testOnlineBookingWithAllowedOnlyFirewallAccess() {
        String address1 = "127.0.0.1";
        String address2 = "192.168.1.1";
        firewallBuilder.accessType(AccessType.ALLOWED_ONLY)
                .allowedAddresses(address1)
                .build();

        // create some bookings
        Booking booking1 = createBooking(9);
        Booking booking2 = createBooking(10);
        Booking booking3 = createBooking(11);
        Booking booking4 = createBooking(12);
        Booking booking5 = createBooking(13);

        startJetty();

        // verify bookings can be submitted from address1 by each user
        checkSubmitBooking(booking1, user1, "password1", address1);
        checkSubmitBooking(booking2, user2, "password2", address1);

        // verify bookings can't be submitted from address2
        checkSubmitBookingFails(booking3, user1, "password1", address2);
        checkSubmitBookingFails(booking4, user2, "password2", address2);

        // changePassword=true so submission should fail
        checkSubmitBookingFails(booking5, user3, "password3", address1);
    }

    /**
     * Verifies that when the firewall access type is set to {@link AccessType#ALLOWED_USER}, only users
     * from the listed addresses can submit online bookings, unless they have their <em>connectFromAnywhere</em> flag
     * set.
     */
    @Test
    public void testOnlineBookingWithAllowedUserFirewallAccess() {
        String address1 = "127.0.0.1";
        String address2 = "192.168.1.1";
        firewallBuilder.accessType(AccessType.ALLOWED_USER)
                .allowedAddresses(address1)
                .enableMultifactorAuthentication(true) // no effect for API users
                .build();

        // create some bookings
        Booking booking1 = createBooking(9);
        Booking booking2 = createBooking(10);
        Booking booking3 = createBooking(11);
        Booking booking4 = createBooking(12);
        Booking booking5 = createBooking(13);

        startJetty();

        // verify bookings can be submitted from address1 by each user
        checkSubmitBooking(booking1, user1, "password1", address1);
        checkSubmitBooking(booking2, user2, "password2", address1);

        // verify bookings can only be submitted from address2 when connectFromAnywhere = true
        checkSubmitBooking(booking3, user1, "password1", address2);      // connectFromAnywhere = true
        checkSubmitBookingFails(booking4, user2, "password2", address2); // connectFromAnywhere = false

        // connectFromAnywhere=true, but changePassword=true also so submission should fail
        checkSubmitBookingFails(booking5, user3, "password3", address1);
    }

    /**
     * Verifies that users can connect when the firewall access type is set to {@link AccessType#UNRESTRICTED}.
     *
     * @param address1 first address
     * @param address2 second address
     * @param user1    a user with {@code connectFromAnywhere=true}
     * @param user2    a user with {@code connectFromAnywhere=false}
     * @param user3    a user with {@code changePassword=true}
     */
    protected void checkUnrestrictedAccess(String address1, String address2, User user1, User user2, User user3) {
        checkGet("/openvpms/login", address1, HTTP_OK);
        checkGet("/openvpms/login", address2, HTTP_OK);
        checkGet("/openvpms/forgotpassword", address1, HTTP_OK);
        checkGet("/openvpms/forgotpassword", address2, HTTP_OK);
        checkGet("/openvpms/resetpassword", address1, HTTP_OK);
        checkGet("/openvpms/resetpassword", address2, HTTP_OK);

        checkGet("/openvpms/ws/booking/v1/locations", address1, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user2.getUsername(), "password2", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user2.getUsername(), "password2", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user2.getUsername(), "password2", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user2.getUsername(), "password2", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);

        // changePassword=true so all requests should fail
        checkGet("/openvpms/ws/booking/v1/locations", address1, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user3.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user3.getUsername(), "incorrect", HTTP_UNAUTHORIZED);

        // TODO - can't adequately test webdav. Need to use selenium
        checkGet("/openvpms/webdav/12345/67890/foo.odt", address1, HTTP_UNAUTHORIZED);
        checkGet("/openvpms/webdav/12345/67890/foo.odt", address2, HTTP_UNAUTHORIZED);

        checkGet("/openvpms/plugins/servlet/test-servlet", address1, HTTP_OK);
        checkGet("/openvpms/plugins/servlet/test-servlet", address2, HTTP_OK);

        checkGet("/openvpms/rest/hello/1/hello", address1, HTTP_OK);
        checkGet("/openvpms/rest/hello/1/hello", address2, HTTP_OK);
    }

    /**
     * Verifies that when the firewall access type is set to {@link AccessType#ALLOWED_ONLY}, only users
     * from the listed addresses can connect.
     *
     * @param address1 an allowed address
     * @param address2 a disallowed address
     * @param user1    a user with {@code connectFromAnywhere=true}
     * @param user2    a user with {@code connectFromAnywhere=false}
     * @param user3    a user with {@code changePassword=true}
     */
    protected void checkAllowedOnly(String address1, String address2, User user1, User user2, User user3) {
        checkGet("/openvpms/login", address1, HTTP_OK);
        checkGet("/openvpms/login", address2, HTTP_UNAUTHORIZED);
        checkGet("/openvpms/forgotpassword", address1, HTTP_OK);
        checkGet("/openvpms/forgotpassword", address2, HTTP_UNAUTHORIZED);
        checkGet("/openvpms/resetpassword", address1, HTTP_OK);
        checkGet("/openvpms/resetpassword", address2, HTTP_UNAUTHORIZED);

        checkGet("/openvpms/ws/booking/v1/locations", address1, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user1.getUsername(), "password1", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user1.getUsername(), "password1", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user2.getUsername(), "password2", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user2.getUsername(), "password2", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user2.getUsername(), "password2", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user2.getUsername(), "password2", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);

        // changePassword=true so all requests should fail
        checkGet("/openvpms/ws/booking/v1/locations", address1, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user3.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user3.getUsername(), "incorrect", HTTP_UNAUTHORIZED);

        // TODO - can't adequately test webdav. Need to use selenium
        checkGet("/openvpms/webdav/12345/67890/foo.odt", address1, HTTP_UNAUTHORIZED);
        checkGet("/openvpms/webdav/12345/67890/foo.odt", address2, HTTP_UNAUTHORIZED);

        checkGet("/openvpms/plugins/servlet/test-servlet", address1, HTTP_OK);
        checkGet("/openvpms/plugins/servlet/test-servlet", address2, HTTP_UNAUTHORIZED);

        checkGet("/openvpms/rest/hello/1/hello", address1, HTTP_OK);
        checkGet("/openvpms/rest/hello/1/hello", address2, HTTP_UNAUTHORIZED);
    }

    /**
     * Verifies that when the firewall access type is set to {@link AccessType#ALLOWED_USER}, only users
     * from the listed addresses can connect, unless they have their <em>connectFromAnywhere</em> flag set.
     * <p/>
     * For this access type, IP filtering can only occur after a user is authenticated, so no filtering is performed
     * for unauthenticated endpoints.
     *
     * @param address1 an allowed address
     * @param address2 a disallowed address
     * @param user1    a user with {@code connectFromAnywhere=true}
     * @param user2    a user with {@code connectFromAnywhere=false}
     * @param user3    a user with {@code changePassword=true}
     */
    protected void checkAllowedUser(String address1, String address2, User user1, User user2, User user3) {
        checkGet("/openvpms/login", address1, HTTP_OK);
        checkGet("/openvpms/login", address2, HTTP_OK);            // can't reject IP until authenticated
        checkGet("/openvpms/forgotpassword", address1, HTTP_OK);
        checkGet("/openvpms/forgotpassword", address2, HTTP_OK);   // can't reject IP until authenticated
        checkGet("/openvpms/resetpassword", address1, HTTP_OK);
        checkGet("/openvpms/resetpassword", address2, HTTP_OK);    // can't reject IP until authenticated

        checkGet("/openvpms/ws/booking/v1/locations", address1, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user1.getUsername(), "password1", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user2.getUsername(), "password2", HTTP_OK);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user2.getUsername(), "password2", HTTP_OK);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user2.getUsername(), "password2", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user2.getUsername(), "password2", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user1.getUsername(), "incorrect", HTTP_UNAUTHORIZED);

        // changePassword=true so all requests should fail
        checkGet("/openvpms/ws/booking/v1/locations", address1, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address1, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address2, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user3.getUsername(), "password3", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v1/locations", address1, user3.getUsername(), "incorrect", HTTP_UNAUTHORIZED);
        checkGet("/openvpms/ws/booking/v2/locations", address2, user3.getUsername(), "incorrect", HTTP_UNAUTHORIZED);

        // TODO - can't adequately test webdav. Need to use selenium
        checkGet("/openvpms/webdav/12345/67890/foo.odt", address1, HTTP_UNAUTHORIZED);
        checkGet("/openvpms/webdav/12345/67890/foo.odt", address2, HTTP_UNAUTHORIZED);

        checkGet("/openvpms/plugins/servlet/test-servlet", address1, HTTP_OK);
        checkGet("/openvpms/plugins/servlet/test-servlet", address2, HTTP_OK); // can't reject IP until authenticated

        checkGet("/openvpms/rest/hello/1/hello", address1, HTTP_OK);
        checkGet("/openvpms/rest/hello/1/hello", address2, HTTP_OK); // can't reject IP until authenticated
    }

    /**
     * Verifies an unauthenticated HTTP GET returns the expected status.
     *
     * @param path       the URL path
     * @param remoteAddr the simulated {@link HttpServletRequest#getRemoteAddr()}
     * @param status     the expected status
     */
    protected void checkGet(String path, String remoteAddr, int status) {
        String url = "http://localhost:" + port + path;
        given().header("X-Forwarded-For", remoteAddr)
                .get(url)
                .then().statusCode(status);
    }

    /**
     * Verifies an authenticated HTTP GET returns the expected status.
     *
     * @param path       the URL path
     * @param remoteAddr the simulated {@link HttpServletRequest#getRemoteAddr()}
     * @param username   the user name
     * @param password   the password
     * @param status     the expected status
     */
    protected void checkGet(String path, String remoteAddr, String username, String password, int status) {
        String url = "http://localhost:" + port + path;
        given().header("X-Forwarded-For", remoteAddr)
                .auth().basic(username, password)
                .get(url)
                .then().statusCode(status);
    }

    /**
     * Starts Jetty .
     */
    protected void startJetty() {
        startJetty(null);
    }

    /**
     * Starts Jetty with debugging enabled.
     */
    protected void startJettyDebug() {
        String jvmArgs = "-Xdebug\n" +
                         "-Xrunjdwp:transport=dt_shmem,server=y,suspend=n,address=openvpms_dev\n" +
                         "-Xnoagent\n" +
                         "-Djava.compiler=NONE";
        startJetty(jvmArgs);
    }

    /**
     * Starts Jetty.
     *
     * @param jvmArgs the JVM args. May be {@code null}
     */
    protected void startJetty(String jvmArgs) {
        LocalConfiguration configuration = new Jetty9xExistingLocalConfiguration(getPath("target/jetty"));
        configuration.addDeployable(new WAR(getPath("target/openvpms.war")));
        configuration.setProperty(GeneralPropertySet.LOGGING, LoggingLevel.HIGH.getLevel());
        if (jvmArgs != null) {
            configuration.setProperty(GeneralPropertySet.JVMARGS, jvmArgs);
        }

        container = new Jetty9xInstalledLocalContainer(configuration);
        container.setHome(getPath("target/jetty"));
        container.setOutput("target/cargo.log");
        Map<String, String> properties = new HashMap<>();

        // override properties defined in openvpms.properties
        properties.put("openvpms.key", System.getProperty("openvpms.key"));
        properties.put("db.url", System.getProperty("db.url"));

        properties.put("catalina.base", "."); // used in log4j2.xml. Logs will go to target/jetty/logs
        container.setSystemProperties(properties);

        container.start();
        port = Integer.parseInt(container.getConfiguration().getPropertyValue(ServletPropertySet.PORT));
        System.out.println("Jetty listening on port " + port);
    }

    /**
     * Verifies a booking can be submitted by a user connecting the specified address,
     * and that the resulting appointment has its createdBy set to the user.
     *
     * @param booking  the booking
     * @param user     the user
     * @param password the user's password
     * @param address  the address the user is connecting from
     */
    private void checkSubmitBooking(Booking booking, User user, String password, String address) {
        ClientConfig config = new ClientConfig()
                .register(JacksonFeature.class)
                .register(HttpAuthenticationFeature.basic(user.getUsername(), password))
                .register(new ErrorResponseFilter());
        Client client = ClientBuilder.newClient(config);

        // submit the booking
        WebTarget target = client.target("http://localhost:" + port + "/openvpms/ws/booking/v2/bookings");
        String response = target.request()
                .header("X-Forwarded-For", address)  // simulate connecting from the address
                .post(javax.ws.rs.client.Entity.entity(booking, APPLICATION_JSON_TYPE), String.class);
        assertNotNull(response);

        // verify the created appointment has been created by the user
        String[] tokens = response.split(":"); // id is encoded before the :
        assertEquals(2, tokens.length);
        long id = NumberUtils.toLong(tokens[0], -1);
        Act act = getArchetypeService().get(ScheduleArchetypes.APPOINTMENT, id, Act.class);
        assertNotNull(act);
        assertEquals(user.getObjectReference(), act.getCreatedBy());
    }

    /**
     * Verifies that submitting a booking fails with a {@link NotAuthorizedException}.
     *
     * @param booking  the booking
     * @param user     the user
     * @param password the user's password
     * @param address  the address the user is connecting from
     */
    private void checkSubmitBookingFails(Booking booking, User user, String password, String address) {
        try {
            checkSubmitBooking(booking, user, password, address);
            fail("Expected checkSubmitBooking() to fail");
        } catch (NotAuthorizedException expected) {
            assertEquals("HTTP 401 Unauthorized", expected.getMessage());
        }
    }

    /**
     * Creates a booking for tomorrow.
     *
     * @param startHour the hour the booking starts
     * @return a new booking
     */
    private Booking createBooking(int startHour) {
        Party location = practiceFactory.newLocation()
                .onlineBooking()
                .build();
        Entity appointmentType = schedulingFactory.newAppointmentType()
                .onlineBooking()
                .build();
        Entity schedule = schedulingFactory.newSchedule()
                .location(location)
                .appointmentTypes(appointmentType)
                .onlineBooking()
                .build();

        Booking booking = new Booking();
        booking.setLocation(location.getId());
        booking.setSchedule(schedule.getId());
        booking.setUser(userFactory.createClinician().getId());
        booking.setAppointmentType(appointmentType.getId());
        Date date = DateRules.getTomorrow();
        booking.setStart(DateRules.getDate(date, startHour, DateUnits.HOURS));
        booking.setEnd(DateRules.getDate(date, startHour + 1, DateUnits.HOURS));
        booking.setTitle("Ms");
        booking.setFirstName("Foo");
        booking.setLastName("Bar");
        booking.setEmail("foo@bar.com");
        booking.setMobile("0412345678");
        booking.setPatientName("Fido");
        return booking;
    }

    /**
     * Returns the absolute path for a relative one.
     *
     * @param path the path
     * @return the absolute path
     */
    private String getPath(String path) {
        return new File(path).getAbsolutePath();
    }

    /**
     * Installs plugins.
     *
     * @throws Exception for any error
     */
    private void installPlugins() throws Exception {
        // remove existing plugins
        Iterator<Plugin> plugins = pluginDAO.getPlugins();
        while (plugins.hasNext()) {
            pluginDAO.remove(plugins.next().getKey());
        }
        installPlugin("org.openvpms.openvpms-test-rest-plugin", "openvpms-test-rest-plugin.jar");
        installPlugin("org.openvpms.openvpms-test-servlet-plugin", "openvpms-test-servlet-plugin.jar");
    }

    /**
     * Installs a plugin.
     *
     * @param key  the plugin key
     * @param name the plugin name
     * @throws Exception for any error
     */
    private void installPlugin(String key, String name) throws Exception {
        String path = "target/" + name;
        try (FileInputStream file = new FileInputStream(path)) {
            pluginDAO.save(key, name, file);
        }
    }
}