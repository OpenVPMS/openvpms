/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.test;

import org.openvpms.domain.practice.Location;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;
import org.springframework.beans.factory.DisposableBean;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the {@link DocumentPrinterService} for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDocumentPrinterService implements DocumentPrinterService, DisposableBean {

    /**
     * The printers.
     */
    private final Map<String, DocumentPrinter> printers = new LinkedHashMap<>();

    /**
     * The default printer.
     */
    private DocumentPrinter defaultPrinter;

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return "Test Document Printer Service";
    }

    /**
     * Returns the default printer.
     *
     * @return the default printer, or {@code null} if there is no default printer
     */
    @Override
    public synchronized DocumentPrinter getDefaultPrinter() {
        return defaultPrinter;
    }

    /**
     * Sets the default printer.
     *
     * @param defaultPrinter the default printer. May be {@code null}
     */
    public synchronized void setDefaultPrinter(DocumentPrinter defaultPrinter) {
        this.defaultPrinter = defaultPrinter;
    }

    /**
     * Returns the default printer for a practice location.
     *
     * @param location the practice location
     * @return the default printer, or {@code null} if there is no default printer
     */
    @Override
    public DocumentPrinter getDefaultPrinter(Location location) {
        return getDefaultPrinter();
    }

    /**
     * Returns a printer by identifier.
     *
     * @param id the printer identifier
     * @return the corresponding printer, or {@code null} if none is found
     */
    @Override
    public synchronized DocumentPrinter getPrinter(String id) {
        return printers.get(id);
    }

    /**
     * Returns the available printers at a practice location.
     *
     * @param location the practice location
     * @return the available printers
     */
    @Override
    public List<DocumentPrinter> getPrinters(Location location) {
        return getPrinters();
    }

    /**
     * Returns the available printers.
     *
     * @return the available printers
     */
    @Override
    public synchronized List<DocumentPrinter> getPrinters() {
        return new ArrayList<>(printers.values());
    }

    /**
     * Adds a printer.
     *
     * @param printer the printer
     */
    public synchronized void addPrinter(DocumentPrinter printer) {
        printers.put(printer.getId(), printer);
    }

    /**
     * Resets the service.
     * <p/>
     * This removes all printers.
     */
    public synchronized void reset() {
        printers.clear();
        defaultPrinter = null;
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     */
    @Override
    public void destroy() {
        reset();
    }

    /**
     * Returns the printer service archetype that this supports.
     *
     * @return an <em>entity.printerService*</em> archetype, or {@code null} for the default printer service
     * implementation
     */
    @Override
    public String getArchetype() {
        return null;
    }
}