/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.service;

import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.MediaTray;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.Sides;

/**
 * Print attributes.
 *
 * @author Tim Anderson
 */
public class PrintAttributes {

    /**
     * The media size name.
     */
    private MediaSizeName mediaSize;

    /**
     * The orientation.
     */
    private OrientationRequested orientation;

    /**
     * The media tray.
     */
    private MediaTray mediaTray;

    /**
     * The page sides to print on.
     */
    private Sides sides;

    /**
     * The no. of copies.
     */
    private int copies = 1;

    /**
     * Sets the media size.
     *
     * @param size the media size
     */
    public void setMediaSize(MediaSizeName size) {
        mediaSize = size;
    }

    /**
     * Returns the media size.
     *
     * @return the media size. May be {@code null}
     */
    public MediaSizeName getMediaSize() {
        return mediaSize;
    }

    /**
     * Sets the orientation.
     *
     * @param orientation the orientation
     */
    public void setOrientation(OrientationRequested orientation) {
        this.orientation = orientation;
    }

    /**
     * Returns the orientation.
     *
     * @return the orientation. May be {@code null}
     */
    public OrientationRequested getOrientation() {
        return orientation;
    }

    /**
     * Sets the media tray.
     *
     * @param tray the tray
     */
    public void setMediaTray(MediaTray tray) {
        mediaTray = tray;
    }

    /**
     * Returns the media tray.
     *
     * @return the media tray. May be {@code null}
     */
    public MediaTray getMediaTray() {
        return mediaTray;
    }

    /**
     * Sets the no. of copies to print.
     *
     * @param copies the no. of copies. If &lt; 1, the value is ignored
     */
    public void setCopies(int copies) {
        this.copies = copies;
    }

    /**
     * Returns the no. of copies to print.
     *
     * @return the no. of copies. Always &gt;= 1
     */
    public int getCopies() {
        return Math.max(copies, 1);
    }

    /**
     * Sets the paper sides to print on.
     *
     * @param sides the sides. May be {@code null}
     */
    public void setSides(Sides sides) {
        this.sides = sides;
    }

    /**
     * Returns the paper sides to print on.
     *
     * @return the sides. May be {@code null}
     */
    public Sides getSides() {
        return sides;
    }
}
