/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.locator;

import org.openvpms.domain.practice.Location;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;

import java.util.List;

/**
 * Locates printers across multiple {@link DocumentPrinterService}s.
 *
 * @author Tim Anderson
 */
public interface DocumentPrinterServiceLocator {

    /**
     * Locates a printer.
     *
     * @param archetype the printer service archetype or {@code null} to indicate the
     *                  {@link #getJavaPrintService Java Print Service API backed service}
     * @param id        the printer identifier
     * @return the corresponding printer
     */
    DocumentPrinter getPrinter(String archetype, String id);

    /**
     * Returns the default printer.
     *
     * @return the default printer, or {@code null} if there is no default printer
     */
    DocumentPrinter getDefaultPrinter();

    /**
     * Returns the default printer for a practice location.
     *
     * @param location the practice location
     * @return the default printer, or {@code null} if there is no default printer
     */
    DocumentPrinter getDefaultPrinter(Location location);

    /**
     * Returns the printers available at a practice location.
     *
     * @param location the practice location
     * @return the printers
     */
    List<DocumentPrinter> getPrinters(Location location);

    /**
     * Returns all available printers.
     *
     * @return the printers
     */
    List<DocumentPrinter> getPrinters();

    /**
     * Returns the available printer services.
     *
     * @return the printer services
     */
    List<DocumentPrinterService> getServices();

    /**
     * Returns the Java Print Service API backed service.
     *
     * @return the service implemented using the Java Print Service API.
     */
    DocumentPrinterService getJavaPrintService();

}
