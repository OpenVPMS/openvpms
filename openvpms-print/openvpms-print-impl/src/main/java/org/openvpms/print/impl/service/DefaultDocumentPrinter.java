/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.impl.service;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.print.service.DocumentPrinter;

import javax.print.PrintService;

/**
 * Default implementation of {@link DocumentPrinter} implemented using the Java Print API.
 * <p/>
 * On top of the mime types supported by the target printer, this supports PDF documents using PDFBox.
 *
 * @author Tim Anderson
 */
class DefaultDocumentPrinter extends AbstractPrintServiceDocumentPrinter {

    /**
     * Constructs a {@link DefaultDocumentPrinter}.
     *
     * @param printService the print service
     * @param handlers     the document handlers
     */
    public DefaultDocumentPrinter(PrintService printService, DocumentHandlers handlers) {
        super(printService, handlers);
    }

}
