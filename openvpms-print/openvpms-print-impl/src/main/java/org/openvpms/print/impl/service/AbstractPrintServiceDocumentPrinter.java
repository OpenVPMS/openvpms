/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.impl.service;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.model.document.Document;
import org.openvpms.domain.internal.document.CompressedDocumentImpl;
import org.openvpms.print.exception.PrinterException;
import org.openvpms.print.impl.exception.PrintNotSupported;
import org.openvpms.print.impl.i18n.PrintMessages;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;
import org.openvpms.print.service.PrintAttributes;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import java.awt.print.PrinterJob;
import java.io.InputStream;

/**
 * A {@link PrintService}-backed implementation of {@link DocumentPrinter}.
 *
 * @author Tim Anderson
 */
public class AbstractPrintServiceDocumentPrinter implements DocumentPrinter {

    /**
     * The underlying print service.
     */
    private final PrintService printService;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The PDF mime type.
     */
    private static final String PDF = "application/pdf";

    /**
     * Constructs an {@link AbstractPrintServiceDocumentPrinter}.
     *
     * @param printService the print service
     * @param handlers     the document handlers
     */
    public AbstractPrintServiceDocumentPrinter(PrintService printService, DocumentHandlers handlers) {
        this.printService = printService;
        this.handlers = handlers;
    }

    /**
     * Returns the printer identifier.
     *
     * @return the printer identifier
     */
    public String getId() {
        return printService.getName();
    }

    /**
     * Returns the printer name.
     *
     * @return the printer name
     */
    @Override
    public String getName() {
        return printService.getName();
    }

    /**
     * Returns the archetype of the {@link DocumentPrinterService} that provides this printer.
     *
     * @return the printer service archetype, or {@code null} if the Java Print Service API provides the printer
     */
    public String getArchetype() {
        return null;
    }

    /**
     * Determines if the printer can be printed to using the Java Print Service API.
     * <p/>
     * When supported, the {@link #getId() identifier} can be used to look up the corresponding {@link PrintService}
     * by name.
     *
     * @return {@code true} if the printer supports the Java Print Service API, otherwise {@code false}
     */
    public boolean canUseJavaPrintServiceAPI() {
        return true;
    }

    /**
     * Determines if the printer can print the specified mime type.
     *
     * @param mimeType the mime type
     * @return {@code true} if the printer can print the specified mime type
     */
    public boolean canPrint(String mimeType) {
        if (supports(mimeType)) {
            return true;
        }
        return PDF.equalsIgnoreCase(mimeType);
    }

    /**
     * Prints a document.
     *
     * @param document   the document to print
     * @param attributes the print attributes
     * @throws PrinterException for any printer error
     */
    public void print(Document document, PrintAttributes attributes) {
        document = getDecompressedDocument(document);
        if (supports(document.getMimeType())) {
            printDocument(document, attributes);
        } else if (PDF.equalsIgnoreCase(document.getMimeType())) {
            printPDF(document, attributes);
        } else {
            throw new PrintNotSupported(PrintMessages.unsupportedDocument(document.getName(), printService.getName()));
        }
    }

    /**
     * Returns a proxy for a document, if it needs decompressing.
     *
     * @param document the document
     * @return the document proxy, if it needs decompressing, otherwise {@code document}
     */
    protected Document getDecompressedDocument(Document document) {
        if (!(document instanceof CompressedDocumentImpl)) {
            document = new CompressedDocumentImpl(document, handlers);
        }
        return document;
    }

    /**
     * Prints a document.
     *
     * @param document   the document to print
     * @param attributes the print attributes
     * @throws PrinterException for any printer error
     */
    private void printDocument(Document document, PrintAttributes attributes) {
        try {
            DocFlavor flavor = new DocFlavor.INPUT_STREAM(document.getMimeType());
            Doc doc = new SimpleDoc(document.getContent(), flavor, null);
            DocPrintJob job = printService.createPrintJob();
            job.print(doc, createAttributes(attributes));
        } catch (Throwable exception) {
            throw new PrinterException(PrintMessages.failedToPrintDocument(document.getName(), printService.getName(),
                                                                           exception.getMessage()), exception);
        }
    }

    /**
     * Prints a PDF using PDFBox.
     *
     * @param document   the PDF document
     * @param attributes the print attributes
     * @throws PrinterException for any error
     */
    private void printPDF(Document document, PrintAttributes attributes) {
        try {
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPrintService(printService);
            try (PDDocument pdf = PDDocument.load(document.getContent())) {
                job.setPageable(new PDFPageable(pdf));
                job.print(createAttributes(attributes));
            }
        } catch (Exception exception) {
            throw new PrinterException(PrintMessages.failedToPrintDocument(document.getName(), printService.getName(),
                                                                           exception.getMessage()),
                                       exception);
        }
    }

    /**
     * Converts a {@link PrintAttributes} to a {@code PrintRequestAttributeSet}.
     *
     * @param attributes the attributes to convert
     * @return the converted attributes
     */
    private PrintRequestAttributeSet createAttributes(PrintAttributes attributes) {
        PrintRequestAttributeSet set = new HashPrintRequestAttributeSet();
        if (attributes.getMediaSize() != null) {
            set.add(attributes.getMediaSize());
        }
        if (attributes.getOrientation() != null) {
            set.add(attributes.getOrientation());
        }
        if (attributes.getMediaTray() != null) {
            set.add(attributes.getMediaTray());
        }
        set.add(new Copies(attributes.getCopies()));
        if (attributes.getSides() != null) {
            set.add(attributes.getSides());
        }
        return set;
    }

    /**
     * Determines if a mime type is supported by the print service.
     *
     * @param mimeType the mime type
     * @return {@code true} if the mime type is supported, otherwise {@code false}
     */
    private boolean supports(String mimeType) {
        return printService.isDocFlavorSupported(new DocFlavor(mimeType, InputStream.class.getName()));
    }
}