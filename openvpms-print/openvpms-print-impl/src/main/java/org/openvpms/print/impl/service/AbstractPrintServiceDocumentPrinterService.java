/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.impl.service;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.domain.practice.Location;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.PrinterName;
import java.util.ArrayList;
import java.util.List;

/**
 * A {@link PrintService}-backed implementation of {@link DocumentPrinterService}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPrintServiceDocumentPrinterService implements DocumentPrinterService {

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * Constructs an {@link AbstractPrintServiceDocumentPrinterService}.
     *
     * @param handlers the document handlers
     */
    protected AbstractPrintServiceDocumentPrinterService(DocumentHandlers handlers) {
        this.handlers = handlers;
    }

    /**
     * Returns the default printer.
     *
     * @return the default printer, or {@code null} if there is no default printer
     */
    public DocumentPrinter getDefaultPrinter() {
        javax.print.PrintService result = PrintServiceLookup.lookupDefaultPrintService();
        return (result != null) ? createDocumentPrinter(result, handlers) : null;
    }

    /**
     * Returns the default printer for a practice location.
     *
     * @param location the practice location
     * @return the default printer, or {@code null} if there is no default printer
     */
    public DocumentPrinter getDefaultPrinter(Location location) {
        return getDefaultPrinter();
    }

    /**
     * Returns a printer by identifier.
     * <p>
     * Returns a printer by identifier.
     *
     * @return the corresponding printer, or {@code null} if none is found
     */
    public DocumentPrinter getPrinter(String id) {
        AttributeSet set = new HashAttributeSet();
        set.add(new PrinterName(id, null));
        javax.print.PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, set);
        return (printServices.length > 0) ? createDocumentPrinter(printServices[0], handlers) : null;
    }

    /**
     * Returns the available printers.
     *
     * @return the available printers
     */
    public List<DocumentPrinter> getPrinters() {
        List<DocumentPrinter> result = new ArrayList<>();
        for (javax.print.PrintService printer : PrintServiceLookup.lookupPrintServices(null, null)) {
            result.add(createDocumentPrinter(printer, handlers));
        }
        return result;
    }

    /**
     * Returns the available printers at a practice location.
     *
     * @param location the practice location
     * @return the available printers
     */
    public List<DocumentPrinter> getPrinters(Location location) {
        return getPrinters();
    }

    /**
     * Returns the printer service archetype that this supports.
     *
     * @return an <em>entity.printerService*</em> archetype, or {@code null} for the default printer service
     * implementation
     */
    public String getArchetype() {
        return null;
    }

    /**
     * Creates a {@link DocumentPrinter} backed by a {@link PrintService}.
     *
     * @param service  the print service
     * @param handlers the document handlers
     * @return a new {@link DocumentPrinter}
     */
    protected abstract DocumentPrinter createDocumentPrinter(PrintService service, DocumentHandlers handlers);
}