/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.impl.locator;

import org.openvpms.domain.practice.Location;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link DocumentPrinterServiceLocator}.
 *
 * @author Tim Anderson
 */
public class DefaultDocumentPrinterServiceLocator implements DocumentPrinterServiceLocator {

    /**
     * The default service.
     */
    private final DocumentPrinterService defaultService;

    /**
     * The plugin manager.
     */
    private final PluginManager manager;

    /**
     * Constructs a {@link DefaultDocumentPrinterServiceLocator}.
     * <p/>
     * This only provides the supplied print service.
     *
     * @param printerService the default printer service
     */
    public DefaultDocumentPrinterServiceLocator(DocumentPrinterService printerService) {
        this(printerService, null);
    }

    /**
     * Constructs a {@link DefaultDocumentPrinterServiceLocator}.
     *
     * @param printerService the default printer service
     * @param manager        the plugin manager
     */
    public DefaultDocumentPrinterServiceLocator(DocumentPrinterService printerService, PluginManager manager) {
        this.defaultService = printerService;
        this.manager = manager;
    }

    /**
     * Locates a printer.
     *
     * @param archetype the printer service archetype or {@code null} to indicate the
     *                  {@link #getJavaPrintService Java Print Service API backed service}
     * @param id        the printer identifier
     * @return the corresponding printer
     */
    @Override
    public DocumentPrinter getPrinter(String archetype, String id) {
        if (archetype == null) {
            return defaultService.getPrinter(id);
        }
        for (DocumentPrinterService service : getServices()) {
            if (archetype.equals(service.getArchetype())) {
                return service.getPrinter(id);
            }
        }
        return null;
    }

    /**
     * Returns the default printer.
     *
     * @return the default printer, or {@code null} if there is no default printer
     */
    @Override
    public DocumentPrinter getDefaultPrinter() {
        for (DocumentPrinterService service : getServices()) {
            DocumentPrinter printer = service.getDefaultPrinter();
            if (printer != null) {
                return printer;
            }
        }
        return null;
    }

    /**
     * Returns the default printer for a practice location.
     *
     * @param location the practice location
     * @return the default printer, or {@code null} if there is no default printer
     */
    @Override
    public DocumentPrinter getDefaultPrinter(Location location) {
        for (DocumentPrinterService service : getServices()) {
            DocumentPrinter printer = service.getDefaultPrinter(location);
            if (printer != null) {
                return printer;
            }
        }
        return null;
    }

    /**
     * Returns the printers available at a practice location.
     *
     * @param location the practice location
     * @return the printers
     */
    @Override
    public List<DocumentPrinter> getPrinters(Location location) {
        List<DocumentPrinter> result = new ArrayList<>();
        for (DocumentPrinterService service : getServices()) {
            result.addAll(service.getPrinters(location));
        }
        return result;
    }

    /**
     * Returns all available printers.
     *
     * @return the printers
     */
    @Override
    public List<DocumentPrinter> getPrinters() {
        List<DocumentPrinter> result = new ArrayList<>();
        for (DocumentPrinterService service : getServices()) {
            result.addAll(service.getPrinters());
        }
        return result;
    }

    /**
     * Returns the available printer services.
     *
     * @return the printer services
     */
    @Override
    public List<DocumentPrinterService> getServices() {
        List<DocumentPrinterService> result = new ArrayList<>();
        result.add(defaultService);
        if (manager != null) {
            result.addAll(manager.getServices(DocumentPrinterService.class));
        }
        return result;
    }

    /**
     * Returns the Java Print Service API backed service.
     *
     * @return the service implemented using the Java Print Service API.
     */
    @Override
    public DocumentPrinterService getJavaPrintService() {
        return defaultService;
    }
}
