/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.impl.service;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;

import javax.print.PrintService;

/**
 * Default implementation of {@link DocumentPrinterService}.
 * <p/>
 * This is implemented using the Java Print Service API.
 *
 * @author Tim Anderson
 */
public class DefaultDocumentPrinterService extends AbstractPrintServiceDocumentPrinterService {

    /**
     * Constructs a {@link DefaultDocumentPrinterService}.
     *
     * @param handlers the document handlers
     */
    public DefaultDocumentPrinterService(DocumentHandlers handlers) {
        super(handlers);
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return "Default Printer Service";
    }

    /**
     * Creates a {@link DocumentPrinter} backed by a {@link PrintService}.
     *
     * @param service  the print service
     * @param handlers the document handlers
     * @return a new {@link DocumentPrinter}
     */
    @Override
    protected DocumentPrinter createDocumentPrinter(PrintService service, DocumentHandlers handlers) {
        return new DefaultDocumentPrinter(service, handlers);
    }
}
