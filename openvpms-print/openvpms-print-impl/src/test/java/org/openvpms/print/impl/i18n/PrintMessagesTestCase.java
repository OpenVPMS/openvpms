/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.impl.i18n;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link PrintMessages} class.
 *
 * @author Tim Anderson
 */
public class PrintMessagesTestCase {

    /**
     * Tests the {@link PrintMessages#failedToPrintDocument(String, String, String)} method.
     */
    @Test
    public void testFailedToPrintDocument() {
        assertEquals("Failed to print a.pdf to printer: foo",
                     PrintMessages.failedToPrintDocument("a.pdf", "printer", "foo").getMessage());
    }

    /**
     * Tests the {@link PrintMessages#unsupportedDocument(String, String)} method.
     */
    @Test
    public void testUnsupportedDocument() {
        assertEquals("a.doc cannot be printed by printer",
                     PrintMessages.unsupportedDocument("a.doc", "printer").getMessage());
    }

    /**
     * Tests the {@link PrintMessages#noPrinter()} method.
     */
    @Test
    public void testNoPrinter() {
        assertEquals("Cannot print. No printer specified and no default printer available",
                     PrintMessages.noPrinter().getMessage());
    }
}
