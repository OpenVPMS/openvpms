#
# OVPMS-2948 Add support to print investigation results when no document is attached
#

#
# Add documentTemplateType lookup for act.patientInvestigation
#
INSERT INTO lookups (version, linkId, arch_short_name, active, arch_version, code, name, description, default_lookup)
SELECT 0,
       UUID(),
       'lookup.documentTemplateType',
       TRUE,
       '1.0',
       'act.patientInvestigation',
       'Investigation',
       NULL,
       FALSE
FROM dual
WHERE NOT EXISTS(SELECT *
                 FROM lookups
                 WHERE arch_short_name = 'lookup.documentTemplateType'
                   AND code = 'act.patientInvestigation');