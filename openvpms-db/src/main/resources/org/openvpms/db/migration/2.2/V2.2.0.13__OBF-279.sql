#
# OBF-279 Add support to deploy plugins from the database
#

CREATE TABLE IF NOT EXISTS `plugins`
(
    `id`              bigint(20)   NOT NULL AUTO_INCREMENT,
    `version`         bigint(20)   NOT NULL,
    `linkId`          varchar(36)  NOT NULL,
    `created`         datetime     DEFAULT NULL,
    `created_id`      bigint(20)   DEFAULT NULL,
    `updated`         datetime     DEFAULT NULL,
    `updated_id`      bigint(20)   DEFAULT NULL,
    `arch_short_name` varchar(100) NOT NULL,
    `arch_version`    varchar(100) NOT NULL,
    `plugin_key`      varchar(255) NOT NULL,
    `name`            varchar(255) NOT NULL,
    `description`     varchar(255) DEFAULT NULL,
    `active`          bit(1)       DEFAULT NULL,
    `data`            longblob     NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `plugin_key` (`plugin_key`),
    UNIQUE KEY `name` (`name`),
    KEY `FKE3A677A0B96D33A7` (`created_id`),
    KEY `FKE3A677A0562D1CF4` (`updated_id`),
    CONSTRAINT `FKE3A677A0562D1CF4` FOREIGN KEY (`updated_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `FKE3A677A0B96D33A7` FOREIGN KEY (`created_id`) REFERENCES `users` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#
# Remove the existing plugin configuration.
#
DELETE d
FROM entities e
         JOIN entity_details d
              ON d.entity_id = e.entity_id
WHERE e.arch_short_name = 'entity.pluginConfiguration';

DELETE e
FROM entities e
WHERE arch_short_name = 'entity.pluginConfiguration';

#
# Enable plugins by default in the practice.
#
INSERT INTO entity_details (entity_id, type, value, name)
SELECT e.entity_id,
       'boolean',
       'true',
       'enablePlugins'
FROM entities e
WHERE e.arch_short_name = 'party.organisationPractice'
  AND NOT EXISTS(SELECT *
                 FROM entity_details d
                 WHERE d.entity_id = e.entity_id
                   AND d.name = 'enablePlugins');
