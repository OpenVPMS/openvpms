#
# OVPMS-2505 Move act.supplierOrder deliveryStatus node to status2 to improve query performance
#

UPDATE acts supplier_order
    JOIN act_details delivery_status
    ON supplier_order.act_id = delivery_status.act_id
        AND delivery_status.name = 'deliveryStatus'
SET supplier_order.status2 = delivery_status.value
WHERE supplier_order.arch_short_name = 'act.supplierOrder';

DELETE delivery_status
FROM acts supplier_order
         JOIN act_details delivery_status
              ON supplier_order.act_id = delivery_status.act_id
WHERE supplier_order.arch_short_name = 'act.supplierOrder'
  AND delivery_status.name = 'deliveryStatus';