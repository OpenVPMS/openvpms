#
# OVPMS-2716 Improve Reporting - Reminders query performance
#

DROP PROCEDURE IF EXISTS OVPMS_2716_add_index;

DELIMITER  $$

CREATE PROCEDURE OVPMS_2716_add_index()
BEGIN
    IF NOT EXISTS(
            SELECT 1
            FROM information_schema.STATISTICS
            WHERE table_schema = DATABASE()
              AND table_name = 'entity_relationships'
              AND index_name = 'entity_relationship_arch_sn_target_idx'
        )
    THEN
        ALTER TABLE entity_relationships
            ADD KEY entity_relationship_arch_sn_target_idx (arch_short_name, target_id);
    END IF;
END $$

DELIMITER ;

CALL OVPMS_2716_add_index();

DROP PROCEDURE OVPMS_2716_add_index;