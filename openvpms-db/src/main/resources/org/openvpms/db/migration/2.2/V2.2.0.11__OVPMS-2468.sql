#
# OVPMS-2468 Increase length of appointment & task notes
#

INSERT INTO act_details (act_id, name, type, value)
SELECT a.act_id,
       'notes',
       'string',
       a.description
FROM acts a
WHERE a.arch_short_name IN ('act.customerAppointment', 'act.customerTask', 'act.calendarBlock', 'act.calendarEvent',
                            'act.rosterEvent')
  AND a.description IS NOT NULL
  AND NOT EXISTS(SELECT *
                 FROM act_details d
                 WHERE d.act_id = a.act_id
                   AND d.name = 'notes');

UPDATE acts a
SET a.description = NULL
WHERE a.arch_short_name IN ('act.customerAppointment', 'act.customerTask', 'act.calendarBlock', 'act.calendarEvent',
                            'act.rosterEvent');
