#
# OVPMS-2757 Laboratory Order authorities missing from Base Role for sites installed post 2.2
#

INSERT INTO roles_authorities (security_role_id, authority_id)
SELECT r.security_role_id,
       g.granted_authority_id
FROM security_roles r
         JOIN granted_authorities g
WHERE r.name = 'Base Role'
  AND g.archetype IN ('act.laboratoryOrder')
  AND g.method IN ('create', 'save', 'remove')
  AND NOT exists(SELECT *
                 FROM roles_authorities x
                 WHERE x.security_role_id = r.security_role_id
                   AND x.authority_id = g.granted_authority_id);
