#
# OVPMS-2432 Add migration for showCommunications preference
#

INSERT INTO entity_details (entity_id, type, value, name)
SELECT
    e.entity_id,
    'boolean',
    'false',
    'showCommunications'
FROM entities e
WHERE e.arch_short_name = 'entity.preferenceGroupHistory'
  AND NOT exists(SELECT *
                 FROM entity_details d
                 WHERE d.entity_id = e.entity_id
                   AND d.name = 'showCommunications');
