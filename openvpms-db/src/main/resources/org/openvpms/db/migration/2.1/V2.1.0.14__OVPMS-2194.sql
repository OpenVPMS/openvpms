#
# OVPMS-2194 2-way SMS messaging
#

#
# Rename act.customerCommunicationSMS to act.smsMessage.
#
UPDATE acts sms
    JOIN participations p
    ON p.act_id = sms.act_id
SET sms.arch_short_name   = 'act.smsMessage',
    sms.activity_end_time = sms.activity_start_time,
    sms.status            = 'SENT',
    sms.status2           = 'COMPLETED',
    p.act_arch_short_name = 'act.smsMessage'
WHERE sms.arch_short_name = 'act.customerCommunicationSMS';

#
# Delete redundant document_acts rows.
#
DELETE da
FROM document_acts da
         JOIN acts sms
              ON da.document_act_id = sms.act_id
WHERE sms.arch_short_name = 'act.smsMessage';

#
# Copy participation.customer to participation.smsRecipient.
#
INSERT INTO participations (version, linkId, arch_short_name, arch_version, active, act_arch_short_name, entity_id,
                            act_id, activity_start_time, activity_end_time)
SELECT 1,
       pcustomer.linkId,
       'participation.smsRecipient',
       '1.0',
       1,
       'act.smsMessage',
       pcustomer.entity_id,
       pcustomer.act_id,
       pcustomer.activity_start_time,
       pcustomer.activity_end_time
FROM acts sms
         JOIN participations pcustomer
              ON pcustomer.act_id = sms.act_id
                  AND pcustomer.arch_short_name = 'participation.customer'
WHERE sms.arch_short_name = 'act.smsMessage'
  AND NOT exists(SELECT *
                 FROM participations psmsparty
                 WHERE psmsparty.act_id = sms.act_id
                   AND psmsparty.arch_short_name = 'participation.smsRecipient');


#
# Rename address to phone.
#
UPDATE acts sms
    JOIN act_details address
    ON sms.act_id = address.act_id
SET address.name = 'phone'
WHERE sms.arch_short_name = 'act.smsMessage'
  AND address.name = 'address';

