#
# OVPMS-2933 Reversing a Delivery clears the credit flag on the Return
#

UPDATE financial_acts f
    JOIN acts a
        ON a.act_id = f.financial_act_id
SET f.credit = TRUE
WHERE a.arch_short_name in ('act.supplierReturnItem', 'act.supplierReturn')
  AND f.credit = FALSE;

UPDATE financial_acts f
    JOIN acts a
        ON a.act_id = f.financial_act_id
SET f.credit = FALSE
WHERE a.arch_short_name in ('act.supplierDeliveryItem', 'act.supplierDelivery')
  AND f.credit = TRUE;
