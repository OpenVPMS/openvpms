#
# OVPMS-2553 Add support for departments
#

#
# Add default departments
#
INSERT INTO entities (version, linkId, arch_short_name, arch_version, name, description, active)
SELECT 1, UUID(), 'entity.department', '1.0', 'Inpatient', 'Inpatient Department', 1
FROM dual
WHERE NOT EXISTS(
        SELECT *
        FROM entities
        WHERE arch_short_name = 'entity.department'
          AND name = 'Inpatient');

INSERT INTO entities (version, linkId, arch_short_name, arch_version, name, description, active)
SELECT 1, UUID(), 'entity.department', '1.0', 'Outpatient', 'Outpatient Department', 1
FROM dual
WHERE NOT EXISTS(
        SELECT *
        FROM entities
        WHERE arch_short_name = 'entity.department'
          AND name = 'Outpatient');
