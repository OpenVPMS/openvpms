#
# OVPMS-2618 Force password change
#


#
# Add change_password column to users table
#
DROP PROCEDURE IF EXISTS sp_add_users_change_password;

DELIMITER $$

CREATE PROCEDURE sp_add_users_change_password()
BEGIN
    DECLARE colName TEXT;
    SELECT column_name
    INTO colName
    FROM information_schema.columns
    WHERE table_schema = DATABASE()
      AND table_name = 'users'
      AND column_name = 'change_password';

    IF colName IS NULL THEN
        ALTER TABLE users
            ADD COLUMN `change_password` bit(1) DEFAULT NULL AFTER `password`;
    END IF;
END$$

DELIMITER ;

CALL sp_add_users_change_password;

DROP PROCEDURE sp_add_users_change_password;

#
# Force administrators to update passwords on next login.
#
UPDATE users u
SET u.change_password = FALSE
WHERE u.change_password IS NULL;

UPDATE users
SET change_password = 1
WHERE user_name = 'admin';

UPDATE users u
    JOIN entity_classifications ec
    ON u.user_id = ec.entity_id
    JOIN lookups l
    ON ec.lookup_id = l.lookup_id
SET u.change_password = 1
WHERE l.code = 'ADMINISTRATOR';