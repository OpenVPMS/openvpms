#
# Add STATEMENT lookup.customerCommunicationReason for OVPMS-2223 Add support to log customer statements in the
# communication log
#

INSERT INTO lookups (version, linkId, arch_short_name, active, arch_version, code, name, description, default_lookup)
SELECT 0,
       UUID(),
       'lookup.customerCommunicationReason',
       1,
       '1.0',
       'STATEMENT',
       'Statement',
       NULL,
       0
FROM dual
WHERE NOT exists(SELECT *
                 FROM lookups e
                 WHERE e.arch_short_name = 'lookup.customerCommunicationReason'
                   AND e.code = 'STATEMENT');

