#
# OVPMS-2549 Suppress medication expiry date if there is none
#

UPDATE acts medication
SET description = TRIM(TRAILING ' Expiry: None' FROM description)
WHERE medication.arch_short_name = 'act.patientMedication'
  AND medication.activity_end_time IS NULL
  AND medication.description LIKE '%Expiry: None';
