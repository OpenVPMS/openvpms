#
# OVPMS-1895 EFTPOS API
#

#
# Add documentTemplateType lookup for act.EFTPOSReceiptCustomer.
#
INSERT INTO lookups (version, linkId, arch_short_name, active, arch_version, code, name, description, default_lookup)
SELECT 0,
       UUID(),
       'lookup.documentTemplateType',
       TRUE,
       '1.0',
       'act.EFTPOSReceiptCustomer',
       'EFTPOS Receipt',
       NULL,
       FALSE
FROM dual
WHERE NOT EXISTS(SELECT *
                 FROM lookups
                 WHERE arch_short_name = 'lookup.documentTemplateType'
                   AND code = 'act.EFTPOSReceiptCustomer');

#
# Add authorities.
#
DROP TABLE IF EXISTS new_authorities;
CREATE TEMPORARY TABLE new_authorities
(
    name        VARCHAR(255) PRIMARY KEY,
    description VARCHAR(255),
    method      VARCHAR(255),
    archetype   VARCHAR(255)
);

INSERT INTO new_authorities (name, description, method, archetype)
VALUES ('EFTPOS Act Create', 'Authority to Create EFTPOS Act', 'create', 'act.EFTPOS*'),
       ('EFTPOS Act Save', 'Authority to Save EFTPOS Act', 'save', 'act.EFTPOS*'),
       ('EFTPOS Act Remove', 'Authority to Remove EFTPOS Act', 'remove', 'act.EFTPOS*');

INSERT INTO granted_authorities (version, linkId, arch_short_name, arch_version, name, description, active,
                                 service_name, method, archetype)
SELECT 0,
       UUID(),
       'security.archetypeAuthority',
       '1.0',
       a.name,
       a.description,
       1,
       'archetypeService',
       a.method,
       a.archetype
FROM new_authorities a
WHERE NOT EXISTS(
        SELECT *
        FROM granted_authorities g
        WHERE g.method = a.method
          AND g.archetype = a.archetype);

#
# Add authorities to Base Role. Note that the remove role isn't added.
#
INSERT INTO roles_authorities (security_role_id, authority_id)
SELECT r.security_role_id,
       g.granted_authority_id
FROM security_roles r
         JOIN granted_authorities g
WHERE r.name = 'Base Role'
  AND g.archetype IN ('act.EFTPOS*')
  AND g.method IN ('create', 'save')
  AND NOT EXISTS(SELECT *
                 FROM roles_authorities x
                 WHERE x.security_role_id = r.security_role_id
                   AND x.authority_id = g.granted_authority_id);

DROP TABLE new_authorities;
