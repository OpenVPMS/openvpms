#
# OVPMS-2752 Process Yes/No responses to Appointment Reminder SMS
#

#
# Configure default values for entity.jobAppointmentReminder confirmAppointment and cancelAppointment attributes.
#
INSERT INTO entity_details (entity_id, type, value, name)
SELECT
    e.entity_id,
    'string',
    'YES',
    'confirmAppointment'
FROM entities e
WHERE e.arch_short_name = 'entity.jobAppointmentReminder'
  AND NOT exists(SELECT *
                 FROM entity_details d
                 WHERE d.entity_id = e.entity_id
                   AND d.name = 'confirmAppointment');

INSERT INTO entity_details (entity_id, type, value, name)
SELECT
    e.entity_id,
    'string',
    'NO',
    'cancelAppointment'
FROM entities e
WHERE e.arch_short_name = 'entity.jobAppointmentReminder'
  AND NOT exists(SELECT *
                 FROM entity_details d
                 WHERE d.entity_id = e.entity_id
                   AND d.name = 'cancelAppointment');
