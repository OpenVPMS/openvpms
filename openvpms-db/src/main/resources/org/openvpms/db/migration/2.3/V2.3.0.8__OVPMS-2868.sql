#
# OVPMS-2868 Add support to record customer referral.
#

#
# Create lookups if they don't exist.
#
DROP TABLE IF EXISTS OVPMS_2868;
CREATE TEMPORARY TABLE OVPMS_2868
(
    code VARCHAR(255) PRIMARY KEY,
    name VARCHAR(255)
);

INSERT INTO OVPMS_2868 (code, name)
VALUES ('CUSTOMER', 'Customer'),
       ('FACEBOOK', 'Facebook'),
       ('GOOGLE', 'Google'),
       ('INSTAGRAM', 'Instagram'),
       ('WALK_IN', 'Walk-in'),
       ('WEBSITE', 'Website'),
       ('WORD_OF_MOUTH', 'Word of mouth'),
       ('YOUTUBE', 'YouTube');

INSERT INTO lookups (version, linkId, arch_short_name, active, arch_version, code, name, default_lookup)
SELECT 0,
       UUID(),
       'lookup.customerReferral',
       1,
       '1.0',
       code,
       name,
       0
FROM OVPMS_2868 l
WHERE NOT EXISTS(SELECT *
                 FROM lookups e
                 WHERE e.arch_short_name = 'lookup.customerReferral'
                   AND e.code = l.code);

DROP TABLE IF EXISTS OVPMS_2868;

#
# Migrate existing data to ensure that referral=CUSTOMER where an entityLink.customerCustomer is record is present.
#
UPDATE entity_details referral
    JOIN entities customer
        ON referral.entity_id = customer.entity_id
    JOIN entity_links lcustomer
        ON lcustomer.source_id = customer.entity_id
SET value = 'CUSTOMER'
WHERE customer.arch_short_name = 'party.customerperson'
  AND lcustomer.arch_short_name = 'entityLink.customerCustomer'
  AND referral.name = 'referral'
  AND referral.value <> 'CUSTOMER';

INSERT INTO entity_details (entity_id, type, value, name)
SELECT customer.entity_id,
       'string',
       'CUSTOMER',
       'referral'
FROM entity_links lcustomer
         JOIN entities customer
              ON lcustomer.source_id = customer.entity_id
WHERE lcustomer.arch_short_name = 'entityLink.customerCustomer'
  AND NOT EXISTS(SELECT *
                 FROM entity_details referral
                 WHERE referral.entity_id = customer.entity_id
                   AND referral.name = 'referral');
