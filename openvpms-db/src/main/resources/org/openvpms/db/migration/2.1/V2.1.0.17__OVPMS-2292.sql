#
# OVPMS-2292 Add support for clinical notes and addenda of unlimited length
#

INSERT INTO document_acts (document_act_id, printed)
SELECT act_id, 0
FROM acts a
WHERE (a.arch_short_name = 'act.patientClinicalNote' OR a.arch_short_name = 'act.patientClinicalAddendum')
  AND NOT EXISTS(SELECT *
                 FROM document_acts d
                 WHERE d.document_act_id = a.act_id);