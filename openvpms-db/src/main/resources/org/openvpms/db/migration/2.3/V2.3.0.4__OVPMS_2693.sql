#
# OVPMS-2693 Add microgram unit-of-measure
#

INSERT INTO lookups (version, linkId, arch_short_name, active, arch_version, code, name, description, default_lookup)
SELECT 0,
       UUID(),
       'lookup.uom',
       1,
       '1.0',
       'MCG',
       'Microgram',
       NULL,
       0
FROM dual
WHERE NOT EXISTS(SELECT *
                 FROM lookups l
                 WHERE l.arch_short_name = 'lookup.uom'
                   AND l.code = 'MCG');

INSERT INTO lookup_details (lookup_id, type, value, name)
SELECT l.lookup_id,
       'string',
       'MC',
       'unitCode'
FROM lookups l
WHERE l.arch_short_name = 'lookup.uom'
  AND l.code = 'MCG'
  AND NOT EXISTS(SELECT *
                 FROM lookup_details d
                 WHERE d.lookup_id = l.lookup_id
                   AND d.name = 'unitCode');

INSERT INTO lookup_details (lookup_id, type, value, name)
SELECT l.lookup_id,
       'string',
       'microgram',
       'printedName'
FROM lookups l
WHERE l.arch_short_name = 'lookup.uom'
  AND l.code = 'MCG'
  AND NOT EXISTS(SELECT *
                 FROM lookup_details d
                 WHERE d.lookup_id = l.lookup_id
                   AND d.name = 'printedName');