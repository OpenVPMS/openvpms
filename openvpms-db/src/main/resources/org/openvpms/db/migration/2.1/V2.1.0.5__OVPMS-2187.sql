#
# OVPMS-2187 Add support for non-administrators to edit products.
#

#
# Replace entity relationships with entity links. Entity relationships require the user to have authorities to save the
# targets of the relationships, whereas entity links do not.
#
# NOTE that this removes any duplicate relationships first to avoid duplicate errors on primary keys (See OVPMS-2265 -
# prior to OpenVPMS 1.2, there was no strategy in place to prevent duplicates).
# The relationship with the highest id is removed. This is because the UI orders on increasing id by default, so the
# relationship with the lowest id is most likely to have the correct details.
#
# Relationships are considered duplicates if they have the same arch_short_name, source_id, target_id,
# active_start_time and active_end_time. This will therefore not remove relationships that are active at the same time.
# These do not cause duplicate key errors and are more complex to correct.
#
DROP PROCEDURE IF EXISTS sp_replace_entity_relationship_with_link;

DELIMITER $$
CREATE PROCEDURE sp_replace_entity_relationship_with_link(IN relationship_type VARCHAR(255), IN link_type VARCHAR(255))
BEGIN

    #
    # Remove duplicate relationships.
    #
    DELETE d
    FROM entity_relationship_details d
             JOIN entity_relationships rel1
                  ON d.entity_relationship_id = rel1.entity_relationship_id
             JOIN entity_relationships rel2
    WHERE rel1.arch_short_name = relationship_type
      AND rel1.arch_short_name = rel2.arch_short_name
      AND rel1.entity_relationship_id > rel2.entity_relationship_id
      AND rel1.source_id = rel2.source_id
      AND rel1.target_id = rel2.target_id
      AND (rel1.active_start_time = rel2.active_start_time OR
           (rel1.active_start_time IS NULL AND rel2.active_start_time IS NULL))
      AND (rel2.active_end_time = rel2.active_end_time OR
           (rel1.active_end_time IS NULL AND rel2.active_end_time IS NULL));

    DELETE rel1
    FROM entity_relationships rel1
             JOIN entity_relationships rel2
    WHERE rel1.arch_short_name = relationship_type
      AND rel1.arch_short_name = rel2.arch_short_name
      AND rel1.entity_relationship_id > rel2.entity_relationship_id
      AND rel1.source_id = rel2.source_id
      AND rel1.target_id = rel2.target_id
      AND (rel1.active_start_time = rel2.active_start_time OR
           (rel1.active_start_time IS NULL AND rel2.active_start_time IS NULL))
      AND (rel2.active_end_time = rel2.active_end_time OR
           (rel1.active_end_time IS NULL AND rel2.active_end_time IS NULL));

    #
    # Create links corresponding to the relationships.
    #
    INSERT INTO entity_links (version, linkId, arch_short_name, arch_version, name, description, active_start_time,
                              active_end_time, sequence, source_id, target_id)
    SELECT version,
           linkId,
           link_type,
           '1.0',
           name,
           description,
           active_start_time,
           active_end_time,
           sequence,
           source_id,
           target_id
    FROM entity_relationships r
    WHERE r.arch_short_name = relationship_type
      AND NOT exists(SELECT *
                     FROM entity_links l
                     WHERE l.source_id = r.source_id
                       AND l.target_id = r.target_id
                       AND (l.active_start_time = r.active_start_time OR
                            (l.active_start_time IS NULL AND l.active_start_time IS NULL))
                       AND (l.active_end_time = r.active_end_time OR
                            (l.active_end_time IS NULL AND l.active_end_time IS NULL))
                       AND l.arch_short_name = link_type);

    #
    # Copy entity_relationship_details.
    #
    INSERT INTO entity_link_details (id, name, type, value)
    SELECT l.id,
           d.name,
           d.type,
           d.value
    FROM entity_relationships r
             JOIN entity_relationship_details d
                  ON r.entity_relationship_id = d.entity_relationship_id
             JOIN entity_links l
                  ON l.arch_short_name = link_type
                      AND l.source_id = r.source_id AND l.target_id = r.target_id
                      AND (l.active_start_time = r.active_start_time
                          OR (l.active_start_time IS NULL AND l.active_start_time IS NULL))
                      AND (l.active_end_time = r.active_end_time
                          OR (l.active_end_time IS NULL AND l.active_end_time IS NULL))
    WHERE r.arch_short_name = relationship_type
      AND NOT exists(SELECT *
                     FROM entity_link_details ld
                     WHERE ld.id = l.id
                       AND ld.name = d.name);

    # Remove the old relationships
    DELETE d
    FROM entity_relationship_details d
             JOIN entity_relationships r
                  ON d.entity_relationship_id = r.entity_relationship_id
    WHERE r.arch_short_name = relationship_type;

    DELETE r
    FROM entity_relationships r
    WHERE r.arch_short_name = relationship_type;

END $$
DELIMITER ;

CALL sp_replace_entity_relationship_with_link('entityRelationship.productDocument', 'entityLink.productDocument');
CALL sp_replace_entity_relationship_with_link('entityRelationship.productEquivalent', 'entityLink.productEquivalent');
CALL sp_replace_entity_relationship_with_link('entityRelationship.productInvestigationType',
                                              'entityLink.productInvestigationType');
CALL sp_replace_entity_relationship_with_link('entityRelationship.productReminder', 'entityLink.productReminder');

DROP PROCEDURE sp_replace_entity_relationship_with_link;

#
# Rename roles that with the same names as the new roles.
#
DROP PROCEDURE IF EXISTS sp_rename_openvpms_role;
DROP PROCEDURE IF EXISTS sp_add_openvpms_role;
DROP PROCEDURE IF EXISTS sp_add_openvpms_authority;
DROP PROCEDURE IF EXISTS sp_add_openvpms_role_authority;
DROP PROCEDURE IF EXISTS sp_remove_openvpms_role_authority;

#
# Renames a role.
#
DELIMITER $$
CREATE PROCEDURE sp_rename_openvpms_role(IN role_from VARCHAR(255), IN role_to VARCHAR(255))
BEGIN
    UPDATE security_roles
    SET name = role_to
    WHERE name = role_from
      AND arch_short_name = 'security.role';
END $$

#
# Adds an authority, if it does not exist.
#
CREATE PROCEDURE sp_add_openvpms_authority(IN auth_name VARCHAR(255), IN auth_description VARCHAR(255),
                                           IN auth_method VARCHAR(255), IN auth_archetype VARCHAR(255))
BEGIN
    INSERT INTO granted_authorities (version, linkId, arch_short_name, arch_version, name, description, active,
                                     service_name, method, archetype)
    SELECT 0,
           UUID(),
           'security.archetypeAuthority',
           '1.0',
           auth_name,
           auth_description,
           1,
           'archetypeService',
           auth_method,
           auth_archetype
    FROM dual
    WHERE NOT exists(
            SELECT *
            FROM granted_authorities auth
            WHERE auth.method = auth_method
              AND auth.archetype = auth_archetype);
    IF ROW_COUNT() = 0 THEN
        # update the description
        UPDATE granted_authorities auth
        SET auth.description = auth_description
        WHERE auth.method = auth_method
          AND auth.archetype = auth_archetype;
    END IF;
END $$

#
# Adds a role, if it does not exist.
#
CREATE PROCEDURE sp_add_openvpms_role(IN role_name VARCHAR(255), IN role_description VARCHAR(255))
BEGIN
    INSERT INTO security_roles (version, linkId, arch_short_name, arch_version, name, description, active)
    SELECT 0,
           UUID(),
           'security.role',
           '1.0',
           role_name,
           role_description,
           1
    FROM dual
    WHERE NOT exists(SELECT *
                     FROM security_roles role
                     WHERE role.name = role_name
                       AND role.arch_short_name = 'security.role');
    IF ROW_COUNT() = 0 THEN
        # update the description
        UPDATE security_roles role
        SET role.description = role_description
        WHERE role.name = role_name
          AND role.arch_short_name = 'security.role';
    END IF;

END $$

#
# Adds a role authority, if it does not exist.
#
CREATE PROCEDURE sp_add_openvpms_role_authority(IN role_name VARCHAR(255), IN auth_method VARCHAR(255),
                                                IN auth_archetype VARCHAR(255))
BEGIN
    INSERT INTO roles_authorities (security_role_id, authority_id)
    SELECT role.security_role_id, granted.granted_authority_id
    FROM security_roles role
             JOIN granted_authorities granted
                  ON granted.method = auth_method
                      AND granted.archetype = auth_archetype
    WHERE role.name = role_name
      AND role.arch_short_name = 'security.role'
      AND NOT exists(SELECT *
                     FROM roles_authorities ra
                     WHERE ra.security_role_id = role.security_role_id
                       AND ra.authority_id = granted.granted_authority_id);
END $$

#
# Removes an authority from a role.
#
CREATE PROCEDURE sp_remove_openvpms_role_authority(IN role_name VARCHAR(255), IN method VARCHAR(255),
                                                   IN archetype VARCHAR(255))
BEGIN
    DELETE ra
    FROM roles_authorities ra
             JOIN security_roles role
                  ON ra.security_role_id = role.security_role_id
             JOIN granted_authorities granted
                  ON granted.granted_authority_id = ra.authority_id
    WHERE role.name = role_name
      AND role.arch_short_name = 'security.role'
      AND granted.service_name = 'archetypeService'
      AND granted.method = method
      AND granted.archetype = archetype;
END $$

DELIMITER ;

#
# Rename any exist roles that would clash with the new roles.
#

CALL sp_rename_openvpms_role('Account Administrator', 'Custom Account Administrator');
CALL sp_rename_openvpms_role('Account Manager', 'Custom Account Manager');
CALL sp_rename_openvpms_role('Clinician', 'Custom Clinician');
CALL sp_rename_openvpms_role('Practice Manager', 'Custom Practice Manager');
CALL sp_rename_openvpms_role('Schedule Administrator', 'Custom Schedule Administrator');
CALL sp_rename_openvpms_role('Schedule Manager', 'Custom Schedule Manager');
CALL sp_rename_openvpms_role('Stock Administrator', 'Custom Stock Administrator');
CALL sp_rename_openvpms_role('Stock Manager', 'Custom Stock Manager');

#
# Set up the default authorities. These should have been loaded, but not all installations have them.
#

CALL sp_add_openvpms_authority('Act Identity Create', 'Authority to Create Act Identity', 'create', 'actIdentity.*');
CALL sp_add_openvpms_authority('Act Identity Save', 'Authority to Save Act Identity', 'save', 'actIdentity.*');
CALL sp_add_openvpms_authority('Act Identity Remove', 'Authority to Remove Act Identity', 'remove', 'actIdentity.*');
CALL sp_add_openvpms_authority('Act Relationships Create', 'Authority to Create Act Relationships', 'create',
                               'actRelationship.*');
CALL sp_add_openvpms_authority('Act Relationships Save', 'Authority to Save Act Relationships', 'save',
                               'actRelationship.*');
CALL sp_add_openvpms_authority('Act Relationships Remove', 'Authority to Remove Act Relationships', 'remove',
                               'actRelationship.*');
CALL sp_add_openvpms_authority('All Create', 'Authority to Create All Archetypes', 'create', '*');
CALL sp_add_openvpms_authority('All Save', 'Authority to Save All Archetypes', 'save', '*');
CALL sp_add_openvpms_authority('All Remove', 'Authority to Remove All Archetypes', 'remove', '*');
CALL sp_add_openvpms_authority('Appointment Create', 'Authority to Create Appointment', 'create',
                               'act.customerAppointment');
CALL sp_add_openvpms_authority('Appointment Save', 'Authority to Save Appointment', 'save', 'act.customerAppointment');
CALL sp_add_openvpms_authority('Appointment Remove', 'Authority to Remove Appointment', 'remove',
                               'act.customerAppointment');
CALL sp_add_openvpms_authority('Bank Deposit Create', 'Authority to Create Bank Deposit', 'create', 'act.bankDeposit');
CALL sp_add_openvpms_authority('Bank Deposit Save', 'Authority to Save Bank Deposit', 'save', 'act.bankDeposit');
CALL sp_add_openvpms_authority('Bank Deposit Remove', 'Authority to Remove Bank Deposit', 'remove', 'act.bankDeposit');
CALL sp_add_openvpms_authority('Calendar Block Create', 'Authority to Create Calendar Block', 'create',
                               'act.calendarBlock');
CALL sp_add_openvpms_authority('Calendar Block Save', 'Authority to Save Calendar Block', 'save', 'act.calendarBlock');
CALL sp_add_openvpms_authority('Calendar Block Remove', 'Authority to Remove Calendar Block', 'remove',
                               'act.calendarBlock');
CALL sp_add_openvpms_authority('Calendar Event Series Create', 'Authority to Create Calendar Event Series', 'create',
                               'act.calendarEventSeries');
CALL sp_add_openvpms_authority('Calendar Event Series Save', 'Authority to Save Calendar Event Series', 'save',
                               'act.calendarEventSeries');
CALL sp_add_openvpms_authority('Calendar Event Series Remove', 'Authority to Remove Calendar Event Series', 'remove',
                               'act.calendarEventSeries');
CALL sp_add_openvpms_authority('Contacts Create', 'Authority to Create Contacts', 'create', 'contact.*');
CALL sp_add_openvpms_authority('Contacts Save', 'Authority to Save Contacts', 'save', 'contact.*');
CALL sp_add_openvpms_authority('Contacts Remove', 'Authority to Remove Contacts', 'remove', 'contact.*');
CALL sp_add_openvpms_authority('Customer Account Act Create', 'Authority to Create Account Act', 'create',
                               'act.customerAccount*');
CALL sp_add_openvpms_authority('Customer Account Act Save', 'Authority to Save Account Act', 'save',
                               'act.customerAccount*');
CALL sp_add_openvpms_authority('Customer Account Act Remove', 'Authority to Remove Account Act', 'remove',
                               'act.customerAccount*');
CALL sp_add_openvpms_authority('Customer Adjustment Create', 'Authority to Create Adjustment', 'create',
                               'act.customerAccount*Adjust');
CALL sp_add_openvpms_authority('Customer Adjustment Save', 'Authority to Save Adjustment', 'save',
                               'act.customerAccount*Adjust');
CALL sp_add_openvpms_authority('Customer Adjustment Remove', 'Authority to Remove Adjustment', 'remove',
                               'act.customerAccount*Adjust');
CALL sp_add_openvpms_authority('Customer Alert Create', 'Authority to Create Customer Alert', 'create',
                               'act.customerAlert');
CALL sp_add_openvpms_authority('Customer Alert Save', 'Authority to Save Customer Alert', 'save', 'act.customerAlert');
CALL sp_add_openvpms_authority('Customer Alert Remove', 'Authority to Remove Customer Alert', 'remove',
                               'act.customerAlert');
CALL sp_add_openvpms_authority('Customer All Acts Create', 'Authority to Create Customer Acts', 'create',
                               'act.customer*');
CALL sp_add_openvpms_authority('Customer All Acts Save', 'Authority to Save Customer Acts', 'save', 'act.customer*');
CALL sp_add_openvpms_authority('Customer All Acts Remove', 'Authority to Remove Customer Acts', 'remove',
                               'act.customer*');
CALL sp_add_openvpms_authority('Customer Balance Create', 'Authority to Create Balance', 'create',
                               'act.customerAccount*Balance');
CALL sp_add_openvpms_authority('Customer Balance Save', 'Authority to Save Balance', 'save',
                               'act.customerAccount*Balance');
CALL sp_add_openvpms_authority('Customer Balance Remove', 'Authority to Remove Balance', 'remove',
                               'act.customerAccount*Balance');
CALL sp_add_openvpms_authority('Customer Charge Items Create', 'Authority to Create Charge Items', 'create',
                               'act.customerAccount*Item');
CALL sp_add_openvpms_authority('Customer Charge Items Save', 'Authority to Save Charge Items', 'save',
                               'act.customerAccount*Item');
CALL sp_add_openvpms_authority('Customer Charge Items Remove', 'Authority to Remove Charge Items', 'remove',
                               'act.customerAccount*Item');
CALL sp_add_openvpms_authority('Customer Charges Create', 'Authority to Create Charges', 'create',
                               'act.customerAccountCharges*');
CALL sp_add_openvpms_authority('Customer Charges Save', 'Authority to Save Charges', 'save',
                               'act.customerAccountCharges*');
CALL sp_add_openvpms_authority('Customer Charges Remove', 'Authority to Remove Charges', 'remove',
                               'act.customerAccountCharges*');
CALL sp_add_openvpms_authority('Customer Communication Create', 'Authority to Create Customer Communication', 'create',
                               'act.customerCommunication*');
CALL sp_add_openvpms_authority('Customer Communication Save', 'Authority to Save Customer Communication', 'save',
                               'act.customerCommunication*');
CALL sp_add_openvpms_authority('Customer Communication Remove', 'Authority to Remove Customer Communication', 'remove',
                               'act.customerCommunication*');
CALL sp_add_openvpms_authority('Customer Document Create', 'Authority to Create Customer Document', 'create',
                               'act.customerDocument*');
CALL sp_add_openvpms_authority('Customer Document Save', 'Authority to Save Customer Document', 'save',
                               'act.customerDocument*');
CALL sp_add_openvpms_authority('Customer Document Remove', 'Authority to Remove Customer Document', 'remove',
                               'act.customerDocument*');
CALL sp_add_openvpms_authority('Customer Estimation Create', 'Authority to Create Customer Estimation', 'create',
                               'act.customerEstimation*');
CALL sp_add_openvpms_authority('Customer Estimation Save', 'Authority to Save Customer Estimation', 'save',
                               'act.customerEstimation*');
CALL sp_add_openvpms_authority('Customer Estimation Remove', 'Authority to Remove Customer Estimation', 'remove',
                               'act.customerEstimation*');
CALL sp_add_openvpms_authority('Customer Order Act Create', 'Authority to Create Customer Orders', 'create',
                               'act.customerOrder*');
CALL sp_add_openvpms_authority('Customer Order Act Save', 'Authority to Save Customer Orders', 'save',
                               'act.customerOrder*');
CALL sp_add_openvpms_authority('Customer Order Act Remove', 'Authority to Save Customer Orders', 'remove',
                               'act.customerOrder*');
CALL sp_add_openvpms_authority('Customer Payments Create', 'Authority to Create Payments', 'create',
                               'act.customerAccountPayment*');
CALL sp_add_openvpms_authority('Customer Payments Save', 'Authority to Save Payments', 'save',
                               'act.customerAccountPayment*');
CALL sp_add_openvpms_authority('Customer Payments Remove', 'Authority to Remove Payments', 'remove',
                               'act.customerAccountPayment*');
CALL sp_add_openvpms_authority('Customer Refund Create', 'Authority to Create Refund', 'create',
                               'act.customerAccountRefund*');
CALL sp_add_openvpms_authority('Customer Refund Save', 'Authority to Save Refund', 'save',
                               'act.customerAccountRefund*');
CALL sp_add_openvpms_authority('Customer Refund Remove', 'Authority to Remove Refund', 'remove',
                               'act.customerAccountRefund*');
CALL sp_add_openvpms_authority('Customer Return Act Create', 'Authority to Create Customer Returns', 'create',
                               'act.customerReturn*');
CALL sp_add_openvpms_authority('Customer Return Act Save', 'Authority to Save Customer Returns', 'save',
                               'act.customerReturn*');
CALL sp_add_openvpms_authority('Customer Return Act Remove', 'Authority to Remove Customer Returns', 'remove',
                               'act.customerReturn*');
CALL sp_add_openvpms_authority('Customers Create', 'Authority to Create Customers', 'create', 'party.customer*');
CALL sp_add_openvpms_authority('Customers Save', 'Authority to Save Customers', 'save', 'party.customer*');
CALL sp_add_openvpms_authority('Customers Remove', 'Authority to Remove Customers', 'remove', 'party.customer*');
CALL sp_add_openvpms_authority('Document Create', 'Authority to Create Document', 'create', 'document.*');
CALL sp_add_openvpms_authority('Document Save', 'Authority to Save Act Document', 'save', 'document.*');
CALL sp_add_openvpms_authority('Document Remove', 'Authority to Remove Document', 'remove', 'document.*');
CALL sp_add_openvpms_authority('Document Logo Act Create', 'Authority to Create Document Logo Act', 'create',
                               'act.documentLogo');
CALL sp_add_openvpms_authority('Document Logo Act Save', 'Authority to Save Document Logo Act', 'save',
                               'act.documentLogo');
CALL sp_add_openvpms_authority('Document Logo Act Remove', 'Authority to Remove Document Logo Act', 'remove',
                               'act.documentLogo');
CALL sp_add_openvpms_authority('Entities Create', 'Authority to Create Entities', 'create', 'entity.*');
CALL sp_add_openvpms_authority('Entities Save', 'Authority to Save Entities', 'save', 'entity.*');
CALL sp_add_openvpms_authority('Entities Remove', 'Authority to Remove Entities', 'remove', 'entity.*');
CALL sp_add_openvpms_authority('Entity Links Create', 'Authority to Create Entity Links', 'create', 'entityLink.*');
CALL sp_add_openvpms_authority('Entity Links Save', 'Authority to Save Entity Links', 'save', 'entityLink.*');
CALL sp_add_openvpms_authority('Entity Links Remove', 'Authority to Remove Entity Links', 'remove', 'entityLink.*');
CALL sp_add_openvpms_authority('Entity Relationships Create', 'Authority to Create Entity Relationships', 'create',
                               'entityRelationship.*');
CALL sp_add_openvpms_authority('Entity Relationships Save', 'Authority to Save Entity Relationships', 'save',
                               'entityRelationship.*');
CALL sp_add_openvpms_authority('Entity Relationships Remove', 'Authority to Remove Entity Relationships', 'remove',
                               'entityRelationship.*');
CALL sp_add_openvpms_authority('Hl7 Message Act Create', 'Authority to Create HL7 Message Act', 'create',
                               'act.HL7Message');
CALL sp_add_openvpms_authority('Hl7 Message Act Save', 'Authority to Save HL7 Message Act', 'save', 'act.HL7Message');
CALL sp_add_openvpms_authority('Hl7 Message Act Remove', 'Authority to Remove HL7 Message Act', 'remove',
                               'act.HL7Message');
CALL sp_add_openvpms_authority('Identity Create', 'Authority to Create Identity', 'create', 'entityIdentity.*');
CALL sp_add_openvpms_authority('Identity Save', 'Authority to Save Identity', 'save', 'entityIdentity.*');
CALL sp_add_openvpms_authority('Identity Remove', 'Authority to Remove Identity', 'remove', 'entityIdentity.*');
CALL sp_add_openvpms_authority('Lookup Create', 'Authority to Create Lookup', 'create', 'lookup.*');
CALL sp_add_openvpms_authority('Lookup Save', 'Authority to Save Lookup', 'save', 'lookup.*');
CALL sp_add_openvpms_authority('Lookup Remove', 'Authority to Remove Lookup', 'remove', 'lookup.*');
CALL sp_add_openvpms_authority('Message Create', 'Authority to Create Messages', 'create', 'act.userMessage');
CALL sp_add_openvpms_authority('Message Save', 'Authority to Save Messages', 'save', 'act.userMessage');
CALL sp_add_openvpms_authority('Message Remove', 'Authority to Remove Messages', 'remove', 'act.userMessage');
CALL sp_add_openvpms_authority('Organisation Create', 'Authority to Create Organisation', 'create',
                               'party.organisation*');
CALL sp_add_openvpms_authority('Organisation Save', 'Authority to Save Organisation', 'save', 'party.organisation*');
CALL sp_add_openvpms_authority('Organisation Remove', 'Authority to Remove Organisation', 'remove',
                               'party.organisation*');
CALL sp_add_openvpms_authority('Participations Create', 'Authority to Create Participations', 'create',
                               'participation.*');
CALL sp_add_openvpms_authority('Participations Save', 'Authority to Save Participations', 'save', 'participation.*');
CALL sp_add_openvpms_authority('Participations Remove', 'Authority to Remove Participations', 'remove',
                               'participation.*');
CALL sp_add_openvpms_authority('Patient Alert Create', 'Authority to Create Alert', 'create', 'act.patientAlert');
CALL sp_add_openvpms_authority('Patient Alert Save', 'Authority to Save Alert', 'save', 'act.patientAlert');
CALL sp_add_openvpms_authority('Patient Alert Remove', 'Authority to Remove Alert', 'remove', 'act.patientAlert');
CALL sp_add_openvpms_authority('Patient All Acts Create', 'Authority to Create All', 'create', 'act.patient*');
CALL sp_add_openvpms_authority('Patient All Acts Save', 'Authority to Save All', 'save', 'act.patient*');
CALL sp_add_openvpms_authority('Patient All Acts Remove', 'Authority to Remove All', 'remove', 'act.patient*');
CALL sp_add_openvpms_authority('Patient Clinical Act Create', 'Authority to Create Clinical', 'create',
                               'act.patientClinical*');
CALL sp_add_openvpms_authority('Patient Clinical Act Save', 'Authority to Save Clinical', 'save',
                               'act.patientClinical*');
CALL sp_add_openvpms_authority('Patient Clinical Act Remove', 'Authority to Remove Clinical', 'remove',
                               'act.patientClinical*');
CALL sp_add_openvpms_authority('Patient Customer Note Create', 'Authority to Create Patient Customer Note', 'create',
                               'act.patientCustomerNote');
CALL sp_add_openvpms_authority('Patient Customer Note Save', 'Authority to Save Patient Customer Note', 'save',
                               'act.patientCustomerNote');
CALL sp_add_openvpms_authority('Patient Customer Note Remove', 'Authority to Remove Patient Customer Note', 'remove',
                               'act.patientCustomerNote');
CALL sp_add_openvpms_authority('Patient Document Create', 'Authority to Create Document', 'create',
                               'act.patientDocument*');
CALL sp_add_openvpms_authority('Patient Document Save', 'Authority to Save Document', 'save', 'act.patientDocument*');
CALL sp_add_openvpms_authority('Patient Document Remove', 'Authority to Remove Document', 'remove',
                               'act.patientDocument*');
CALL sp_add_openvpms_authority('Patient Insurance Claim Create', 'Authority to Create Patient Insurance Claim',
                               'create', 'act.patientInsuranceClaim');
CALL sp_add_openvpms_authority('Patient Insurance Claim Save', 'Authority to Save Patient Insurance Claim', 'save',
                               'act.patientInsuranceClaim');
CALL sp_add_openvpms_authority('Patient Insurance Claim Remove', 'Authority to Remove Patient Insurance Claim',
                               'remove', 'act.patientInsuranceClaim');
CALL sp_add_openvpms_authority('Patient Insurance Claim Attachment Create',
                               'Authority to Create Patient Insurance Claim Attachment', 'create',
                               'act.patientInsuranceClaimAttachment');
CALL sp_add_openvpms_authority('Patient Insurance Claim Attachment Save',
                               'Authority to Save Patient Insurance Claim Attachment', 'save',
                               'act.patientInsuranceClaimAttachment');
CALL sp_add_openvpms_authority('Patient Insurance Claim Attachment Remove',
                               'Authority to Remove Patient Insurance Claim Attachment', 'remove',
                               'act.patientInsuranceClaimAttachment');
CALL sp_add_openvpms_authority('Patient Insurance Claim Condition Create',
                               'Authority to Create Patient Insurance Claim Condition', 'create',
                               'act.patientInsuranceClaimItem');
CALL sp_add_openvpms_authority('Patient Insurance Claim Condition Save',
                               'Authority to Save Patient Insurance Claim Condition', 'save',
                               'act.patientInsuranceClaimItem');
CALL sp_add_openvpms_authority('Patient Insurance Claim Condition Remove',
                               'Authority to Remove Patient Insurance Claim Condition', 'remove',
                               'act.patientInsuranceClaimItem');
CALL sp_add_openvpms_authority('Patient Insurance Policy Create', 'Authority to Create Patient Insurance Policy',
                               'create', 'act.patientInsurancePolicy');
CALL sp_add_openvpms_authority('Patient Insurance Policy Save', 'Authority to Save Patient Insurance Policy', 'save',
                               'act.patientInsurancePolicy');
CALL sp_add_openvpms_authority('Patient Insurance Policy Remove', 'Authority to Remove Patient Insurance Policy',
                               'remove', 'act.patientInsurancePolicy');
CALL sp_add_openvpms_authority('Patient Investigation Create', 'Authority to Create Investigation', 'create',
                               'act.patientInvestigation*');
CALL sp_add_openvpms_authority('Patient Investigation Save', 'Authority to Save Investigation', 'save',
                               'act.patientInvestigation*');
CALL sp_add_openvpms_authority('Patient Investigation Remove', 'Authority to Remove Investigation', 'remove',
                               'act.patientInvestigation*');
CALL sp_add_openvpms_authority('Patient Medication Create', 'Authority to Create Medication', 'create',
                               'act.patientMedication');
CALL sp_add_openvpms_authority('Patient Medication Save', 'Authority to Save Medication', 'save',
                               'act.patientMedication');
CALL sp_add_openvpms_authority('Patient Medication Remove', 'Authority to Remove Medication', 'remove',
                               'act.patientMedication');
CALL sp_add_openvpms_authority('Patient Prescription Create', 'Authority to Create Prescription', 'create',
                               'act.patientPrescription');
CALL sp_add_openvpms_authority('Patient Prescription Save', 'Authority to Save Prescription', 'save',
                               'act.patientPrescription');
CALL sp_add_openvpms_authority('Patient Prescription Remove', 'Authority to Remove Prescription', 'remove',
                               'act.patientPrescription');
CALL sp_add_openvpms_authority('Patient Reminder Create', 'Authority to Create Reminder', 'create',
                               'act.patientReminder');
CALL sp_add_openvpms_authority('Patient Reminder Save', 'Authority to Save Reminder', 'save', 'act.patientReminder');
CALL sp_add_openvpms_authority('Patient Reminder Remove', 'Authority to Remove Reminder', 'remove',
                               'act.patientReminder');
CALL sp_add_openvpms_authority('Patient Reminder Item Create', 'Authority to Create Reminder Item', 'create',
                               'act.patientReminderItem*');
CALL sp_add_openvpms_authority('Patient Reminder Item Save', 'Authority to Save Reminder Item', 'save',
                               'act.patientReminderItem*');
CALL sp_add_openvpms_authority('Patient Reminder Item Remove', 'Authority to Remove Reminder Item', 'remove',
                               'act.patientReminderItem*');
CALL sp_add_openvpms_authority('Patient Weight Create', 'Authority to Create Weight', 'create', 'act.patientWeight');
CALL sp_add_openvpms_authority('Patient Weight Save', 'Authority to Save Weight', 'save', 'act.patientWeight');
CALL sp_add_openvpms_authority('Patient Weight Remove', 'Authority to Remove Weight', 'remove', 'act.patientWeight');
CALL sp_add_openvpms_authority('Patients Create', 'Authority to Create Patients', 'create', 'party.patient*');
CALL sp_add_openvpms_authority('Patients Save', 'Authority to Save Patients', 'save', 'party.patient*');
CALL sp_add_openvpms_authority('Patients Remove', 'Authority to Remove Patients', 'remove', 'party.patient*');
CALL sp_add_openvpms_authority('Preferences Entity Create', 'Authority to Create Preferences', 'create',
                               'entity.preference*');
CALL sp_add_openvpms_authority('Preferences Entity Save', 'Authority to Save Preferences', 'save',
                               'entity.preference*');
CALL sp_add_openvpms_authority('Preferences Entity Remove', 'Authority to Remove Preferences', 'remove',
                               'entity.preference*');
CALL sp_add_openvpms_authority('Product Batches Create', 'Authority to Create Product Batches', 'create',
                               'entity.productBatch');
CALL sp_add_openvpms_authority('Product Batches Save', 'Authority to Save Product Batches', 'save',
                               'entity.productBatch');
CALL sp_add_openvpms_authority('Product Batches Remove', 'Authority to Remove Product Batches', 'remove',
                               'entity.productBatch');
CALL sp_add_openvpms_authority('Product Doses Create', 'Authority to Create Product Doses', 'create',
                               'entity.productDose');
CALL sp_add_openvpms_authority('Product Doses Save', 'Authority to Save Product Doses', 'save', 'entity.productDose');
CALL sp_add_openvpms_authority('Product Doses Remove', 'Authority to Remove Product Doses', 'remove',
                               'entity.productDose');
CALL sp_add_openvpms_authority('Product Prices Create', 'Authority to Create Product Prices', 'create',
                               'productPrice.*');
CALL sp_add_openvpms_authority('Product Prices Save', 'Authority to Save Product Prices', 'save', 'productPrice.*');
CALL sp_add_openvpms_authority('Product Prices Remove', 'Authority to Remove Product Prices', 'remove',
                               'productPrice.*');
CALL sp_add_openvpms_authority('Products Create', 'Authority to Create Products', 'create', 'product.*');
CALL sp_add_openvpms_authority('Products Save', 'Authority to Save Products', 'save', 'product.*');
CALL sp_add_openvpms_authority('Products Remove', 'Authority to Remove Products', 'remove', 'product.*');
CALL sp_add_openvpms_authority('Shift Create', 'Authority to Create Shift', 'create', 'act.rosterEvent');
CALL sp_add_openvpms_authority('Shift Save', 'Authority to Save Shift', 'save', 'act.rosterEvent');
CALL sp_add_openvpms_authority('Shift Remove', 'Authority to Remove Shift', 'remove', 'act.rosterEvent');
CALL sp_add_openvpms_authority('Stock Transaction Create', 'Authority to Create Stock', 'create', 'act.stock*');
CALL sp_add_openvpms_authority('Stock Transaction Save', 'Authority to Save Stock', 'save', 'act.stock*');
CALL sp_add_openvpms_authority('Stock Transaction Remove', 'Authority to Remove Stock', 'remove', 'act.stock*');
CALL sp_add_openvpms_authority('Supplier Account Acts Create', 'Authority to Create Supplier Account Acts', 'create',
                               'act.supplierAccount*');
CALL sp_add_openvpms_authority('Supplier Account Acts Save', 'Authority to Save Supplier Account Acts', 'save',
                               'act.supplierAccount*');
CALL sp_add_openvpms_authority('Supplier Account Acts Remove', 'Authority to Remove Supplier Account Acts', 'remove',
                               'act.supplierAccount*');
CALL sp_add_openvpms_authority('Supplier All Acts Create', 'Authority to Create Supplier Acts', 'create',
                               'act.supplier*');
CALL sp_add_openvpms_authority('Supplier All Acts Save', 'Authority to Save Supplier Acts', 'save', 'act.supplier*');
CALL sp_add_openvpms_authority('Supplier All Acts Remove', 'Authority to Remove Supplier Acts', 'remove',
                               'act.supplier*');
CALL sp_add_openvpms_authority('Supplier Delivery Acts Create', 'Authority to Create Supplier Delivery Acts', 'create',
                               'act.supplierDelivery*');
CALL sp_add_openvpms_authority('Supplier Delivery Acts Save', 'Authority to Save Supplier Delivery Acts', 'save',
                               'act.supplierDelivery*');
CALL sp_add_openvpms_authority('Supplier Delivery Acts Remove', 'Authority to Remove Supplier Delivery Acts', 'remove',
                               'act.supplierDelivery*');
CALL sp_add_openvpms_authority('Supplier Document Acts Create', 'Authority to Create Supplier Document Acts', 'create',
                               'act.supplierDocument*');
CALL sp_add_openvpms_authority('Supplier Document Acts Save', 'Authority to Save Supplier Document Acts', 'save',
                               'act.supplierDocument*');
CALL sp_add_openvpms_authority('Supplier Document Acts Remove', 'Authority to Remove Supplier Document Acts', 'remove',
                               'act.supplierDocument*');
CALL sp_add_openvpms_authority('Supplier Order Acts Create', 'Authority to Create Supplier Order Acts', 'create',
                               'act.supplierOrder*');
CALL sp_add_openvpms_authority('Supplier Order Acts Save', 'Authority to Save Supplier Order Acts', 'save',
                               'act.supplierOrder*');
CALL sp_add_openvpms_authority('Supplier Order Acts Remove', 'Authority to Remove Supplier Order Acts', 'remove',
                               'act.supplierOrder*');
CALL sp_add_openvpms_authority('Supplier Return Acts Create', 'Authority to Create Supplier Return Acts', 'create',
                               'act.supplierReturn*');
CALL sp_add_openvpms_authority('Supplier Return Acts Save', 'Authority to Save Supplier Return Acts', 'save',
                               'act.supplierReturn*');
CALL sp_add_openvpms_authority('Supplier Return Acts Remove', 'Authority to Remove Supplier Return Acts', 'remove',
                               'act.supplierReturn*');
CALL sp_add_openvpms_authority('Suppliers Create', 'Authority to Create Suppliers', 'create', 'party.supplier*');
CALL sp_add_openvpms_authority('Suppliers Save', 'Authority to Save Suppliers', 'save', 'party.supplier*');
CALL sp_add_openvpms_authority('Suppliers Remove', 'Authority to Remove Suppliers', 'remove', 'party.supplier*');
CALL sp_add_openvpms_authority('Task Create', 'Authority to Create Task', 'create', 'act.customerTask');
CALL sp_add_openvpms_authority('Task Save', 'Authority to Save Task', 'save', 'act.customerTask');
CALL sp_add_openvpms_authority('Task Remove', 'Authority to Remove Task', 'remove', 'act.customerTask');
CALL sp_add_openvpms_authority('Till Balance Create', 'Authority to Create Till Balance', 'create', 'act.tillBalance');
CALL sp_add_openvpms_authority('Till Balance Save', 'Authority to Save Till Balance', 'save', 'act.tillBalance');
CALL sp_add_openvpms_authority('Till Balance Remove', 'Authority to Remove Till Balance', 'remove', 'act.tillBalance');
CALL sp_add_openvpms_authority('Till Balance Adjustment Create', 'Authority to Create Till Balance Adjustment',
                               'create', 'act.tillBalanceAdjustment');
CALL sp_add_openvpms_authority('Till Balance Adjustment Save', 'Authority to Save Till Balance Adjustment', 'save',
                               'act.tillBalanceAdjustment');
CALL sp_add_openvpms_authority('Till Balance Adjustment Remove', 'Authority to Remove Till Balance Adjustment',
                               'remove', 'act.tillBalanceAdjustment');
CALL sp_add_openvpms_authority('User Create', 'Authority to Create User', 'create', 'security.*');
CALL sp_add_openvpms_authority('User Save', 'Authority to Save User', 'save', 'security.*');
CALL sp_add_openvpms_authority('User Remove', 'Authority to Remove User', 'remove', 'security.*');

#
# Set up roles.
#
CALL sp_add_openvpms_role('Account Administrator', 'Administer customer accounts');
CALL sp_add_openvpms_role('Account Manager', 'Manage customer accounts');
CALL sp_add_openvpms_role('Administration', 'Full control');
CALL sp_add_openvpms_role('Base Role', 'Authorities required by all non-Administration/Online Booking users');
CALL sp_add_openvpms_role('Clinician', 'Create and remove Prescriptions');
CALL sp_add_openvpms_role('Insurance Claims', 'Create and remove patient Insurance Claims');
CALL sp_add_openvpms_role('Online Booking', 'Restricted role to enable external providers to submit online bookings');
CALL sp_add_openvpms_role('Practice Manager', 'Manage all customer and patient information');
CALL sp_add_openvpms_role('Schedule Administrator', 'Remove Calendar Blocks and Shifts');
CALL sp_add_openvpms_role('Schedule Manager', 'Create and update Calendar Blocks and Shifts');
CALL sp_add_openvpms_role('Stock Administrator',
                          'Manage Products, remove Supplier Orders and Deliveries, remove Suppliers');
CALL sp_add_openvpms_role('Stock Manager',
                          'Manage Product Batches, Adjustments, Transfers, Orders and Deliveries, create and update Suppliers');
CALL sp_add_openvpms_role_authority('Account Administrator', 'remove', 'act.bankDeposit');
CALL sp_add_openvpms_role_authority('Account Administrator', 'create', 'act.customerAccount*');
CALL sp_add_openvpms_role_authority('Account Administrator', 'remove', 'act.customerAccount*');
CALL sp_add_openvpms_role_authority('Account Administrator', 'remove', 'act.tillBalanceAdjustment');
CALL sp_add_openvpms_role_authority('Account Manager', 'create', 'act.customerAccount*Balance');
CALL sp_add_openvpms_role_authority('Account Manager', 'save', 'act.customerAccount*Balance');
CALL sp_add_openvpms_role_authority('Account Manager', 'remove', 'act.customerAccountCharges*');
CALL sp_add_openvpms_role_authority('Account Manager', 'remove', 'act.customerOrder*');
CALL sp_add_openvpms_role_authority('Account Manager', 'remove', 'act.customerAccountPayment*');
CALL sp_add_openvpms_role_authority('Account Manager', 'create', 'act.customerAccountRefund*');
CALL sp_add_openvpms_role_authority('Account Manager', 'save', 'act.customerAccountRefund*');
CALL sp_add_openvpms_role_authority('Account Manager', 'remove', 'act.customerAccountRefund*');
CALL sp_add_openvpms_role_authority('Account Manager', 'remove', 'act.customerReturn*');
CALL sp_add_openvpms_role_authority('Account Manager', 'remove', 'act.tillBalance');
CALL sp_add_openvpms_role_authority('Account Manager', 'create', 'act.tillBalanceAdjustment');
CALL sp_add_openvpms_role_authority('Account Manager', 'save', 'act.tillBalanceAdjustment');
CALL sp_add_openvpms_role_authority('Administration', 'create', '*');
CALL sp_add_openvpms_role_authority('Administration', 'remove', '*');
CALL sp_add_openvpms_role_authority('Administration', 'save', '*');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'act.calendarBlock');
CALL sp_remove_openvpms_role_authority('Base Role', 'save', 'act.calendarBlock');
CALL sp_remove_openvpms_role_authority('Base Role', 'remove', 'act.calendarBlock');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'act.customerAccount*');
CALL sp_remove_openvpms_role_authority('Base Role', 'remove', 'act.customerCommunication*');
CALL sp_remove_openvpms_role_authority('Base Role', 'remove', 'act.customerOrder*');
CALL sp_remove_openvpms_role_authority('Base Role', 'remove', 'act.customerReturn*');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'act.documentLogo');
CALL sp_remove_openvpms_role_authority('Base Role', 'save', 'act.documentLogo');
CALL sp_remove_openvpms_role_authority('Base Role', 'remove', 'act.documentLogo');
CALL sp_remove_openvpms_role_authority('Base Role', 'remove', 'act.patientCustomerNote');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'act.patientPrescription');
CALL sp_remove_openvpms_role_authority('Base Role', 'remove', 'act.patientPrescription');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'entity.productBatch');
CALL sp_remove_openvpms_role_authority('Base Role', 'save', 'entity.productBatch');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'product.*');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'act.rosterEvent');
CALL sp_remove_openvpms_role_authority('Base Role', 'save', 'act.rosterEvent');
CALL sp_remove_openvpms_role_authority('Base Role', 'remove', 'act.rosterEvent');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'act.supplier*');
CALL sp_remove_openvpms_role_authority('Base Role', 'save', 'act.supplier*');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'party.supplier*');
CALL sp_remove_openvpms_role_authority('Base Role', 'save', 'party.supplier*');
CALL sp_remove_openvpms_role_authority('Base Role', 'create', 'act.tillBalanceAdjustment');
CALL sp_remove_openvpms_role_authority('Base Role', 'save', 'act.tillBalanceAdjustment');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'actIdentity.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'actIdentity.*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'actIdentity.*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'actRelationship.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'actRelationship.*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'actRelationship.*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerAppointment');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerAppointment');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.customerAppointment');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.bankDeposit');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.bankDeposit');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.calendarEventSeries');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.calendarEventSeries');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.calendarEventSeries');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'contact.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'contact.*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'contact.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerAccount*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerAlert');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerAlert');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.customerAlert');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerAccount*Item');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerAccount*Item');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.customerAccount*Item');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerAccountCharges*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerAccountCharges*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerCommunication*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerCommunication*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerDocument*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerDocument*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerEstimation*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerEstimation*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.customerEstimation*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerOrder*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerOrder*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerAccountPayment*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerAccountPayment*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerReturn*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerReturn*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'party.customer*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'party.customer*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'party.customer*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'document.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'document.*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'document.*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'entityLink.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'entityLink.*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'entityLink.*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'entityRelationship.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'entityRelationship.*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'entityRelationship.*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.HL7Message');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.HL7Message');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.HL7Message');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'entityIdentity.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'entityIdentity.*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'entityIdentity.*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.userMessage');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.userMessage');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.userMessage');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'party.organisation*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'participation.*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'participation.*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'participation.*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientAlert');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientAlert');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientAlert');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientClinical*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientClinical*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientClinical*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientCustomerNote');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientCustomerNote');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientDocument*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientDocument*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientDocument*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientInsurancePolicy');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientInsurancePolicy');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientInsurancePolicy');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientInvestigation*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientInvestigation*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientInvestigation*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientMedication');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientMedication');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientMedication');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientPrescription');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientReminder');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientReminder');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientReminder');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientReminderItem*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientReminderItem*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientReminderItem*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.patientWeight');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.patientWeight');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.patientWeight');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'party.patient*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'party.patient*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'party.patient*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'entity.preference*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'entity.preference*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'entity.preference*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'product.*');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerTask');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerTask');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.customerTask');
CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.tillBalance');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.tillBalance');
CALL sp_add_openvpms_role_authority('Clinician', 'create', 'act.patientPrescription');
CALL sp_add_openvpms_role_authority('Clinician', 'remove', 'act.patientPrescription');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'create', 'act.patientInsuranceClaim');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'save', 'act.patientInsuranceClaim');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'remove', 'act.patientInsuranceClaim');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'create', 'act.patientInsuranceClaimAttachment');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'save', 'act.patientInsuranceClaimAttachment');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'remove', 'act.patientInsuranceClaimAttachment');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'create', 'act.patientInsuranceClaimItem');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'save', 'act.patientInsuranceClaimItem');
CALL sp_add_openvpms_role_authority('Insurance Claims', 'remove', 'act.patientInsuranceClaimItem');
CALL sp_add_openvpms_role_authority('Online Booking', 'create', 'actRelationship.*');
CALL sp_add_openvpms_role_authority('Online Booking', 'save', 'actRelationship.*');
CALL sp_add_openvpms_role_authority('Online Booking', 'create', 'act.customerAppointment');
CALL sp_add_openvpms_role_authority('Online Booking', 'save', 'act.customerAppointment');
CALL sp_add_openvpms_role_authority('Online Booking', 'create', 'participation.*');
CALL sp_add_openvpms_role_authority('Online Booking', 'save', 'participation.*');
CALL sp_add_openvpms_role_authority('Practice Manager', 'create', 'act.customer*');
CALL sp_add_openvpms_role_authority('Practice Manager', 'save', 'act.customer*');
CALL sp_add_openvpms_role_authority('Practice Manager', 'remove', 'act.customer*');
CALL sp_add_openvpms_role_authority('Practice Manager', 'create', 'act.patient*');
CALL sp_add_openvpms_role_authority('Practice Manager', 'save', 'act.patient*');
CALL sp_add_openvpms_role_authority('Practice Manager', 'remove', 'act.patient*');
CALL sp_add_openvpms_role_authority('Schedule Administrator', 'remove', 'act.calendarBlock');
CALL sp_add_openvpms_role_authority('Schedule Administrator', 'remove', 'act.rosterEvent');
CALL sp_add_openvpms_role_authority('Schedule Manager', 'create', 'act.calendarBlock');
CALL sp_add_openvpms_role_authority('Schedule Manager', 'save', 'act.calendarBlock');
CALL sp_add_openvpms_role_authority('Schedule Manager', 'create', 'act.rosterEvent');
CALL sp_add_openvpms_role_authority('Schedule Manager', 'save', 'act.rosterEvent');
CALL sp_add_openvpms_role_authority('Stock Administrator', 'create', 'entity.productDose');
CALL sp_add_openvpms_role_authority('Stock Administrator', 'remove', 'entity.productDose');
CALL sp_add_openvpms_role_authority('Stock Administrator', 'create', 'productPrice.*');
CALL sp_add_openvpms_role_authority('Stock Administrator', 'remove', 'productPrice.*');
CALL sp_add_openvpms_role_authority('Stock Administrator', 'create', 'product.*');
CALL sp_add_openvpms_role_authority('Stock Administrator', 'remove', 'product.*');
CALL sp_add_openvpms_role_authority('Stock Administrator', 'remove', 'act.supplier*');
CALL sp_add_openvpms_role_authority('Stock Administrator', 'remove', 'party.supplier*');
CALL sp_add_openvpms_role_authority('Stock Manager', 'create', 'entity.productBatch');
CALL sp_add_openvpms_role_authority('Stock Manager', 'save', 'entity.productBatch');
CALL sp_add_openvpms_role_authority('Stock Manager', 'remove', 'entity.productBatch');
CALL sp_add_openvpms_role_authority('Stock Manager', 'save', 'entity.productDose');
CALL sp_add_openvpms_role_authority('Stock Manager', 'save', 'productPrice.*');
CALL sp_add_openvpms_role_authority('Stock Manager', 'create', 'act.stock*');
CALL sp_add_openvpms_role_authority('Stock Manager', 'save', 'act.stock*');
CALL sp_add_openvpms_role_authority('Stock Manager', 'remove', 'act.stock*');
CALL sp_add_openvpms_role_authority('Stock Manager', 'create', 'act.supplier*');
CALL sp_add_openvpms_role_authority('Stock Manager', 'save', 'act.supplier*');
CALL sp_add_openvpms_role_authority('Stock Manager', 'create', 'party.supplier*');
CALL sp_add_openvpms_role_authority('Stock Manager', 'save', 'party.supplier*');

DROP PROCEDURE sp_rename_openvpms_role;
DROP PROCEDURE sp_add_openvpms_role;
DROP PROCEDURE sp_add_openvpms_authority;
DROP PROCEDURE sp_add_openvpms_role_authority;
DROP PROCEDURE sp_remove_openvpms_role_authority;

#
# Assign the Stock Administrator and Stock Manager roles to users that have the Stock Control role.
#
# The distinct clause is required to handle the situation where a user has been assigned two Stock Control role
# (see OVPMS-2252).
#
INSERT INTO user_roles (user_id, security_role_id)
SELECT DISTINCT ur.user_id, new_stock.security_role_id
FROM user_roles ur
         JOIN security_roles stock_control
              ON stock_control.name = 'Stock Control'
                  AND ur.security_role_id = stock_control.security_role_id
         JOIN security_roles new_stock
WHERE new_stock.name IN ('Stock Administrator', 'Stock Manager')
  AND NOT exists(
        SELECT *
        FROM user_roles existing
        WHERE existing.user_id = ur.user_id
          AND existing.security_role_id = new_stock.security_role_id);

/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

#
# Delete the Stock Control role.
#
DELETE ur
FROM user_roles ur
         JOIN security_roles role
              ON role.name = 'Stock Control'
                  AND role.arch_short_name = 'security.role'
                  AND ur.security_role_id = role.security_role_id;

DELETE ra
FROM security_roles role
         JOIN roles_authorities ra
              ON role.name = 'Stock Control'
                  AND role.arch_short_name = 'security.role'
                  AND ra.security_role_id = role.security_role_id;
DELETE role
FROM security_roles role
WHERE role.name = 'Stock Control'
  AND role.arch_short_name = 'security.role';

#
# Link the Clinician role to users with Clinician lookup.userType.
#
INSERT INTO user_roles(user_id, security_role_id)
SELECT u.user_id, role.security_role_id
FROM users u
         JOIN entities e
              ON u.user_id = e.entity_id
         JOIN entity_classifications ec ON e.entity_id = ec.entity_id
         JOIN lookups l
              ON ec.lookup_id = l.lookup_id
                  AND l.arch_short_name = 'lookup.userType'
                  AND l.code = 'CLINICIAN'
         JOIN security_roles role
              ON role.name = 'Clinician'
                  AND role.arch_short_name = 'security.role'
WHERE NOT exists(
        SELECT *
        FROM user_roles ur
        WHERE ur.user_id = u.user_id
          AND role.security_role_id = ur.security_role_id);