#
# OVPMS-2966 Incorrect lookup.documentTemplateType code for investigations in base.xml
#
UPDATE lookups l1
    LEFT JOIN lookups l2
    ON l2.code = 'act.patientInvestigation'
        AND l2.arch_short_name = 'lookup.documentTemplateType'
SET l1.code = 'act.patientInvestigation'
WHERE l1.arch_short_name = 'lookup.documentTemplateType'
  AND l1.code = 'act.patientDocumentInvestigation'
  AND l2.code IS NULL;

# The following shouldn't be required. It will only occur if someone has manually created a lookup.documentTemplateType
# with code=act.patientInvestigation.
# De-activate rather than delete, as a more complex migration is required for deletion, and it's unlikely.
UPDATE lookups l
SET l.active      = 0,
    l.description = 'This lookup is redundant and can be deleted.'
WHERE l.arch_short_name = 'lookup.documentTemplateType'
  AND l.code = 'act.patientDocumentInvestigation';