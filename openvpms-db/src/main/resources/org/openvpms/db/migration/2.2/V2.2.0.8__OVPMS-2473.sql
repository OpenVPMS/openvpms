#
# OVPMS-2473 Clearing tendered amount in cash payment causes receipt print failure
#

DROP TABLE IF EXISTS tmp_cash;
CREATE TEMPORARY TABLE tmp_cash
(
    act_id bigint(20) NOT NULL PRIMARY KEY,
    amount decimal(18, 3)
);

INSERT INTO tmp_cash (act_id, amount)
SELECT cash.act_id, rounded_amount.value
FROM acts cash
         JOIN act_details rounded_amount
              ON cash.act_id = rounded_amount.act_id
                  AND rounded_amount.name = 'roundedAmount'
         LEFT JOIN act_details tendered
                   ON cash.act_id = tendered.act_id
                       AND tendered.name = 'tendered'
WHERE cash.arch_short_name = 'act.customerAccountPaymentCash'
  AND tendered.act_id IS NULL;

INSERT INTO act_details (act_id, name, type, value)
SELECT cash.act_id,
       'tendered',
       'money',
       amount
FROM tmp_cash cash;

UPDATE act_details xchange
    JOIN tmp_cash cash
    ON xchange.act_id = cash.act_id
SET value = '0.0'
WHERE xchange.name = 'change';

DROP TABLE tmp_cash;