#
# OVPMS-2995 Reversal of payment with No Terminal EFT transaction creates a Pending act.EFTPOSPayment
#

DELETE eft_details
FROM acts payment
         JOIN act_relationships r_eft
              ON r_eft.source_id = payment.act_id
                  AND r_eft.arch_short_name = 'actRelationship.customerAccountRefundEFTPOS'
         JOIN acts eft
              ON r_eft.target_id = eft.act_id
         JOIN act_details eft_details
              ON eft_details.act_id = eft.act_id
WHERE payment.arch_short_name = 'act.customerAccountPaymentEFT'
  AND eft.arch_short_name = 'act.EFTPOSRefund';

DELETE eft
FROM acts payment
         JOIN act_relationships r_eft
              ON r_eft.source_id = payment.act_id
                  AND r_eft.arch_short_name = 'actRelationship.customerAccountRefundEFTPOS'
         JOIN acts eft
              ON r_eft.target_id = eft.act_id
WHERE payment.arch_short_name = 'act.customerAccountPaymentEFT'
  AND eft.arch_short_name = 'act.EFTPOSRefund';

DELETE eft_details
FROM acts refund
         JOIN act_relationships r_eft
              ON r_eft.source_id = refund.act_id
                  AND r_eft.arch_short_name = 'actRelationship.customerAccountPaymentEFTPOS'
         JOIN acts eft
              ON r_eft.target_id = eft.act_id
         JOIN act_details eft_details
              ON eft_details.act_id = eft.act_id
WHERE refund.arch_short_name = 'act.customerAccountRefundEFT'
  AND eft.arch_short_name = 'act.EFTPOSPayment';

DELETE eft
FROM acts refund
         JOIN act_relationships r_eft
              ON r_eft.source_id = refund.act_id
                  AND r_eft.arch_short_name = 'actRelationship.customerAccountPaymentEFTPOS'
         JOIN acts eft
              ON r_eft.target_id = eft.act_id
WHERE refund.arch_short_name = 'act.customerAccountRefundEFT'
  AND eft.arch_short_name = 'act.EFTPOSPayment';