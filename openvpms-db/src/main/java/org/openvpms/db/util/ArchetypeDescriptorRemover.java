/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.util;

import org.apache.commons.lang3.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Removes an archetype descriptor using SQL.
 *
 * @author Tim Anderson
 */
public class ArchetypeDescriptorRemover {

    /**
     * Returns a map of archetypes to their corresponding archetype identifiers.
     *
     * @param connection the database connection
     * @return the archetype -> archetype id map
     * @throws SQLException for any SQL error
     */
    public Map<String, Long> getArchetypes(Connection connection) throws SQLException {
        String sql = "SELECT archetype_desc_id, name FROM archetype_descriptors";
        Map<String, Long> descriptors = new HashMap<>();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            try (ResultSet set = statement.executeQuery()) {
                while (set.next()) {
                    long id = set.getLong(1);
                    String name = set.getString(2);
                    int index = StringUtils.ordinalIndexOf(name, ".", 2);
                    String archetype = (index != -1) ? name.substring(0, index) : name;
                    descriptors.put(archetype, id);
                }
            }
        }
        return descriptors;
    }

    /**
     * Removes an archetype.
     *
     * @param archetype  the archetype
     * @param connection the connection
     * @throws SQLException for any SQL error
     */
    public void remove(String archetype, Connection connection) throws SQLException {
        Map<String, Long> archetypes = getArchetypes(connection);
        Long id = archetypes.get(archetype);
        if (id != null) {
            remove(id, connection);
        }
    }

    /**
     * Removes an archetype.
     *
     * @param id         the archetype descriptor identifier
     * @param connection the database connection
     * @throws SQLException for any SQL error
     */
    public void remove(long id, Connection connection) throws SQLException {
        // remove node descriptors first
        String nodeSQL = "SELECT node_desc_id FROM node_descriptors WHERE archetype_desc_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(nodeSQL)) {
            statement.setLong(1, id);
            removeNodeDescriptors(statement, connection);
        }

        // remove the archetype descriptor
        String archetypeSQL = "DELETE a FROM archetype_descriptors a WHERE a.archetype_desc_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(archetypeSQL)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        }
    }

    /**
     * Removes node descriptors matching a statement.
     *
     * @param statement  the statement selecting node descriptor ids
     * @param connection the database connection
     * @throws SQLException for any SQL error
     */
    private void removeNodeDescriptors(PreparedStatement statement, Connection connection) throws SQLException {
        try (ResultSet set = statement.executeQuery()) {
            while (set.next()) {
                removeNodeDescriptor(set.getLong(1), connection);
            }
        }
    }

    /**
     * Removes a node descriptor.
     * <p/>
     * This is called recursively to delete any child node descriptors first.
     *
     * @param id         the node descriptor id
     * @param connection the database connection
     * @throws SQLException for any SQL error
     */
    private void removeNodeDescriptor(long id, Connection connection) throws SQLException {
        // remove child node descriptors
        String childSQL = "SELECT node_desc_id FROM node_descriptors WHERE parent_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(childSQL)) {
            statement.setLong(1, id);
            removeNodeDescriptors(statement, connection);
        }

        // remove assertion descriptors for the node
        removeAssertionDescriptors(id, connection);

        // remove the node descriptor
        String deleteSQL = "DELETE n FROM node_descriptors n WHERE node_desc_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(deleteSQL)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        }
    }

    /**
     * Removes all assertion descriptors for a node descriptor.
     *
     * @param id         the node descriptor id
     * @param connection the database connection
     * @throws SQLException for any SQL error
     */
    private void removeAssertionDescriptors(long id, Connection connection) throws SQLException {
        String sql = "DELETE ad FROM assertion_descriptors ad WHERE node_desc_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        }
    }
}