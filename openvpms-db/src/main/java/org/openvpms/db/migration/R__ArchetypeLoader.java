/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration;

import org.flywaydb.core.api.migration.MigrationChecksumProvider;
import org.flywaydb.core.api.migration.jdbc.BaseJdbcMigration;
import org.openvpms.db.service.ArchetypeMigrator;
import org.openvpms.db.service.Migrator;

import java.sql.Connection;

/**
 * A repeatable migration to load archetypes.
 * <p/>
 * This loads archetypes if their checksum has changed since they were last loaded.
 * <p/>
 * NOTE: actual loading is performed by the {@link ArchetypeMigrator} registered via {@link #setMigrator}. This is
 * required as Flyway doesn't appear to provide a way to pass beans to Java migrations.
 * <p/>
 * If no loader is registered, migration will fail.
 * <p/>
 * This class is not thread-safe.
 *
 * @author Tim Anderson
 * @see ArchetypeChecksumMojo
 */
public class R__ArchetypeLoader extends BaseJdbcMigration implements MigrationChecksumProvider {

    /**
     * Singleton command used to perform the load.
     */
    private static Migrator migrator;

    /**
     * /**
     * Constructs a {@link R__ArchetypeLoader}.
     */
    public R__ArchetypeLoader() {
        super();
    }

    /**
     * Computes the checksum of the migration.
     *
     * @return The checksum of the migration.
     */
    @Override
    public Integer getChecksum() {
        return getMigrator().getChecksum();
    }

    /**
     * Executes this migration. The execution will automatically take place within a transaction, when the underlying
     * database supports it.
     *
     * @param connection The connection to use to execute statements.
     * @throws Exception when the migration failed.
     */
    @Override
    public void migrate(Connection connection) throws Exception {
        getMigrator().migrate();
    }

    /**
     * Registers a migrator to migrate archetypes.
     *
     * @param migrator the migrator. May be {@code null}
     */
    public static void setMigrator(Migrator migrator) {
        R__ArchetypeLoader.migrator = migrator;
    }

    /**
     * Returns the migrator.
     *
     * @return the migrator
     * @throws IllegalStateException if the migrator hasn't been registered
     */
    private Migrator getMigrator() {
        if (migrator == null) {
            throw new IllegalStateException("Archetype Migrator has not been registered");
        }
        return migrator;
    }
}
