package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.api.migration.jdbc.BaseJdbcMigration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Encrypts user passwords for OVPMS-428.
 *
 * @author Tim Anderson
 */
public class V2_1_0_12__OVPMS_428 extends BaseJdbcMigration {

    /**
     * Executes this migration. The execution will automatically take place within a transaction, when the underlying
     * database supports it.
     *
     * @param connection The connection to use to execute statements.
     * @throws Exception when the migration failed.
     */
    @Override
    public void migrate(Connection connection) throws Exception {
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        try (PreparedStatement select = connection.prepareStatement("SELECT user_id, password FROM users");
             PreparedStatement update = connection.prepareStatement("UPDATE users SET password=? WHERE user_id=?")) {
            try (ResultSet resultSet = select.executeQuery()) {
                while (resultSet.next()) {
                    long id = resultSet.getLong(1);
                    String password = resultSet.getString(2);
                    if (password != null && password.length() > 0) {
                        String encoded = encoder.encode(password);
                        update.setString(1, encoded);
                        update.setLong(2, id);
                        update.executeUpdate();
                    }
                }
            }
        }
    }
}
