/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service;

/**
 * Database version information.
 *
 * @author Tim Anderson
 */
public class DbVersionInfo {

    /**
     * The current version. May be {@code null}
     */
    private final DbVersion version;

    /**
     * The version that needs to be migrated to. May be {@code null}
     */
    private final DbVersion migrationVersion;

    /**
     * The number of changes that need to be applied to bring the database up-to-date.
     */
    private final int changes;

    /**
     * The number of changes that have been applied that aren't in the current codebase.
     */
    private final int futureChanges;

    /**
     * Constructs a {@link DbVersionInfo}.
     *
     * @param version   the current version. May be {@code null}
     * @param migrationVersion the version that needs to be migrated to. May be {@code null}
     * @param changes          the number of changes that need to be applied to bring the database up-to-date
     * @param futureChanges    the number of changes that have been applied that aren't in the current codebase
     */
    public DbVersionInfo(DbVersion version, DbVersion migrationVersion, int changes, int futureChanges) {
        this.version = version;
        this.migrationVersion = migrationVersion;
        this.changes = changes;
        this.futureChanges = futureChanges;
    }

    /**
     * Returns the current database version.
     *
     * @return the current database version, or {@code null} if it is not known
     */
    public DbVersion getVersion() {
        return version;
    }

    /**
     * Returns the version that the database needs to be migrated to, if it is out of date.
     * <p/>
     * If {@link #needsUpdate()} returns {@code true}, but no version is returned by this method, a meta-data update
     * may need to be run.
     *
     * @return the version to migrate to, or {@code null} if the database doesn't need migration
     */
    public DbVersion getMigrationVersion() {
        return migrationVersion;
    }

    /**
     * Determines if the database needs to be updated.
     *
     * @return {@code true} if the database needs to be updated, otherwise {@code false}
     */
    public boolean needsUpdate() {
        return getChanges() > 0;
    }

    /**
     * Returns the number of migration steps that need to be applied to bring the database up-to-date.
     *
     * @return the number of migration steps, or {@code 0} if no update is required
     */
    public int getChanges() {
        return changes;
    }

    /**
     * Determines if the database has been updated to a version that isn't available in the current codebase.
     *
     * @return {@code true} if the database has been updated to a future version, otherwise {@code false}
     */
    public boolean hasFutureVersion() {
        return getFutureChanges() > 0;
    }

    /**
     * Returns the number of changes that have been applied beyond those available in the current codebase.
     *
     * @return the number of future changes, or {@code 0} if there are none
     */
    public int getFutureChanges() {
        return futureChanges;
    }
}