/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.callback.FlywayCallback;
import org.openvpms.db.service.Credentials;

import javax.sql.DataSource;

/**
 * Creates {@link Flyway} instances.
 *
 * @author Tim Anderson
 */
class FlywayFactory {

    /**
     * Creates a new {@link Flyway} instance.
     *
     * @param driver   the database driver
     * @param url      the database URL
     * @param user     the user
     * @param listener  the listener for migration events. May be {@code null}
     * @return a new instance
     */
    public static Flyway create(String driver, String url, Credentials user, FlywayCallback listener) {
        DataSource dataSource = DataSourceFactory.createDataSource(driver, url, user);
        return create(dataSource, listener);
    }

    /**
     * Creates a new {@link Flyway} instance.
     *
     * @param dataSource the data source
     * @return a new instance
     */
    public static Flyway create(DataSource dataSource) {
        return create(dataSource, null);
    }

    /**
     * Creates a new {@link Flyway} instance.
     *
     * @param dataSource the data source
     * @param listener  the listener for migration events. May be {@code null}
     * @return a new instance
     */
    public static Flyway create(DataSource dataSource, FlywayCallback listener) {
        Flyway flyway = new Flyway();
        flyway.setLocations("org/openvpms/db/migration");
        flyway.setDataSource(dataSource);
        if (listener != null) {
            flyway.setCallbacks(listener);
        }
        return flyway;
    }
}