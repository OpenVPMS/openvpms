/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service;

/**
 * Performs migration.
 * <p/>
 * The checksum returned by {@link #getChecksum()} is compared with the persistent checksum; if they are different,
 * {@link #migrate()} will be invoked to perform migration.
 *
 * @author Tim Anderson
 */
public interface Migrator {

    /**
     * Returns the checksum.
     *
     * @return the checksum. May be {@code null}
     */
    Integer getChecksum();

    /**
     * Performs the migration.
     */
    void migrate();
}
