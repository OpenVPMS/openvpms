/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.StringUtils;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationVersion;
import org.flywaydb.core.api.callback.FlywayCallback;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.Schema;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.dbsupport.Table;
import org.flywaydb.core.internal.util.PlaceholderReplacer;
import org.flywaydb.core.internal.util.scanner.Resource;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.system.common.crypto.DefaultPasswordEncryptorFactory;
import org.openvpms.component.system.common.crypto.PasswordEncryptorFactory;
import org.openvpms.db.migration.R__ArchetypeLoader;
import org.openvpms.db.migration.R__PluginLoader;
import org.openvpms.db.service.ArchetypeMigrator;
import org.openvpms.db.service.Checksums;
import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseAdminService;
import org.openvpms.db.service.DbVersionInfo;
import org.openvpms.db.service.PluginMigrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of the {@link DatabaseAdminService}.
 * <p/>
 * This must be constructed with a database user that has admin rights.
 *
 * @author Tim Anderson
 */
public class DatabaseAdminServiceImpl extends AbstractDatabaseService implements DatabaseAdminService {

    /**
     * The database user. Must have admin rights in order to run migration scripts.
     */
    private final Credentials adminUser;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DatabaseAdminService.class);

    /**
     * Constructs a {@link DatabaseAdminServiceImpl}.
     *
     * @param driver    the driver class name
     * @param url       the driver url
     * @param adminUser the database administrator
     */
    public DatabaseAdminServiceImpl(String driver, String url, Credentials adminUser) {
        this(driver, url, adminUser, new ChecksumsImpl());
    }

    /**
     * Constructs a {@link DatabaseAdminServiceImpl}.
     *
     * @param driver    the driver class name
     * @param url       the driver url
     * @param adminUser the database administrator
     * @param listener  the listener for migration events. May be {@code null}
     */
    public DatabaseAdminServiceImpl(String driver, String url, Credentials adminUser, FlywayCallback listener) {
        this(driver, url, adminUser, new ChecksumsImpl(), listener);
    }

    /**
     * Constructs a {@link DatabaseAdminServiceImpl}.
     *
     * @param driver    the driver class name
     * @param url       the driver url
     * @param adminUser the database administrator
     * @param checksums determines the archetype and plugin checksums
     */
    public DatabaseAdminServiceImpl(String driver, String url, Credentials adminUser, Checksums checksums) {
        this(driver, url, adminUser, checksums, null);
    }

    /**
     * Constructs a {@link DatabaseAdminServiceImpl}.
     *
     * @param driver    the driver class name
     * @param url       the driver url
     * @param adminUser the database administrator
     * @param checksums determines the archetype and plugin checksums
     * @param listener  the listener for migration events. May be {@code null}
     */
    public DatabaseAdminServiceImpl(String driver, String url, Credentials adminUser, Checksums checksums,
                                    FlywayCallback listener) {
        super(driver, url, FlywayFactory.create(driver, url, adminUser, listener), checksums);
        this.adminUser = adminUser;
    }

    /**
     * Determines if the database exists.
     *
     * @return {@code true} if the database exists, otherwise {@code false}
     * @throws SQLException for any SQL error
     */
    @Override
    public boolean exists() throws SQLException {
        try (BasicDataSource rootAdmin = createDataSource(getRootURL(), adminUser);
             Connection connection = rootAdmin.getConnection()) {
            return exists(connection);
        }
    }

    /**
     * Creates the database.
     *
     * @param user         credentials of the user with read/write access
     * @param readOnlyUser credentials of the user with read-only access
     * @param host         the host the users are connecting from
     * @param createTables if {@code true}, create the tables, and base-line
     * @throws SQLException for any SQL error
     */
    @Override
    public void create(Credentials user, Credentials readOnlyUser, String host, boolean createTables)
            throws SQLException {
        checkUsers(user, readOnlyUser);
        try (BasicDataSource rootAdmin = createDataSource(getRootURL(), adminUser);
             Connection connection = rootAdmin.getConnection()) {
            checkPermissions(connection);
            boolean found = exists(connection);
            if (!found) {
                DbSupport support = DbSupportFactory.createDbSupport(connection, true);
                createDatabase(support);
                createUsers(user, readOnlyUser, host, support);
            } else if (!createTables) {
                throw new SQLException("Cannot create " + getSchemaName() + " as it already exists");
            }
        }
        if (createTables) {
            // register no-op migrators, to avoid loading archetypes and plugins
            R__ArchetypeLoader.setMigrator(new NoOpMigrator());
            R__PluginLoader.setMigrator(new NoOpMigrator());
            try (Connection connection = getDataSource().getConnection()) {
                DbSupport support = DbSupportFactory.createDbSupport(connection, true);
                Schema<?> schema = support.getOriginalSchema();
                if (schema.allTables().length == 0) {
                    Resource resource = getResource("org/openvpms/db/schema/schema.sql");
                    SqlScript script = new SqlScript(resource.loadAsString("UTF-8"), support);
                    script.execute(support.getJdbcTemplate());
                    MigrationInfo version = getBaselineVersion();
                    if (version != null) {
                        baseline(version.getVersion(), version.getDescription());
                    }
                } else {
                    throw new SQLException("Cannot create " + getSchemaName()
                                           + " as there are tables already present");
                }
            } finally {
                resetMigrators();
            }
        }
    }

    /**
     * Creates the users.
     * <p/>
     * If the users already exist, they will be recreated.
     *
     * @param user         credentials of the user with read/write access
     * @param readOnlyUser credentials of the user with read-only access
     * @param host         the host the users are connecting from
     * @throws SQLException for any SQL error
     */
    @Override
    public void createUsers(Credentials user, Credentials readOnlyUser, String host) throws SQLException {
        checkUsers(user, readOnlyUser);
        try (Connection connection = getDataSource().getConnection()) {
            checkPermissions(connection);
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);
            createUsers(user, readOnlyUser, host, support);
        }
    }

    /**
     * Updates the database to the latest version.
     *
     * @param archetypeMigrator the archetype migrator, or {@code null} if archetypes aren't being updated
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being updated
     * @throws SQLException for any error
     */
    @Override
    public void update(ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator) throws SQLException {
        update(null, archetypeMigrator, pluginMigrator);
    }

    /**
     * Updates the database to a specific version.
     *
     * @param version           the migration version
     * @param archetypeMigrator the archetype migrator, or {@code null} if archetypes aren't being updated
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being updated
     * @throws SQLException for any error
     */
    @Override
    public void update(String version, ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator)
            throws SQLException {
        checkPreconditions();
        baseline();
        if (needsChecksumUpdate(getInfo())) {
            doRepair(); // this will repair all checksums
        }
        try {
            R__ArchetypeLoader.setMigrator(archetypeMigrator != null ? getArchetypeMigrator(archetypeMigrator)
                                                                     : new NoOpMigrator());
            R__PluginLoader.setMigrator(pluginMigrator != null ? getPluginMigrator(pluginMigrator) :
                                        new NoOpMigrator());
            Flyway flyway = getFlyway();
            if (version != null) {
                flyway.setTargetAsString(version);
            }
            flyway.migrate();
        } finally {
            resetMigrators();
        }
    }

    /**
     * Repairs the Flyway metadata table. This will perform the following actions:
     * <ul>
     * <li>Remove any failed migrations on databases without DDL transactions (User objects left behind must still be
     * cleaned up manually)</li>
     * <li>Correct wrong checksums</li>
     * </ul>
     *
     * @throws SQLException    if the admin user has insufficient permissions
     * @throws FlywayException for if the repair fails
     */
    public void repair() throws SQLException {
        checkPermissions();
        doRepair();
    }

    /**
     * Returns the schema version.
     *
     * @param schema the schema
     * @return the schema version, or {@code null} if it cannot be determined
     */
    protected String getExistingVersion(Schema<?> schema) {
        Table acts = schema.getTable("acts");
        if (acts != null && acts.hasColumn("status2")) {
            return "1.9";
        }
        return null;
    }

    /**
     * Verifies that strong password encryption is supported.
     *
     * @throws RuntimeException if strong password encryption is not supported
     */
    protected void checkPasswordEncryptionSupport() {
        PasswordEncryptorFactory factory
                = new DefaultPasswordEncryptorFactory("cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        // NOTE: the above key is not used for anything other than testing
        PasswordEncryptor encryptor = factory.create();
        encryptor.encrypt("dummy");
    }

    /**
     * Repairs the Flyway metadata table. This will perform the following actions:
     * <ul>
     * <li>Remove any failed migrations on databases without DDL transactions (User objects left behind must still be
     * cleaned up manually)</li>
     * <li>Correct wrong checksums</li>
     * </ul>
     *
     * @throws FlywayException for if the repair fails
     */
    private void doRepair() {
        try {
            installDisabledMigrators();
            getFlyway().repair();
        } finally {
            resetMigrators();
        }
    }

    /**
     * Verifies the admin user, user and read-only user are all different.
     *
     * @param user         the user
     * @param readOnlyUser the read-only user
     * @throws SQLException if the one or more users are the same
     */
    private void checkUsers(Credentials user, Credentials readOnlyUser) throws SQLException {
        if (adminUser.getUser().equals(user.getUser())) {
            throw new SQLException("Admin user cannot be the same as read/write user");
        }
        if (adminUser.getUser().equals(readOnlyUser.getUser())) {
            throw new SQLException("Admin user cannot be the same as read-only user");
        }
        if (user.getUser().equals(readOnlyUser.getUser())) {
            throw new SQLException("Read/write and read-only users cannot be the same");
        }
    }

    /**
     * Verifies that the admin user has sufficient permissions to administer the database.
     *
     * @throws SQLException if the user doesn't have sufficient permissions
     */
    private void checkPermissions() throws SQLException {
        try (Connection connection = getDataSource().getConnection()) {
            checkPermissions(connection);
        }
    }

    /**
     * Verifies that the admin user has sufficient permissions to administer the database.
     *
     * @param connection the SQL connection
     * @throws SQLException if the user doesn't have sufficient permissions
     */
    private void checkPermissions(Connection connection) throws SQLException {
        String sql = "SELECT 1 FROM mysql.user " +
                     "WHERE user = ? " +
                     "AND (host = ? OR host = '%') " +
                     "AND select_priv = 'Y' " +
                     "AND update_priv = 'Y' " +
                     "AND delete_priv = 'Y' " +
                     "AND create_priv = 'Y' " +
                     "AND drop_priv = 'Y' " +
                     "AND grant_priv = 'Y' " +
                     "AND index_priv = 'Y' " +
                     "AND alter_priv = 'Y' " +
                     "AND execute_priv = 'Y' " +
                     "AND create_routine_priv = 'Y' " +
                     "AND create_user_priv = 'Y'";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            boolean failed = false;
            String userHost = getUserHost(connection);
            statement.setString(1, StringUtils.substringBefore(userHost, "@"));
            statement.setString(2, StringUtils.substringAfter(userHost, "@"));

            // NOTE: the following query fails on MariaDB using Connector/J
            try (ResultSet set = statement.executeQuery()) {
                if (!set.next()) {
                    failed = true;
                }
            } catch (SQLException exception) {
                log.error("Failed to query mysql.user: {}", exception.getMessage(), exception);
                failed = true;
            }
            if (failed) {
                throw new SQLException("User " + userHost
                                       + " doesn't have enough permissions to administer the database");
            }
        }
    }

    /**
     * Determines the current user associated with a connection.
     *
     * @param connection the JDBC connection
     * @return the connection user
     */
    private String getUserHost(Connection connection) throws SQLException {
        String result;
        try (Statement statement = connection.createStatement();
             ResultSet set = statement.executeQuery("SELECT CURRENT_USER() AS user")) {
            if (set.next()) {
                result = set.getString(1);
            } else {
                throw new SQLException("Cannot determine connection user");
            }
        }
        return result;
    }

    /**
     * Creates the users.
     *
     * @param user         credentials of the user with read/write access
     * @param readOnlyUser credentials of the user with read-only access
     * @param host         the host the users are connecting from
     * @param support      Flyway database support
     */
    private void createUsers(Credentials user, Credentials readOnlyUser, String host, DbSupport support) {
        createUser(user, host, support);
        createReadOnlyUser(support, readOnlyUser, host);
    }

    /**
     * Creates the database.
     *
     * @param support Flyway database support
     */
    private void createDatabase(DbSupport support) {
        Map<String, String> placeholders = new HashMap<>();
        placeholders.put("database", getSchemaName());
        runScript("org/openvpms/db/schema/database.sql", placeholders, support);
    }

    /**
     * Creates the read/write user.
     *
     * @param user    the user
     * @param host    the host the user is connecting from
     * @param support Flyway database support
     */
    private void createUser(Credentials user, String host, DbSupport support) {
        createUser(user, host, "org/openvpms/db/schema/user.sql", support);
    }

    /**
     * Creates the read-only user.
     *
     * @param user    the user
     * @param host    the host the user is connecting from
     * @param support Flyway database support
     */
    private void createReadOnlyUser(DbSupport support, Credentials user, String host) {
        createUser(user, host, "org/openvpms/db/schema/readonlyuser.sql", support);
    }

    /**
     * Creates a user.
     *
     * @param user    the user
     * @param host    the host the user is connecting from
     * @param path    the resource path to the script
     * @param support Flyway database support
     */
    private void createUser(Credentials user, String host, String path, DbSupport support) {
        Map<String, String> placeholders = new HashMap<>();
        placeholders.put("database", getSchemaName());
        placeholders.put("user", user.getUser());
        placeholders.put("password", user.getPassword());
        placeholders.put("host", host);
        runScript(path, placeholders, support);
    }

    /**
     * Runs an SQL script.
     *
     * @param path         the resource path to the script
     * @param placeholders script placeholders
     * @param support      Flyway database support
     */
    private void runScript(String path, Map<String, String> placeholders, DbSupport support) {
        Resource resource = getResource(path);
        SqlScript script = new SqlScript(support, resource, new PlaceholderReplacer(placeholders, "${", "}"),
                                         StandardCharsets.UTF_8.name(), false);
        script.execute(support.getJdbcTemplate());
    }

    /**
     * Determines if the schema exists.
     *
     * @param connection the database connection
     * @return {@code true} if the schema exists
     * @throws SQLException for any error
     */
    private boolean exists(Connection connection) throws SQLException {
        String schemaName = getSchemaName();
        boolean found = false;
        DatabaseMetaData metaData = connection.getMetaData();
        try (ResultSet set = metaData.getCatalogs()) {
            while (set.next()) {
                String schema = set.getString("TABLE_CAT");
                if (schemaName.equalsIgnoreCase(schema)) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    /**
     * Verifies that preconditions are met before performing a database update.
     *
     * @throws SQLException for any error
     */
    private void checkPreconditions() throws SQLException {
        checkFutureVersion();
        // verify strong encryption is supported. This is required by the migration for OVPMS-2233.
        try {
            checkPasswordEncryptionSupport();
        } catch (Exception exception) {
            throw new SQLException(
                    "Unable to perform database migration. Strong password encryption is not supported.\n" +
                    "Please update to a newer version of Java.", exception);
        }
        checkPermissions();
    }

    /**
     * Determines if a future version has been applied to the database.
     *
     * @throws SQLException if a future version has been applied
     */
    private void checkFutureVersion() throws SQLException {
        DbVersionInfo info = getVersionInfo();
        if (info.hasFutureVersion()) {
            throw new SQLException("A future database version has been applied. The database is "
                                   + info.getFutureChanges() + " version ahead of the current release.");
        }
    }

    /**
     * Helper to create a resource.
     *
     * @param path the resource path
     * @return a new resource
     */
    private Resource getResource(String path) {
        return new ClassPathResource(path, getClass().getClassLoader());
    }

    /**
     * Baselines the database, if there are existing tables.
     *
     * @throws SQLException for any error
     */
    private void baseline() throws SQLException {
        MigrationInfo current = getInfo().current();
        if (current == null) {
            // no schema version information
            try (Connection connection = getDataSource().getConnection()) {
                DbSupport support = DbSupportFactory.createDbSupport(connection, true);
                Schema<?> schema = support.getOriginalSchema();
                if (schema.allTables().length != 0) {
                    // there are tables in the db. Make sure they belong to the most recent version
                    String existing = getExistingVersion(schema);
                    if (existing != null) {
                        // pre-existing schema. Don't want to create it again
                        baseline(MigrationVersion.fromVersion(existing), "Initial schema");
                    } else {
                        throw new SQLException("This database needs to be manually migrated to OpenVPMS 1.9");
                    }
                }
            }
        }
    }

    /**
     * <p>Baselines an existing database, excluding all migrations up to and including version.</p>
     *
     * @param version     the version
     * @param description the description
     * @throws FlywayException if the schema baselining failed
     */
    private void baseline(MigrationVersion version, String description) {
        Flyway flyway = getFlyway();
        flyway.setBaselineVersion(version);
        flyway.setBaselineDescription(description);
        flyway.baseline();
    }
}