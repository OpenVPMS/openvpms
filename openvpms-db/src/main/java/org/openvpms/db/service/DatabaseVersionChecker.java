/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service;

import java.sql.SQLException;

/**
 * Checks that the database has been updated to the latest version.
 *
 * @author Tim Anderson
 */
public class DatabaseVersionChecker {

    /**
     * Default constructor.
     */
    public DatabaseVersionChecker() {
        super();
    }

    /**
     * Constructs a {@link DatabaseVersionChecker}.
     *
     * @param service the database service
     * @throws SQLException if the database has not be updated to the latest version
     */
    public DatabaseVersionChecker(DatabaseService service) throws SQLException {
        check(service);
    }

    /**
     * Verifies that a database has been updated to the latest version.
     *
     * @param service the database service
     * @throws SQLException if the database has not been updated to the latest version, or is ahead of the current
     *                      codebase
     */
    public void check(DatabaseService service) throws SQLException {
        DbVersionInfo info = service.getVersionInfo();
        if (info.needsUpdate()) {
            int changes = info.getChanges();
            DbVersion version = info.getMigrationVersion();
            if (version.getVersion() != null) {
                throw new SQLException("The database needs to be updated to version " + version
                                       + ". " + changes + " changes need to be applied.");
            } else {
                // checksum(s) need to be updated or archetypes need to be loaded
                throw new SQLException("The database needs to be updated. " + changes + " changes need to be applied.");
            }
        } else if (info.hasFutureVersion()) {
            throw new SQLException("A future database version has been applied. The database is "
                                   + info.getFutureChanges() + " versions ahead of the current release.");
        }
    }

}
