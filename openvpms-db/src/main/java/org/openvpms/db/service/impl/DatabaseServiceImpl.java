/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.openvpms.db.service.Checksums;
import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

/**
 * Default implementation of the {@link DatabaseService}.
 *
 * @author Tim Anderson
 */
public class DatabaseServiceImpl extends AbstractDatabaseService {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DatabaseServiceImpl.class);

    /**
     * Constructs a {@link DatabaseServiceImpl}.
     *
     * @param driver the driver class name
     * @param url    the driver url
     * @param user   the JDBC user credentials
     */
    public DatabaseServiceImpl(String driver, String url, Credentials user) {
        this(driver, url, createDataSource(driver, url, user), new ChecksumsImpl());
    }

    /**
     * Constructs a {@link DatabaseServiceImpl}.
     *
     * @param driver     the driver class name
     * @param url        the driver url
     * @param dataSource the data source
     */
    public DatabaseServiceImpl(String driver, String url, DataSource dataSource) {
        this(driver, url, dataSource, new ChecksumsImpl());
    }

    /**
     * Constructs a {@link DatabaseServiceImpl}.
     *
     * @param driver     the driver class name
     * @param url        the driver url
     * @param dataSource the data source
     * @param checksums  determines the archetype and plugin checksums
     */
    public DatabaseServiceImpl(String driver, String url, DataSource dataSource, Checksums checksums) {
        super(driver, url, FlywayFactory.create(dataSource), checksums);
        log.info("Using database {}, URL={}", getSchemaName(), url);
    }


}
