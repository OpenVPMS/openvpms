/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openvpms.db.service.ArchetypeChecksum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Objects;
import java.util.Properties;

/**
 * Default implementation of {@link ArchetypeChecksum}.
 * <p/>
 * The checksum is determined by the presence of META-INF/openvpms-archetypes.properties resources
 * containing a checksum.default or checksum.override property indicating the archetype checksum. <br/>
 * Only one of each may be present; the checksum.override will be used in preference to the checksum.default.
 *
 * @author Tim Anderson
 */
public class ArchetypeChecksumImpl implements ArchetypeChecksum {

    /**
     * The resources path.
     */
    private final String path;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ArchetypeChecksumImpl.class);

    /**
     * The properties resource path.
     */
    private static final String PROPERTIES = "META-INF/openvpms-archetypes.properties";

    /**
     * Default constructor.
     */
    public ArchetypeChecksumImpl() {
        this(PROPERTIES);
    }

    /**
     * Constructs an {@link ArchetypeChecksumImpl}.
     *
     * @param path the resource path
     */
    public ArchetypeChecksumImpl(String path) {
        this.path = path;
    }

    /**
     * Returns the archetype checksum.
     *
     * @return the archetype checksum
     */
    @Override
    public Integer getChecksum() {
        Pair<Integer, URL> result;
        MutableObject<Pair<Integer, URL>> system = new MutableObject<>();
        MutableObject<Pair<Integer, URL>> override = new MutableObject<>();
        try {
            File file = new File(path);
            if (file.exists()) {
                URL url = file.toURI().toURL();
                getChecksums(url, system, override);
            } else {
                Enumeration<URL> resources = getClass().getClassLoader().getResources(path);
                while (resources.hasMoreElements()) {
                    URL url = resources.nextElement();
                    getChecksums(url, system, override);
                }
            }
        } catch (IOException exception) {
            throw new IllegalStateException("Failed to read " + path, exception);
        }
        if (system.getValue() == null && override.getValue() == null) {
            throw new IllegalStateException("No checksums found");
        }
        result = (override.getValue() != null) ? override.getValue() : system.getValue();
        log.info("Using checksum={} from {}", result.getLeft(), result.getRight());
        return result.getLeft();
    }

    /**
     * Loads checksums from the specified URL.
     *
     * @param url      the url
     * @param system   the system checksum
     * @param override the override checksum
     */
    private void getChecksums(URL url, MutableObject<Pair<Integer, URL>> system,
                              MutableObject<Pair<Integer, URL>> override) {
        try (InputStream stream = url.openStream()) {
            Properties properties = new Properties();
            properties.load(stream);
            getChecksum("checksum.default", properties, url, system);
            getChecksum("checksum.override", properties, url, override);
        } catch (IOException exception) {
            throw new IllegalStateException("Failed to read " + url, exception);
        }
    }

    /**
     * Loads a checksum if it exists.
     *
     * @param name       the checksum property name
     * @param properties the properties
     * @param url        the URL the properties were read from
     * @param checksum   the checksum pair
     */
    private void getChecksum(String name, Properties properties, URL url, MutableObject<Pair<Integer, URL>> checksum) {
        Pair<Integer, URL> found = getChecksum(name, properties, url, checksum.getValue());
        if (found != null) {
            checksum.setValue(found);
        }
    }

    /**
     * Returns a checksum if it exists.
     *
     * @param name       the checksum property name
     * @param properties the properties
     * @param url        the URL the properties were read from
     * @param existing   the existing checksum. May be {@code null}
     * @return the checksum and URL. May be {@code null}
     */
    private Pair<Integer, URL> getChecksum(String name, Properties properties, URL url, Pair<Integer, URL> existing) {
        Pair<Integer, URL> result = null;
        Integer value = getChecksum(name, properties);
        if (value != null) {
            if (existing == null) {
                result = new ImmutablePair<>(value, url);
            } else if (Objects.equals(existing.getLeft(), value)) {
                // ignore resources where they have the same value. This can occur with the jetty-maven-plugin
                // duplicating jars on the classpath when overlays are used
                result = existing;
            } else {
                throw new IllegalStateException("Duplicate " + name + " property defined in " + existing.getRight()
                                                + " and " + url);
            }
        }
        return result;
    }

    /**
     * Returns a checksum property.
     *
     * @param name       the property name
     * @param properties the properties
     * @return the checksum or {@code null} if none exists
     */
    private Integer getChecksum(String name, Properties properties) {
        String value = properties.getProperty(name);
        return value != null ? Integer.parseUnsignedInt(value, 16) : null;
    }
}