/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.apache.commons.dbcp2.BasicDataSource;
import org.openvpms.db.service.Credentials;

import javax.sql.DataSource;

/**
 * Factory for {@link DataSource}s.
 *
 * @author Tim Anderson
 */
class DataSourceFactory {
    /**
     * Creates a new data source.
     *
     * @param driver the driver class name
     * @param url    the database URL
     * @param user   the user
     * @return a new data source
     */
    public static BasicDataSource createDataSource(String driver, String url, Credentials user) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user.getUser());
        dataSource.setPassword(user.getPassword());
        return dataSource;
    }
}