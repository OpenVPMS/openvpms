/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service;

/**
 * Database credentials.
 *
 * @author Tim Anderson
 */
public class Credentials {

    /**
     * The username.
     */
    private final String user;

    /**
     * The user's password.
     */
    private final String password;

    /**
     * Constructs a {@link Credentials}.
     *
     * @param user     the username
     * @param password the user's password
     */
    public Credentials(String user, String password) {
        this.user = user;
        this.password = password;
    }

    /**
     * Returns the username.
     *
     * @return the username
     */
    public String getUser() {
        return user;
    }

    /**
     * Returns the user's password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }
}
