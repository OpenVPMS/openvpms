/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.apache.commons.dbcp2.BasicDataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationInfoService;
import org.flywaydb.core.internal.info.MigrationInfoServiceImpl;
import org.openvpms.db.migration.R__ArchetypeLoader;
import org.openvpms.db.migration.R__PluginLoader;
import org.openvpms.db.service.ArchetypeMigrator;
import org.openvpms.db.service.Checksums;
import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseService;
import org.openvpms.db.service.DbVersion;
import org.openvpms.db.service.DbVersionInfo;
import org.openvpms.db.service.Migrator;
import org.openvpms.db.service.PluginMigrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Abstract implementation of {@link DatabaseService}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDatabaseService implements DatabaseService {

    /**
     * The database driver class name.
     */
    private final String driver;

    /**
     * The root database URL.
     */
    private final String rootURL;

    /**
     * The database schema name.
     */
    private final String schemaName;

    /**
     * Flyway.
     */
    private final Flyway flyway;

    /**
     * Changed checksums, keyed on version.
     */
    private final Map<String, Integer> changedChecksums;

    /**
     * The archetype and plugin checksums.
     */
    private final Checksums checksums;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractDatabaseService.class);

    /**
     * Constructs an {@link AbstractDatabaseService}.
     *
     * @param driver    the driver class name
     * @param url       the driver url
     * @param flyway    the Flyway instance to get schema info from
     * @param checksums determines the archetype and plugin checksums
     */
    public AbstractDatabaseService(String driver, String url, Flyway flyway, Checksums checksums) {
        this.driver = driver;
        DbURLParser parser = new DbURLParser(url);
        rootURL = parser.getRootUrl();
        schemaName = parser.getSchemaName();
        this.flyway = flyway;
        this.checksums = checksums;
        changedChecksums = getChangedChecksums();
        log.info("Using database {}, URL={}", schemaName, url);
    }

    /**
     * Returns the schema name.
     *
     * @return the schema name
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * Returns the current database version.
     *
     * @return the current database version, or {@code null} if it is not known
     */
    public String getVersion() {
        String result = null;
        MigrationInfo current = getInfo().current();
        if (current != null) {
            result = current.getVersion().toString();
        }
        return result;
    }

    /**
     * Returns the database version information.
     *
     * @return the version information
     */
    @Override
    public DbVersionInfo getVersionInfo() {
        MigrationInfoServiceImpl service = (MigrationInfoServiceImpl) getInfo();
        DbVersion version = toVersionInfo(service.current());
        DbVersion migrationVersion = getMigrationVersion(service);
        int changes = getChangesToApply(service);

        MigrationInfo[] future = service.future();
        int futureChanges = future.length;
        return new DbVersionInfo(version, migrationVersion, changes, futureChanges);
    }

    /**
     * Returns the database size.
     *
     * @return the database size, in bytes
     * @throws SQLException for any SQL error
     */
    @Override
    public long getSize() throws SQLException {
        long result = -1;
        try (Connection connection = getDataSource().getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(
                    "SELECT SUM(data_length + index_length) AS size " +
                    "FROM information_schema.tables " +
                    "WHERE TABLE_SCHEMA = ?")) {
                statement.setString(1, getSchemaName());
                ResultSet set = statement.executeQuery();
                if (set.next()) {
                    result = set.getLong(1);
                }
            }
        }
        return result;
    }

    /**
     * Returns the migration info.
     *
     * @return the migration info
     */
    public MigrationInfoService getInfo() {
        try {
            installDisabledMigrators();
            return flyway.info();
        } finally {
            resetMigrators();
        }
    }

    /**
     * Returns the Flyway service.
     *
     * @return the Flyway service
     */
    protected Flyway getFlyway() {
        return flyway;
    }

    /**
     * Returns the datasource.
     *
     * @return the datasource
     */
    protected DataSource getDataSource() {
        return flyway.getDataSource();
    }

    /**
     * Returns the root database URL. This is the database URL minus the schema name.
     *
     * @return the root URL
     */
    protected String getRootURL() {
        return rootURL;
    }

    /**
     * Creates a new {@link DataSource}.
     *
     * @param url  the database URL
     * @param user the user
     * @return a new data source
     */
    protected BasicDataSource createDataSource(String url, Credentials user) {
        return DataSourceFactory.createDataSource(driver, url, user);
    }

    /**
     * Returns the version to use for when marking the baseline after database creation.
     *
     * @return the version, or {@code null} if there is none
     */
    protected MigrationInfo getBaselineVersion() {
        MigrationInfo result = null;
        MigrationInfo[] info = getInfo().pending();
        int index = info.length - 1;
        while (index >= 0) {
            if (info[index].getVersion() != null) {
                // need to exclude repeatable migrations which have no version
                result = info[index];
                break;
            } else {
                index--;
            }
        }
        return result;
    }

    /**
     * Installs migrators that return checksums but cannot be used for migration.
     */
    protected void installDisabledMigrators() {
        R__ArchetypeLoader.setMigrator(disabledMigrator(checksums::getArchetypeChecksum));
        R__PluginLoader.setMigrator(disabledMigrator(checksums::getPluginChecksum));
    }

    /**
     * Reset the singleton migrators.
     */
    protected void resetMigrators() {
        R__ArchetypeLoader.setMigrator(null);
        R__PluginLoader.setMigrator(null);
    }

    /**
     * Returns a {@link Migrator} to migrate plugins.
     *
     * @param migrator the migrator to delegate to
     * @return a new migrator
     */
    protected Migrator getArchetypeMigrator(ArchetypeMigrator migrator) {
        return new Migrator() {
            @Override
            public Integer getChecksum() {
                return checksums.getArchetypeChecksum();
            }

            @Override
            public void migrate() {
                migrator.migrate();
            }
        };
    }

    /**
     * Returns a {@link Migrator} to migrate plugins.
     *
     * @param migrator the migrator to delegate to
     * @return a new migrator
     */
    protected Migrator getPluginMigrator(PluginMigrator migrator) {
        return new Migrator() {
            @Override
            public Integer getChecksum() {
                return checksums.getPluginChecksum();
            }

            @Override
            public void migrate() {
                migrator.migrate();
            }
        };
    }

    /**
     * Determines if a checksum needs to be updated.
     * <p/>
     * This only evaluates checksums if there are no failed migrations.
     * <p/>
     * This is because invoking {@link DatabaseAdminServiceImpl#repair()} to correct checksums also removes failed
     * migrations, which is not desirable to do on an automatic basis.
     *
     * @param migrationInfo the migration info service
     * @return {@code true} if a checksum needs to be updated, otherwise {@code false}
     */
    protected boolean needsChecksumUpdate(MigrationInfoService migrationInfo) {
        for (MigrationInfo info : migrationInfo.all()) {
            if (info.getState().isFailed()) {
                return false;
            }
        }
        for (MigrationInfo info : migrationInfo.applied()) {
            if (info.getVersion() != null) {
                // need to exclude repeatable migrations, which have no version
                String version = info.getVersion().getVersion();
                Integer newCheckSum = changedChecksums.get(version);
                if (newCheckSum != null && !newCheckSum.equals(info.getChecksum())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Creates a new data source.
     *
     * @param driver the driver class name
     * @param url    the database URL
     * @param user   the user
     * @return a new data source
     */
    protected static DataSource createDataSource(String driver, String url, Credentials user) {
        return createDataSource(driver, url, user.getUser(), user.getPassword());
    }

    /**
     * Creates a new data source.
     *
     * @param driver   the driver class name
     * @param url      the database URL
     * @param user     the user
     * @param password the password
     * @return a new data source
     */
    protected static DataSource createDataSource(String driver, String url, String user, String password) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    /**
     * Returns the most recent version of the database that needs to be updated to.
     *
     * @return the most recent version of the database, or {@code null} if no update is required
     */
    private DbVersion getMigrationVersion(MigrationInfoService service) {
        MigrationInfo[] info = service.pending();
        return toVersionInfo(info.length > 0 ? info[info.length - 1] : null);
    }

    /**
     * Determines the number of changes that need to be applied to update the database to the latest version.
     *
     * @param info the migration info service
     * @return the number of changes to apply, or {@code 0} if no changes are required
     */
    private int getChangesToApply(MigrationInfoServiceImpl info) {
        int result = info.pending().length + info.failed().length;
        if (needsChecksumUpdate(info)) {
            result++;
        }
        return result;
    }

    /**
     * Converts a {@link MigrationInfo} to a {@link DbVersion}.
     *
     * @param info the info to convert. May be {@code null}
     * @return the corresponding {@link DbVersion}. May be {@link null}
     */
    private DbVersion toVersionInfo(MigrationInfo info) {
        DbVersion result = null;
        if (info != null) {
            String version = info.getVersion() != null ? info.getVersion().toString() : null;
            result = new DbVersion(version, info.getDescription());
        }
        return result;
    }

    /**
     * Returns the checksums of any migration that has changed subsequent to being released.
     * <p/>
     * If the migration has been successfully applied, its checksum will need to be updated in order for Flyway
     * to subsequently migrate the database as it aborts with an error if an applied checksum has changed.
     *
     * @return the changed checksums, keyed on migration version
     */
    private Map<String, Integer> getChangedChecksums() {
        Map<String, Integer> result = new HashMap<>();
        result.put("2.1.0.3", -427272242);  // migration for OVPMS-2208
        result.put("2.1.0.5", 229982045);   // migration for OVPMS-2252, OVPMS-2265
        result.put("2.1.0.9", 1610869084);  // migration for OVPMS-2082, updated to allow for longer
        // universalServiceIdentifier
        result.put("2.1.0.11", 1116763144); // migration for OVPMS-428, updated by OVPMS-2354
        result.put("2.1.0.13", 1611424731);  // migration for OVPMS-2241, updated by OBF-269 and OVPMS-2450
        return result;
    }

    /**
     * Creates a migrator that returns the checksum, but cannot be used to perform migration.
     *
     * @return a new migrator
     */
    private Migrator disabledMigrator(Supplier<Integer> checksum) {
        return new Migrator() {
            @Override
            public Integer getChecksum() {
                return checksum.get();
            }

            @Override
            public void migrate() {
                throw new IllegalStateException("Cannot perform migration");
            }
        };
    }
}