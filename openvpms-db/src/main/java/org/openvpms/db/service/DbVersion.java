/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service;

/**
 * Database version.
 *
 * @author Tim Anderson
 */
public class DbVersion {

    /**
     * The version.
     */
    private final String version;

    /**
     * The version description.
     */
    private final String description;

    /**
     * Constructs a {@link DbVersion}.
     *
     * @param version     the version
     * @param description the version description
     */
    public DbVersion(String version, String description) {
        this.version = version;
        this.description = description;
    }

    /**
     * Returns the version.
     *
     * @return the version. If {@code null}, the version is a for a repeatable migration
     */
    public String getVersion() {
        return version;
    }

    /**
     * Returns the version description.
     *
     * @return the version description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the version.
     *
     * @return the version. If {@code null}, the version is a for a repeatable migration
     */
    public String toString() {
        return version;
    }
}