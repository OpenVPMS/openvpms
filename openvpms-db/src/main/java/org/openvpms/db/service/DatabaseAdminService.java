/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service;

import java.sql.SQLException;

/**
 * Provides support to create and update the OpenVPMS database.
 *
 * @author Tim Anderson
 */
public interface DatabaseAdminService extends DatabaseService {

    /**
     * Determines if the database exists.
     *
     * @return {@code true} if the database exists, otherwise {@code false}
     * @throws SQLException for any SQL error
     */
    boolean exists() throws SQLException;

    /**
     * Creates the database.
     *
     * @param user         credentials of the user with read/write access
     * @param readOnlyUser credentials of the user with read-only access
     * @param host         the host the users are connecting from
     * @param createTables if {@code true}, create the tables, and base-line
     * @throws SQLException for any SQL error
     */
    void create(Credentials user, Credentials readOnlyUser, String host, boolean createTables) throws SQLException;

    /**
     * Creates the users.
     * <p/>
     * If the users already exist, they will be recreated.
     *
     * @param user         credentials of the user with read/write access
     * @param readOnlyUser credentials of the user with read-only access
     * @param host         the host the users are connecting from
     * @throws SQLException for any SQL error
     */
    void createUsers(Credentials user, Credentials readOnlyUser, String host) throws SQLException;

    /**
     * Updates the database to the latest version.
     *
     * @param archetypeMigrator the archetype migrator, or {@code null} if archetypes aren't being updated
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being updated
     * @throws SQLException for any error
     */
    void update(ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator) throws SQLException;

    /**
     * Updates the database to a specific version.
     *
     * @param version           the migration version
     * @param archetypeMigrator the archetype migrator, or {@code null} if archetypes aren't being updated
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being updated
     * @throws SQLException for any error
     */
    void update(String version, ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator) throws SQLException;

}