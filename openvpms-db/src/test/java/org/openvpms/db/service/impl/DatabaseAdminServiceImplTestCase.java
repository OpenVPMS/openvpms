/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.db.service.Checksums;
import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import javax.sql.DataSource;
import java.security.InvalidKeyException;
import java.sql.SQLException;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Tests the {@link DatabaseAdminServiceImpl} class.
 *
 * @author Tim Anderson
 */
@ContextConfiguration(locations = {"/applicationContext.xml"})
public class DatabaseAdminServiceImplTestCase extends AbstractJUnit4SpringContextTests {

    /**
     * The JDBC driver.
     */
    @Value("${jdbc.driverClassName}")
    private String driverClassName;

    /**
     * The JDBC url.
     */
    @Value("${jdbc.url}")
    private String url;

    /**
     * The JDBC username.
     */
    @Value("${jdbc.admin.username}")
    private String user;

    /**
     * The JDBC password.
     */
    @Value("${jdbc.admin.password}")
    private String password;

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The checksums.
     */
    private Checksums checksums;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        checksums = new Checksums() {
            @Override
            public Integer getArchetypeChecksum() {
                return null;
            }

            @Override
            public Integer getPluginChecksum() {
                return null;
            }
        };
    }

    /**
     * Verifies that if strong encryption is not available, database migration fails before execution.
     * <p/>
     * Strong encryption is required by the migratio for OVPMS-2233.
     */
    @Test
    public void testUpdateWithoutStrongEncryption() {
        Credentials credentials = new Credentials(user, password);
        DatabaseAdminService service = new DatabaseAdminServiceImpl(driverClassName, url, credentials, checksums) {
            @Override
            protected void checkPasswordEncryptionSupport() {
                super.checkPasswordEncryptionSupport();
                // simulate the JCE -> Spring exception chain
                throw new IllegalArgumentException("Unable to intialize due to invalid secret key",
                                                   new InvalidKeyException("Illegal key size"));
            }
        };
        try {
            service.update(null, null);
            fail("Expected update to fail");
        } catch (SQLException expected) {
            assertEquals("Unable to perform database migration. Strong password encryption is not supported.\n" +
                         "Please update to a newer version of Java.", expected.getMessage());
        }
    }

    /**
     * Verifies that updating is not possible if the database is ahead of the current codebase.
     */
    @Test
    public void testUpdateWithFutureVersion() {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        Integer installedRank = template.queryForObject("SELECT MAX(installed_rank) + 1 AS installed_rank " +
                                                        "FROM schema_version", Integer.class);
        assertNotNull(installedRank);
        template.update("INSERT INTO schema_version (installed_rank, version, description, type, script, checksum, " +
                        "installed_by, installed_on, execution_time, success) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        installedRank, "10.0.0.0", "Dummy future version", "SQL", "dummy.sql", 0, "openvpms",
                        new Date(), 0, 1);

        Credentials credentials = new Credentials(user, password);
        DatabaseAdminService service = new DatabaseAdminServiceImpl(driverClassName, url, credentials, checksums);
        try {
            service.update(null, null);
            fail("Expected update to fail");
        } catch (SQLException expected) {
            assertEquals("A future database version has been applied. The database is 1 version ahead of the current " +
                         "release.", expected.getMessage());
        }

        // clean up
        template.update("DELETE FROM schema_version WHERE installed_rank = ?", installedRank);
    }

    /**
     * Verifies that the users passed to
     * {@link DatabaseAdminServiceImpl#create(Credentials, Credentials, String, boolean)} must be different.
     */
    @Test
    public void testCreateFailsForSameUsers() {
        Credentials admin = new Credentials(user, password);
        Credentials user = new Credentials("foo", "bar");
        DatabaseAdminService service = new DatabaseAdminServiceImpl(driverClassName, url, admin);
        try {
            service.create(admin, user, "localhost", true);
        } catch (SQLException expected) {
            assertEquals("Admin user cannot be the same as read/write user", expected.getMessage());
        }

        try {
            service.create(user, admin, "localhost", true);
        } catch (SQLException expected) {
            assertEquals("Admin user cannot be the same as read-only user", expected.getMessage());
        }

        try {
            service.create(user, user, "localhost", true);
        } catch (SQLException expected) {
            assertEquals("Read/write and read-only users cannot be the same", expected.getMessage());
        }
    }

    /**
     * Verifies that the users passed to
     * {@link DatabaseAdminServiceImpl#createUsers(Credentials, Credentials, String)} are different.
     */
    @Test
    public void testCreateUsersFailsForSameUsers() {
        Credentials admin = new Credentials(user, password);
        Credentials user = new Credentials("foo", "bar");
        DatabaseAdminService service = new DatabaseAdminServiceImpl(driverClassName, url, admin);
        try {
            service.createUsers(admin, user, "localhost");
        } catch (SQLException expected) {
            assertEquals("Admin user cannot be the same as read/write user", expected.getMessage());
        }

        try {
            service.createUsers(user, admin, "localhost");
        } catch (SQLException expected) {
            assertEquals("Admin user cannot be the same as read-only user", expected.getMessage());
        }

        try {
            service.createUsers(user, user, "localhost");
        } catch (SQLException expected) {
            assertEquals("Read/write and read-only users cannot be the same", expected.getMessage());
        }
    }
}
