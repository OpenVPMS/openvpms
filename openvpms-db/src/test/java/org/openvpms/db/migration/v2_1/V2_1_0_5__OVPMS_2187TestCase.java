/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the OVPMS-2187 migration, including the fixes for OVPMS-2252 and OVPMS-2265.
 *
 * @author Tim Anderson
 */
@ContextConfiguration(locations = {"/applicationContext.xml"})
public class V2_1_0_5__OVPMS_2187TestCase extends AbstractJUnit4SpringContextTests {

    public static class RelationshipData {

        private long id;

        private Date startTime;

        private Date endTime;

        private long sourceId;

        private long targetId;

        public void setId(long id) {
            this.id = id;
        }

        public long getId() {
            return id;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public long getSourceId() {
            return sourceId;
        }

        public void setSourceId(long sourceId) {
            this.sourceId = sourceId;
        }

        public long getTargetId() {
            return targetId;
        }

        public void setTargetId(long targetId) {
            this.targetId = targetId;
        }
    }

    public static class RoleData {

        private long userId;

        private long roleId;

        private String roleName;

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public long getRoleId() {
            return roleId;
        }

        public void setRoleId(long roleId) {
            this.roleId = roleId;
        }

        public String getRoleName() {
            return roleName;
        }

        public void setRoleName(String roleName) {
            this.roleName = roleName;
        }
    }

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        Connection connection = dataSource.getConnection();
        DbSupport support = DbSupportFactory.createDbSupport(connection, true);

        // load up test database baselined on V2.1.0.1 that has the problems reported in OVPMS-2252 and OVPMS-2265
        ClassPathResource sql = new ClassPathResource("db-V2.1.0.1__OVPMS-2187.sql", getClass().getClassLoader());
        SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
        script.execute(support.getJdbcTemplate());
        connection.close();
    }

    /**
     * Tests migration.
     *
     * @throws SQLException for any SQL error
     */
    @Test
    public void testMigration() throws SQLException {
        // product 129 has duplicate reminders. Relationships 443 and 444 are exact duplicates.
        // These will be replaced with a single entity link.
        // Relationship 445 is active at the same time as 443 and 444, but will still be migrated.
        List<RelationshipData> product129RemindersPreMigrate
                = getEntityRelationships("entityRelationship.productReminder", 129);
        assertEquals(3, product129RemindersPreMigrate.size());
        checkRelationship(product129RemindersPreMigrate, 443, 129, 1022, "2020-04-02 07:52:01", null);
        checkRelationship(product129RemindersPreMigrate, 444, 129, 1022, "2020-04-02 07:52:01", null);
        checkRelationship(product129RemindersPreMigrate, 445, 129, 1022, "2020-04-02 09:38:48", null);

        // product 900 has 1 equivalent
        List<RelationshipData> product900EquivalentsPreMigrate
                = getEntityRelationships("entityRelationship.productEquivalent", 900);
        assertEquals(1, product900EquivalentsPreMigrate.size());
        checkRelationship(product900EquivalentsPreMigrate, 447, 900, 898, "2020-04-02 13:31:51", null);

        // product 900 has 1 investigation type
        List<RelationshipData> product900InvestigationTypesPreMigrate
                = getEntityRelationships("entityRelationship.productInvestigationType", 900);
        assertEquals(1, product900InvestigationTypesPreMigrate.size());
        checkRelationship(product900InvestigationTypesPreMigrate, 448, 900, 1150, "2020-04-02 13:32:14", null);

        // product 900 has 1 document
        List<RelationshipData> product900DocumentsPreMigrate
                = getEntityRelationships("entityRelationship.productDocument", 900);
        assertEquals(1, product900DocumentsPreMigrate.size());
        checkRelationship(product900DocumentsPreMigrate, 446, 900, 1171, "2020-04-02 13:31:44", null);

        // check role counts prior to migration
        checkRole("Clinician", 0);
        checkRole("Stock Control", 2);
        checkRole("Stock Administrator", 0);
        checkRole("Stock Manager", 0);

        // user 1160 is a clinician with two roles named Stock Control
        List<RoleData> user1160PreMigrate = getRoleData(1160);
        assertEquals(3, user1160PreMigrate.size());
        checkRole(user1160PreMigrate, 1160, 1, "Stock Control");
        checkRole(user1160PreMigrate, 1160, 3, "Base Role");
        checkRole(user1160PreMigrate, 1160, 6, "Stock Control");

        // user 1027 is an administrator
        List<RoleData> user1027PreMigrate = getRoleData(1027);
        assertEquals(1, user1027PreMigrate.size());
        checkRole(user1027PreMigrate, 1027, 5, "Administration");

        // user 1161 is a clinician
        List<RoleData> user1161PreMigrate = getRoleData(1161);
        assertEquals(1, user1161PreMigrate.size());
        checkRole(user1161PreMigrate, 1161, 3, "Base Role");

        service.update("2.1.0.5", null, null);

        // check product reminders
        List<RelationshipData> product129RemindersPostMigrate = getEntityLinks("entityLink.productReminder", 129);
        assertEquals(2, product129RemindersPostMigrate.size());
        checkRelationship(product129RemindersPostMigrate, 129, 1022, "2020-04-02 07:52:01", null);
        checkRelationship(product129RemindersPostMigrate, 129, 1022, "2020-04-02 09:38:48", null);

        // check product equivalents
        List<RelationshipData> product900EquivalentsPostMigrate
                = getEntityLinks("entityLink.productEquivalent", 900);
        assertEquals(1, product900EquivalentsPostMigrate.size());
        checkRelationship(product900EquivalentsPostMigrate, 900, 898, "2020-04-02 13:31:51", null);

        List<RelationshipData> product900InvestigationTypesPostMigrate
                = getEntityLinks("entityLink.productInvestigationType", 900);
        assertEquals(1, product900InvestigationTypesPostMigrate.size());
        checkRelationship(product900InvestigationTypesPostMigrate, 900, 1150, "2020-04-02 13:32:14", null);

        List<RelationshipData> product900DocumentsPostMigrate
                = getEntityLinks("entityLink.productDocument", 900);
        assertEquals(1, product900DocumentsPostMigrate.size());
        checkRelationship(product900DocumentsPostMigrate, 900, 1171, "2020-04-02 13:31:44", null);

        // check roles post migration
        checkRole("Clinician", 1);
        checkRole("Stock Control", 0);
        checkRole("Stock Administrator", 1);
        checkRole("Stock Manager", 1);

        // user 1160 should now have a Clinician, Stock Administrator and StockManager role in addition to the
        // Base Role. The Stock Control roles should be removed
        List<RoleData> user1160PostMigrate = getRoleData(1160);
        assertEquals(4, user1160PostMigrate.size());
        checkRole(user1160PostMigrate, 1160, "Clinician");
        checkRole(user1160PostMigrate, 1160, 3, "Base Role");
        checkRole(user1160PostMigrate, 1160, "Stock Administrator");
        checkRole(user1160PostMigrate, 1160, "Stock Manager");

        // user 1027 roles should be unchanged
        List<RoleData> user1027PostMigrate = getRoleData(1027);
        assertEquals(1, user1027PostMigrate.size());
        checkRole(user1027PostMigrate, 1027, 5, "Administration");

        // user 1161 should now have Clinician role
        List<RoleData> user1161PostMigrate = getRoleData(1161);
        assertEquals(2, user1161PostMigrate.size());
        checkRole(user1161PostMigrate, 1161, "Clinician");
        checkRole(user1161PostMigrate, 1161, 3, "Base Role");
    }

    /**
     * Verifies that a role is present in a collection.
     *
     * @param roles    the roles
     * @param userId   the user id
     * @param roleId   the role id
     * @param roleName the role name
     */
    private void checkRole(List<RoleData> roles, int userId, long roleId, String roleName) {
        RoleData match = null;
        for (RoleData data : roles) {
            if (data.getUserId() == userId && data.getRoleId() == roleId && roleName.equals(data.getRoleName())) {
                match = data;
                break;
            }
        }
        assertNotNull(match);
    }

    /**
     * Verifies that a role is present in a collection.
     *
     * @param roles    the roles
     * @param userId   the user id
     * @param roleName the role name
     */
    private void checkRole(List<RoleData> roles, int userId, String roleName) {
        RoleData match = null;
        for (RoleData data : roles) {
            if (data.getUserId() == userId && roleName.equals(data.getRoleName())) {
                match = data;
                break;
            }
        }
        assertNotNull(match);
    }

    /**
     * Verifies a relationship is present in a collection.
     *
     * @param relationships the relationships to check
     * @param id            the relationship id
     * @param sourceId      the source id
     * @param targetId      the target id
     * @param startTime     the relationship start time
     * @param endTime       the relationship end time
     */
    private void checkRelationship(List<RelationshipData> relationships, long id, long sourceId, long targetId,
                                   String startTime, String endTime) {
        RelationshipData match = null;
        for (RelationshipData data : relationships) {
            if (data.getId() == id) {
                match = data;
                break;
            }
        }
        assertNotNull(match);
        assertEquals(sourceId, match.getSourceId());
        assertEquals(targetId, match.getTargetId());
        assertEquals(getDate(startTime), match.getStartTime());
        assertEquals(getDate(endTime), match.getEndTime());
    }

    /**
     * Verifies a relationship is present in a collection.
     *
     * @param relationships the relationships to check
     * @param sourceId      the source id
     * @param targetId      the target id
     * @param startTime     the relationship start time
     * @param endTime       the relationship end time
     */
    private void checkRelationship(List<RelationshipData> relationships, long sourceId, long targetId,
                                   String startTime, String endTime) {
        Date startDate = getDate(startTime);
        Date endDate = getDate(endTime);
        RelationshipData match = null;
        for (RelationshipData data : relationships) {
            if (sourceId == data.getSourceId() && targetId == data.getTargetId()
                && Objects.equals(startDate, data.getStartTime())
                && Objects.equals(endDate, data.getEndTime())) {
                match = data;
                break;
            }
        }
        assertNotNull(match);
    }

    /**
     * Converts a date from a string.
     *
     * @param date the date. May be {@code null}
     * @return the corresponding date, or {@code null} if {@code date} is {@code null}
     */
    private Date getDate(String date) {
        return date != null ? Timestamp.valueOf(date) : null;
    }

    /**
     * Queries entity relationship data.
     *
     * @param archetype the relationship archetype
     * @param source    the source of the relationship
     * @return the matching data
     */
    private List<RelationshipData> getEntityRelationships(String archetype, long source) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template.query(
                "SELECT entity_relationship_id as id, source_id as sourceId, target_id as targetId, " +
                "active_start_time as startTime, active_end_time as endTime " +
                "FROM entity_relationships r " +
                "WHERE r.arch_short_name = '" + archetype + "' AND r.source_id = " + source + " " +
                "ORDER BY entity_relationship_id",
                BeanPropertyRowMapper.newInstance(RelationshipData.class));
    }

    /**
     * Queries role data.
     *
     * @param userId the user id
     * @return the corresponding user data
     */
    private List<RoleData> getRoleData(long userId) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template.query(
                "SELECT user_role.user_id as userId, role.security_role_id as roleId, role.name as roleName " +
                "FROM user_roles user_role " +
                "JOIN security_roles role on user_role.security_role_id = role.security_role_id " +
                "WHERE user_role.user_id = " + userId + " " +
                "ORDER BY role.security_role_id",
                BeanPropertyRowMapper.newInstance(RoleData.class));
    }

    /**
     * Queries entity link data.
     *
     * @param archetype the relationship archetype
     * @param source    the source of the relationship
     * @return the matching data
     */
    private List<RelationshipData> getEntityLinks(String archetype, long source) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template.query(
                "SELECT id as id, source_id as sourceId, target_id as targetId, " +
                "active_start_time as startTime, active_end_time as endTime " +
                "FROM entity_links l " +
                "WHERE l.arch_short_name = '" + archetype + "' AND l.source_id = " + source + " " +
                "ORDER BY id",
                BeanPropertyRowMapper.newInstance(RelationshipData.class));
    }

    /**
     * Checks the number of roles with the specified name.
     *
     * @param name          the role name
     * @param expectedCount the expected count
     */
    private void checkRole(String name, int expectedCount) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        int count = template.queryForObject("select count(*) from security_roles where name = '" + name + "'",
                                            int.class);
        assertEquals(count, expectedCount);
    }

}
