/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_2;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the migration for OVPMS-2525.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_2_0_17__OVPMS_2525TestCase {

    public static class SMS {

        private long id;

        private List<Participant> participants;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public List<Participant> getParticipants() {
            return participants;
        }

        public void setParticipants(List<Participant> participants) {
            this.participants = participants;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return Long.hashCode(id);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         * <p>
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof Participant && id == ((Participant) obj).id;
        }
    }

    /**
     * Wraps <em>act.smsReply</em>.
     */
    public static class Reply {

        private long id;

        private Date startTime;

        private Date endTime;

        private long smsId;

        private List<Participant> participants;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public long getSmsId() {
            return smsId;
        }

        public void setSmsId(long smsId) {
            this.smsId = smsId;
        }

        public List<Participant> getParticipants() {
            return participants;
        }

        public void setParticipants(List<Participant> participants) {
            this.participants = participants;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return Long.hashCode(id);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         * <p>
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof Reply && id == ((Reply) obj).id;
        }
    }

    public static class Participant {
        private long id;

        private String archetype;

        private Date startTime;

        private Date endTime;

        private long entity;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getArchetype() {
            return archetype;
        }

        public void setArchetype(String archetype) {
            this.archetype = archetype;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public long getEntityId() {
            return entity;
        }

        public void setEntity(long entity) {
            this.entity = entity;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return Long.hashCode(id);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         * <p>
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof Participant && id == ((Participant) obj).id;
        }

    }

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;

    /**
     * The expected no. of replies.
     */
    private static final int REPLIES = 32;

    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.2.0.16 (pre OpenVPMS 2.3)
            ClassPathResource sql = new ClassPathResource("db-V2.2.0.16__OVPMS-2525.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Tests the migration.
     *
     * @throws SQLException for any SQL error
     */
    @Test
    public void testMigration() throws SQLException {
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");

        // check pre-conditions
        List<SMS> beforeSMS = getMessages();
        List<Reply> beforeReply = getReplies();

        assertEquals(16, beforeSMS.size());
        assertEquals(REPLIES, beforeReply.size());
        checkSMSReplyCounts(false);
        checkParticipationCounts("participation.smsRecipient", 16);
        checkParticipationCounts("participation.smsContact", 0);
        checkParticipationCounts("participation.customer", 107);
        checkParticipationCounts("participation.patient", 230);
        checkParticipationCounts("participation.location", 19);

        // run the migration
        service.update("2.2.0.17", null, null);

        // check migrated counts
        List<SMS> afterSMS = getMessages();
        List<Reply> afterReply = getReplies();

        assertEquals(16, afterSMS.size());
        assertEquals(REPLIES, afterReply.size());
        checkParticipants(beforeSMS, afterReply);  // verify that the participants on SMSes before migration were copied
        checkSMSReplyCounts(true);  // document_acts rows deleted
        checkParticipationCounts("participation.smsRecipient", 0);
        checkParticipationCounts("participation.smsContact", 48);
        checkParticipationCounts("participation.customer", 139);
        checkParticipationCounts("participation.patient", 260);
        checkParticipationCounts("participation.location", 51);
    }

    /**
     * Checks participants have been duplicated on to replies.
     *
     * @param beforeSMS    the SMS messages, before migration
     * @param afterReplies the replies, post migration
     */
    private void checkParticipants(List<SMS> beforeSMS, List<Reply> afterReplies) {
        Map<Long, SMS> map = new HashMap<>();
        for (SMS message : beforeSMS) {
            map.put(message.getId(), message);
        }
        for (Reply reply : afterReplies) {
            SMS message = map.get(reply.getSmsId());
            assertNotNull(message);
            assertEquals(message.getParticipants().size(), reply.getParticipants().size());
            for (Participant participant : message.getParticipants()) {
                String archetype = participant.getArchetype();
                if (archetype.equals("participation.smsRecipient")) {
                    archetype = "participation.smsContact"; // archetype renamed during migration
                }
                checkReplyParticipant(archetype, participant.getEntityId(), reply);
            }
        }
    }

    /**
     * Checks a participation record with the specified archetype and entity identifier exists on a reply.
     *
     * @param archetype the archetype
     * @param entityId  the entity identifier
     * @param reply     the reply
     */
    private void checkReplyParticipant(String archetype, long entityId, Reply reply) {
        boolean found = false;
        for (Participant other : reply.getParticipants()) {
            if (archetype.equals(other.getArchetype())) {
                assertEquals(entityId, other.getEntityId());
                assertEquals(reply.getStartTime(), other.getStartTime());
                assertEquals(reply.getEndTime(), other.getEndTime());
                found = true;
            }
        }
        assertTrue(found);
    }

    /**
     * Checks that there are the expected number of act.smsReply records.
     *
     * @param actsOnly if {@code true} there should not be any document_acts rows
     */
    private void checkSMSReplyCounts(boolean actsOnly) {
        Integer count1 = template.queryForObject(
                "SELECT COUNT(*) FROM acts WHERE arch_short_name = 'act.smsReply'", Integer.class);
        assertNotNull(count1);
        assertEquals(REPLIES, count1.intValue());

        Integer count2 = template.queryForObject(
                "SELECT COUNT(*) FROM acts a JOIN document_acts da ON a.act_id = da.document_act_id " +
                "WHERE arch_short_name = 'act.smsReply'", Integer.class);
        assertNotNull(count2);
        if (actsOnly) {
            assertEquals(0, count2.intValue());
        } else {
            assertEquals(REPLIES, count2.intValue());
        }
    }

    /**
     * Verifies that the count of participation records matches that expected.
     *
     * @param archetype the participation archetype
     * @param expected  the expected count
     */
    private void checkParticipationCounts(String archetype, int expected) {
        Integer count = template.queryForObject(
                "SELECT COUNT(*) FROM participations WHERE arch_short_name = '" + archetype + "'", Integer.class);
        assertNotNull(count);
        assertEquals(expected, count.intValue());
    }

    /**
     * Returns the act.smsMessage acts.
     */
    private List<SMS> getMessages() {
        List<SMS> messages = template.query(
                "SELECT sms.act_id id " +
                "FROM acts sms " +
                "WHERE sms.arch_short_name = 'act.smsMessage' " +
                "ORDER BY sms.act_id",
                BeanPropertyRowMapper.newInstance(SMS.class));
        for (SMS message : messages) {
            message.setParticipants(getParticipants(message.getId()));
        }
        return messages;
    }

    /**
     * Returns the act.smsReply acts.
     */
    private List<Reply> getReplies() {
        List<Reply> replies = template.query(
                "SELECT reply.act_id id, reply.activity_start_time startTime, reply.activity_end_time endTime, " +
                "r_sms.source_id smsId " +
                "FROM acts reply " +
                "LEFT JOIN act_relationships r_sms ON reply.act_id = r_sms.target_id " +
                "WHERE reply.arch_short_name = 'act.smsReply' " +
                "ORDER BY reply.act_id",
                BeanPropertyRowMapper.newInstance(Reply.class));
        for (Reply reply : replies) {
            reply.setParticipants(getParticipants(reply.getId()));
        }
        return replies;
    }

    /**
     * Returns the participants of an act.
     *
     * @param id the act identifier
     * @return the participants
     */
    private List<Participant> getParticipants(long id) {
        return template.query("SELECT participant.participation_id id, participant.arch_short_name archetype, " +
                              "participant.activity_start_time startTime, participant.activity_end_time endTime, " +
                              "participant.entity_id entityId " +
                              "FROM participations participant " +
                              "WHERE act_id = " + id, BeanPropertyRowMapper.newInstance(Participant.class));
    }

}
