/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_2;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the migration for OVPMS-2468 Increase length of appointment & task notes.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_2_0_11__OVPMS_2468TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;


    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.6 (OpenVPMS 2.1.2)
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Tests the migration.
     *
     * @throws SQLException for any SQL error
     */
    @Test
    public void testMigration() throws SQLException {
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");

        checkAct(299, "act.customerAppointment", "appointment notes", null);
        checkAct(300, "act.calendarBlock", "calendar block notes", null);
        checkAct(301, "act.customerTask", "task notes", null);
        checkAct(302, "act.calendarEvent", "service ratio notes", null);

        // run the migration
        service.update("2.2.0.11", null, null);

        // verify the description has moved to notes
        checkAct(299, "act.customerAppointment", null, "appointment notes");
        checkAct(300, "act.calendarBlock", null, "calendar block notes");
        checkAct(301, "act.customerTask", null, "task notes");
        checkAct(302, "act.calendarEvent", null, "service ratio notes");
    }

    /**
     * Verifies the act description and notes match that expected.
     *
     * @param id          the act id
     * @param archetype   the act archetype
     * @param description the expected description. May be {@code null}
     * @param notes       the expected notes. May be {@code null}
     */
    private void checkAct(long id, String archetype, String description, String notes) {
        String sql = "SELECT a.description, d.value notes " +
                     "FROM acts a " +
                     "LEFT JOIN act_details d ON a.act_id = d.act_id " +
                     "AND d.name = 'notes' " +
                     "where a.act_id = " + id + " AND a.arch_short_name = '" + archetype + "'";

        Map<String, Object> map = DataAccessUtils.singleResult(template.queryForList(sql));
        assertNotNull(map);
        assertEquals(description, map.get("description"));
        assertEquals(notes, map.get("notes"));
    }
}
