/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_2;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the migration for OVPMS-2505.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_2_0_12__OVPMS_2505TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;

    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.6 (OpenVPMS 2.1.2)
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Tests the migration.
     *
     * @throws SQLException for any SQL error
     */
    @Test
    public void testMigration() throws SQLException {
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");

        // check pre-conditions
        checkAct(305, "PART", true);
        checkAct(307, "PENDING", true);
        checkAct(311, "FULL", true);
        checkCounts(316, 3);

        // run the migration
        service.update("2.2.0.12", null, null);

        // check migrated acts and counts
        checkAct(305, "PART", false);
        checkAct(307, "PENDING", false);
        checkAct(311, "FULL", false);
        checkCounts(313, 0);
    }

    /**
     * Verifies the act description and notes match that expected.
     *
     * @param id             the act id
     * @param deliveryStatus the expected delivery status
     * @param preMigration   if {@code true}, the delivery status comes from the act_details table, else it comes
     *                       from the status2 column
     */
    private void checkAct(long id, String deliveryStatus, boolean preMigration) {
        String sql = "SELECT a.status2, d.value delivery_status " +
                     "FROM acts a " +
                     "LEFT JOIN act_details d ON a.act_id = d.act_id " +
                     "AND d.name = 'deliveryStatus' " +
                     "where a.act_id = " + id + " AND a.arch_short_name = 'act.supplierOrder'";
        Map<String, Object> map = DataAccessUtils.singleResult(template.queryForList(sql));
        assertNotNull(map);
        if (preMigration) {
            assertNull(map.get("status2"));
            assertEquals(deliveryStatus, map.get("delivery_status"));
        } else {
            assertEquals(deliveryStatus, map.get("status2"));
            assertNull(map.get("delivery_status"));
        }
    }

    /**
     * Verifies that count of status2=null and deliveryStatus rows that expected.
     *
     * @param expectedStatus2Count        the expected no. of null status2 columns
     * @param expectedDeliveryStatusCount the expected no. of deliveryStatus rows
     */
    private void checkCounts(int expectedStatus2Count, int expectedDeliveryStatusCount) {
        Integer status2Count = template.queryForObject(
                "SELECT COUNT(*) FROM acts WHERE status2 IS NULL", Integer.class);
        assertNotNull(status2Count);
        assertEquals(expectedStatus2Count, status2Count.intValue());

        Integer deliveryStatusCount = template.queryForObject(
                "SELECT COUNT(*) FROM act_details WHERE name = 'deliveryStatus'", Integer.class);
        assertNotNull(deliveryStatusCount);
        assertEquals(expectedDeliveryStatusCount, deliveryStatusCount.intValue());
    }

}