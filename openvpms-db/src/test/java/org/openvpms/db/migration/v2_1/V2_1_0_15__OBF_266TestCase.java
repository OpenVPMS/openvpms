/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the migration for OBF-266 Prevent addition of duplicate roles and authorities.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_15__OBF_266TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.14
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.14__OBF-266.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
    }

    /**
     * Test the migration.
     * <p/>
     * The db-V2.1.0.14__OBF-266.sql dump has multiple duplicate roles and authorities.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMigration() throws Exception {
        // check roles prior to migration
        checkRole(3, "Administration");
        checkRole(2, "Base Role");
        checkRole(13, "Stock Manager");
        checkRole(16, "Administration"); // duplicates
        checkRole(17, "Base Role");
        checkRole(25, "Stock Manager");
        checkRole(29, "Administration");
        checkRole(30, "Base Role");
        checkRole(38, "Stock Manager");

        // check authorities prior to migration
        checkAuthority(113, "Appointment Create");
        checkAuthority(3, "Identity Save");
        checkAuthority(11, "Patient Document Remove");

        checkAuthority(256, "Appointment Create");   // duplicates
        checkAuthority(335, "Identity Save");
        checkAuthority(363, "Patient Document Remove");
        checkAuthority(481, "Appointment Create");
        checkAuthority(560, "Identity Save");
        checkAuthority(588, "Patient Document Remove");

        service.update("2.1.0.15", null, null);

        // check roles post migration
        checkRole(3, "Administration");
        checkRole(2, "Base Role");
        checkRole(13, "Stock Manager");

        // duplicates should have their ids prefixed
        checkRole(16, "16 Administration");
        checkRole(17, "17 Base Role");
        checkRole(25, "25 Stock Manager");
        checkRole(29, "29 Administration");
        checkRole(30, "30 Base Role");
        checkRole(38, "38 Stock Manager");

        // check authorities post migration
        checkAuthority(113, "Appointment Create");
        checkAuthority(3, "Identity Save");
        checkAuthority(11, "Patient Document Remove");

        // duplicates should have their ids prefixed
        checkAuthority(256, "256 Appointment Create");
        checkAuthority(335, "335 Identity Save");
        checkAuthority(363, "363 Patient Document Remove");
        checkAuthority(481, "481 Appointment Create");
        checkAuthority(560, "560 Identity Save");
        checkAuthority(588, "588 Patient Document Remove");
    }

    /**
     * Checks a role.
     *
     * @param id   the role id
     * @param name the role name
     */
    private void checkRole(long id, String name) {
        Data role = getRole(id);
        checkData(role, id, name);
    }

    /**
     * Returns a role by id.
     *
     * @param id the role identifier
     * @return the corresponding role or {@code null} if none is found
     */
    private Data getRole(long id) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT r.security_role_id AS id, r.name as name " +
                     "FROM security_roles r " +
                     "WHERE r.arch_short_name = 'security.role' and r.security_role_id = " + id;
        return template.queryForObject(sql, BeanPropertyRowMapper.newInstance(Data.class));
    }

    /**
     * Checks an authority.
     *
     * @param id   the authority id
     * @param name the authority name
     */
    private void checkAuthority(long id, String name) {
        Data authority = getAuthority(id);
        checkData(authority, id, name);
    }

    /**
     * Checks data.
     *
     * @param data the data to check
     * @param id   the expected id
     * @param name the expected name
     */
    private void checkData(Data data, long id, String name) {
        assertNotNull(data);
        assertEquals(data.getId(), id);
        assertEquals(data.getName(), name);
    }

    /**
     * Returns an authority by id.
     *
     * @param id the authority identifier
     * @return the corresponding authority or {@code null} if none is found
     */
    private Data getAuthority(long id) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT a.granted_authority_id AS id, a.name as name " +
                     "FROM granted_authorities a " +
                     "WHERE a.arch_short_name = 'security.archetypeAuthority' and a.granted_authority_id = " + id;
        return template.queryForObject(sql, BeanPropertyRowMapper.newInstance(Data.class));
    }


    private static class Data {

        private long id;

        private String name;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
