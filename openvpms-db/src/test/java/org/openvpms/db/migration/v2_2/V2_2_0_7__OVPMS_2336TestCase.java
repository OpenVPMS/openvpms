/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_2;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.migration.V2_2.V2_2_0_7__OVPMS_2336;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link V2_2_0_7__OVPMS_2336} migration.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_2_0_7__OVPMS_2336TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;

    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.6
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Tests the migration.
     * <p/>
     * This should delete 4 archetypes that are no longer in use.
     *
     * @throws SQLException for any SQL error
     */
    @Test
    public void testMigration() throws SQLException {
        // check preconditions
        assertEquals(499, count("archetype_descriptors"));
        assertEquals(4408, count("node_descriptors"));
        assertEquals(911, count("assertion_descriptors"));
        assertTrue(exists("participation.author"));
        assertTrue(exists("entityLink.productInvestigationType"));
        assertTrue(exists("entityLink.investigationLaboratory"));
        assertTrue(exists("act.customerCommunicationSMS"));

        // run the migration
        service.update("2.2.0.7", null, null);

        // check post-conditions
        assertEquals(495, count("archetype_descriptors"));
        assertEquals(4378, count("node_descriptors"));
        assertEquals(905, count("assertion_descriptors"));
        assertFalse(exists("participation.author"));
        assertFalse(exists("entityLink.productInvestigationType"));
        assertFalse(exists("entityLink.investigationLaboratory"));
        assertFalse(exists("act.customerCommunicationSMS"));
    }

    /**
     * Checks an archetype descriptor.
     *
     * @param archetype the archetype
     * @return {@code true} if the archetype exists, otherwise {@code false}
     */
    private boolean exists(String archetype) {
        String sql = "SELECT archetype_desc_id FROM archetype_descriptors WHERE name = '" + archetype + ".1.0'";
        Long result = DataAccessUtils.singleResult(template.queryForList(sql, Long.class));
        return result != null;
    }

    /**
     * Counts rows in a table.
     *
     * @param table the table
     * @return the row count
     */
    private int count(String table) {
        Integer result = template.queryForObject("SELECT COUNT(*) FROM " + table, Integer.class);
        assertNotNull(result);
        return result;
    }
}