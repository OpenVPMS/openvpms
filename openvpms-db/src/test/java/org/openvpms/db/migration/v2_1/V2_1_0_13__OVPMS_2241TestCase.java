/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the migration for OVPMS-2241 Create/update user information.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_13__OVPMS_2241TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * Determines if MySQL 5.5 is being used.
     */
    private boolean isMySQL55;


    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            String productVersion = connection.getMetaData().getDatabaseProductVersion();
            isMySQL55 = productVersion.startsWith("5.5");
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);


            // load up test database baselined on V2.1.0.6
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
    }

    /**
     * Tests OVPMS-2241 migration.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMigration() throws Exception {
        // check author participations prior to migration
        checkAuthorParticipations(35);

        // run the migration
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        service.update("2.1.0.13", null, null);

        // verify createdDate has been migrated to created for customers
        checkEntity(12, "party.customerperson", "2020-01-26 10:21:10"); // Judith Bourke
        checkEntity(39, "party.customerperson", "2020-01-26 10:28:19"); // Melanie Broke

        // verify createdDate has been migrated to created for patients
        checkEntity(5, "party.patientpet", "2020-01-26 10:00:19"); // Firefly
        checkEntity(33, "party.patientpet", "2020-01-26 10:24:19"); // Fido

        // verify that no other entities have created times
        checkNoEntitiesWithCreated();

        // check reminder migration
        // NOTE: MySQL 5.5 truncates timestamps, whereas MySQL 5.7 rounds them
        String created1 = isMySQL55 ? "2020-01-26 10:28:31" : "2020-01-26 10:28:32";
        String created2 = isMySQL55 ? "2020-02-11 09:51:25" : "2020-02-11 09:51:26";
        checkReminder(48, created1, "2020-01-26 10:28:31.835", null);   // legacy reminder, no author
        checkReminder(272, created2, "2020-02-11 09:51:25.682", 1027L); // has author

        // check act migration
        checkAuthorParticipations(0);   // all participation.author records should have been removed
        checkAct(1, "act.patientClinicalNote", "2012-10-15 00:00:00", null);   // legacy act, no author
        checkAct(276, "act.patientClinicalNOte", "2020-02-11 10:00:58", 1160L);  // has author

        // verify future data startTimes are not migrated to created
        checkAct(271, "act.customerAppointment", null, 1027L); // dated in 2100

        // check price migration
        checkPrice(1, "productPrice.unitPrice", "2020-01-26 10:28:20");
        checkPrice(399, "productPrice.fixedPrice", "2020-01-26 10:28:25");

        // verify future data start_times are not migrated to created
        checkPrice(1006, "productPrice.unitPrice", null); // dated in 2100
    }

    /**
     * Verifies that only party.customerperson and party.patientpet have created populated.
     */
    private void checkNoEntitiesWithCreated() {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT COUNT(*) " +
                     "FROM entities " +
                     "WHERE arch_short_name NOT IN ('party.customerperson', 'party.patientpet') " +
                     "AND created IS NOT NULL";
        Integer count = template.queryForObject(sql, Integer.class);
        assertNotNull(count);
        assertEquals(0, count.intValue());
    }

    /**
     * Verifies the number of author participations matches that expected.
     *
     * @param expected the expected no. of author participations
     */
    private void checkAuthorParticipations(int expected) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT COUNT(*) FROM participations WHERE arch_short_name = 'participation.author'";
        Integer count = template.queryForObject(sql, Integer.class);
        assertNotNull(count);
        assertEquals(expected, count.intValue());
    }

    /**
     * Checks reminder migration.
     *
     * @param id          the reminder id
     * @param created     the expected created time
     * @param initialTime the expected initialTime
     * @param createdBy   the expected createdBy id. May be {@code null}
     */
    private void checkReminder(long id, String created, String initialTime, Long createdBy) {
        ReminderData act = getReminder(id);
        assertNotNull(act);
        assertEquals(Timestamp.valueOf(created), act.getCreated());
        assertEquals(createdBy, act.getCreatedBy());
        assertNull(act.getUpdated());
        assertNull(act.getUpdatedBy());
        assertNull(act.getCreatedTime()); // for reminders, this gets renamed to initialTime
        assertEquals(Timestamp.valueOf(created), act.getCreated());
        assertEquals(initialTime, act.getInitialTime());
    }

    /**
     * Check product price migration.
     *
     * @param id        the price id
     * @param archetype the price archetype
     * @param created   the expected created time. May be {@code null}
     */
    private void checkPrice(long id, String archetype, String created) {
        Data price = getPrice(id, archetype);
        assertNotNull(price);
        if (created != null) {
            assertEquals(Timestamp.valueOf(created), price.getCreated());
        } else {
            assertNull(price.getCreated());
        }
        assertNull(price.getCreatedBy());
        assertNull(price.getUpdated());
        assertNull(price.getUpdatedBy());
    }

    /**
     * Returns an entity given its id and archetype.
     *
     * @param id        the entity id
     * @param archetype the entity archetype
     * @return the corresponding entity, or {@code null if none is found}
     */
    private EntityData getEntity(long id, String archetype) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT e.entity_id AS id, e.created, e.created_id AS createdBy, e.updated, " +
                     "e.updated_id AS updatedBy, created_date.value as createdDate " +
                     "FROM entities e " +
                     "LEFT JOIN entity_details created_date on e.entity_id = created_date.entity_id " +
                     "and created_date.name = 'createdDate'" +
                     " WHERE e.entity_id = " + id + " and arch_short_name = '" + archetype + "'";
        return template.queryForObject(sql, BeanPropertyRowMapper.newInstance(EntityData.class));
    }

    /**
     * Check entity migration.
     *
     * @param id        the entity id
     * @param archetype the entity archetype
     * @param created   the expected created time. May be {@code null}
     */
    private void checkEntity(long id, String archetype, String created) {
        EntityData entity = getEntity(id, archetype);
        assertNotNull(entity);
        if (created == null) {
            assertNull(entity.getCreated());
        } else {
            assertEquals(Timestamp.valueOf(created), entity.getCreated());
        }
        assertNull(entity.getCreatedBy());
        assertNull(entity.getUpdated());
        assertNull(entity.getUpdatedBy());
        assertNull(entity.getCreatedDate()); // for customers and patients, this gets migrated to created
    }

    /**
     * Check act migration.
     *
     * @param id        the act id
     * @param archetype the act archetype
     * @param created   the expected created time. May be {@code null}
     * @param createdBy the expected createdBy id. May be {@code null}
     */
    private void checkAct(long id, String archetype, String created, Long createdBy) {
        Data act = getAct(id, archetype);
        assertNotNull(act);
        if (created == null) {
            assertNull(act.getCreated());
        } else {
            assertEquals(Timestamp.valueOf(created), act.getCreated());
        }
        assertEquals(createdBy, act.getCreatedBy());
        assertNull(act.getUpdated());
        assertNull(act.getUpdatedBy());
    }

    /**
     * Returns an act given its id and archetype.
     *
     * @param id        the act id
     * @param archetype the act archetype
     * @return the corresponding act data, or {@code null if none is found}
     */
    private Data getAct(long id, String archetype) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT a.act_id AS id, a.created, a.created_id AS createdBy, a.updated, " +
                     "a.updated_id AS updatedBy " +
                     "FROM acts a " +
                     "WHERE a.act_id = " + id + " and arch_short_name = '" + archetype + "'";
        return template.queryForObject(sql, BeanPropertyRowMapper.newInstance(Data.class));
    }

    /**
     * Returns a document given its id.
     *
     * @param id the document id
     * @return the corresponding document data, or {@code null if none is found}
     */
    private Data getDocument(long id) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT d.document_id AS id, d.created, d.created_id AS createdBy, d.updated, " +
                     "d.updated_id AS updatedBy " +
                     "FROM documents d " +
                     "WHERE d.document_id = " + id + " and arch_short_name = 'document.other'";
        return template.queryForObject(sql, BeanPropertyRowMapper.newInstance(Data.class));
    }

    /**
     * Returns a reminder given its id.
     *
     * @param id the act id
     * @return the corresponding entity, or {@code null if none is found}
     */
    private ReminderData getReminder(long id) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT a.act_id AS id, a.created, a.created_id AS createdBy, a.updated, " +
                     "a.updated_id AS updatedBy, created_time.value AS createdTime, " +
                     "initial_time.value AS initialTime " +
                     "FROM acts a " +
                     "LEFT JOIN act_details created_time ON a.act_id = created_time.act_id " +
                     "AND created_time.name = 'createdTime' " +
                     "LEFT JOIN act_details initial_time ON a.act_id = initial_time.act_id " +
                     "AND initial_time.name = 'initialTime' " +
                     "WHERE a.act_id = " + id + " and arch_short_name = 'act.patientReminder'";
        return template.queryForObject(sql, BeanPropertyRowMapper.newInstance(ReminderData.class));
    }

    /**
     * Returns a product price given its id and archetype.
     *
     * @param id        the price id
     * @param archetype the price archetype
     * @return the corresponding price data, or {@code null if none is found}
     */
    private Data getPrice(long id, String archetype) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT p.product_price_id AS id, p.created, p.created_id AS createdBy, p.updated, " +
                     "p.updated_id AS updatedBy " +
                     "FROM product_prices p " +
                     "WHERE p.product_price_id = " + id + " and arch_short_name = '" + archetype + "'";
        return template.queryForObject(sql, BeanPropertyRowMapper.newInstance(Data.class));
    }

    private static class Data {

        private long id;

        private Timestamp created;

        private Long createdBy;

        private Timestamp updated;

        private Long updatedBy;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Timestamp created) {
            this.created = created;
        }

        public Long getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Long createdBy) {
            this.createdBy = createdBy;
        }

        public Date getUpdated() {
            return updated;
        }

        public void setUpdated(Timestamp updated) {
            this.updated = updated;
        }

        public Long getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Long updatedBy) {
            this.updatedBy = updatedBy;
        }
    }

    private static class EntityData extends Data {
        private String createdDate;


        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }
    }

    private static class ReminderData extends Data {
        private String createdTime;

        private String initialTime;

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public String getInitialTime() {
            return initialTime;
        }

        public void setInitialTime(String initialTime) {
            this.initialTime = initialTime;
        }

    }

}
