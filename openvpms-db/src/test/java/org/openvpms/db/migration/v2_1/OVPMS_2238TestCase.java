/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

/**
 * Verifies that the schema_version checksum for the V2.1.0.3__OVPMS-2208 migration is updated, if it has already
 * been run.
 * <p/>
 * This is necessary as the migration script for OVPMS-2208 was updated after 2.1.2 release, and the updated script has
 * a different checksum. The new checksum needs to be updated on existing migrations, to avoid Flyway errors.
 *
 * @author Tim Anderson
 */
public class OVPMS_2238TestCase extends AbstractMigrationChecksumTest {

    /**
     * Returns the path to a dump prior to the migration.
     *
     * @return the resource path
     */
    @Override
    protected String getPreMigrationDump() {
        return "db-V2.0.0.6__OVPMS-2208.sql";
    }

    /**
     * Returns the checksum of the original migration script.
     *
     * @return the checksum of the original migration script
     */
    @Override
    protected int getOriginalMigrationChecksum() {
        return -162652147;
    }

    /**
     * Returns the path to a dump done after the original migration.
     *
     * @return the resource path
     */
    @Override
    protected String getPostMigrationDump() {
        return "db-V2.1.0.6__OVPMS-2238.sql";
    }

    /**
     * Returns the checksum of the updated migration script.
     *
     * @return the checksum of the updated migration script
     */
    @Override
    protected int getUpdatedMigrationChecksum() {
        return -427272242;
    }

    /**
     * Returns the migration version.
     *
     * @return the migration version
     */
    @Override
    protected String getMigrationVersion() {
        return "2.1.0.3";
    }

    /**
     * Returns the migration description.
     *
     * @return the migration description
     */
    @Override
    protected String getMigrationDescription() {
        return "OVPMS-2208";
    }

}