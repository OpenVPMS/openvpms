/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_2;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests the migration for OVPMS-2443 Print API.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_2_0_5__OVPMS_2443TestCase {

    /**
     * Wraps <em>entity.printer</em>.
     */
    public static class Printer {

        private long id;

        private String name;

        private String printerReference;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPrinterReference() {
            return printerReference;
        }

        public void setPrinterReference(String printerReference) {
            this.printerReference = printerReference;
        }
    }

    /**
     * Wraps <em>party.organisationPractice</em> defaultPrinter.
     */
    public static class DefaultPrinter {

        private long id;

        private String defaultPrinter;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getDefaultPrinter() {
            return defaultPrinter;
        }

        public void setDefaultPrinter(String defaultPrinter) {
            this.defaultPrinter = defaultPrinter;
        }
    }

    /**
     * Wraps <em>entity.jobScheduledReport</em> printer.
     */
    public static class ReportPrinter {

        private long id;

        private String printer;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getPrinter() {
            return printer;
        }

        public void setPrinter(String printer) {
            this.printer = printer;
        }
    }

    public static class DocumentTemplatePrinter {

        private long id;

        private String printerName;

        private String printer;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getPrinterName() {
            return printerName;
        }

        public void setPrinterName(String printerName) {
            this.printerName = printerName;
        }

        public String getPrinter() {
            return printer;
        }

        public void setPrinter(String printer) {
            this.printer = printer;
        }
    }


    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;

    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.6
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Tests migration.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMigration() throws Exception {
        // check entity.printer pre-migration
        List<Printer> preMigrationPrinters = getPrinters();
        assertEquals(2, preMigrationPrinters.size());
        checkPrinter(preMigrationPrinters.get(0), 1178, "Samsung M2070 Series", null);
        checkPrinter(preMigrationPrinters.get(1), 1179, "Samsung M2070 Series: USB001", null);

        // check party.organisationLocation defaultPrinter pre-migration
        List<DefaultPrinter> preMigrationDefaultPrinters = getDefaultPrinters();
        assertEquals(2, preMigrationDefaultPrinters.size());
        checkDefaultPrinter(preMigrationDefaultPrinters.get(0), 111, "Samsung M2070 Series: USB001");
        checkDefaultPrinter(preMigrationDefaultPrinters.get(1), 112, "Samsung M2070 Series");

        // check entityRelationship.documentTemplatePrinter printerName pre-migration
        List<DocumentTemplatePrinter> preMigrationTemplates = getDocumentTemplatePrinters();
        assertEquals(2, preMigrationTemplates.size());
        checkDocumentTemplatePrinter(preMigrationTemplates.get(0), 445, "Samsung M2070 Series: USB001", null);
        checkDocumentTemplatePrinter(preMigrationTemplates.get(1), 446, "Samsung M2070 Series", null);

        // check entity.jobScheduledReport printer pre-migration
        List<ReportPrinter> preMigrationReportPrinters = getScheduledReportJobPrinters();
        assertEquals(1, preMigrationReportPrinters.size());
        checkReportPrinter(preMigrationReportPrinters.get(0), 1177, "Samsung M2070 Series: USB001");

        // perform the migration
        service.update("2.2.0.5", null, null);

        // check entity.printer post-migration
        // NOTE: printer names with colons will be escaped
        List<Printer> postMigrationPrinters = getPrinters();
        assertEquals(2, postMigrationPrinters.size());
        checkPrinter(postMigrationPrinters.get(0), 1178, "Samsung M2070 Series", ":Samsung M2070 Series");

        checkPrinter(postMigrationPrinters.get(1), 1179,
                     "Samsung M2070 Series: USB001", ":Samsung M2070 Series\\: USB001");

        // check party.organisationLocation defaultPrinter post-migration
        List<DefaultPrinter> postMigrationDefaultPrinters = getDefaultPrinters();
        assertEquals(2, postMigrationDefaultPrinters.size());
        checkDefaultPrinter(postMigrationDefaultPrinters.get(0), 111, ":Samsung M2070 Series\\: USB001");
        checkDefaultPrinter(postMigrationDefaultPrinters.get(1), 112, ":Samsung M2070 Series");

        // check entityRelationship.documentTemplatePrinter printer post-migration
        List<DocumentTemplatePrinter> postMigrationTemplates = getDocumentTemplatePrinters();
        assertEquals(2, postMigrationTemplates.size());
        checkDocumentTemplatePrinter(postMigrationTemplates.get(0), 445, null, ":Samsung M2070 Series\\: USB001");
        checkDocumentTemplatePrinter(postMigrationTemplates.get(1), 446, null, ":Samsung M2070 Series");

        // check entity.jobScheduledReport printer post-migration
        List<ReportPrinter> postMigrationReportPrinters = getScheduledReportJobPrinters();
        assertEquals(1, postMigrationReportPrinters.size());
        checkReportPrinter(postMigrationReportPrinters.get(0), 1177, ":Samsung M2070 Series\\: USB001");
    }

    /**
     * Verify a printer matches that expected.
     *
     * @param printer          the printer
     * @param id               the expected id
     * @param name             the expected name
     * @param printerReference the expected printer reference. May be {@code null}
     */
    private void checkPrinter(Printer printer, long id, String name, String printerReference) {
        assertEquals(id, printer.getId());
        assertEquals(name, printer.getName());
        assertEquals(printerReference, printer.getPrinterReference());
    }

    /**
     * Verifies the default printer associated with a practice location matches that expected.
     *
     * @param printer        the printer to check
     * @param id             the expected location id
     * @param defaultPrinter the expected default printer
     */
    private void checkDefaultPrinter(DefaultPrinter printer, long id, String defaultPrinter) {
        assertEquals(id, printer.getId());
        assertEquals(defaultPrinter, printer.getDefaultPrinter());
    }

    /**
     * Verifies a document template printer matches that expected.
     *
     * @param documentTemplatePrinter the document template printer to check
     * @param id                      the relationship id
     * @param printerName             the expected printer name
     * @param printer                 the expected printer
     */
    private void checkDocumentTemplatePrinter(DocumentTemplatePrinter documentTemplatePrinter,
                                              long id, String printerName, String printer) {
        assertEquals(id, documentTemplatePrinter.getId());
        assertEquals(printerName, documentTemplatePrinter.getPrinterName());
        assertEquals(printer, documentTemplatePrinter.getPrinter());
    }

    /**
     * Verifies the printer associated with a scheduled report job matches that expected.
     *
     * @param job     the job
     * @param id      the expected job id
     * @param printer the expected printer
     */
    private void checkReportPrinter(ReportPrinter job, long id, String printer) {
        assertEquals(id, job.getId());
        assertEquals(printer, job.getPrinter());
    }

    /**
     * Returns the available printers.
     * <p/>
     * Pre-migration, the {@link Printer#getPrinterReference()} will be {@code null}.
     *
     * @return the printers.
     */
    private List<Printer> getPrinters() {
        return template.query(
                "SELECT p.entity_id id, p.name, printer.value printerReference " +
                "FROM entities p " +
                "LEFT JOIN entity_details printer " +
                "ON p.entity_id = printer.entity_id " +
                "AND printer.name = 'printer' " +
                "WHERE p.arch_short_name = 'entity.printer' " +
                "ORDER BY p.entity_id",
                BeanPropertyRowMapper.newInstance(Printer.class));
    }

    /**
     * Returns the practice location default printers.
     */
    private List<DefaultPrinter> getDefaultPrinters() {
        return template.query(
                "SELECT l.entity_id id, default_printer.value defaultPrinter " +
                "FROM entities l " +
                "LEFT JOIN entity_details default_printer " +
                "ON l.entity_id = default_printer.entity_id " +
                "AND default_printer.name = 'defaultPrinter' " +
                "WHERE l.arch_short_name = 'party.organisationLocation' " +
                "ORDER BY l.entity_id",
                BeanPropertyRowMapper.newInstance(DefaultPrinter.class));
    }

    /**
     * Returns the entity.jobScheduledReport printers.
     */
    private List<ReportPrinter> getScheduledReportJobPrinters() {
        return template.query(
                "SELECT l.entity_id id, printer.value printer " +
                "FROM entities l " +
                "LEFT JOIN entity_details printer " +
                "ON l.entity_id = printer.entity_id " +
                "AND printer.name = 'printer' " +
                "WHERE l.arch_short_name = 'entity.jobScheduledReport' " +
                "ORDER BY l.entity_id",
                BeanPropertyRowMapper.newInstance(ReportPrinter.class));
    }

    /**
     * Returns the entityRelationship.documentTemplatePrinter.
     * <p/>
     * Pre-migration the printer is {@code null}. Post-migration the printerName is {@code null}.
     */
    private List<DocumentTemplatePrinter> getDocumentTemplatePrinters() {
        return template.query(
                "SELECT l.entity_relationship_id id, printer_name.value printerName, printer.value printer " +
                "FROM entity_relationships l " +
                "LEFT JOIN entity_relationship_details printer_name " +
                "ON l.entity_relationship_id = printer_name.entity_relationship_id " +
                "AND printer_name.name = 'printerName' " +
                "LEFT JOIN entity_relationship_details printer " +
                "ON l.entity_relationship_id = printer.entity_relationship_id " +
                "AND printer.name = 'printer' " +
                "WHERE l.arch_short_name = 'entityRelationship.documentTemplatePrinter' " +
                "ORDER BY l.entity_relationship_id",
                BeanPropertyRowMapper.newInstance(DocumentTemplatePrinter.class));
    }

}
