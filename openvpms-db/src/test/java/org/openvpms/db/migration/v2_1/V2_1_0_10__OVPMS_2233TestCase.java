/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.system.common.crypto.DefaultPasswordEncryptorFactory;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the migration {@link V2_1_0_10__OVPMS_2233} for OVPMS-2233 - 3rd party password encryption.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_10__OVPMS_2233TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC connection.
     */
    private Connection connection;


    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        connection = dataSource.getConnection();
        DbSupport support = DbSupportFactory.createDbSupport(connection, true);

        // load up test database baselined on V2.1.0.9
        ClassPathResource sql = new ClassPathResource("db-pre-V2.1.0.10__OVPMS-2233.sql", getClass().getClassLoader());
        SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
        script.execute(support.getJdbcTemplate());
    }

    /**
     * Close the connection.
     *
     * @throws Exception for any error
     */
    @After
    public void tearDown() throws Exception {
        if (connection != null) {
            connection.close();
        }
        connection = null;
    }

    /**
     * Verifies that the following are encrypted by the {@link V2_1_0_10__OVPMS_2233} migration:
     * <p/>
     * <ul>
     *     <li>entity.mailService - password</li>
     *     <li>party.organisationLocation - smartFlowSheetKey</li>
     *     <li>entityRelationship.supplierStockLocationESCI - password</li>
     *     <li>entity.pluginDeputy - accessToken</li>
     *     <li>entity.insuranceServicePetSure - password</li>
     * </ul>
     *
     * @throws Exception for any error
     */
    @Test
    public void testMigrate() throws Exception {
        String key = "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g";

        // check passwords prior to migration
        List<Data> mailServersPreMigrate = getEntities("entity.mailServer", "password");
        assertEquals(2, mailServersPreMigrate.size());
        checkPlainText(mailServersPreMigrate, "gmail", "gmailp");
        checkPlainText(mailServersPreMigrate, "iinet", "iinetp");

        List<Data> locationsPreMigrate = getEntities("party.organisationLocation", "smartFlowSheetKey");
        assertEquals(3, locationsPreMigrate.size());
        checkPlainText(locationsPreMigrate, "Main Clinic", "1234567890");
        checkPlainText(locationsPreMigrate, "Branch Clinic 1", "987654321");
        checkPlainText(locationsPreMigrate, "Branch Clinic 2", null);

        List<Data> esciPreMigrate = getESCIRelationships();
        assertEquals(3, esciPreMigrate.size());
        checkPlainText(esciPreMigrate, 17, "cenvetp");
        checkPlainText(esciPreMigrate, 18, "provetp1");
        checkPlainText(esciPreMigrate, 20, "provetp2");

        List<Data> deputyPreMigrate = getEntities("entity.pluginDeputy", "accessToken");
        assertEquals(1, deputyPreMigrate.size());
        checkPlainText(deputyPreMigrate, "Deputy Service", "deputyp1");

        List<Data> petsurePreMigrate = getEntities("entity.insuranceServicePetSure", "password");
        assertEquals(1, petsurePreMigrate.size());
        checkPlainText(petsurePreMigrate, "PetSure test service", "petsurep");

        // now run the migration
        System.setProperty("openvpms.key", key); // TODO - would like a better way of passing keys around
        service.update("2.1.0.10", null, null);

        // verify passwords have been migrated
        DefaultPasswordEncryptorFactory factory = new DefaultPasswordEncryptorFactory(key);
        PasswordEncryptor encryptor = factory.create();

        List<Data> mailServersPostMigrate = getEntities("entity.mailServer", "password");
        assertEquals(2, mailServersPostMigrate.size());
        checkEncrypted(mailServersPostMigrate, "gmail", "gmailp", encryptor);
        checkEncrypted(mailServersPostMigrate, "iinet", "iinetp", encryptor);

        List<Data> locationsPostMigrate = getEntities("party.organisationLocation", "smartFlowSheetKey");
        assertEquals(3, locationsPostMigrate.size());
        checkEncrypted(locationsPostMigrate, "Main Clinic", "1234567890", encryptor);
        checkEncrypted(locationsPostMigrate, "Branch Clinic 1", "987654321", encryptor);
        checkEncrypted(locationsPostMigrate, "Branch Clinic 2", null, encryptor);

        List<Data> esciPostMigrate = getESCIRelationships();
        assertEquals(3, esciPostMigrate.size());
        checkEncrypted(esciPostMigrate, 17, "cenvetp", encryptor);
        checkEncrypted(esciPostMigrate, 18, "provetp1", encryptor);
        checkEncrypted(esciPostMigrate, 20, "provetp2", encryptor);

        List<Data> deputyPostMigrate = getEntities("entity.pluginDeputy", "accessToken");
        assertEquals(1, deputyPostMigrate.size());
        checkEncrypted(deputyPostMigrate, "Deputy Service", "deputyp1", encryptor);

        List<Data> petsurePostMigrate = getEntities("entity.insuranceServicePetSure", "password");
        assertEquals(1, petsurePostMigrate.size());
        checkEncrypted(petsurePostMigrate, "PetSure test service", "petsurep", encryptor);
    }

    /**
     * Verifies that data with the specified name and plain-text password is present in a list.
     *
     * @param list     the data list
     * @param name     the expected name
     * @param password the expected password. May be {@code null}
     */
    private void checkPlainText(List<Data> list, String name, String password) {
        Data match = find(list, data -> data.getName().equals(name));
        assertEquals(password, match.getPassword());
    }

    /**
     * Verifies that data with the specified id and plain-text password is present in a list.
     *
     * @param list     the data list
     * @param id       the expected id
     * @param password the expected password. May be {@code null}
     */
    private void checkPlainText(List<Data> list, long id, String password) {
        Data match = find(list, data -> data.getId() == id);
        assertEquals(password, match.getPassword());
    }

    /**
     * Finds data in a list.
     *
     * @param list      the list
     * @param predicate the predicate to match
     * @return the first match
     */
    private Data find(List<Data> list, Predicate<Data> predicate) {
        Data match = list.stream().filter(predicate).findFirst().orElse(null);
        assertNotNull(match);
        return match;
    }

    /**
     * Verifies that data with the specified name and encrypted password is present in a list.
     *
     * @param list      the data list
     * @param name      the expected name
     * @param password  the expected password, in plain text. May be {@code null}
     * @param encryptor the encryptor to use to decrypt the password
     */
    private void checkEncrypted(List<Data> list, String name, String password, PasswordEncryptor encryptor) {
        Data match = find(list, data -> data.getName().equals(name));
        checkEncrypted(match, password, encryptor);
    }

    /**
     * Verifies that data with the specified id and encrypted password is present in a list.
     *
     * @param list      the data list
     * @param id        the expected id
     * @param password  the expected password, in plain text. May be {@code null}
     * @param encryptor the encryptor to use to decrypt the password
     */
    private void checkEncrypted(List<Data> list, long id, String password, PasswordEncryptor encryptor) {
        Data match = find(list, data -> data.getId() == id);
        checkEncrypted(match, password, encryptor);
    }

    /**
     * Verifies a password has been encrypted.
     *
     * @param data      the data
     * @param password  the expected password, in plain text. May be {@code null}
     * @param encryptor the encryptor to use to decrypt the password
     */
    private void checkEncrypted(Data data, String password, PasswordEncryptor encryptor) {
        if (password != null) {
            assertNotEquals(password, data.getPassword()); // password should have changed
            assertEquals(password, encryptor.decrypt(data.getPassword()));
        } else {
            assertNull(data.getPassword());
        }
    }

    /**
     * Returns entity password data.
     *
     * @param archetype the archetype
     * @param node      the password node
     * @return the entity data
     */
    private List<Data> getEntities(String archetype, String node) {
        return getData("entities", "entity_details", "entity_id", archetype, node);
    }

    /**
     * Returns ESCI relationship data.
     *
     * @return the relationship data
     */
    private List<Data> getESCIRelationships() {
        return getData("entity_relationships", "entity_relationship_details", "entity_relationship_id",
                       "entityRelationship.supplierStockLocationESCI", "password");
    }

    /**
     * Queries password data.
     *
     * @param table     the primary table
     * @param details   the details table
     * @param id        the id column to join on
     * @param archetype the archetype
     * @param node      the password node
     * @return the matching data
     */
    private List<Data> getData(String table, String details, String id, String archetype, String node) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template.query(
                "SELECT e." + id + " AS id, e.name AS name, d.value AS password " +
                "FROM " + table + " e " +
                "LEFT JOIN " + details + " d ON e." + id + " = d." + id + " AND d.name=? " +
                "WHERE e.arch_short_name=?",
                BeanPropertyRowMapper.newInstance(Data.class), node, archetype);

    }

    /**
     * Password data
     */
    private static class Data {

        private long id;

        private String name;

        private String password;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }

}
