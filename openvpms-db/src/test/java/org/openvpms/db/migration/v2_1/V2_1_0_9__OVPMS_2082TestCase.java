/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the OVPMS-2082 migration.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_9__OVPMS_2082TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        Connection connection = dataSource.getConnection();
        DbSupport support = DbSupportFactory.createDbSupport(connection, true);

        // load up test database baselined on V2.1.0.6
        ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
        SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
        script.execute(support.getJdbcTemplate());
        connection.close();
    }

    /**
     * Tests OVPMS-2082 migration.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMigration() throws Exception {
        checkInvestigationTypePreMigration(1008, "Rabies Test", null);
        checkInvestigationTypePreMigration(1010, "Histopath Test", null);
        checkInvestigationTypePreMigration(1150, "Lab Test (In House)", "123456789");
        checkProductPreMigration(642, "Histopathology", 1010);
        checkProductPreMigration(1121, "Lab Test (In House)", 1150);

        // verify there is an entityLink.investigationLaboratory between Lab Test (In House) and laboratory 1119
        checkEntityLink(1150, 1119, "entityLink.investigationLaboratory", true);

        // run the upgrade
        service.update("2.1.0.9", null, null);

        checkTest(1008, "entity.laboratoryTest", "Rabies Test", null);
        long test1 = checkTest(1010, "entity.laboratoryTest", "Histopath Test", null);
        long test2 = checkTest(1150, "entity.laboratoryTestHL7", "HL7 Laboratory Test - 123456789", "123456789");

        checkProductPostMigration(642, "Histopathology", 1010);
        checkProductPostMigration(1121, "Lab Test (In House)", 1150);

        // verify the tests are linked to the products
        checkProductTest(642, "Histopathology", test1);
        checkProductTest(1121, "Lab Test (In House)", test2);

        // verify the entityLink.investigationTypeLaboratory has been moved to entityLink.investigationLaboratory
        checkEntityLink(1150, 1119, "entityLink.investigationTypeLaboratory", true);
        checkEntityLink(1150, 1119, "entityLink.investigationLaboratory", false);
    }

    /**
     * Verifies an investigation type exists, pre-migration.
     *
     * @param id                 the investigation type identifier
     * @param name               the investigation type name
     * @param universalServiceId the universal service identifier
     */
    private void checkInvestigationTypePreMigration(long id, String name, String universalServiceId) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT count(*) " +
                     "FROM entities e ";
        if (universalServiceId != null) {
            sql += "JOIN entity_details service_id ON e.entity_id = service_id.entity_id AND " +
                   " service_id.name = 'universalServiceIdentifier' AND service_id.value='" + universalServiceId + "' ";
        }
        sql += "WHERE e.entity_id = " + id + " AND arch_short_name = 'entity.investigationType' AND e.name = '"
               + name + "'";
        Long result = template.queryForObject(sql, Long.class);
        assertNotNull(result);
        assertEquals(1, result.longValue());
    }

    /**
     * Verifies there is a relationship between a product and investigation type, pre-migration.
     *
     * @param id                  the product id
     * @param name                the product name
     * @param investigationTypeId the investigation type id
     */
    private void checkProductPreMigration(long id, String name, long investigationTypeId) {
        checkProductInvestigationRelationship(id, name, investigationTypeId, true);
    }

    /**
     * Verifies there is no relationship between a product and investigation type, post-migration.
     *
     * @param id                  the product id
     * @param name                the product name
     * @param investigationTypeId the investigation type id
     */
    private void checkProductPostMigration(long id, String name, long investigationTypeId) {
        checkProductInvestigationRelationship(id, name, investigationTypeId, false);
    }

    /**
     * Checks a relationship between a product and investigation type.
     *
     * @param id                  the product id
     * @param name                the product name
     * @param investigationTypeId the investigation type id
     * @param exists              if {@code true}, the relationship must exist, else it mustn't
     */
    private void checkProductInvestigationRelationship(long id, String name, long investigationTypeId, boolean exists) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT count(*) " +
                     "FROM entities product JOIN entity_links l ON product.entity_id = l.source_id " +
                     "AND l.arch_short_name = 'entityLink.productInvestigationType' " +
                     "JOIN entities investigation_type ON l.target_id = investigation_type.entity_id " +
                     "WHERE product.entity_id = " + id + " AND product.arch_short_name = 'product.service' " +
                     "AND product.name = '" + name + "' AND investigation_type.entity_id = " + investigationTypeId;
        Long result = template.queryForObject(sql, Long.class);
        assertNotNull(result);
        assertEquals(exists ? 1 : 0, result.longValue());
    }

    /**
     * Verifies a test has been linked to an investigation type.
     * <p/>
     * For those investigation types with a universalServiceIdentifier, verifies there is a corresponding
     * entityIdentity.laboratoryTest.
     *
     * @param investigationTypeId the investigation type identifier
     * @param archetype           the test archetype
     * @param name                the test name
     * @param id                  the test identifier. May be {@code null}
     * @return the entity identifier of the test
     */
    private long checkTest(long investigationTypeId, String archetype, String name, String id) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT test.entity_id " +
                     "FROM entities test " +
                     "JOIN entity_links l " +
                     "ON test.entity_id = l.source_id " +
                     "AND l.arch_short_name = 'entityLink.laboratoryTestInvestigationType'" +
                     "JOIN entities investigation_type " +
                     "ON investigation_type.entity_id = l.target_id " +
                     "AND investigation_type.entity_id = " + investigationTypeId +
                     " AND investigation_type.arch_short_name = 'entity.investigationType'" +
                     "WHERE test.arch_short_name = '" + archetype + "' " +
                     " AND test.name = '" + name + "'";
        Long testId = template.queryForObject(sql, Long.class);
        assertNotNull(testId);

        String sql2 = "SELECT count(*) FROM entity_identities WHERE entity_id = " + testId;
        if (id != null) {
            sql2 += " AND identity='" + id + "'";
        }
        Long count = template.queryForObject(sql2, Long.class);
        assertNotNull(count);
        assertEquals(count.longValue(), (id != null) ? 1 : 0);
        return testId;
    }

    /**
     * Verifies there is a relationship between a product and laboratory test.
     *
     * @param id     the product id
     * @param name   the product name
     * @param testId the test id
     */
    private void checkProductTest(long id, String name, long testId) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT count(*) " +
                     "FROM entities product JOIN entity_links l ON product.entity_id = l.source_id " +
                     "AND l.arch_short_name = 'entityLink.productLaboratoryTest' " +
                     "JOIN entities test ON l.target_id = test.entity_id " +
                     "WHERE product.entity_id = " + id + " AND product.arch_short_name = 'product.service' " +
                     "AND product.name = '" + name + "' AND test.entity_id = " + testId;
        Long result = template.queryForObject(sql, Long.class);
        assertNotNull(result);
        assertEquals(1, result.longValue());
    }

    private void checkEntityLink(long source, long target, String archetype, boolean exists) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "SELECT count(*) " +
                     "FROM entity_links l where l.arch_short_name = '" + archetype + "' and l.source_id = " + source +
                     " AND l.target_id = " + target;
        Long result = template.queryForObject(sql, Long.class);
        assertNotNull(result);
        assertEquals(exists ? 1 : 0, result.longValue());
    }

}
