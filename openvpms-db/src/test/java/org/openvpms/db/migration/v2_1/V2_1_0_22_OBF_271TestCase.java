/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.apache.commons.lang3.StringUtils;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the migration for OBF-271 Add index on entity_identities.identity.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_22_OBF_271TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;


    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.6
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Verifies that identities longer than 100 characters are truncated, and those under remain unchanged.
     *
     * @throws SQLException for any error
     */
    @Test
    public void testMigration() throws SQLException {
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");

        long identity1 = createIdentity("0123456789");

        String identity2Text = StringUtils.repeat("0123456789", 11);
        assertEquals(110, identity2Text.length());
        long identity2 = createIdentity(identity2Text);

        // run the migration
        service.update("2_1_0_23", null, null);

        // verify identities < 100 are unchanged
        checkIdentity(identity1, "0123456789", null);

        // verify identities > 100 are truncated

        String identity2Expected = StringUtils.repeat("0123456789", 10);
        assertEquals(100, identity2Expected.length());
        checkIdentity(identity2, identity2Expected, identity2Text);
    }

    /**
     * Creates an identity.
     *
     * @param identity the identity
     * @return the id of the EntityIdentity
     */
    private long createIdentity(String identity) {
        SimpleJdbcInsert insert = new SimpleJdbcInsert(dataSource)
                .withTableName("entity_identities")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<>(1);
        parameters.put("linkId", UUID.randomUUID().toString());
        parameters.put("arch_short_name", "entityIdentity.microchip");
        parameters.put("arch_version", "1.0");
        parameters.put("active", true);
        parameters.put("identity", identity);
        parameters.put("version", 0);
        Number id = insert.executeAndReturnKey(parameters);
        return id.longValue();
    }

    /**
     * Checks an identity matches that expected.
     *
     * @param id       the entity identifier id
     * @param expected the expected value
     * @param migrated the expected migrated text. May be {@code null}
     */
    private void checkIdentity(long id, String expected, String migrated) {
        String sql = "SELECT identity, d.value migrated " +
                     "FROM entity_identities i " +
                     "LEFT JOIN entity_identity_details d ON i.entity_identity_id = d.entity_identity_id " +
                     "AND d.name = 'migrated' " +
                     "where i.entity_identity_id = " + id;
        Map<String, Object> map = DataAccessUtils.singleResult(template.queryForList(sql));
        assertNotNull(map);
        assertEquals(expected, map.get("identity"));
        assertEquals(migrated, map.get("migrated"));
    }

}
