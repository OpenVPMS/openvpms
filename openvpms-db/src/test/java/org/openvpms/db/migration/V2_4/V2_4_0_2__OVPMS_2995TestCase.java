/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.V2_4;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the migration for OVPMS-2995.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_4_0_2__OVPMS_2995TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;

    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.4.0.1
            ClassPathResource sql = new ClassPathResource("db-V2.4.0.1__OVPMS-2995.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Tests the migration.
     *
     * @throws SQLException for any SQL error
     */
    @Test
    public void testMigration() throws SQLException {
        checkActCounts(395);
        checkActIdentityCounts(13);
        checkActIdentityCounts("actIdentity.EFTPOSParentId", 4);
        checkActIdentityCounts("actIdentity.EFTPOSAuthCode", 2);
        checkActIdentityCounts("actIdentity.EFTPOSTransactionSample", 2);
        checkActCounts("act.EFTPOSPayment", 3);
        checkActCounts("act.EFTPOSRefund", 3);
        checkActExists(514, "act.EFTPOSRefund", true);
        checkActExists(509, "act.EFTPOSPayment", true);
        checkActDetailCounts(704);
        checkActDetailCounts("act.EFTPOSPayment", 8);
        checkActDetailCounts("act.EFTPOSRefund", 2);
        checkActRelationshipCounts(226);
        checkActRelationshipCounts("actRelationship.customerAccountPaymentEFTPOS", 3);
        checkActRelationshipCounts("actRelationship.customerAccountRefundEFTPOS", 3);
        checkRelationshipExists(206, "actRelationship.customerAccountPaymentEFTPOS", true);
        checkRelationshipExists(214, "actRelationship.customerAccountRefundEFTPOS", true);

        // run the migration
        service.update("2.4.0.2", null, null);

        // check migrated counts
        checkActCounts(393);
        checkActCounts("act.EFTPOSPayment", 2);
        checkActCounts("act.EFTPOSRefund", 2);
        checkActIdentityCounts(11);
        checkActIdentityCounts("actIdentity.EFTPOSParentId", 2);
        checkActIdentityCounts("actIdentity.EFTPOSAuthCode", 2);
        checkActIdentityCounts("actIdentity.EFTPOSTransactionSample", 2);
        checkActDetailCounts(702);
        checkActRelationshipCounts(224);
        checkActRelationshipCounts("actRelationship.customerAccountPaymentEFTPOS", 2);
        checkActRelationshipCounts("actRelationship.customerAccountRefundEFTPOS", 2);
        checkRelationshipExists(206, "actRelationship.customerAccountPaymentEFTPOS", false);
        checkRelationshipExists(214, "actRelationship.customerAccountRefundEFTPOS", false);
        checkActExists(514, "act.EFTPOSRefund", false);
        checkActExists(509, "act.EFTPOSPayment", false);
    }

    /**
     * Checks if an act exists or not.
     *
     * @param id        the act id
     * @param archetype the archetype
     * @param exists    if {@code true} the act must exist, otherwise it must be deleted
     */
    private void checkActExists(long id, String archetype, boolean exists) {
        checkExists(id, archetype, "acts", "act_id", exists);
    }

    /**
     * Checks if a relationship exists or not.
     *
     * @param id        the act id
     * @param archetype the archetype
     * @param exists    if {@code true} the relationship must exist, otherwise it must be deleted
     */
    private void checkRelationshipExists(long id, String archetype, boolean exists) {
        checkExists(id, archetype, "act_relationships", "act_relationship_id", exists);
    }

    /**
     * Checks if a record exists or not.
     *
     * @param id        the record id
     * @param archetype the archetype
     * @param table     the table
     * @param column    the id column
     * @param exists    if {@code true} the record must exist, otherwise it must be deleted
     */
    private void checkExists(long id, String archetype, String table, String column, boolean exists) {
        String sql = "SELECT COUNT(*) FROM " + table + " WHERE arch_short_name='" + archetype + "' AND "
                     + column + "=" + id;
        checkCount(sql, exists ? 1 : 0);
    }

    /**
     * Verifies that the count of acts records matches that expected.
     *
     * @param expected the expected count
     */
    private void checkActCounts(int expected) {
        checkCount("acts", null, expected);
    }

    /**
     * Verifies that the count of acts records matches that expected.
     *
     * @param archetype the act archetype
     * @param expected  the expected count
     */
    private void checkActCounts(String archetype, int expected) {
        checkCount("acts", archetype, expected);
    }

    /**
     * Verifies that the count of act identity records matches that expected.
     *
     * @param expected the expected count
     */
    private void checkActIdentityCounts(int expected) {
        checkCount("act_identities", null, expected);
    }

    /**
     * Verifies that the count of act identity records matches that expected.
     *
     * @param archetype the identity archetype
     * @param expected the expected count
     */
    private void checkActIdentityCounts(String archetype, int expected) {
        checkCount("act_identities", archetype, expected);
    }

    /**
     * Verifies that the count of act_details records matches that expected.
     *
     * @param expected the expected count
     */
    private void checkActDetailCounts(int expected) {
        checkCount("act_details", null, expected);
    }

    /**
     * Verifies that the count of act_details records matches that expected.
     *
     * @param archetype the act archetype
     * @param expected  the expected count
     */
    private void checkActDetailCounts(String archetype, int expected) {
        String sql = "SELECT COUNT(*) FROM acts a JOIN act_details d ON a.act_id = d.act_id WHERE a.arch_short_name='"
                     + archetype + "'";
        checkCount(sql, expected);
    }

    /**
     * Verifies that the count of act_relationships records matches that expected.
     *
     * @param expected the expected count
     */
    private void checkActRelationshipCounts(int expected) {
        checkCount("act_relationships", null, expected);
    }

    /**
     * Verifies that the count of act_relationships records matches that expected.
     *
     * @param archetype the relationship archetype
     * @param expected  the expected count
     */
    private void checkActRelationshipCounts(String archetype, int expected) {
        checkCount("act_relationships", archetype, expected);
    }

    /**
     * Verifies the count of records in a table matches that expected.
     *
     * @param table     the table
     * @param archetype the archetype. May be {@code null}
     * @param expected  the expected no. of records
     */
    private void checkCount(String table, String archetype, int expected) {
        String sql = "SELECT COUNT(*) FROM " + table;
        if (archetype != null) {
            sql += " WHERE arch_short_name='" + archetype + "'";
        }
        checkCount(sql, expected);
    }

    /**
     * Verifies that the count returned by an SQL statement matches that expected.
     *
     * @param sql      the SQL
     * @param expected the expected count
     */
    private void checkCount(String sql, int expected) {
        Integer count = template.queryForObject(sql, Integer.class);
        assertNotNull(count);
        assertEquals(expected, count.intValue());
    }
}
