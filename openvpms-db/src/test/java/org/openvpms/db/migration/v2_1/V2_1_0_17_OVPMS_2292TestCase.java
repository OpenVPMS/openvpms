/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the migration for OVPMS-2292 Add support for clinical notes and addenda of unlimited length.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_17_OVPMS_2292TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;


    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.6
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Verifies that document_act rows are added for each act.patientClinicalNote and act.patientClinicalAddenda.
     *
     * @throws SQLException for any error
     */
    @Test
    public void testMigration() throws SQLException {

        // verify the notes and addenda present for patient 13
        List<Long> noteIds = getActIds("act.patientClinicalNote");
        assertEquals(5, noteIds.size());
        assertEquals(1, noteIds.get(0).longValue());
        assertEquals(9, noteIds.get(1).longValue());
        assertEquals(13, noteIds.get(2).longValue());
        assertEquals(18, noteIds.get(3).longValue());
        assertEquals(283, noteIds.get(4).longValue());

        List<Long> addendaIds = getActIds("act.patientClinicalAddendum");
        assertEquals(2, addendaIds.size());
        assertEquals(284, addendaIds.get(0).longValue());
        assertEquals(285, addendaIds.get(1).longValue());

        // verify there are no existing document_acts rows for the acts
        for (long id : noteIds) {
            checkDocumentAct(id, false);
        }

        for (long id : addendaIds) {
            checkDocumentAct(id, false);
        }

        // run the migration
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        service.update("2_1_0_17", null, null);

        // verify document_acts rows have been inserted for each one
        for (long id : noteIds) {
            checkDocumentAct(id, true);
        }

        for (long id : addendaIds) {
            checkDocumentAct(id, true);
        }
    }

    /**
     * Returns the acts for patient 13.
     *
     * @param archetype the act archetype
     * @return the matching ids, in ascending order
     */
    private List<Long> getActIds(String archetype) {
        String sql = "SELECT a.act_id AS id " +
                     "FROM acts a " +
                     "JOIN participations p " +
                     " on p.act_id = a.act_id and p.arch_short_name = 'participation.patient' " +
                     " and p.entity_id = 13 " +
                     "WHERE a.arch_short_name = '" + archetype + "' " +
                     "ORDER BY a.act_id";
        return template.queryForList(sql, Long.class);
    }

    /**
     * Verifies that a document act exists/doesn't exist for the specified identifier.
     *
     * @param id    the act identifier
     * @param exist if {@code true} expect the row to exist
     */
    private void checkDocumentAct(long id, boolean exist) {
        String sql = "SELECT count(*) " +
                     "FROM acts a " +
                     "JOIN document_acts d on a.act_id = d.document_act_id " +
                     "WHERE d.document_act_id = " + id;
        if (exist) {
            // no document should be present, and printed should be false.
            sql += " and d.document_id is null and d.printed = 0";
        }
        Long result = template.queryForObject(sql, Long.class);
        assertNotNull(result);
        if (exist) {
            assertEquals(1, result.longValue());
        } else {
            assertEquals(0, result.longValue());
        }
    }
}
