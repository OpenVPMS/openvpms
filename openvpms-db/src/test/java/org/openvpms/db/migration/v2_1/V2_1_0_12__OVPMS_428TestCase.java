/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the migration {@link V2_1_0_12__OVPMS_428} for OVPMS-428 - User password hashing.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_12__OVPMS_428TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC connection.
     */
    private Connection connection;


    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        connection = dataSource.getConnection();
        DbSupport support = DbSupportFactory.createDbSupport(connection, true);

        // load up test database baselined on V2.1.0.9
        ClassPathResource sql = new ClassPathResource("db-pre-V2.1.0.10__OVPMS-2233.sql", getClass().getClassLoader());
        SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
        script.execute(support.getJdbcTemplate());
    }

    /**
     * Close the connection.
     *
     * @throws Exception for any error
     */
    @After
    public void tearDown() throws Exception {
        if (connection != null) {
            connection.close();
        }
        connection = null;
    }

    /**
     * Verifies that the following are encrypted by the {@link V2_1_0_10__OVPMS_2233} migration:
     * <p/>
     * <ul>
     *     <li>entity.mailService - password</li>
     *     <li>party.organisationLocation - smartFlowSheetKey</li>
     *     <li>entityRelationship.supplierStockLocationESCI - password</li>
     *     <li>entity.pluginDeputy - accessToken</li>
     *     <li>entity.insuranceServicePetSure - password</li>
     * </ul>
     *
     * @throws Exception for any error
     */
    @Test
    public void testMigrate() throws Exception {
        List<Password> preMigrate = getPasswords();

        // check passwords prior to migration
        assertEquals(2, preMigrate.size());
        checkPlainText(preMigrate, 1, "admin");
        checkPlainText(preMigrate, 2, "vet");

        // now run the migration. Migrating from 2.1.0.9, so need to migrate 3rd party passwords as well
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        service.update("2.1.0.12", null, null);

        // verify passwords have been migrated
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        List<Password> postMigrate = getPasswords();
        assertEquals(2, postMigrate.size());
        checkEncrypted(postMigrate, 1, "admin", encoder);
        checkEncrypted(postMigrate, 2, "vet", encoder);
    }

    /**
     * Returns all the user passwords.
     *
     * @return the user passwords
     */
    protected List<Password> getPasswords() {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template.query(
                "SELECT users.user_id AS id, users.password " +
                "FROM users",
                BeanPropertyRowMapper.newInstance(Password.class));
    }

    /**
     * Verifies that data with the specified id and plain-text password is present in a list.
     *
     * @param list     the data list
     * @param id       the expected id
     * @param password the expected password. May be {@code null}
     */
    private void checkPlainText(List<Password> list, long id, String password) {
        Password match = find(list, id);
        assertEquals(password, match.getPassword());
    }

    /**
     * Finds data in a list.
     *
     * @param list the list
     * @param id   the user id
     * @return the first match
     */
    private Password find(List<Password> list, long id) {
        Password match = list.stream().filter(password -> id == password.getId()).findFirst().orElse(null);
        assertNotNull(match);
        return match;
    }

    /**
     * Verifies that data with the specified id and encrypted password is present in a list.
     *
     * @param list     the data list
     * @param id       the expected id
     * @param password the expected password, in plain text
     * @param encoder  the encryptor to use to decrypt the password
     */
    private void checkEncrypted(List<Password> list, long id, String password, PasswordEncoder encoder) {
        Password match = find(list, id);
        assertNotEquals(password, match.getPassword()); // password should have changed
        assertTrue(encoder.matches(password, match.getPassword()));
    }

    /**
     * Password data
     */
    private static class Password {

        private long id;

        private String password;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }

}
