/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the migration for OVPMS-2346 Migrate entity.scheduledJob relative date parameters.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_19_OVPMS_2346TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseAdminService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;


    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.6
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Verifies that act.supplierDelivery rows with supplierInvoiceIds get replaced with
     * actIdentity.supplierInvoiceESCI.
     *
     * @throws SQLException for any error
     */
    @Test
    public void testMigration() throws SQLException {
        checkDateParameter("2020-08-14 11:54:54.294", 0);
        checkDateParameter("2020-08-14 11:54:54.294", 1);

        // run the migration
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        service.update("2_1_0_18", null, null);

        int count = getRowCount();    // count rows in entity_details
        service.update("2_1_0_19", null, null);   // run the date parameter migration

        checkDateParameter(null, 0);  // verify the parameters are no longer present
        checkDateParameter(null, 1);

        assertEquals(count - 2, getRowCount()); // verify only 2 rows have been removed
    }

    /**
     * Checks a date parameter value for entity.jobScheduledReport with id = 1177.
     *
     * @param expected the expected value. May be {@code null}
     * @param index    the parameter index
     */
    private void checkDateParameter(String expected, int index) {
        String sql = "SELECT param_value.value " +
                     "FROM entities report " +
                     "JOIN entity_details param_name " +
                     "    ON report.entity_id = param_name.entity_id " +
                     "JOIN entity_details param_type " +
                     "    ON report.entity_id = param_type.entity_id " +
                     "JOIN entity_details param_value " +
                     "    ON report.entity_id = param_value.entity_id " +
                     "JOIN entity_details param_expr_type " +
                     "    ON report.entity_id = param_expr_type.entity_id " +
                     "WHERE report.arch_short_name = 'entity.jobScheduledReport' " +
                     "    AND param_type.value = 'java.util.Date' " +
                     "    AND param_value.type = 'sql-timestamp' " +
                     "    AND param_expr_type.value <> 'VALUE' " +
                     "    AND param_name.name = 'paramName" + index + "' " +
                     "    AND param_value.name = 'paramValue" + index + "' " +
                     "    AND param_type.name = 'paramType" + index + "' " +
                     "    AND param_expr_type.name = 'paramExprType" + index + "' " +
                     "    AND report.entity_id = 1177";

        assertEquals(expected, DataAccessUtils.singleResult(template.queryForList(sql, String.class)));
    }

    /**
     * Returns the row count of the entity_details table.
     *
     * @return the row count
     */
    private int getRowCount() {
        Integer count = template.queryForObject("SELECT count(*) FROM entity_details", Integer.class);
        assertNotNull(count);
        return count;
    }
}
