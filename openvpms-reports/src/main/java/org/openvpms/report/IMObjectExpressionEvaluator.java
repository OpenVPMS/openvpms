/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.apache.commons.jxpath.Functions;
import org.openvpms.component.business.service.archetype.helper.NodeResolver;
import org.openvpms.component.business.service.archetype.helper.ResolvingPropertySet;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.jasper.ReportContext;

import java.util.Map;


/**
 * Evaluates report expressions.
 * <p/>
 * Expressions may take one of two forms:
 * <ol>
 * <li>node1.node2.nodeN</li>
 * <li>[expr]</li>
 * </ol>
 * Expressions of the first type are evaluated using {@link NodeResolver};
 * the second by {@code JXPath}.
 *
 * @author Tim Anderson
 */
public class IMObjectExpressionEvaluator extends AbstractExpressionEvaluator<IMObject> {

    /**
     * The node resolver.
     */
    private NodeResolver resolver;

    /**
     * Constructs a {@link IMObjectExpressionEvaluator}.
     *
     * @param object     the object
     * @param parameters parameters available to expressions as variables. May be {@code null}
     * @param fields     additional report fields. These override any in the report. May be {@code null}
     * @param reportName the report name, for error reporting purposes
     * @param service    the archetype service
     * @param lookups    the lookup service
     * @param functions  the JXPath extension functions
     */
    public IMObjectExpressionEvaluator(IMObject object, Parameters parameters, Map<String, Object> fields,
                                       String reportName, ArchetypeService service, LookupService lookups,
                                       Functions functions) {
        super(object, new ReportContext(parameters,
                                        fields != null ? new ResolvingPropertySet(fields, service, lookups) : null,
                                        reportName, service,
                                        lookups, null, functions));
    }

    /**
     * Constructs a {@link IMObjectExpressionEvaluator}.
     *
     * @param object   the object
     * @param resolver the node resolver. May be {@code null}
     * @param context  the report context
     */
    public IMObjectExpressionEvaluator(IMObject object, NodeResolver resolver, ReportContext context) {
        super(object, context);
        this.resolver = resolver;
    }

    /**
     * Returns a node value.
     *
     * @param name the node name
     * @return the node value
     */
    protected Object getNodeValue(String name) {
        if (resolver == null) {
            resolver = new NodeResolver(getObject(), getService(), getLookups());
        }
        return getValue(name, resolver);
    }

}
