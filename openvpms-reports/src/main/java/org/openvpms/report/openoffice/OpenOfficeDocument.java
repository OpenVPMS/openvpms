/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import com.sun.star.awt.Point;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XModel;
import com.sun.star.frame.XStorable;
import com.sun.star.io.XInputStream;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.lib.uno.adapter.ByteArrayToXInputStreamAdapter;
import com.sun.star.lib.uno.adapter.XOutputStreamToByteArrayAdapter;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextField;
import com.sun.star.text.XTextFieldsSupplier;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.util.XCloseable;
import com.sun.star.util.XRefreshable;
import com.sun.star.view.DuplexMode;
import com.sun.star.view.XPrintable;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.jodconverter.local.office.LocalOfficeContext;
import org.openvpms.archetype.rules.doc.DocumentException;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;
import org.openvpms.domain.internal.document.CompressedDocumentImpl;
import org.openvpms.report.DocFormats;
import org.openvpms.report.ParameterType;
import org.openvpms.report.PrintProperties;
import org.openvpms.report.ReportException;
import org.openvpms.report.i18n.ReportMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.print.attribute.standard.Sides;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;


/**
 * Thin wrapper around an OpenOffice document.
 *
 * @author Tim Anderson
 */
public class OpenOfficeDocument implements AutoCloseable {

    /**
     * The document name.
     */
    private final String name;

    /**
     * String for debugging.
     */
    private final String debug;

    /**
     * The document component.
     */
    private final XComponent document;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The user fields, keyed on name.
     */
    private Map<String, Field> userFields;

    /**
     * The input fields, keyed on name.
     */
    private Map<String, Field> inputFields;

    /**
     * Prefix for user fields.
     * NOTE: in OO 2.1, <em>fieldmaster</em> was named <em>FieldMaster</em>.
     */
    private static final String USER_FIELD_PREFIX = "com.sun.star.text.fieldmaster.User.";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OpenOfficeDocument.class);


    /**
     * Constructs an {@link OpenOfficeDocument}.
     *
     * @param document the source document
     * @param context  the OpenOffice context
     * @param handlers the document handlers
     * @throws OpenOfficeException for any error
     */
    public OpenOfficeDocument(Document document, LocalOfficeContext context, DocumentHandlers handlers) {
        name = document.getName();
        this.debug = "id=" + document.getId() + ", name=" + name;

        XComponentLoader loader = context.getComponentLoader();
        if (loader == null) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToLoadOpenOfficeDocument(name));
        }

        byte[] content;
        if (!(document instanceof CompressedDocumentImpl)) {
            // TODO - any binary document retrieved directly from the archetype service is likely to be compressed.
            // Need a method to handle these transparently
            document = new CompressedDocumentImpl(document, handlers);
        }
        try (InputStream input = document.getContent();
             ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            IOUtils.copy(input, output);
            content = output.toByteArray();
        } catch (Exception exception) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToLoadOpenOfficeDocument(name), exception);
        }
        XInputStream xstream = new ByteArrayToXInputStreamAdapter(content);

        PropertyValue[] properties;
        PropertyValue readOnly = property("ReadOnly", Boolean.TRUE);
        PropertyValue hidden = property("Hidden", Boolean.TRUE);
        PropertyValue asTemplate = property("AsTemplate", true);
        // AsTemplate tells office to create a new document from the given stream
        PropertyValue inputStream = property("InputStream", xstream);

        String extension = FilenameUtils.getExtension(name);
        if (DocFormats.RTF_EXT.equalsIgnoreCase(extension)) {
            // need to specify FilterName to avoid awful performance loading RTF. See REP-17
            PropertyValue filter = property("FilterName", "Rich Text Format");
            properties = new PropertyValue[]{filter, readOnly, hidden, asTemplate, inputStream};
        } else {
            properties = new PropertyValue[]{readOnly, hidden, asTemplate, inputStream};
        }

        XComponent component = run("sending document " + debug, () -> {
            try {
                return loader.loadComponentFromURL("private:stream", "_blank", 0, properties);
            } catch (Exception exception) {
                throw new OpenOfficeDocumentException(ReportMessages.failedToLoadOpenOfficeDocument(name), exception);
            }
        });
        if (component == null) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToLoadOpenOfficeDocument(name));
        }
        this.document = component;
        this.handlers = handlers;
    }

    /**
     * Returns the document name.
     *
     * @return the document name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the underlying component.
     *
     * @return the underlying component
     */
    public XComponent getComponent() {
        return document;
    }

    /**
     * Returns the set of user field names.
     *
     * @return the list of user field names.
     * @throws OpenOfficeException if the fields cannot be accessed
     */
    public List<String> getUserFieldNames() {
        getFields();
        return new ArrayList<>(userFields.keySet());
    }

    /**
     * Gets the value of a user field.
     *
     * @param name the field name
     * @return the value of the field
     * @throws OpenOfficeException if the field cannot be accessed
     */
    public String getUserField(String name) {
        getFields();
        Field field = userFields.get(name);
        if (field == null) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToGetOpenOfficeField(name, name));
        }
        return getContent(field);
    }

    /**
     * Sets the value of a user field.
     *
     * @param name  the field name
     * @param value the value of the field
     * @throws OpenOfficeException if the field cannot be updated
     */
    public void setUserField(String name, String value) {
        getFields();
        Field field = userFields.get(name);
        if (field == null) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToGetOpenOfficeField(name, name));
        }
        setContent(field, value);
    }

    /**
     * Returns the set of input fields.
     * <p/>
     * These refer to fields whose values should be prompted for.
     *
     * @return the list of input field names.
     * @throws OpenOfficeException if the fields cannot be accessed
     */
    public Map<String, ParameterType> getInputFields() {
        Map<String, ParameterType> result = new LinkedHashMap<>();
        getFields();
        for (Field field : inputFields.values()) {
            ParameterType param = new ParameterType(field.getName(), String.class, field.getValue(),
                                                    field.getContent());
            result.put(field.getName(), param);
        }
        return result;
    }

    /**
     * Determines if an input field exists with the specified name.
     *
     * @param name the input field name
     * @return {@code true} if the input field exists, otherwise {@code false}
     */
    public boolean hasInputField(String name) {
        getFields();
        return inputFields.containsKey(name);
    }

    /**
     * Determines if a user field exists with the specified name.
     *
     * @param name the user field name
     * @return {@code true} if the user field exists, otherwise {@code false}
     */
    public boolean hasUserField(String name) {
        getFields();
        return userFields.containsKey(name);
    }

    /**
     * Returns the value of an input field.
     *
     * @param name the input field name
     * @return the input field value. May be {@code null}
     */
    public String getInputField(String name) {
        getFields();
        Field field = inputFields.get(name);
        if (field == null) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToGetOpenOfficeField(name, name));
        }
        return getContent(field);
    }

    /**
     * Sets the value of an input field.
     *
     * @param name  the input field name
     * @param value the input field value. May be {@code null}
     */
    public void setInputField(String name, String value) {
        getFields();
        Field field = inputFields.get(name);
        if (field == null) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToGetOpenOfficeField(name, name));
        }
        // TODO - add support for InputUser fields
        setContent(field, value);
    }

    /**
     * Refreshes the document fields.
     */
    public void refresh() {
        run("refreshing " + debug, () -> {
            XEnumerationAccess fields = getTextFields();
            if (fields != null) {
                XRefreshable refreshable = UnoRuntime.queryInterface(XRefreshable.class, fields);
                if (refreshable != null) {
                    refreshable.refresh();
                }
            }
        });
    }

    /**
     * Exports the document.
     *
     * @param mimeType the mime-type of the document format to export to
     * @return the exported document serialized to a byte array
     * @throws OpenOfficeException if the document cannot be exported
     */
    public byte[] export(String mimeType) {
        return run("exporting " + debug + " to " + mimeType, () -> {
            XTextDocument textDocument = UnoRuntime.queryInterface(XTextDocument.class, document);
            if (textDocument == null) {
                throw new OpenOfficeDocumentException(ReportMessages.failedToExportOpenOfficeDocument(name));
            }
            XStorable storable = UnoRuntime.queryInterface(XStorable.class, textDocument);
            if (storable == null) {
                throw new OpenOfficeDocumentException(ReportMessages.failedToExportOpenOfficeDocument(name));
            }
            XOutputStreamToByteArrayAdapter stream = new XOutputStreamToByteArrayAdapter();

            PropertyValue[] properties;
            PropertyValue outputStream = property("OutputStream", stream);
            PropertyValue overwrite = property("Overwrite", true);
            switch (mimeType) {
                case DocFormats.PDF_TYPE: {
                    PropertyValue filter = property("FilterName", "writer_pdf_Export");
                    properties = new PropertyValue[]{outputStream, overwrite, filter};
                    break;
                }
                case DocFormats.DOC_TYPE: {
                    PropertyValue filter = property("FilterName", "MS Word 97");
                    properties = new PropertyValue[]{outputStream, overwrite, filter};
                    break;
                }
                case DocFormats.TEXT_TYPE: {
                    PropertyValue filter = property("FilterName", "Text");
                    properties = new PropertyValue[]{outputStream, overwrite, filter};
                    break;
                }
                case DocFormats.HTML_TYPE: {
                    PropertyValue filter = property("FilterName", "HTML (StarWriter)");
                    properties = new PropertyValue[]{outputStream, overwrite, filter};
                    break;
                }
                default:
                    properties = new PropertyValue[]{outputStream, overwrite};
                    break;
            }

            try {
                storable.storeToURL("private:stream", properties);
                stream.closeOutput();
            } catch (Exception exception) {
                throw new OpenOfficeDocumentException(ReportMessages.failedToExportOpenOfficeDocument(name), exception);
            }
            return stream.getBuffer();
        });
    }

    /**
     * Exports the document.
     *
     * @param mimeType the mime-type of the document format to export to
     * @param name     the document name
     * @return the exported document
     * @throws OpenOfficeException if the source document cannot be exported
     * @throws DocumentException   if the target document cannot be created
     */
    public Document export(String mimeType, String name) {
        byte[] content = export(mimeType);
        switch (mimeType) {
            case DocFormats.PDF_TYPE:
                name = getFileName(name, DocFormats.PDF_EXT);
                break;
            case DocFormats.DOC_TYPE:
                name = getFileName(name, DocFormats.DOC_EXT);
                break;
            case DocFormats.TEXT_TYPE:
                name = getFileName(name, DocFormats.TEXT_EXT);
                break;
            case DocFormats.HTML_TYPE:
                name = getFileName(name, DocFormats.HTML_EXT);
                break;
        }

        try {
            DocumentHandler handler = handlers.get(name, "document.other", mimeType);
            return handler.create(name, new ByteArrayInputStream(content), mimeType, content.length);
        } catch (OpenVPMSException exception) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToExportOpenOfficeDocument(name), exception);
        }
    }

    /**
     * Prints a document.
     *
     * @param properties the print properties
     * @throws OpenOfficeException for any error
     */
    public void print(PrintProperties properties) {
        run("printing " + debug, () -> {
            XPrintable printable = UnoRuntime.queryInterface(XPrintable.class, document);
            if (printable == null) {
                throw new OpenOfficeException(ReportMessages.failedToPrintOpenOfficeDocument(name));
            }
            int copies = properties.getCopies();
            String printer = properties.getPrinterName();
            Sides sides = properties.getSides();
            Short printSides = getDuplexMode(sides);
            PropertyValue[] printerDesc = {property("Name", printer)};
            PropertyValue[] printOpts = {property("Wait", true), property("CopyCount", copies),
                                         property("DuplexMode", printSides)};
            try {
                printable.setPrinter(printerDesc);
                printable.print(printOpts);
            } catch (ReportException exception) {
                throw exception;
            } catch (Throwable exception) {
                throw new OpenOfficeException(ReportMessages.failedToPrintOpenOfficeDocument(name), exception);
            }
        });
    }

    /**
     * Closes the document.
     */
    @Override
    public void close() {
        run("closing " + debug, () -> {
            try {
                XModel model = UnoRuntime.queryInterface(XModel.class, document);
                if (model != null) {
                    XCloseable closeable = UnoRuntime.queryInterface(XCloseable.class, model);
                    if (closeable != null) {
                        closeable.close(false);
                    }
                }
            } catch (Throwable exception) {
                log.error("Failed to close document", exception);
            }
        });
    }

    /**
     * Returns the user text fields.
     *
     * @return the user text fields, keyed on name
     * @throws OpenOfficeException if the fields can't be accessed
     */
    protected Map<String, Field> getUserTextFields() {
        Map<String, Field> result = new LinkedHashMap<>();
        XNameAccess fields = getTextFieldMasters();
        if (fields != null) {
            for (String elementName : fields.getElementNames()) {
                try {
                    // need to do a case-insensitive comparison for OVPMS-749
                    if (elementName.regionMatches(true, 0, USER_FIELD_PREFIX, 0, USER_FIELD_PREFIX.length())) {
                        String name = elementName.substring(USER_FIELD_PREFIX.length());
                        Object fieldMaster = fields.getByName(elementName);
                        XPropertySet propertySet = UnoRuntime.queryInterface(XPropertySet.class, fieldMaster);
                        if (propertySet != null) {
                            String value = (String) propertySet.getPropertyValue("Content");
                            Field field = new Field(name, value, propertySet);
                            result.put(name, field);
                        }
                    }
                } catch (Exception exception) {
                    throw new OpenOfficeDocumentException(ReportMessages.failedToGetOpenOfficeUserFields(name), exception);
                }
            }
        }
        return result;
    }

    /**
     * Returns the input text fields.
     *
     * @return the input text fields, keyed on name
     * @throws OpenOfficeException if the fields can't be accessed
     */
    protected Map<String, Field> getInputTextFields() {
        Map<String, Field> result = new LinkedHashMap<>();
        XEnumeration fields = getTextFieldsEnumeration();
        if (fields != null) {
            int seed = 0;
            try {
                while (fields.hasMoreElements()) {
                    XPropertySet set = getInputFieldPropertySet(fields.nextElement());
                    if (set != null) {
                        String name = "inputField" + (++seed);
                        String value = StringUtils.trimToNull((String) set.getPropertyValue("Hint"));
                        String content = StringUtils.trimToNull((String) set.getPropertyValue("Content"));
                        if (value == null) {
                            value = content;
                        }
                        result.put(name, new Field(name, value, set, null, content));
                    }
                }
            } catch (Throwable exception) {
                throw new OpenOfficeDocumentException(ReportMessages.failedToGetOpenOfficeInputFields(name), exception);
            }
        }
        return result;
    }

    /**
     * Sets the content of a field.
     *
     * @param field the field
     * @param value the new value. May be {@code null}
     * @throws OpenOfficeException if the field cannot be updated
     */
    protected void setContent(Field field, String value) {
        try {
            field.getPropertySet().setPropertyValue("Content", value);
            field.setChanged(true);
        } catch (Throwable exception) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToSetOpenOfficeField(field.getName(), name),
                                                  exception);
        }
    }

    /**
     * Returns the content of a field.
     *
     * @param field the field
     * @return the field content. May be {@code null}
     * @throws OpenOfficeException if the field cannot be accessed
     */
    protected String getContent(Field field) {
        Object content;
        try {
            content = field.getPropertySet().getPropertyValue("Content");
        } catch (Throwable exception) {
            throw new OpenOfficeDocumentException(ReportMessages.failedToGetOpenOfficeField(field.getName(), name),
                                                  exception);
        }
        return content != null ? content.toString() : null;
    }

    /**
     * Returns the text field supplier interface.
     *
     * @return the text field supplier interface. May be {@code null}
     */
    protected XTextFieldsSupplier getTextFieldSupplier() {
        return UnoRuntime.queryInterface(XTextFieldsSupplier.class, document);
    }

    /**
     * Returns the text fields.
     *
     * @return the text fields. May be {@code null}
     */
    protected XEnumerationAccess getTextFields() {
        XTextFieldsSupplier supplier = getTextFieldSupplier();
        return (supplier != null) ? supplier.getTextFields() : null;
    }

    /**
     * Returns an enumeration over the text fields.
     *
     * @return anb enumeration, or {@code null} if there are none in the container
     */
    protected XEnumeration getTextFieldsEnumeration() {
        XEnumerationAccess fields = getTextFields();
        return (fields != null) ? fields.createEnumeration() : null;
    }

    /**
     * Returns a list of all text field masters.
     *
     * @return the text field masters. May be {@code null}
     */
    protected XNameAccess getTextFieldMasters() {
        XTextFieldsSupplier textFieldSupplier = getTextFieldSupplier();
        return textFieldSupplier != null ? textFieldSupplier.getTextFieldMasters() : null;
    }

    /**
     * Determines if a field is an input text field.
     *
     * @param field the field
     * @return {@code true} if the field is an input text field
     */
    protected boolean isInputField(Object field) {
        XServiceInfo info = UnoRuntime.queryInterface(XServiceInfo.class, field);
        return info != null && info.supportsService("com.sun.star.text.TextField.Input");
    }

    /**
     * Helper to return the property set of a field, if the supplied field is
     * an input field.
     *
     * @param field the field
     * @return the input field's property set, or {@code null} if it isn't an input field
     */
    protected XPropertySet getInputFieldPropertySet(Object field) {
        XTextField text = UnoRuntime.queryInterface(XTextField.class, field);
        if (text != null && (isInputField(text) || isInputUserField(text))) {
            return UnoRuntime.queryInterface(XPropertySet.class, text);
        }
        return null;
    }

    /**
     * Returns the duplex mode.
     *
     * @param sides the sides to print on. May be {@code null}
     * @return the sides to print on
     */
    private Short getDuplexMode(Sides sides) {
        if (sides != null) {
            if (sides == Sides.ONE_SIDED) {
                return DuplexMode.OFF;
            } else if (sides == Sides.DUPLEX || sides == Sides.TWO_SIDED_LONG_EDGE) {
                return DuplexMode.LONGEDGE;
            } else if (sides == Sides.TUMBLE || sides == Sides.TWO_SIDED_SHORT_EDGE) {
                return DuplexMode.SHORTEDGE;
            }
        }
        return DuplexMode.UNKNOWN;
    }

    /**
     * Determines if a field is an input user text field.
     * <p/>
     * These fields update the user field identified by their <em>Content</em> property.
     *
     * @param field the field
     * @return {@code true} if the field is an input text field
     */
    private boolean isInputUserField(Object field) {
        XServiceInfo info = UnoRuntime.queryInterface(XServiceInfo.class, field);
        return info != null && info.supportsService("com.sun.star.text.TextField.InputUser");
    }

    /**
     * Lazily initialises user and input fields.
     */
    private void getFields() {
        if (userFields == null || inputFields == null) {
            run("initialising fields for " + debug, () -> {
                userFields = getUserTextFields();
                inputFields = getInputTextFields();
            });
        }
    }

    /**
     * Helper to create a new {@code PropertyValue}.
     *
     * @param name  the property name
     * @param value the property value
     * @return a new {@code PropertyValue}
     */
    private PropertyValue property(String name, Object value) {
        PropertyValue property = new PropertyValue();
        property.Name = name;
        property.Value = value;
        return property;
    }

    /**
     * Concatenates a base name and extension.
     *
     * @param name the base name
     * @param ext  the extension, minus the prefixing period
     * @return the file name
     */
    private String getFileName(String name, String ext) {
        return name + "." + ext;
    }

    /**
     * Runs a command, timing it if debug is enabled.
     *
     * @param debug   the debug string
     * @param command the command to execute
     */
    private void run(String debug, Runnable command) {
        run(debug, () -> {
            command.run();
            return null;
        });
    }

    /**
     * Runs a command, timing it if debug is enabled.
     *
     * @param debug   the debug string
     * @param command the command to execute
     * @return the result of the command
     */
    private <T> T run(String debug, Supplier<T> command) {
        StopWatch stopWatch = null;
        try {
            if (log.isDebugEnabled()) {
                log.debug("Starting " + debug);
                stopWatch = new StopWatch();
                stopWatch.start();
            }
            return command.get();
        } finally {
            if (stopWatch != null) {
                stopWatch.stop();
                log.debug("Completed " + debug + " in " + stopWatch);
            }
        }
    }

    /**
     * Field wrapper.
     */
    protected static class Field {

        private final String value;

        private final XPropertySet field;

        private final Point position;

        private final String content;

        private String name;

        private boolean changed;

        /**
         * Constructs a {@link Field}.
         *
         * @param name       the field name
         * @param value      the field value
         * @param properties the field's properties
         */
        public Field(String name, String value, XPropertySet properties) {
            this(name, value, properties, null, null);
        }

        /**
         * Constructs a {@link Field}.
         *
         * @param value      the field value
         * @param properties the field's properties
         * @param position   the field position. May be {@code null}
         * @param content    the field content. May  be {@code null}
         */
        public Field(String value, XPropertySet properties, Point position, String content) {
            this(null, value, properties, position, content);
        }


        /**
         * Constructs a {@link Field}.
         *
         * @param name       the field name
         * @param value      the field value
         * @param properties the field's properties
         * @param position   the field position. May be {@code null}
         * @param content    the field content. May  be {@code null}
         */
        public Field(String name, String value, XPropertySet properties, Point position, String content) {
            this.name = name;
            this.field = properties;
            this.value = value;
            this.position = position;
            this.content = content;
        }

        /**
         * Returns the field name.
         *
         * @return the field name
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the field name.
         *
         * @param name the field name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Returns the field value.
         *
         * @return the field value
         */
        public String getValue() {
            return value;
        }

        /**
         * Returns the field's properties.
         *
         * @return the properties
         */
        public XPropertySet getPropertySet() {
            return field;
        }

        /**
         * Returns the field position.
         *
         * @return the field position. May be {@code null}
         */
        public Point getPosition() {
            return position;
        }

        /**
         * Returns the field content.
         *
         * @return the field content. May be {@code null}
         */
        public String getContent() {
            return content;
        }

        /**
         * Determines if the field has changed.
         *
         * @return {@code true} if the field has changed
         */
        public boolean isChanged() {
            return changed;
        }

        /**
         * Determines if the field has been changed.
         *
         * @param changed if {@code true}, indicates that the field has changed
         */
        public void setChanged(boolean changed) {
            this.changed = changed;
        }

    }
}
