/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice.print;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.model.document.Document;
import org.openvpms.print.exception.PrinterException;
import org.openvpms.print.impl.service.AbstractPrintServiceDocumentPrinter;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.PrintAttributes;
import org.openvpms.report.DocFormats;
import org.openvpms.report.PrintProperties;

import javax.print.PrintService;

/**
 * An {@link DocumentPrinter} that supports printing OpenOffice documents via {@link OpenOfficePrintService},
 * in addition to the documents supported by the Java Print API {@link PrintService}.
 *
 * @author Tim Anderson
 */
public class OpenOfficeDocumentPrinter extends AbstractPrintServiceDocumentPrinter {

    /**
     * The OpenOffice print service.
     */
    private final OpenOfficePrintService openOfficePrintService;

    /**
     * Constructs an {@link OpenOfficeDocumentPrinter}.
     *
     * @param openOfficePrintService the OpenOffice print service
     * @param handlers               the document handlers
     * @param printService           the Java Print API print service
     */
    public OpenOfficeDocumentPrinter(OpenOfficePrintService openOfficePrintService, DocumentHandlers handlers,
                                     PrintService printService) {
        super(printService, handlers);
        this.openOfficePrintService = openOfficePrintService;
    }

    /**
     * Determines if the printer can print the specified mime type.
     *
     * @param mimeType the mime type
     * @return {@code true} if the printer can print the specified mime type
     */
    @Override
    public boolean canPrint(String mimeType) {
        return isSupportedByOpenOffice(mimeType) || super.canPrint(mimeType);
    }

    /**
     * Prints a document.
     *
     * @param document   the document to print
     * @param attributes the print attributes
     * @throws PrinterException for any printer error
     */
    @Override
    public void print(Document document, PrintAttributes attributes) {
        if (isSupportedByOpenOffice(document.getMimeType())) {
            PrintProperties properties = new PrintProperties(getId(), attributes);
            openOfficePrintService.print(getDecompressedDocument(document), properties);
        } else {
            super.print(document, attributes);
        }
    }

    /**
     * Determines if a mime type is supported by OpenOffice.
     *
     * @param mimeType the mime type
     * @return {@code true} if it is supported, otherwise {@code false}
     */
    private boolean isSupportedByOpenOffice(String mimeType) {
        return DocFormats.ODT_TYPE.equals(mimeType) || DocFormats.DOC_TYPE.equals(mimeType);
    }
}