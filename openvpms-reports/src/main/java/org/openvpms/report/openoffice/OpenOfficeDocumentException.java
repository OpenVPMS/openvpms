/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.openvpms.component.i18n.Message;

/**
 * OpenOffice document exception.
 *
 * @author Tim Anderson
 */
public class OpenOfficeDocumentException extends OpenOfficeException {

    /**
     * Constructs an {@link OpenOfficeDocumentException}.
     *
     * @param message the exception message
     */
    public OpenOfficeDocumentException(Message message) {
        super(message);
    }

    /**
     * Constructs an {@link OpenOfficeDocumentException}.
     *
     * @param message the exception message
     * @param cause   the cause
     */
    public OpenOfficeDocumentException(Message message, Throwable cause) {
        super(message, cause);
    }
}
