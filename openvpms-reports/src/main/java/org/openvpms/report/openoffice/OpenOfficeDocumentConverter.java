/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.apache.commons.io.FilenameUtils;
import org.jodconverter.local.office.LocalOfficeContext;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.report.ReportException;
import org.openvpms.report.i18n.ReportMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

import static org.openvpms.report.Report.IS_EMAIL;


/**
 * Converts a {@link Document} from one format to another using OpenOffice.
 *
 * @author Tim Anderson
 */
public class OpenOfficeDocumentConverter implements DocumentConverter {

    /**
     * The OpenOffice service.
     */
    private final OpenOfficeService officeService;

    /**
     * The document handlers
     */
    private final DocumentHandlers handlers;

    /**
     * Supported conversions on source mime type -> target mime type.
     */
    private static final String[][] MIME_MAP = {{DocFormats.ODT_TYPE, DocFormats.PDF_TYPE},
                                                {DocFormats.DOC_TYPE, DocFormats.PDF_TYPE},
                                                {DocFormats.DOCX_TYPE, DocFormats.PDF_TYPE},
                                                {DocFormats.RTF_TYPE, DocFormats.HTML_TYPE}};

    /**
     * Supported conversions on extension -> target mime type.
     */
    private static final String[][] EXT_MAP = {{DocFormats.ODT_EXT, DocFormats.PDF_TYPE},
                                               {DocFormats.DOC_EXT, DocFormats.PDF_TYPE},
                                               {DocFormats.DOCX_EXT, DocFormats.PDF_TYPE},
                                               {DocFormats.ODT_EXT, DocFormats.HTML_TYPE},
                                               {DocFormats.DOC_EXT, DocFormats.HTML_TYPE},
                                               {DocFormats.DOCX_EXT, DocFormats.HTML_TYPE},
                                               {DocFormats.RTF_EXT, DocFormats.HTML_TYPE}};

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OpenOfficeDocumentConverter.class);

    /**
     * Constructs a {@link OpenOfficeDocumentConverter}.
     *
     * @param officeService the OpenOffice service
     * @param handlers      the document handlers
     */
    public OpenOfficeDocumentConverter(OpenOfficeService officeService, DocumentHandlers handlers) {
        this.officeService = officeService;
        this.handlers = handlers;
    }

    /**
     * Determines if a document can be converted.
     *
     * @param document the document to convert
     * @param mimeType the target mime type
     * @return {@code true} if the document can be converted, otherwise {@code false}
     */
    @Override
    public boolean canConvert(Document document, String mimeType) {
        return canConvert(document.getName(), document.getMimeType(), mimeType);
    }

    /**
     * Determines if a document can be converted.
     *
     * @param fileName       the document file name
     * @param sourceMimeType the document mime type
     * @param targetMimeType the target mime type
     * @return {@code true} if the document can be converted, otherwise {@code false}
     */
    @Override
    public boolean canConvert(String fileName, String sourceMimeType, String targetMimeType) {
        for (String[] map : MIME_MAP) {
            if (map[0].equals(sourceMimeType) && map[1].equals(targetMimeType)) {
                return true;
            }
        }
        if (fileName != null) {
            String ext = FilenameUtils.getExtension(fileName);
            for (String[] map : EXT_MAP) {
                if (map[0].equals(ext) && map[1].equals(targetMimeType)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Converts a document.
     *
     * @param document the document to convert
     * @param mimeType the target mime type
     * @return the converted document
     * @throws ReportException if the document cannot be converted
     */
    @Override
    public Document convert(Document document, String mimeType) {
        return convert(document, mimeType, false);
    }

    /**
     * Converts a document.
     *
     * @param document the document to convert
     * @param mimeType the target mime type
     * @param email    if {@code true} indicates that the document will be emailed. Documents that support parameters
     *                 can perform custom formatting
     * @return the converted document
     * @throws ReportException if the document cannot be converted
     */
    @Override
    public Document convert(Document document, String mimeType, boolean email) {
        String name = FilenameUtils.getBaseName(document.getName());
        return convert(document, mimeType, email, "convert", (doc) -> doc.export(mimeType, name));
    }

    /**
     * Exports the document.
     *
     * @param document the document to export
     * @param mimeType the mime-type of the document format to export to
     * @param email    if {@code true} indicates that the document will be emailed. Documents that support parameters
     *                 can perform custom formatting
     * @return the exported document serialized to a byte array
     * @throws ReportException if the document cannot be exported
     */
    @Override
    public byte[] export(Document document, String mimeType, boolean email) {
        return convert(document, mimeType, email, "export", (doc) -> doc.export(mimeType));
    }

    /**
     * Converts a document, logging the time to covert if debugging is enabled.
     *
     * @param document  the document to convert
     * @param mimeType  the mime-type of the document format to export to
     * @param email     if {@code true} indicates that the document will be emailed. Documents that support parameters
     *                  can perform custom formatting
     * @param action    the action, for logging purposes
     * @param converter the converter
     * @return the result of the conversion
     */
    private <T> T convert(Document document, String mimeType, boolean email, String action,
                          Function<OpenOfficeDocument, T> converter) {
        T result;
        if (!canConvert(document, mimeType)) {
            throw new OpenOfficeException(ReportMessages.unsupportedConversion(document.getName(),
                                                                               document.getMimeType(), mimeType));
        }
        ConvertTask<T> task = new ConvertTask<>(document, email, converter, action, mimeType);
        try {
            result = task.run(officeService);
        } catch (OpenVPMSException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new OpenOfficeDocumentException(ReportMessages.conversionFailed(document.getName(), mimeType),
                                                  exception);
        }
        return result;
    }

    private class ConvertTask<T> extends OpenOfficeTask<T> {

        private final Document document;

        private final boolean email;

        private final Function<OpenOfficeDocument, T> converter;

        private final String action;

        private final String mimeType;

        public ConvertTask(Document document, boolean email, Function<OpenOfficeDocument, T> converter,
                           String action, String mimeType) {
            super(log.isDebugEnabled());
            this.document = document;
            this.email = email;
            this.converter = converter;
            this.action = action;
            this.mimeType = mimeType;
        }

        /**
         * Runs the task.
         *
         * @param context the OpenOffice context
         * @return the result of the task
         */
        @Override
        protected T run(LocalOfficeContext context) {
            try (OpenOfficeDocument doc = createDocument(document, context)) {
                if (doc.hasUserField(IS_EMAIL)) {
                    doc.setUserField(IS_EMAIL, Boolean.toString(email));
                }
                return converter.apply(doc);
            }
        }

        @Override
        protected void start() {
            log.debug("Running {}, document='{}', id={}, mimeType={}, to={}", action, document.getName(),
                      document.getId(), document.getMimeType(), mimeType);
        }

        /**
         * Invoked when the task completes and debugging is enabled.
         *
         * @param exception the exception, if the task failed, or {@code null} if it succeeded
         * @param delay     the delay in ms before the task was executed
         * @param elapsed   the total elapsed time of the task
         */
        @Override
        protected void stop(Exception exception, long delay, long elapsed) {
            if (exception != null) {
                log.debug("Failed to {}, document='{}', id={}, delay={}, elapsed={}: {}",
                          action, document.getName(), document.getId(), duration(delay), duration(elapsed),
                          exception.getMessage(), exception);
            } else {
                log.debug("Completed {}, document='{}', id={}, delay={}, elapsed={}",
                          action, document.getName(), document.getId(), duration(delay), duration(elapsed));
            }
        }

        /**
         * Creates an OpenOffice document.
         *
         * @param document the document
         * @param context  the OpenOffice context
         * @return a new OpenOffice document
         */
        private OpenOfficeDocument createDocument(Document document, LocalOfficeContext context) {
            OpenOfficeDocument doc = new OpenOfficeDocument(document, context, handlers);
            doc.refresh();
            // workaround to avoid corruption of generated doc
            // when the source document contains user fields.
            // Alternative approach is to do a Thread.sleep(1000).
            return doc;
        }
    }
}
