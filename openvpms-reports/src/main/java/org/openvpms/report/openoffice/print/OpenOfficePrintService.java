/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.report.openoffice.print;

import org.jodconverter.local.office.LocalOfficeContext;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;
import org.openvpms.report.PrintProperties;
import org.openvpms.report.i18n.ReportMessages;
import org.openvpms.report.openoffice.OpenOfficeDocument;
import org.openvpms.report.openoffice.OpenOfficeException;
import org.openvpms.report.openoffice.OpenOfficeService;
import org.openvpms.report.openoffice.OpenOfficeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * OpenOffice print service.
 *
 * @author Tim Anderson
 * @author Benjamin Charlton
 */
public class OpenOfficePrintService {

    /**
     * The OpenOffice service.
     */
    private final OpenOfficeService officeService;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OpenOfficePrintService.class);

    /**
     * Constructs a {@link OpenOfficePrintService}.
     *
     * @param officeService the OpenOffice service
     * @param handlers      the document handlers
     */
    public OpenOfficePrintService(OpenOfficeService officeService, DocumentHandlers handlers) {
        this.officeService = officeService;
        this.handlers = handlers;
    }

    /**
     * Prints a document.
     *
     * @param document   the document to print
     * @param properties the print properties
     * @throws OpenVPMSException for any error
     */
    public void print(Document document, PrintProperties properties) {
        PrintTask task = new PrintTask(document, properties);
        try {
            task.run(officeService);
        } catch (OpenVPMSException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new OpenOfficeException(ReportMessages.failedToPrintOpenOfficeDocument(document.getName()),
                                          exception);
        }
    }

    private class PrintTask extends OpenOfficeTask<Void> {

        private final Document document;

        private final PrintProperties properties;

        public PrintTask(Document document, PrintProperties properties) {
            super(log.isDebugEnabled());
            this.document = document;
            this.properties = properties;
        }

        /**
         * Runs the task.
         *
         * @param context the OpenOffice context
         * @return the result of the task
         */
        @Override
        protected Void run(LocalOfficeContext context) {
            try (OpenOfficeDocument doc = new OpenOfficeDocument(document, context, handlers)) {
                doc.print(properties);
            }
            return null;
        }

        /**
         * Invoked when the task starts and debugging is enabled.
         */
        @Override
        protected void start() {
            log.debug("Printing document='{}', id={}", document.getName(), document.getId());
        }

        /**
         * Invoked when the task completes and debugging is enabled.
         *
         * @param exception the exception, if the task failed, or {@code null} if it succeeded
         * @param delay     the delay in ms before the task was executed
         * @param elapsed   the total elapsed time of the task
         */
        @Override
        protected void stop(Exception exception, long delay, long elapsed) {
            if (exception != null) {
                log.debug("Failed to print document='{}', id={}, delay={}, elapsed={}: {}",
                          document.getName(), document.getId(), duration(delay), duration(elapsed),
                          exception.getMessage(), exception);
            } else {
                log.debug("Printed document='{}', id={}, delay={}, elapsed={}",
                          document.getName(), document.getId(), duration(delay), duration(elapsed));
            }
        }
    }

}
