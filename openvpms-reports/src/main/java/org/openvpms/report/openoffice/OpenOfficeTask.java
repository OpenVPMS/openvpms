/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.jodconverter.local.office.LocalOfficeContext;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A task queued and executed by {@link OpenOfficeService}.
 *
 * @author Tim Anderson
 */
public abstract class OpenOfficeTask<T> {

    /**
     * Determines if debugging is enabled.
     */
    private final boolean debug;

    /**
     * Constructs an {@link OpenOfficeTask}.
     *
     * @param debug if {@code true}, enable debugging
     */
    public OpenOfficeTask(boolean debug) {
        this.debug = debug;
    }

    /**
     * Runs the task.
     *
     * @param service the OpenOffice service
     * @return the result of the task
     */
    public final T run(OpenOfficeService service) {
        T result;
        RuntimeException failure = null;
        final AtomicLong delay = new AtomicLong();
        final StopWatch stopWatch = new StopWatch();
        try {
            if (debug) {
                start();
            }
            result = service.call(context -> {
                if (debug) {
                    stopWatch.split();
                    delay.set(stopWatch.getSplitTime());
                    stopWatch.unsplit();
                }
                return run(context);
            });
            if (debug) {
                stop(null, delay.get(), stopWatch.getTime());
            }
        } catch (RuntimeException exception) {
            failure = exception;
            throw failure;
        } finally {
            if (debug) {
                stop(failure, delay.get(), stopWatch.getTime());
            }
        }
        return result;
    }

    /**
     * Runs the task.
     *
     * @param context the OpenOffice context
     * @return the result of the task
     */
    protected abstract T run(LocalOfficeContext context);

    /**
     * Invoked when the task starts and debugging is enabled.
     * <p/>
     * This implementation is a no-op.
     */
    protected void start() {
    }

    /**
     * Invoked when the task completes and debugging is enabled.
     * <p/>
     * This implementation is a no-op.
     *
     * @param exception the exception, if the task failed, or {@code null} if it succeeded
     * @param delay     the delay in ms before the task was executed
     * @param elapsed   the total elapsed time of the task
     */
    protected void stop(Exception exception, long delay, long elapsed) {

    }

    /**
     * Helper to format a duration.
     *
     * @param duration the duration
     * @return the formatted duration
     */
    protected String duration(long duration) {
        return DurationFormatUtils.formatDurationHMS(duration);
    }
}