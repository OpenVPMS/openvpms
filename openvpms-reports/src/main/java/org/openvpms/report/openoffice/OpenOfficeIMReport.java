/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.jxpath.Functions;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jodconverter.local.office.LocalOfficeContext;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.DocFormats;
import org.openvpms.report.ExpressionEvaluator;
import org.openvpms.report.ExpressionEvaluatorFactory;
import org.openvpms.report.IMReport;
import org.openvpms.report.ParameterType;
import org.openvpms.report.PrintProperties;
import org.openvpms.report.ReportException;
import org.openvpms.report.i18n.ReportMessages;
import org.openvpms.report.util.ProtectedExpressionEvaluator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;


/**
 * Generates a report using an OpenOffice document as the template.
 *
 * @author Tim Anderson
 */
public class OpenOfficeIMReport<T> implements IMReport<T> {

    /**
     * The document template.
     */
    private final Document template;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The JXPath extension functions.
     */
    private final Functions functions;

    /**
     * The OpenOffice service.
     */
    private final OpenOfficeService officeService;

    /**
     * Cache of parameters.
     */
    private Map<String, ParameterType> parameters;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OpenOfficeIMReport.class);

    /**
     * Report parameter enabling a boolean flag to be supplied to indicate if the report is being emailed.
     * This enables different formatting to be used when emailing as opposed to printing. e.g. if letterhead is
     * used when printing, this can be included when emailing.
     */
    private static final String IS_EMAIL = "IsEmail";


    /**
     * Constructs an {@link OpenOfficeIMReport}.
     *
     * @param template      the document template
     * @param service       the archetype service
     * @param lookups       the lookup service
     * @param handlers      the document handlers
     * @param functions     the factory for JXPath extension functions
     * @param officeService the OpenOffice service
     */
    public OpenOfficeIMReport(Document template, ArchetypeService service, LookupService lookups,
                              DocumentHandlers handlers, Functions functions, OpenOfficeService officeService) {
        this.template = template;
        this.service = service;
        this.lookups = lookups;
        this.handlers = handlers;
        this.functions = functions;
        this.officeService = officeService;
    }

    /**
     * Returns the report name.
     *
     * @return the report name.
     */
    @Override
    public String getName() {
        return template.getName();
    }

    /**
     * Returns the set of parameter types that may be supplied to the report.
     *
     * @return the parameter types
     */
    public Set<ParameterType> getParameterTypes() {
        Map<String, ParameterType> params = getParameters();
        return new LinkedHashSet<>(params.values());
    }

    /**
     * Determines if the report accepts the named parameter.
     *
     * @param name the parameter name
     * @return {@code true} if the report accepts the parameter, otherwise {@code false}
     */
    public boolean hasParameter(String name) {
        return getParameters().containsKey(name);
    }

    /**
     * Returns the default mime type for report documents.
     *
     * @return the default mime type
     */
    public String getDefaultMimeType() {
        String mimeType = template.getMimeType();
        if (!ArrayUtils.contains(getMimeTypes(), mimeType)) {
            mimeType = DocFormats.PDF_TYPE;
        }
        return mimeType;
    }

    /**
     * Returns the supported mime types for report documents.
     *
     * @return the supported mime types
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    public String[] getMimeTypes() {
        return new String[]{DocFormats.ODT_TYPE, DocFormats.DOC_TYPE, DocFormats.PDF_TYPE, DocFormats.TEXT_TYPE};
    }

    /**
     * Not supported.
     *
     * @throws UnsupportedOperationException if invoked
     */
    public Document generate(Map<String, Object> parameters, Map<String, Object> fields) {
        throw new UnsupportedOperationException();
    }

    /**
     * Not supported.
     *
     * @throws UnsupportedOperationException if invoked
     */
    public Document generate(Map<String, Object> parameters, Map<String, Object> fields, String mimeType) {
        throw new UnsupportedOperationException();
    }

    /**
     * Generates a report for a collection of objects.
     * <p/>
     * The default mime type will be used to select the output format.
     *
     * @param objects the objects to report on
     * @return a document containing the report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Document generate(Iterable<T> objects) {
        return generate(objects, getDefaultMimeType());
    }

    /**
     * Generates a report for a collection of objects.
     *
     * @param objects  the objects to report on
     * @param mimeType the output format of the report
     * @return a document containing the report
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    public Document generate(Iterable<T> objects, String mimeType) {
        Map<String, Object> empty = Collections.emptyMap();
        return generate(objects, empty, null, mimeType);
    }

    /**
     * Generates a report for a collection of objects.
     * <p/>
     * The default mime type will be used to select the output format.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @return a document containing the report
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    public Document generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields) {
        return generate(objects, parameters, fields, getDefaultMimeType());
    }

    /**
     * Generates a report for a collection of objects.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param mimeType   the output format of the report
     * @return a document containing the report
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    public Document generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                             String mimeType) {
        try {
            return withOpenOffice("generate", context -> {
                try (OpenOfficeDocument doc = create(objects, parameters, fields, context)) {
                    return export(doc, mimeType);
                }
            });
        } catch (ReportException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new ReportException(ReportMessages.failedToGenerateReport(template.getName(), exception.getMessage()),
                                      exception);
        }
    }

    /**
     * Generates a report for a collection of objects to the specified stream.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param mimeType   the output format of the report
     * @param stream     the stream to write to
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    public void generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                         String mimeType, OutputStream stream) {
        try {
            withOpenOffice("generate", context -> {
                try (OpenOfficeDocument doc = create(objects, parameters, fields, context)) {
                    byte[] content = doc.export(mimeType);
                    stream.write(content);
                } catch (ReportException exception) {
                    throw exception;
                } catch (Throwable exception) {
                    throw new ReportException(ReportMessages.failedToGenerateReport(template.getName(),
                                                                                    exception.getMessage()), exception);
                }
                return null;
            });
        } catch (ReportException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new ReportException(ReportMessages.failedToGenerateReport(template.getName(), exception.getMessage()),
                                      exception);
        }
    }

    /**
     * Determines if printing via the Java Print API is supported.
     *
     * @return {@code true} if printing is supported, otherwise {@code false}
     */
    @Override
    public boolean canPrint() {
        return true;
    }

    /**
     * Not supported.
     *
     * @throws UnsupportedOperationException if invoked
     */
    public void print(Map<String, Object> parameters, Map<String, Object> fields, PrintProperties properties) {
        throw new UnsupportedOperationException();
    }

    /**
     * Prints a report directly to a printer.
     *
     * @param objects    the objects to report on
     * @param properties the print properties
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void print(Iterable<T> objects, PrintProperties properties) {
        print(objects, Collections.emptyMap(), null, properties);
    }

    /**
     * Prints a report directly to a printer.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param properties the print properties
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    public void print(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                      PrintProperties properties) {
        try {
            withOpenOffice("print", context -> {
                try (OpenOfficeDocument doc = create(objects, parameters, fields, context)) {
                    doc.print(properties);
                }
                return null;
            });
        } catch (ReportException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new ReportException(ReportMessages.failedToPrintReport(
                    template.getName(), properties.getPrinterName(), exception.getMessage()), exception);
        }
    }

    /**
     * Creates an OpenOffice document from a collection of objects.
     * Note that the collection is limited to a single object.
     *
     * @param objects    the objects to generate the document from
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param context    the OpenOffice context
     * @return a new OpenOffice document
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    protected OpenOfficeDocument create(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                                        LocalOfficeContext context) {
        OpenOfficeDocument doc;
        Iterator<T> iter = objects.iterator();
        T object = null;
        if (iter.hasNext()) {
            object = iter.next();
        }
        if (object == null || iter.hasNext()) {
            throw new ReportException(ReportMessages.failedToGenerateReport(template.getName(),
                                                                            "Can only report on single objects"));
        }

        doc = createDocument(template, context, handlers);
        if (parameters != null) {
            populateInputFields(doc, parameters);
        }
        populateUserFields(doc, object, parameters, fields);

        // refresh the text fields
        doc.refresh();
        return doc;
    }

    /**
     * Returns the name of the user fields that should be prompted for.
     *
     * @param document the document
     * @return the user field names
     */
    protected Set<String> getInputFields(OpenOfficeDocument document) {
        Map<String, ParameterType> fields = document.getInputFields();
        return fields.keySet();
    }

    /**
     * Creates a new document.
     *
     * @param template the document template
     * @param context  the OpenOffice context
     * @param handlers the document handlers
     * @return a new document
     * @throws OpenOfficeException for any error
     */
    protected OpenOfficeDocument createDocument(Document template, LocalOfficeContext context,
                                                DocumentHandlers handlers) {
        return new OpenOfficeDocument(template, context, handlers);
    }

    /**
     * Populates input fields with parameters of the same name.
     *
     * @param document   the document
     * @param parameters the parameters
     */
    protected void populateInputFields(OpenOfficeDocument document, Map<String, Object> parameters) {
        for (Map.Entry<String, Object> p : parameters.entrySet()) {
            String name = p.getKey();
            if (document.hasInputField(name)) {
                String value = (p.getValue() != null) ? p.getValue().toString() : null;
                document.setInputField(name, value);
            }
        }
    }

    /**
     * Populates user fields in a document.
     * <p/>
     * If a field exists with the same name as a parameter, then this will be populated with the parameter value.
     *
     * @param document   the document
     * @param object     the object to evaluate expressions with
     * @param parameters the parameters. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     */
    protected void populateUserFields(OpenOfficeDocument document, T object, Map<String, Object> parameters,
                                      Map<String, Object> fields) {
        ExpressionEvaluator eval = ExpressionEvaluatorFactory.create(object, fields, template.getName(), service,
                                                                     lookups, functions);
        ProtectedExpressionEvaluator evaluator = new ProtectedExpressionEvaluator(getName(), log);
        List<String> userFields = document.getUserFieldNames();
        for (String name : userFields) {
            String value = getParameter(name, parameters);
            if (value == null) {
                value = document.getUserField(name);
                if (!StringUtils.isEmpty(value)) {
                    IMObject current = (object instanceof IMObject) ? (IMObject) object : null;
                    value = evaluator.getFormattedValue(value, eval, current);
                }
            }
            document.setUserField(name, value);
        }
    }

    /**
     * Invokes a function with an open {@link LocalOfficeContext}.
     * <p/>
     * If debugging is enabled, this invokes the time taken, as well as any exception.
     *
     * @param action   the action, for logging purposes
     * @param function the function to invoke
     * @return the result of the function. May be {@code null}
     */
    private <R> R withOpenOffice(String action, Function<LocalOfficeContext, R> function) {
        Task<R> task = new Task<>(action, function);
        return task.run(officeService);
    }

    /**
     * Helper to return the string value of a parameter, if it exists.
     *
     * @param name       the parameter name
     * @param parameters the parameters. May be {@code null}
     * @return the parameter value, or {@code null} if it is not found
     */
    private String getParameter(String name, Map<String, Object> parameters) {
        String result = null;
        if (parameters != null) {
            Object value = parameters.get(name);
            if (value != null) {
                result = value.toString();
            }
        }
        return result;
    }

    /**
     * Returns the document parameters.
     * NOTE: this is an expensive operation as the document needs to be loaded then subsequently discarded. TODO
     *
     * @return the document parameters
     */
    private Map<String, ParameterType> getParameters() {
        if (parameters == null) {
            try {
                withOpenOffice("get parameters", context -> {
                    try (OpenOfficeDocument doc = createDocument(template, context, handlers)) {
                        parameters = doc.getInputFields();
                        if (doc.hasUserField(IS_EMAIL)) {
                            // enable the IsEmail user field to be specified as a parameter
                            // Note: MS-Word documents can specify IsEmail, but can't do anything with them as
                            // OpenOffice won't process MS-Word field expressions.
                            parameters.put(IS_EMAIL, new ParameterType(IS_EMAIL, boolean.class, IS_EMAIL, true, false));
                        }
                    }
                    return null;
                });
            } catch (ReportException exception) {
                throw  exception;
            } catch (Throwable exception) {
                throw new ReportException(ReportMessages.failedToGetOpenOfficeInputFields(getName()), exception);
            }
        }
        return parameters;
    }

    /**
     * Exports a document, serializing to a {@link Document}.
     *
     * @param doc      the document to export
     * @param mimeType the mime-type of the document     *
     * @return a new document
     * @throws OpenOfficeException for any error
     */
    private Document export(OpenOfficeDocument doc, String mimeType) {
        String name = template.getName();
        if (!DocFormats.ODT_TYPE.equals(mimeType)) {
            name = FilenameUtils.removeExtension(name);
        }
        return doc.export(mimeType, name);
    }

    private class Task<R> extends OpenOfficeTask<R> {

        private final String action;

        private final Function<LocalOfficeContext, R> function;

        public Task(String action, Function<LocalOfficeContext, R> function) {
            super(log.isDebugEnabled());
            this.action = action;
            this.function = function;
        }

        /**
         * Runs the task.
         *
         * @param context the OpenOffice context
         * @return the result of the task
         */
        @Override
        protected R run(LocalOfficeContext context) {
            return function.apply(context);
        }

        /**
         * Invoked when the task starts and debugging is enabled.
         */
        @Override
        protected void start() {
            log.debug("Running {}, document='{}'", action, getName());
        }

        /**
         * Invoked when the task completes and debugging is enabled.
         *
         * @param exception the exception, if the task failed, or {@code null} if it succeeded
         * @param delay     the delay in ms before the task was executed
         * @param elapsed   the total elapsed time of the task
         */
        @Override
        protected void stop(Exception exception, long delay, long elapsed) {
            if (exception != null) {
                log.debug("Failed to {}, document='{}', delay={}, elapsed={}: {}",
                          action, getName(), duration(delay), duration(elapsed), exception.getMessage(), exception);
            } else {
                log.debug("Completed {}, document='{}', delay={}, elapsed={}",
                          action, getName(), duration(delay), duration(elapsed));
            }
        }
    }
}
