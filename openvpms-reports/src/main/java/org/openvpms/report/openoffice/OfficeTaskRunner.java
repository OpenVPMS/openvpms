/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.jodconverter.core.office.OfficeContext;
import org.jodconverter.core.office.OfficeException;
import org.jodconverter.core.office.OfficeManager;
import org.jodconverter.core.task.OfficeTask;
import org.jodconverter.local.office.OfficeConnectionException;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.report.i18n.ReportMessages;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

/**
 * Helper to run a {@link OfficeTask}, preserving {@link OpenVPMSException}s and adapting {@link OfficeException}
 * where the error message is too low level.
 *
 * @author Tim Anderson
 */
class OfficeTaskRunner {

    /**
     * Runs a task.
     *
     * @param officeManager the OfficeManager supplier
     * @param task          the task to run
     * @throws OpenVPMSException for any error
     */
    public static void run(Supplier<OfficeManager> officeManager, OfficeTask task) {
        ProtectedTask protectedTask = new ProtectedTask(task);
        try {
            officeManager.get().execute(protectedTask);
        } catch (OfficeConnectionException exception) {
            throw new OpenOfficeConnectionException(ReportMessages.failedToConnectToOpenOffice(exception.getMessage()),
                                                    exception);
        } catch (OfficeException exception) {
            rethrow(exception, protectedTask);
        }
    }

    /**
     * Rethrows an {@link OfficeException} exception as an {@link OpenVPMSException}.
     *
     * @param exception the exception
     * @param task      the task that triggered the exception
     */
    private static void rethrow(OfficeException exception, ProtectedTask task) {
        if (task.caught instanceof OpenVPMSException) {
            // throw the original exception
            throw task.caught;
        }
        // try and provide some context; the JODConverter messages are a little low level
        if (exception.getCause() instanceof InterruptedException) {
            throw new OpenOfficeException(ReportMessages.openOfficeTaskInterrupted(), exception);
        } else if (exception.getCause() instanceof CancellationException) {
            throw new OpenOfficeException(ReportMessages.openOfficeTaskCancelled(), exception);
        } else if (exception.getCause() instanceof TimeoutException) {
            throw new OpenOfficeException(ReportMessages.openOfficeTaskTimedOut(), exception);
        }
        throw new OpenOfficeException(ReportMessages.openOfficeError(), exception);
    }

    /**
     * Runs an {@link OfficeTask} catching and providing access to any {@link RuntimeException} thrown.
     */
    static class ProtectedTask implements OfficeTask {

        /**
         * The task to delegate to.
         */
        private final OfficeTask task;

        /**
         * The exception caught during execution of the task.
         */
        private RuntimeException caught;

        /**
         * Constructs a {@link ProtectedTask}.
         *
         * @param task the task to delegate to
         */
        public ProtectedTask(OfficeTask task) {
            this.task = task;
        }

        @Override
        public void execute(@NonNull OfficeContext context) throws OfficeException {
            try {
                task.execute(context);
            } catch (OfficeException exception) {
                throw exception;
            } catch (RuntimeException exception) {
                this.caught = exception;
                throw exception;
            }
        }
    }
}
