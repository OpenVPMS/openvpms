/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.jodconverter.core.office.OfficeManager;
import org.jodconverter.core.office.OfficeUtils;
import org.jodconverter.core.task.OfficeTask;
import org.jodconverter.local.office.LocalOfficeContext;
import org.jodconverter.local.office.LocalOfficeManager;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.component.business.service.security.RunAs;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.report.i18n.ReportMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.jodconverter.core.office.AbstractOfficeManagerPool.DEFAULT_TASK_EXECUTION_TIMEOUT;
import static org.jodconverter.core.office.AbstractOfficeManagerPool.DEFAULT_TASK_QUEUE_TIMEOUT;

/**
 * Service to configure and start {@link OfficeManager}.
 *
 * @author Tim Anderson
 */
public class OpenOfficeService implements DisposableBean {

    /**
     * The settings.
     */
    private final Settings settings;

    /**
     * The configuration used to start the service.
     */
    private final AtomicReference<OpenOfficeConfig> configUsed = new AtomicReference<>();

    /**
     * Specifies the maximum time allowed to process a task.
     */
    private Long taskExecutionTimeout = DEFAULT_TASK_EXECUTION_TIMEOUT;

    /**
     * Specifies the maximum living time of a task in the conversion queue.
     */
    private Long taskQueueTimeout = DEFAULT_TASK_QUEUE_TIMEOUT;

    /**
     * The OpenOffice manager.
     */
    private OfficeManager officeManager;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OpenOfficeService.class);

    /**
     * Constructs an {@link OpenOfficeService}.
     *
     * @param settings the settings
     */
    public OpenOfficeService(Settings settings) {
        this.settings = settings;
    }

    /**
     * Specifies the maximum time allowed to process a task. If the processing time of a task is
     * longer than this timeout, this task will be aborted and the next task is processed.
     *
     * <p>&nbsp; <b><i>Default</i></b>: 120000 (2 minutes)
     *
     * @param taskExecutionTimeout The task execution timeout, in milliseconds.
     */
    public void setTaskExecutionTimeout(@Nullable Long taskExecutionTimeout) {
        this.taskExecutionTimeout = taskExecutionTimeout;
    }

    /**
     * Specifies the maximum living time of a task in the conversion queue. The task will be removed
     * from the queue if the waiting time is longer than this timeout.
     *
     * <p>&nbsp; <b><i>Default</i></b>: 30000 (30 seconds)
     *
     * @param taskQueueTimeout The task queue timeout, in milliseconds.
     */
    public void setTaskQueueTimeout(@Nullable Long taskQueueTimeout) {
        this.taskQueueTimeout = taskQueueTimeout;
    }

    /**
     * Executes a task.
     *
     * @param task the task
     * @throws OpenVPMSException for any error
     */
    public void run(Consumer<LocalOfficeContext> task) {
        Consumer<LocalOfficeContext> delegator = RunAs.inheritSecurityContext(task);
        execute(context -> delegator.accept((LocalOfficeContext) context));
    }

    /**
     * Executes a task that returns a result.
     *
     * @param task the task
     * @return the result of the task
     * @throws OpenVPMSException for any error
     */
    public <T> T call(Function<LocalOfficeContext, T> task) {
        AtomicReference<T> result = new AtomicReference<>();
        run(context -> result.set(task.apply(context)));
        return result.get();
    }

    /**
     * Returns the configuration.
     *
     * @return the configuration
     */
    public OpenOfficeConfig getConfig() {
        OpenOfficeConfig result = configUsed.get();
        return result != null ? result : getCurrentConfig();
    }

    /**
     * Determines if the service is running.
     *
     * @return {@code true} if the service is running, otherwise {@code false}
     */
    public boolean isRunning() {
        OfficeManager current = getOfficeManager();
        return current != null && current.isRunning();
    }

    /**
     * Restarts the service.
     */
    public synchronized void restart() {
        stop();
        start();
    }

    /**
     * Destroys this.
     */
    @Override
    public void destroy() {
        stop();
    }

    /**
     * Returns the OpenOffice manager.
     *
     * @return the OpenOffice manager. May be {@code null}
     */
    protected synchronized OfficeManager getOfficeManager() {
        return officeManager;
    }

    /**
     * Returns the OpenOffice manager, starting it if required.
     *
     * @return the OpenOffice manager
     */
    protected synchronized OfficeManager getRunning() {
        if (officeManager == null) {
            start();
        }
        return officeManager;
    }

    /**
     * Executes an {@link OpenOfficeTask}.
     *
     * @param task the task to execute
     * @throws OpenVPMSException for any error
     */
    private void execute(OfficeTask task) {
        OfficeTaskRunner.run(this::getRunning, task);
    }

    /**
     * Starts the {@link OfficeManager}.
     */
    private void start() {
        OpenOfficeConfig config = getCurrentConfig();
        String path = config.getPath();
        List<Integer> ports = config.getPorts();
        int maxTasksPerProcess = config.getMaxTasksPerProcess();
        configUsed.set(config);

        log.info("Starting OpenOffice at path '{}' on ports={}, maxTasksPerProcess={}", path,
                 StringUtils.join(ports, ','), maxTasksPerProcess);
        try {
            officeManager = LocalOfficeManager.builder()
                    .officeHome(path)
                    .portNumbers(ArrayUtils.toPrimitive(ports.toArray(new Integer[0])))
                    .taskExecutionTimeout(taskExecutionTimeout)
                    .taskQueueTimeout(taskQueueTimeout)
                    .maxTasksPerProcess(config.getMaxTasksPerProcess())
                    .build();
            officeManager.start();
        } catch (Exception exception) {
            throw new OpenOfficeConnectionException(ReportMessages.failedToConnectToOpenOffice(exception.getMessage()),
                                                    exception);
        }
    }

    /**
     * Stops OpenOffice.
     */
    private synchronized void stop() {
        if (officeManager != null) {
            OfficeUtils.stopQuietly(officeManager);
            officeManager = null;
        }
    }

    /**
     * Returns the current configuration.
     *
     * @return the current configuration
     */
    private OpenOfficeConfig getCurrentConfig() {
        return new OpenOfficeConfig(settings);
    }

}