/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jodconverter.local.office.LocalOfficeManager;
import org.jodconverter.local.office.LocalOfficeUtils;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The OpenOffice configuration.
 *
 * @author Tim Anderson
 */
public class OpenOfficeConfig {

    /**
     * The default port.
     */
    public static final int DEFAULT_PORT = 8100;

    /**
     * The default maximum number of tasks an OpenOffice process can execute before restarting.
     */
    public static final int DEFAULT_MAX_TASKS_PER_PROCESS = LocalOfficeManager.DEFAULT_MAX_TASKS_PER_PROCESS;

    /**
     * The path property.
     */
    public static final String PATH = "path";

    /**
     * The ports property.
     */
    public static final String PORTS = "ports";

    /**
     * The maximum no. of tasks per process property.
     */
    public static final String MAX_TASKS = "maxTasksPerProcess";

    /**
     * The home directory.
     */
    private final String path;

    /**
     * The ports.
     */
    private final List<Integer> ports;

    /**
     * The no. of tasks to execute, before restarting OpenOffice, or {@code 0} to never restart.
     */
    private final int maxTasksPerProcess;

    /**
     * Constructs an {@link OpenOfficeConfig} that returns default values.
     */
    public OpenOfficeConfig() {
        path = getDefaultPath();
        ports = getDefaultPort();
        maxTasksPerProcess = DEFAULT_MAX_TASKS_PER_PROCESS;
    }

    /**
     * Constructs a {@link OpenOfficeConfig} that returns values configured from settings.
     *
     * @param settings the settings
     */
    public OpenOfficeConfig(Settings settings) {
        path = getPath(settings);
        ports = getPorts(settings);
        maxTasksPerProcess = settings.getInt(SettingsArchetypes.OPEN_OFFICE, MAX_TASKS,
                                             LocalOfficeManager.DEFAULT_MAX_TASKS_PER_PROCESS);
    }

    /**
     * Returns the home directory.
     *
     * @return the home directory
     */
    public String getPath() {
        return path;
    }

    /**
     * Returns the ports to run OpenOffice on.
     *
     * @return the ports
     */
    public List<Integer> getPorts() {
        return ports;
    }

    /**
     * Returns the maximum number of tasks an OpenOffice process can execute before restarting.
     *
     * @return the maximum number of tasks, or {@code 0} to never restart
     */
    public int getMaxTasksPerProcess() {
        return maxTasksPerProcess;
    }

    /**
     * Returns the default home directory.
     *
     * @return the default home directory.
     */
    public static String getDefaultPath() {
        return LocalOfficeUtils.getDefaultOfficeHome().getAbsolutePath();
    }

    /**
     * Returns the path.
     *
     * @param settings the settings
     * @return the home directory
     */
    private String getPath(Settings settings) {
        String result = settings.getString(SettingsArchetypes.OPEN_OFFICE, PATH, null);
        if (StringUtils.isEmpty(result)) {
            result = getDefaultPath();
        }
        return result;
    }

    /**
     * Returns the ports to run on.
     *
     * @param settings the settings
     * @return the ports
     */
    private List<Integer> getPorts(Settings settings) {
        List<Integer> result = null;
        String string = settings.getString(SettingsArchetypes.OPEN_OFFICE, PORTS, null);
        if (!StringUtils.isEmpty(string)) {
            result = Stream.of(string.split("\\s*,\\s*"))
                    .mapToInt(str -> NumberUtils.toInt(str, -1))
                    .filter((port) -> port != -1)
                    .sorted()
                    .distinct()
                    .boxed()
                    .collect(Collectors.toList());
            if (!result.isEmpty()) {
                result = Collections.unmodifiableList(result);
            }
        }
        if (result == null || result.isEmpty()) {
            result = getDefaultPort();
        }
        return result;
    }

    /**
     * Returns the default port.
     *
     * @return the default port
     */
    private List<Integer> getDefaultPort() {
        return Collections.singletonList(DEFAULT_PORT);
    }
}
