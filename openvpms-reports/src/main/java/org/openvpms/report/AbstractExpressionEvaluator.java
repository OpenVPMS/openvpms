/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.apache.commons.jxpath.JXPathContext;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.business.service.archetype.helper.IMObjectVariables;
import org.openvpms.component.business.service.archetype.helper.LookupHelper;
import org.openvpms.component.business.service.archetype.helper.PropertyResolver;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.component.system.common.util.PropertyState;
import org.openvpms.report.jasper.ReportContext;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Date;


/**
 * Abstract implementation of the {@link ExpressionEvaluator} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractExpressionEvaluator<T> implements ExpressionEvaluator {

    /**
     * The object.
     */
    private final T object;

    /**
     * The report context.
     */
    private final ReportContext context;

    /**
     * The JXPath expression context.
     */
    private JXPathContext expressionContext;

    /**
     * Constructs an {@link AbstractExpressionEvaluator}.
     *
     * @param object  the object
     * @param context the report context
     */
    public AbstractExpressionEvaluator(T object, ReportContext context) {
        this.object = object;
        this.context = context;
    }

    /**
     * Returns the object.
     *
     * @return the object
     */
    public T getObject() {
        return object;
    }

    /**
     * Returns the value of an expression.
     * If the expression is of the form [expr] it will be evaluated using {@link #getJXPathValue(String)} else it will
     * be evaluated using {@link #getNodeValue(String)}.
     *
     * @param expression the expression
     * @return the result of the expression
     */
    @Override
    public Object getValue(String expression) {
        Object result;
        if (isJXPath(expression)) {
            result = getJXPathValue(expression);
        } else if (isField(expression)) {
            result = getFieldValue(expression);
        } else {
            result = getNodeValue(expression);
        }
        return result;
    }

    /**
     * Evaluates an xpath expression.
     *
     * @param expression the expression
     * @return the result of the expression
     */
    public Object evaluate(String expression) {
        return getExpressionContext().getValue(expression);
    }

    /**
     * Evaluates an xpath against an object.
     *
     * @param object     the object
     * @param expression the expression
     * @return the result of the expression
     */
    @Override
    public Object evaluate(Object object, String expression) {
        return getExpressionContext(object).getValue(expression);
    }

    /**
     * Returns the formatted value of an expression.
     *
     * @param expression the expression
     * @return the result of the expression
     */
    @Override
    public String getFormattedValue(String expression) {
        Object value = getValue(expression);
        if (value instanceof Date) {
            Date date = (Date) value;
            return DateFormat.getDateInstance(DateFormat.MEDIUM).format(date);
        } else if (value instanceof Money) {
            return NumberFormat.getCurrencyInstance().format(value);
        } else if (value instanceof BigDecimal) {
            DecimalFormat format = new DecimalFormat("#,##0.00;-#,##0.00");
            return format.format(value);
        } else if (value instanceof IMObject) {
            return getValue((IMObject) value);
        } else if (value instanceof Reference) {
            return getValue((Reference) value);
        } else if (value != null) {
            return value.toString();
        }
        return null;
    }

    /**
     * Evaluates an expression.
     *
     * @param expression the expression to evaluate. Must be of the form {@code [expr]}
     * @return the value of the expression
     */
    protected Object getJXPathValue(String expression) {
        String eval = expression.substring(1, expression.length() - 1);
        return evaluate(eval);
    }

    /**
     * Returns a node value.
     *
     * @param name the node name
     * @return the node value
     */
    protected abstract Object getNodeValue(String name);

    /**
     * Returns a field value.
     *
     * @param name the field name
     * @return the field value
     */
    protected Object getFieldValue(String name) {
        return getValue(context.getFields().resolve(name));
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return context.getArchetypeService();
    }

    /**
     * Returns the lookup service.
     *
     * @return the lookup service
     */
    protected LookupService getLookups() {
        return context.getLookupService();
    }

    /**
     * Determines if an expression refers to a field.
     *
     * @param expression the expression
     * @return {@code true} if the expression refers to a field
     */
    protected boolean isField(String expression) {
        PropertySet fields = context.getFields();
        return fields != null && fields.exists(expression);
    }

    /**
     * Determines if an expression is an xpath expression.
     *
     * @param expression the expression
     * @return {@code true} if the expression has a leading [ and trailing ]
     */
    protected boolean isJXPath(String expression) {
        return expression.startsWith("[") && expression.endsWith("]");
    }

    /**
     * Creates variables to evaluate expressions against.
     *
     * @return new variables
     */
    protected IMObjectVariables createVariables() {
        Parameters parameters = context.getParameters();
        ArchetypeService service = getService();
        LookupService lookups = getLookups();
        return parameters != null && !parameters.isEmpty() ? new Variables(parameters, service, lookups)
                                                           : new IMObjectVariables(service, lookups);
    }

    /**
     * Helper to return a value for an object, for display purposes.
     * If the object is a:
     * <ul>
     * <li>Relationship, returns the name/description of the target object; else</li>
     * <li>returns the object's name, or its description if the name is null</li>
     * <ul>
     *
     * @param object the object. May be {@code null}
     * @return a value for the object
     */
    protected String getValue(IMObject object) {
        String value = null;
        if (object instanceof Relationship) {
            value = getValue(((Relationship) object).getTarget());
        } else if (object != null) {
            value = object.getName();
            if (value == null) {
                value = object.getDescription();
            }
        }
        if (value == null) {
            value = "";
        }
        return value;
    }

    /**
     * Helper to return a value for an object, for display purposes.
     *
     * @param ref the object reference. May be {@code null}
     * @return a value for the object
     */
    protected String getValue(Reference ref) {
        IMObject object = (ref != null) ? getService().get(ref) : null;
        return (object != null) ? getValue(object) : null;
    }

    /**
     * Helper to return a the value of a node, handling collection nodes.
     * If the node doesn't exist, a localised message indicating this will be returned.
     *
     * @param name     the node name
     * @param resolver the node resolver
     * @return the node value
     */
    protected Object getValue(String name, PropertyResolver resolver) {
        PropertyState state = resolver.resolve(name);
        return getValue(state);
    }

    /**
     * Returns the context to evaluate expressions with.
     *
     * @return the context
     */
    private JXPathContext getExpressionContext() {
        if (expressionContext == null) {
            expressionContext = getExpressionContext(object);
        }
        return expressionContext;
    }

    /**
     * Returns the context to evaluate expressions with.
     *
     * @param object the object to evaluate against
     * @return the context
     */
    private JXPathContext getExpressionContext(Object object) {
        JXPathContext result = JXPathHelper.newContext(object, context.getFunctions());
        PropertySet fields = context.getFields();
        if (fields != null || context.getParameters() != null) {
            IMObjectVariables variables = createVariables();
            if (fields != null) {
                for (String name : fields.getNames()) {
                    Object value = fields.get(name);
                    if (value != null) {
                        variables.add(name, value);
                    }
                }
            }
            result.setVariables(variables);
        }
        return result;
    }

    /**
     * Returns the value of a {@link PropertyState}, handling collection nodes.
     *
     * @param state the state
     * @return the value
     */
    @SuppressWarnings("unchecked")
    private Object getValue(PropertyState state) {
        Object result = null;
        NodeDescriptor descriptor = state.getNode();
        Object value;
        if (descriptor != null && descriptor.isLookup()) {
            value = LookupHelper.getName(getService(), getLookups(), descriptor, state.getParent());
        } else {
            value = state.getValue();
        }
        if (value != null) {
            if (state.getNode() != null && state.getNode().isCollection()) {
                if (value instanceof Collection) {
                    Collection<IMObject> values = (Collection<IMObject>) value;
                    StringBuilder descriptions = new StringBuilder();
                    for (IMObject object : values) {
                        descriptions.append(getValue(object));
                        descriptions.append('\n');
                    }
                    result = descriptions.toString();
                } else if (value instanceof IMObject) {
                    // single value collection.
                    IMObject object = (IMObject) value;
                    result = getValue(object);
                } else {
                    result = value;
                }
            } else {
                result = value;
            }
        }
        return result;
    }

    /**
     * Variables that include report parameters.
     */
    protected static class Variables extends IMObjectVariables {

        private final Parameters parameters;

        public Variables(Parameters parameters, ArchetypeService service, LookupService lookups) {
            super(service, lookups);
            this.parameters = parameters;
        }

        /**
         * Determines if a variable exists.
         *
         * @param name the variable name
         * @return {@code true} if the variable exists
         */
        @Override
        public boolean exists(String name) {
            return super.exists(name) || parameters.exists(name);
        }

        /**
         * Returns the value of the specified variable.
         *
         * @param varName variable name
         * @return Object value
         * @throws IllegalArgumentException if there is no such variable.
         */
        @Override
        public Object getVariable(String varName) {
            if (parameters.exists(varName)) {
                return parameters.get(varName);
            } else {
                return super.getVariable(varName);
            }
        }
    }

}
