/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.report.ExpressionEvaluator;

/**
 * Report messages.
 *
 * @author Tim Anderson
 */
public class ReportMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("REPORT", ReportMessages.class);

    /**
     * Default constructor.
     */
    private ReportMessages() {

    }

    /**
     * Returns a message indicating that a report cannot be generated.
     *
     * @param name   the report name
     * @param reason the reason for the failure
     * @return the message
     */
    public static Message failedToGenerateReport(String name, String reason) {
        return messages.create(1, name, reason);
    }

    /**
     * Returns a message indicating that a report failed to print.
     *
     * @param name    the report name
     * @param printer the printer name
     * @param reason  the reason for the failure
     * @return the message
     */
    public static Message failedToPrintReport(String name, String printer, String reason) {
        return messages.create(2, name, printer, reason);
    }

    /**
     * Returns a message indicating that a document cannot be converted to a particular mime type.
     *
     * @param name     the report name
     * @param mimeType the mime type
     * @return the message
     */
    public static Message cannotConvertJasperReport(String name, String mimeType) {
        return messages.create(10, name, mimeType);
    }

    /**
     * Returns a message indicating that parameters cannot be retrieved from a report.
     *
     * @param name   the report name
     * @param reason the reason for the failure
     * @return the message
     */
    public static Message failedToGetParameters(String name, String reason) {
        return messages.create(11, name, reason);
    }

    /**
     * Returns a message indicating that a JasperReport has no pages to print.
     *
     * @param name the template name
     * @return the message
     */
    public static Message noPagesToPrint(String name) {
        return messages.create(12, name);
    }

    /**
     * Returns a message indicating that a JasperReport has been terminated as it has generated more than the maximum
     * number of pages.
     *
     * @param name     the template name
     * @param maxPages the maximum number of pages
     * @return the message
     */
    public static Message maxPagesExceeded(String name, int maxPages) {
        return messages.create(13, name, maxPages);
    }

    /**
     * Returns a message indicating that a resource referred to a report cannot be accessed.
     * <p/>
     *
     * @param name     the template name
     * @param resource the resource name. A URL or a file path
     * @return the message
     */
    public static Message reportResourceNotFound(String name, String resource) {
        return messages.create(14, name, resource);
    }

    /**
     * Returns a message indicating that a printer could not be located.
     *
     * @param printer the printer name
     * @return the message
     */
    public static Message printerNotFound(String printer) {
        return messages.create(15, printer);
    }

    /**
     * Returns a message indicating that a report failed to be created.
     *
     * @param name   the report name
     * @param reason the reason for the failure
     * @return the message
     */
    public static Message failedToCreateReport(String name, String reason) {
        return messages.create(20, name, reason);
    }

    /**
     * Returns a message indicating that a sub-report cannot be found.
     *
     * @param name   the sub-report name
     * @param parent the parent report
     * @return the message
     */
    public static Message failedToFindSubReport(String name, String parent) {
        return messages.create(21, name, parent);
    }

    /**
     * Returns a message indicating that there is no template for a <em>lookup.documentTemplateType</em>.
     *
     * @param type the type
     * @return the message
     */
    public static Message noTemplateForType(String type) {
        return messages.create(30, type);
    }

    /**
     * Returns a message indicating that a template is not supported.
     *
     * @param name the template name
     * @return the message
     */
    public static Message unsupportedTemplate(String name) {
        return messages.create(31, name);
    }

    /**
     * Returns a message indicating that a template has no document.
     *
     * @param name the template name
     * @return the message
     */
    public static Message templateHasNoDocument(String name) {
        return messages.create(32, name);
    }

    /**
     * Reports a message indicating that a JasperReport template couldn't be read.
     *
     * @param name the template name
     */
    public static Message failedToReadJasperReport(String name) {
        return messages.create(40, name);
    }

    /**
     * Returns a message indicating that a type doesn't have an {@link ExpressionEvaluator}.
     *
     * @param type the type
     * @return the message
     */
    public static Message noExpressionEvaluatorForType(Class<?> type) {
        return messages.create(50, type.getName());
    }

    /**
     * Returns a message indicating that an OpenOffice document couldn't be created.
     *
     * @param name the document name
     * @return the message
     */
    public static Message failedToLoadOpenOfficeDocument(String name) {
        return messages.create(60, name);
    }

    /**
     * Returns a message indicating that user text couldn't be retrieved from an OpenOffice document.
     *
     * @param name the document name
     * @return the message
     */
    public static Message failedToGetOpenOfficeUserFields(String name) {
        return messages.create(61, name);
    }

    /**
     * Returns a message indicating that an OpenOffice document field couldn't be retrieved.
     *
     * @param fieldName    the field name
     * @param documentName the document name
     * @return the message
     */
    public static Message failedToGetOpenOfficeField(String fieldName, String documentName) {
        return messages.create(62, fieldName, documentName);
    }

    /**
     * Returns a message indicating that input fields couldn't be retrieved from an OpenOffice document.
     *
     * @param name the document name
     * @return the message
     */
    public static Message failedToGetOpenOfficeInputFields(String name) {
        return messages.create(63, name);
    }

    /**
     * Returns a message indicating that a field couldn't be set in an OpenOffice document.
     *
     * @param fieldName    the field name
     * @param documentName the document name
     * @return the message
     */
    public static Message failedToSetOpenOfficeField(String fieldName, String documentName) {
        return messages.create(64, fieldName, documentName);
    }

    /**
     * Returns a message indicating that an OpenOffice document couldn't be printed.
     *
     * @param name the document name
     * @return the message
     */
    public static Message failedToPrintOpenOfficeDocument(String name) {
        return messages.create(65, name);
    }

    /**
     * Returns a message indicating that an OpenOffice document couldn't be exported.
     *
     * @param name the document name
     * @return the message
     */
    public static Message failedToExportOpenOfficeDocument(String name) {
        return messages.create(66, name);
    }

    /**
     * Returns a message indicating a document conversion is not supported.
     *
     * @param name       the document name
     * @param mimeType   the mime type being converted from
     * @param toMimeType the mime type being converted to
     * @return the message
     */
    public static Message unsupportedConversion(String name, String mimeType, String toMimeType) {
        return messages.create(70, name, mimeType, toMimeType);
    }

    /**
     * Returns a message indicating that document conversion failed.
     *
     * @param name     the document name
     * @param mimeType the mime type being converted to
     * @return the message
     */
    public static Message conversionFailed(String name, String mimeType) {
        return messages.create(71, name, mimeType);
    }

    /**
     * Returns a message indicating that OpenOffice couldn't be connected to.
     *
     * @param reason the reason for the failure
     * @return the reason
     */
    public static Message failedToConnectToOpenOffice(String reason) {
        return messages.create(80, reason);
    }

    /**
     * Returns a message indicating an unspecified OpenOffice error has been encountered.
     *
     * @return the message
     */
    public static Message openOfficeError() {
        return messages.create(81);
    }

    /**
     * Returns a message indicating that an OpenOffice task was interrupted.
     *
     * @return the message
     */
    public static Message openOfficeTaskInterrupted() {
        return messages.create(82);
    }

    /**
     * Returns a message indicating that an OpenOffice task was cancelled.
     *
     * @return the message
     */
    public static Message openOfficeTaskCancelled() {
        return messages.create(83);
    }

    /**
     * Returns a message indicating that an OpenOffice task timed out.
     *
     * @return the message
     */
    public static Message openOfficeTaskTimedOut() {
        return messages.create(84);
    }

    /**
     * Returns a message to indicate that an object has no document.
     *
     * @param name the object name
     * @return the message
     */
    public static Message noDocument(String name) {
        return messages.create(100, name);
    }

}
