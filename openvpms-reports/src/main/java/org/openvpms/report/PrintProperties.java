/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.openvpms.print.service.PrintAttributes;

import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.MediaTray;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.Sides;

/**
 * Print properties.
 *
 * @author Tim Anderson
 */
public class PrintProperties extends PrintAttributes {

    /**
     * The printer name.
     */
    private final String printerName;

    /**
     * Constructs a (@link PrintProperties}.
     *
     * @param printerName the printer name
     */
    public PrintProperties(String printerName) {
        this.printerName = printerName;
    }

    /**
     * Constructs a (@link PrintProperties}.
     *
     * @param printerName the printer name
     */
    public PrintProperties(String printerName, PrintAttributes attributes) {
        this(printerName);
        setMediaSize(attributes.getMediaSize());
        setOrientation(attributes.getOrientation());
        setMediaTray(attributes.getMediaTray());
        setSides(attributes.getSides());
        setCopies(attributes.getCopies());
    }

    /**
     * Returns the printer name.
     *
     * @return the printer name
     */
    public String getPrinterName() {
        return printerName;
    }

    /**
     * Returns a string representation of this.
     *
     * @return a string representation of this
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("printerName=").append(printerName);
        MediaSizeName mediaSize = getMediaSize();
        if (mediaSize != null) {
            result.append(", mediaSize=").append(mediaSize);
        }
        OrientationRequested orientation = getOrientation();
        if (orientation != null) {
            result.append(", orientation=").append(orientation);
        }
        MediaTray tray = getMediaTray();
        if (tray != null) {
            result.append(", mediaTray=").append(tray);
        }
        Sides sides = getSides();
        if (sides != null) {
            result.append(", sides=").append(sides);
        }
        int copies = getCopies();
        result.append(", copies=").append(copies);
        return result.toString();
    }
}

