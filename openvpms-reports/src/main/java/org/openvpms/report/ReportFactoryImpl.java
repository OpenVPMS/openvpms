/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.jxpath.Functions;
import org.openvpms.archetype.function.factory.ArchetypeFunctionsFactory;
import org.openvpms.archetype.rules.doc.BaseDocumentTemplate;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.report.i18n.ReportMessages;
import org.openvpms.report.msword.MsWordIMReport;
import org.openvpms.report.openoffice.OpenOfficeIMReport;
import org.openvpms.report.openoffice.OpenOfficeService;


/**
 * A factory for {@link Report} instances.
 *
 * @author Tim Anderson
 */
public class ReportFactoryImpl extends AbstractReportFactory {

    /**
     * The OpenOffice service.
     */
    private final OpenOfficeService officeService;

    /**
     * Constructs an {@link ReportFactoryImpl}.
     *
     * @param service           the archetype service
     * @param lookups           the lookup service
     * @param handlers          the document handlers
     * @param factory           the factory for JXPath extension functions
     * @param documentRules     the document rules
     * @param documentConverter the document converter
     * @param officeService     the OpenOffice service
     * @param settings          the settings
     */
    public ReportFactoryImpl(IArchetypeService service, LookupService lookups, DocumentHandlers handlers,
                             ArchetypeFunctionsFactory factory, DocumentRules documentRules,
                             DocumentConverter documentConverter, OpenOfficeService officeService, Settings settings) {
        super(service, lookups, handlers, factory, documentRules, documentConverter, settings);
        this.officeService = officeService;
    }

    /**
     * Creates a report where the template is an OpenOffice or Word document.
     *
     * @param document  the template
     * @param template  the template
     * @param service   the archetype service. This is a read-only proxy
     * @param functions the JXPath extension functions
     * @return a new report
     */
    @Override
    protected IMReport<IMObject> createDocumentReport(Document document, BaseDocumentTemplate template,
                                                      ArchetypeService service, Functions functions) {
        IMReport<IMObject> report;
        String fileName = template.getFileName();
        String ext = FilenameUtils.getExtension(fileName);
        if (isODT(ext)) {
            report = new OpenOfficeIMReport<>(document, service, getLookups(), getHandlers(), functions, officeService);
        } else if (isDOC(ext)) {
            report = new MsWordIMReport<>(document, service, getLookups(), getHandlers(), functions, officeService);
        } else {
            throw new ReportException(ReportMessages.unsupportedTemplate(fileName));
        }
        return report;
    }

    /**
     * Creates a new report for a collection of {@link ObjectSet}s where the template is an OpenOffice or Word document.
     *
     * @param document  the template
     * @param template  the template
     * @param service   the archetype service. This is a read-only proxy
     * @param functions the JXPath extension functions
     * @return a new report
     */
    @Override
    protected IMReport<ObjectSet> createDocumentObjectSetReport(Document document, BaseDocumentTemplate template,
                                                                ArchetypeService service, Functions functions) {
        return new OpenOfficeIMReport<>(document, service, getLookups(), getHandlers(), functions, officeService);
    }
}