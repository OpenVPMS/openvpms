/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.util;

import org.apache.commons.lang.StringUtils;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.report.ExpressionEvaluator;
import org.slf4j.Logger;

/**
 * An {@link ExpressionEvaluator} that traps and logs exceptions.
 *
 * @author Tim Anderson
 */
public class ProtectedExpressionEvaluator {

    /**
     * The report name, for error reporting purposes.
     */
    private final String reportName;

    /**
     * The logger.
     */
    private final Logger log;

    /**
     * Constructs a {@link ProtectedExpressionEvaluator}.
     *
     * @param reportName the report name, for error reporting purposes.
     * @param log        the logger to log to
     */
    public ProtectedExpressionEvaluator(String reportName, Logger log) {
        this.reportName = reportName;
        this.log = log;
    }

    /**
     * Evaluates an expression, catching any exception.
     *
     * @param expression the expression
     * @param evaluator  the expression evaluator to delegate to
     * @param current    the current object. May be {@code null}
     * @return the result of the expression. May be {@code null}
     */
    public Object evaluate(String expression, ExpressionEvaluator evaluator, IMObject current) {
        Object result = null;
        try {
            result = evaluator.evaluate(expression);
        } catch (Throwable exception) {
            log(expression, current, null, exception);
        }
        return result;
    }

    /**
     * Evaluates an expression, catching any exception.
     *
     * @param object     the object
     * @param expression the expression
     * @param current    the current object. May be {@code null}
     * @return the result of the expression. May be {@code null}
     */
    public Object evaluate(Object object, String expression, ExpressionEvaluator evaluator, IMObject current) {
        Object result = null;
        try {
            result = evaluator.evaluate(object, expression);
        } catch (Throwable exception) {
            log(expression, current, object, exception);
        }
        return result;
    }

    /**
     * Returns the formatted value of an expression.
     *
     * @param expression the expression
     * @param current    the current object. May be {@code null}
     * @return the result of the expression
     */
    public String getFormattedValue(String expression, ExpressionEvaluator evaluator, IMObject current) {
        String result;
        try {
            result = evaluator.getFormattedValue(expression);
        } catch (Throwable exception) {
            result = exception.getMessage();
            log(expression, current, null, exception);
        }
        return result;
    }

    /**
     * Invoked to log an evaluation failure.
     *
     * @param expression the expression being evaluated
     * @param current    the current record object, or {@code null} if there is none
     * @param value      the value the expression is being evaluated against, or {@code null} if there is none,
     *                   or it is the data source object.
     * @param exception  the exception
     */
    public void log(String expression, IMObject current, Object value, Throwable exception) {
        if (log.isWarnEnabled()) {
            if (value instanceof IMObject) {
                value = ((IMObject) value).getObjectReference();
            }
            StringBuilder builder = new StringBuilder("Failed to evaluate '" + expression + "' in report="
                                                      + reportName);
            if (value != null) {
                builder.append(" against object=").append(StringUtils.abbreviate(value.toString(), 255));
            }
            if (current != null) {
                builder.append(". Current record object=").append(current.getObjectReference());
            }
            builder.append(": ").append(exception.getMessage());
            if (log.isWarnEnabled()) {
                log.warn(builder.toString());
            } else if (log.isDebugEnabled()) {
                log.debug(builder.toString(), exception);
            }
        }
    }

}
