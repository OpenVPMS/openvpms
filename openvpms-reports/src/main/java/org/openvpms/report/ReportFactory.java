/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.openvpms.archetype.rules.doc.BaseDocumentTemplate;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.system.common.query.ObjectSet;

/**
 * Factory for {@link Report}s.
 *
 * @author Tim Anderson
 */
public interface ReportFactory {

    /**
     * Determines if a document template contains mergeable content.
     *
     * @param template the template
     * @return {@code true} if the template contains mergeable content, otherwise {@code false}
     */
    boolean hasMergeableContent(BaseDocumentTemplate template);

    /**
     * Creates a new report.
     *
     * @param template the document template
     * @return a new report
     * @throws ReportException for any report error
     */
    Report createReport(DocumentTemplate template);

    /**
     * Determines if a template can be used to create a report via {@link #createIMObjectReport(BaseDocumentTemplate)}.
     *
     * @param template the template
     * @return {@code true} if the template can be used to create a report
     */
    boolean isIMObjectReport(BaseDocumentTemplate template);

    /**
     * Creates a new report for a collection of {@link IMObject}s.
     *
     * @param template the document template
     * @return a new report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    IMReport<IMObject> createIMObjectReport(BaseDocumentTemplate template);

    /**
     * Determines if a template can be used to create a report via {@link #createObjectSetReport(BaseDocumentTemplate)}.
     *
     * @param template    the template
     * @param cardinality the no. of objects being reported on. OpenOffice/Word templates only support a single object
     * @return {@code true} if the template can be used to create a report
     */
    boolean isObjectSetReport(BaseDocumentTemplate template, int cardinality);

    /**
     * Creates a new report for a collection of {@link ObjectSet}s.
     *
     * @param template the document template
     * @return a new report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    IMReport<ObjectSet> createObjectSetReport(BaseDocumentTemplate template);

    /**
     * Returns a report to process static content.
     *
     * @param template the template
     * @return a new report
     */
    IMReport<IMObject> createStaticContentReport(DocumentTemplate template);

    /**
     * Returns a report to process static content.
     *
     * @param document the document
     * @return a new report
     */
    IMReport<IMObject> createStaticContentReport(Document document);

}
