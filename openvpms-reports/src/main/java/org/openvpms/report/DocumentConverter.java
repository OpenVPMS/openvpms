/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.openvpms.component.model.document.Document;

/**
 * Document converter.
 *
 * @author Tim Anderson
 */
public interface DocumentConverter {

    /**
     * Determines if a document can be converted.
     *
     * @param document the document to convert
     * @param mimeType the target mime type
     * @return {@code true} if the document can be converted, otherwise {@code false}
     */
    boolean canConvert(Document document, String mimeType);

    /**
     * Determines if a document can be converted.
     *
     * @param fileName       the document file name
     * @param sourceMimeType the document mime type
     * @param targetMimeType the target mime type
     * @return {@code true} if the document can be converted, otherwise {@code false}
     */
    boolean canConvert(String fileName, String sourceMimeType, String targetMimeType);

    /**
     * Converts a document.
     *
     * @param document the document to convert
     * @param mimeType the target mime type
     * @return the converted document
     * @throws ReportException if the document cannot be converted
     */
    Document convert(Document document, String mimeType);

    /**
     * Converts a document.
     *
     * @param document the document to convert
     * @param mimeType the target mime type
     * @param email    if {@code true} indicates that the document will be emailed. Documents that support parameters
     *                 can perform custom formatting
     * @return the converted document
     * @throws ReportException if the document cannot be converted
     */
    Document convert(Document document, String mimeType, boolean email);

    /**
     * Exports the document.
     *
     * @param document the document to export
     * @param mimeType the mime-type of the document format to export to
     * @param email    if {@code true} indicates that the document will be emailed. Documents that support parameters
     *                 can perform custom formatting
     * @return the exported document serialized to a byte array
     * @throws ReportException if the document cannot be exported
     */
    byte[] export(Document document, String mimeType, boolean email);
}