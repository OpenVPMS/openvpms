/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import org.apache.commons.jxpath.Functions;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.report.Parameters;

/**
 * Context information for reporting.
 *
 * @author Tim Anderson
 */
public class ReportContext {

    /**
     * Report parameters. May be {@code null}
     */
    private final Parameters parameters;

    /**
     * Additional fields. May be {@code null}
     */
    private final PropertySet fields;

    /**
     * The report name, for error reporting purposes.
     */
    private final String reportName;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The JXPath extension functions.
     */
    private final Functions functions;


    /**
     * Constructs a {@link ReportContext}.
     *
     * @param parameters the report parameters
     * @param fields     fields to pass to the report. May be {@code null}
     * @param reportName the report name, for error reporting purposes
     * @param service    the archetype service
     * @param lookups    the lookup service
     * @param handlers   the document handlers. May be {@code null}
     * @param functions  the JXPath extension functions
     */
    public ReportContext(Parameters parameters, PropertySet fields, String reportName, ArchetypeService service,
                         LookupService lookups, DocumentHandlers handlers, Functions functions) {

        this.parameters = parameters;
        this.fields = fields;
        this.reportName = reportName;
        this.functions = functions;
        this.service = service;
        this.handlers = handlers;
        this.lookups = lookups;
    }

    /**
     * Returns the parameters.
     *
     * @return the parameters. May be {@code null}
     */
    public Parameters getParameters() {
        return parameters;
    }

    /**
     * Returns the fields.
     *
     * @return the fields. May be {@code null}
     */
    public PropertySet getFields() {
        return fields;
    }

    /**
     * Returns the report name.
     *
     * @return the report name
     */
    public String getReportName() {
        return reportName;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    public ArchetypeService getArchetypeService() {
        return service;
    }

    /**
     * Returns the JXPath extension functions.
     *
     * @return the JXPath extension functions
     */
    public Functions getFunctions() {
        return functions;
    }

    /**
     * Returns the lookup service.
     *
     * @return the lookup service
     */
    public LookupService getLookupService() {
        return lookups;
    }

    /**
     * Returns the document handlers.
     *
     * @return the document handlers. May be {@code null}
     */
    public DocumentHandlers getDocumentHandlers() {
        return handlers;
    }

}
