/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.SimpleReportContext;

import java.util.Map;

/**
 * Jasper Report helper.
 *
 * @author Tim Amderson
 */
public class JasperReportHelper {

    /**
     * Creates a new {@code ReportContext} from a map of parameters. Parameters can be null.
     *
     * @param parameters the parameters
     * @return a new report context
     */
    public static SimpleReportContext createReportContext(Map<String, Object> parameters) {
        SimpleReportContext result = new SimpleReportContext();
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            result.setParameterValue(entry.getKey(), entry.getValue());
        }
        return result;

    }
}
