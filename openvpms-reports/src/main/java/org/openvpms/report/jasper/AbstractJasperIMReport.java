/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRRewindableDataSource;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.ReportContext;
import net.sf.jasperreports.engine.SimpleJasperReportsContext;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXmlExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.fill.JREvaluator;
import net.sf.jasperreports.engine.query.JRQueryExecuter;
import net.sf.jasperreports.engine.query.JRQueryExecuterFactoryBundle;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.ExporterConfiguration;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.ReportExportConfiguration;
import net.sf.jasperreports.export.SimpleCsvExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterConfiguration;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.export.SimpleXmlExporterOutput;
import net.sf.jasperreports.export.WriterExporterOutput;
import net.sf.jasperreports.governors.MaxPagesGovernor;
import net.sf.jasperreports.governors.MaxPagesGovernorException;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.jxpath.Functions;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.i18n.Message;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.DocFormats;
import org.openvpms.report.ParameterType;
import org.openvpms.report.PrintProperties;
import org.openvpms.report.ReportException;
import org.openvpms.report.i18n.ReportMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttribute;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.CopiesSupported;
import javax.print.attribute.standard.PrinterName;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import static net.sf.jasperreports.engine.util.JRLoader.EXCEPTION_MESSAGE_KEY_INPUT_STREAM_FROM_URL_OPEN_ERROR;
import static net.sf.jasperreports.repo.RepositoryUtil.EXCEPTION_MESSAGE_KEY_BYTE_DATA_NOT_FOUND;

/**
 * Abstract implementation of the {@link JasperIMReport} interface.
 * <p/>
 * The <em>maxPage</em> setting (i.e. <em>settings.getInt(SettingsArchetypes.REPORT_SETTINGS, "maxPages", ...)</em>) is
 * used to limit the no. of pages a report can print or export, to terminate misbehaving reports.<br/>
 * Note that this is disabled when a report is exported to CSV or XLS, as pagination is turned off for these.
 *
 * @author Tim Anderson
 */
public abstract class AbstractJasperIMReport<T> implements JasperIMReport<T> {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The JXPath extension functions.
     */
    private final Functions functions;

    /**
     * The jasper reports context.
     */
    private final SimpleJasperReportsContext context;

    /**
     * The JDBC query factory.
     */
    private final JDBCQueryExecuterFactory factory;

    /**
     * The maximum no. of pages a report can generate, before it is terminated.
     */
    private final int maxPages;

    /**
     * The supported mime types.
     */
    private static final String[] MIME_TYPES = {DocFormats.PDF_TYPE, DocFormats.RTF_TYPE, DocFormats.XLS_TYPE,
                                                DocFormats.CSV_TYPE, DocFormats.TEXT_TYPE};

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractJasperIMReport.class);

    /**
     * Header to use when exporting to HTML.
     */
    private static final String HTML_HEADER =
            "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" " +
            "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
            "<html>\n" +
            "<head>\n" +
            "    <title></title>\n" +
            "</head>\n" +
            "<body>";

    /**
     * Footer to use when exporting to HTML.
     */
    private static final String HTML_FOOTER = "</body>\n" + "</html>";


    /**
     * Constructs an {@link AbstractJasperIMReport}.
     *
     * @param service   the archetype service
     * @param lookups   the lookup service
     * @param handlers  the document handlers
     * @param functions the JXPath extension functions
     * @param settings  the settings
     */
    public AbstractJasperIMReport(ArchetypeService service, LookupService lookups, DocumentHandlers handlers,
                                  Functions functions, Settings settings) {
        this.service = service;
        this.lookups = lookups;
        this.handlers = handlers;
        this.functions = functions;
        context = new SimpleJasperReportsContext();
        maxPages = settings.getInt(SettingsArchetypes.REPORT_SETTINGS, "maxPages", 1000);
        if (maxPages > 0) {
            // limit the number of pages the report can generate, to stop badly behaving reports going into infinite
            // loops and consuming too much memory
            context.setProperty(MaxPagesGovernor.PROPERTY_MAX_PAGES_ENABLED, Boolean.toString(true));
            context.setProperty(MaxPagesGovernor.PROPERTY_MAX_PAGES, Integer.toString(maxPages));
        }

        // need to register the factory even if its not used, as they are registered when the template is loaded.
        factory = new JDBCQueryExecuterFactory(service, lookups, functions);
        List<JDBCQueryExecutorFactoryBundle> extensions = Collections.singletonList(
                new JDBCQueryExecutorFactoryBundle(factory));
        context.setExtensions(JRQueryExecuterFactoryBundle.class, extensions);
    }

    /**
     * Returns the set of parameter types that may be supplied to the report.
     *
     * @return the parameter types
     * @throws ReportException if a parameter expression can't be evaluated
     */
    @Override
    public Set<ParameterType> getParameterTypes() {
        Set<ParameterType> types = new LinkedHashSet<>();
        JasperReport report = getReport();
        for (JRParameter p : report.getParameters()) {
            if (!p.isSystemDefined() && p.isForPrompting()) {
                JRExpression expression = p.getDefaultValueExpression();
                Object defaultValue = null;
                if (expression != null) {
                    try {
                        defaultValue = getEvaluator().evaluate(expression);
                    } catch (ReportException exception) {
                        throw exception;
                    } catch (Throwable exception) {
                        throw new ReportException(ReportMessages.failedToGetParameters(
                                getName(), exception.getMessage()), exception);
                    }
                }
                ParameterType type = new ParameterType(p.getName(), p.getValueClass(), p.getDescription(),
                                                       defaultValue);
                types.add(type);
            }
        }
        return types;
    }

    /**
     * Determines if the report accepts the named parameter.
     *
     * @param name the parameter name
     * @return {@code true} if the report accepts the parameter, otherwise {@code false}
     */
    @Override
    public boolean hasParameter(String name) {
        for (JRParameter p : getReport().getParameters()) {
            if (Objects.equals(p.getName(), name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the default mime type for report documents.
     *
     * @return the default mime type
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public String getDefaultMimeType() {
        return DocFormats.PDF_TYPE;
    }

    /**
     * Returns the supported mime types for report documents.
     *
     * @return the supported mime types
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public String[] getMimeTypes() {
        return MIME_TYPES;
    }

    /**
     * Generates a report.
     * <p>
     * The default mime type will be used to select the output format.
     *
     * @param parameters a map of parameter names and their values, to pass to the report
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @return a document containing the report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public Document generate(Map<String, Object> parameters, Map<String, Object> fields) {
        return generate(parameters, fields, getDefaultMimeType());
    }

    /**
     * Generates a report.
     *
     * @param parameters a map of parameter names and their values, to pass to the report
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param mimeType   the output format of the report
     * @return a document containing the report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public Document generate(Map<String, Object> parameters, Map<String, Object> fields, String mimeType) {
        Document document;
        Map<String, Object> properties = getDefaultParameters();
        if (parameters != null) {
            properties.putAll(parameters);
        }
        if (mimeType.equals(DocFormats.CSV_TYPE) || mimeType.equals(DocFormats.XLS_TYPE)) {
            properties.put(JRParameter.IS_IGNORE_PAGINATION, true);
        }
        JasperReport report = getReport();
        MutableObject<JRQueryExecuter> executor = new MutableObject<>();
        try {
            document = runProtectedReport(() -> {
                executor.setValue(initDataSource(properties, fields, report, context));
                JasperPrint print = JasperFillManager.getInstance(context).fill(report, properties);
                return export(print, properties, mimeType);
            });
        } finally {
            if (executor.getValue() != null) {
                executor.getValue().close();
            }
        }
        return document;
    }

    /**
     * Generates a report for a collection of objects.
     * <p>
     * The default mime type will be used to select the output format.
     *
     * @param objects the objects to report on
     * @return a document containing the report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public Document generate(Iterable<T> objects) {
        return generate(objects, getDefaultMimeType());
    }

    /**
     * Generates a report for a collection of objects.
     *
     * @param objects  the objects to report on
     * @param mimeType the output format of the report
     * @return a document containing the report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public Document generate(Iterable<T> objects, String mimeType) {
        Map<String, Object> empty = Collections.emptyMap();
        return generate(objects, empty, null, mimeType);
    }

    /**
     * Generates a report for a collection of objects.
     * <p>
     * The default mime type will be used to select the output format.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @return a document containing the report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Document generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields) {
        return generate(objects, parameters, fields, getDefaultMimeType());
    }

    /**
     * Generates a report for a collection of objects.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param mimeType   the output format of the report
     * @return a document containing the report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Document generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                             String mimeType) {
        Map<String, Object> modifiedParameters = getGenerationParameters(parameters, mimeType);
        return runProtectedReport(() -> {
            JasperPrint print = report(objects, modifiedParameters, fields);
            return export(print, modifiedParameters, mimeType);
        });
    }

    /**
     * Generates a report for a collection of objects to the specified stream.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param mimeType   the output format of the report
     * @param stream     the stream to write to
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                         String mimeType, OutputStream stream) {
        Map<String, Object> modifiedParameters = getGenerationParameters(parameters, mimeType);
        runProtectedReport(() -> {
            JasperPrint report = report(objects, modifiedParameters, fields);
            return export(report, stream, modifiedParameters, mimeType);
        });
    }

    /**
     * Prints a report directly to a printer.
     *
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param properties the print properties
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void print(Map<String, Object> parameters, Map<String, Object> fields, PrintProperties properties) {
        Map<String, Object> params = getDefaultParameters();
        JasperReport report = getReport();
        if (parameters != null) {
            params.putAll(parameters);
        }
        MutableObject<JRQueryExecuter> executor = new MutableObject<>();
        try {
            printProtectedReport(properties, () -> {
                executor.setValue(initDataSource(params, fields, report, context));
                JasperPrint print = JasperFillManager.getInstance(context).fill(getReport(), params);
                print(print, properties);
                return null;
            });
        } finally {
            if (executor.getValue() != null) {
                executor.getValue().close();
            }
        }
    }

    /**
     * Prints a report directly to a printer.
     *
     * @param objects    the objects to report on
     * @param properties the print properties
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public void print(Iterable<T> objects, PrintProperties properties) {
        Map<String, Object> empty = Collections.emptyMap();
        print(objects, empty, null, properties);
    }

    /**
     * Prints a report directly to a printer.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report
     * @param fields     additional fields available to the report. May be {@code null}
     * @param properties the print properties
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void print(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                      PrintProperties properties) {
        printProtectedReport(properties, () -> {
            JasperPrint print = report(objects, parameters, fields);
            print(print, properties);
            return null;
        });
    }

    /**
     * Generates a report.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @return the report
     * @throws JRException for any error
     */
    protected JasperPrint report(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields)
            throws JRException {
        JRDataSource source = createDataSource(objects, parameters, fields);
        HashMap<String, Object> properties = new HashMap<>(getDefaultParameters());
        if (parameters != null) {
            properties.putAll(parameters);
        }
        properties.put("dataSource", source);  // custom data source name, to avoid casting
        properties.put(JRParameter.REPORT_DATA_SOURCE, source);
        return JasperFillManager.getInstance(context).fill(getReport(), properties, source);
    }

    /**
     * Creates a data source for a collection of objects.
     *
     * @param objects    an iterator over the collection of objects
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @return a new data source
     */
    protected abstract JRRewindableDataSource createDataSource(Iterable<T> objects, Map<String, Object> parameters,
                                                               Map<String, Object> fields);

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getArchetypeService() {
        return service;
    }

    /**
     * Returns the lookup service.
     *
     * @return the lookup service
     */
    protected LookupService getLookupService() {
        return lookups;
    }

    /**
     * Returns the document handlers.
     *
     * @return the document handlers
     */
    protected DocumentHandlers getDocumentHandlers() {
        return handlers;
    }

    /**
     * Returns the JXPath extension functions.
     *
     * @return the JXPath extension functions
     */
    protected Functions getFunctions() {
        return functions;
    }

    /**
     * Returns the default report parameters to use when filling the report.
     *
     * @return the report parameters
     */
    protected Map<String, Object> getDefaultParameters() {
        return new HashMap<>();
    }

    /**
     * Converts a report to a document.
     *
     * @param report     the report to convert
     * @param parameters export parameters
     * @param mimeType   the mime-type of the document
     * @return a document containing the report
     * @throws ReportException           for any error
     * @throws ArchetypeServiceException for any archetype service error
     */
    protected Document export(JasperPrint report, Map<String, Object> parameters, String mimeType) {
        Document document;
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream(2048);
            String ext = export(report, output, parameters, mimeType);
            byte[] data = output.toByteArray();

            String name = report.getName() + "." + ext;
            DocumentHandler handler = handlers.get(name, DocumentArchetypes.DEFAULT_DOCUMENT, mimeType);
            ByteArrayInputStream stream = new ByteArrayInputStream(data);
            document = handler.create(name, stream, mimeType, data.length);
        } catch (JRException exception) {
            throw adaptJRException(exception, () -> ReportMessages.failedToGenerateReport(
                    getName(), exception.getMessage()));
        }
        return document;
    }

    /**
     * Exports a report to a stream as the specified mime type.
     *
     * @param report     the report to export
     * @param stream     the stream to write to
     * @param mimeType   the mime type to export as
     * @param parameters export parameters
     * @return the file name extension of the mime-type
     * @throws JRException if the export fails
     */
    protected String export(JasperPrint report, OutputStream stream, Map<String, Object> parameters,
                            String mimeType) throws JRException {
        String ext;
        if (DocFormats.PDF_TYPE.equals(mimeType)) {
            exportToPDF(report, stream);
            ext = DocFormats.PDF_EXT;
        } else if (DocFormats.HTML_TYPE.equals(mimeType)) {
            exportToHTML(report, stream);
            ext = DocFormats.HTML_EXT;
        } else if (DocFormats.RTF_TYPE.equals(mimeType)) {
            exportToRTF(report, stream);
            ext = DocFormats.RTF_EXT;
        } else if (DocFormats.XLS_TYPE.equals(mimeType)) {
            exportToXLS(report, stream);
            ext = DocFormats.XLS_EXT;
        } else if (DocFormats.CSV_TYPE.equals(mimeType)) {
            exportToCSV(report, stream);
            ext = DocFormats.CSV_EXT;
        } else if (DocFormats.XML_TYPE.equals(mimeType)) {
            exportToXML(report, stream);
            ext = DocFormats.XML_EXT;
        } else if (DocFormats.TEXT_TYPE.equals(mimeType)) {
            exportToText(report, stream, parameters);
            ext = DocFormats.TEXT_EXT;
        } else if (DocFormats.ODT_TYPE.equals(mimeType)) {
            exportToODT(report, stream);
            ext = DocFormats.ODT_EXT;
        } else if (DocFormats.DOCX_TYPE.equals(mimeType)) {
            exportToDOCX(report, stream);
            ext = DocFormats.DOCX_EXT;
        } else {
            throw new ReportException(ReportMessages.cannotConvertJasperReport(getName(), mimeType));
        }
        return ext;
    }

    /**
     * Returns the expression evaluator.
     *
     * @return the expression evaluator
     * @throws JRException if the evaluator can't be loaded
     */
    protected abstract JREvaluator getEvaluator() throws JRException;

    /**
     * Returns the jasper reports context.
     *
     * @return the jasper reports context
     */
    protected SimpleJasperReportsContext getJasperReportsContext() {
        return context;
    }

    /**
     * Returns parameters to use when generating reports.
     * <p/>
     * For CSV and XLS mime types, pagination is disabled.
     *
     * @param parameters the source parameters. May be {@code null}
     * @param mimeType   the mime type
     * @return the parameters. Never {@code null}
     */
    private Map<String, Object> getGenerationParameters(Map<String, Object> parameters, String mimeType) {
        Map<String, Object> result = (parameters != null) ? new HashMap<>(parameters) : new HashMap<>();
        if (mimeType.equals(DocFormats.CSV_TYPE) || mimeType.equals(DocFormats.XLS_TYPE)) {
            result.put(JRParameter.IS_IGNORE_PAGINATION, true);
        }
        return result;
    }

    /**
     * Exports a generated jasper report to PDF.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToPDF(JasperPrint report, OutputStream stream) throws JRException {
        exportStream(report, stream, new JRPdfExporter());
    }

    /**
     * Exports a generated jasper report to HTML.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToHTML(JasperPrint report, OutputStream stream) throws JRException {
        HtmlExporter exporter = new HtmlExporter();
        // set the HTML header and footer. The JasperReports default is to centre everything
        SimpleHtmlExporterConfiguration configuration = new SimpleHtmlExporterConfiguration();
        configuration.setHtmlHeader(HTML_HEADER);
        configuration.setHtmlFooter(HTML_FOOTER);
        exporter.setConfiguration(configuration);
        exporter.setExporterInput(new SimpleExporterInput(report));
        exporter.setExporterOutput(new SimpleHtmlExporterOutput(stream));
        exporter.exportReport();
    }

    /**
     * Exports a generated jasper report to an ODT stream.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToODT(JasperPrint report, OutputStream stream) throws JRException {
        exportStream(report, stream, new JROdtExporter());
    }

    /**
     * Exports a generated jasper report to a DOCX stream.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToDOCX(JasperPrint report, OutputStream stream) throws JRException {
        exportStream(report, stream, new JRDocxExporter());
    }

    /**
     * Exports a generated jasper report to an RTF stream.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToRTF(JasperPrint report, OutputStream stream) throws JRException {
        exportWriter(report, stream, new JRRtfExporter());
    }

    /**
     * Exports a generated jasper report to a stream as XLS.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToXLS(JasperPrint report, OutputStream stream) throws JRException {
        JRXlsExporter exporter = new JRXlsExporter();
        SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
        configuration.setDetectCellType(true);
        configuration.setWhitePageBackground(false);
        configuration.setIgnorePageMargins(true);
        configuration.setCollapseRowSpan(true);
        configuration.setRemoveEmptySpaceBetweenRows(true);
        configuration.setRemoveEmptySpaceBetweenColumns(true);
        exporter.setConfiguration(configuration);
        exportStream(report, stream, exporter);
    }

    /**
     * Exports a generated jasper report to a stream as CSV.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToCSV(JasperPrint report, OutputStream stream) throws JRException {
        JRCsvExporter exporter = new JRCsvExporter();
        SimpleCsvExporterConfiguration configuration = new SimpleCsvExporterConfiguration();
        configuration.setFieldDelimiter(",");
        configuration.setRecordDelimiter("\n");
        exporter.setConfiguration(configuration);
        exportWriter(report, stream, exporter);
    }

    /**
     * Exports a generated jasper report to a stream as XML.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToXML(JasperPrint report, OutputStream stream) throws JRException {
        JRXmlExporter exporter = new JRXmlExporter();
        exporter.setExporterInput(new SimpleExporterInput(report));
        exporter.setExporterOutput(new SimpleXmlExporterOutput(stream));
        exporter.exportReport();
    }

    /**
     * Exports a generated jasper report to a stream as plain text.
     *
     * @param report the report
     * @param stream the stream to write to
     * @throws JRException if the export fails
     */
    private void exportToText(JasperPrint report, OutputStream stream, Map<String, Object> parameters)
            throws JRException {
        JRTextExporter exporter = new JRTextExporter();
        ReportContext context = JasperReportHelper.createReportContext(parameters);
        exporter.setReportContext(context);
        exportWriter(report, stream, exporter);
    }

    /**
     * Exports a report using a stream.
     *
     * @param report   the report to export
     * @param stream   the stream to export to
     * @param exporter the exporter
     * @throws JRException if the export fails
     */
    private void exportStream(JasperPrint report, OutputStream stream,
                              Exporter<ExporterInput, ? extends ReportExportConfiguration,
                                      ? extends ExporterConfiguration, OutputStreamExporterOutput> exporter)
            throws JRException {
        exporter.setExporterInput(new SimpleExporterInput(report));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
        exporter.exportReport();
    }

    /**
     * Exports a report using a writer.
     *
     * @param report   the report to export
     * @param stream   the underlying stream to export to
     * @param exporter the exporter
     * @throws JRException if the export fails
     */
    private void exportWriter(JasperPrint report, OutputStream stream,
                              Exporter<ExporterInput, ? extends ReportExportConfiguration,
                                      ? extends ExporterConfiguration, WriterExporterOutput> exporter)
            throws JRException {
        exporter.setExporterInput(new SimpleExporterInput(report));
        exporter.setExporterOutput(new SimpleWriterExporterOutput(stream));
        exporter.exportReport();
    }

    /**
     * Prints a {@code JasperPrint} to a printer.
     *
     * @param print      the object to print
     * @param properties the print properties
     * @throws ReportException if {@code print} contains no pages, or the printer cannot be located
     * @throws JRException     for any error
     */
    private void print(JasperPrint print, PrintProperties properties) throws JRException {
        if (print.getPages().isEmpty()) {
            throw new ReportException(ReportMessages.noPagesToPrint(getName()));
        }
        if (log.isDebugEnabled()) {
            log.debug("print: {}", properties);
        }

        PrintService printService = getPrintService(properties.getPrinterName());

        PrintRequestAttributeSet set = new HashPrintRequestAttributeSet();
        addAttribute(set, properties.getMediaSize());
        addAttribute(set, properties.getOrientation());
        addAttribute(set, properties.getMediaTray());
        addAttribute(set, properties.getSides());
        int copies = properties.getCopies();
        int count = 1;
        if (copies > 1) {
            if (supportsCopies(printService, copies)) {
                set.add(new Copies(copies));
            } else {
                // printer doesn't support the requested copies, so create multiple jobs instead
                count = copies;
                log.debug("Multiple copies not supported by '{}', creating {} jobs", properties.getPrinterName(),
                          copies);
            }
        }
        SimplePrintServiceExporterConfiguration printConfiguration = new SimplePrintServiceExporterConfiguration();
        printConfiguration.setPrintRequestAttributeSet(set);
        printConfiguration.setPrintService(printService);

        JRPrintServiceExporter exporter = new JRPrintServiceExporter();
        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.setConfiguration(printConfiguration);

        // print it
        for (int i = 0; i < count; ++i) {
            exporter.exportReport();
        }
    }

    /**
     * Looks up a print service, given its name.
     *
     * @param printerName the printer name
     * @return the corresponding print service
     * @throws ReportException if the print service cannot be found
     */
    private PrintService getPrintService(String printerName) {
        PrintService result;
        // set the printer name
        PrintServiceAttributeSet serviceAttributeSet = new HashPrintServiceAttributeSet();
        serviceAttributeSet.add(new PrinterName(printerName, null));
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, serviceAttributeSet);
        if (services.length > 0) {
            result = services[0];
        } else {
            throw new ReportException(ReportMessages.printerNotFound(printerName));
        }
        return result;
    }

    /**
     * Adds an attribute to a set, if the attribute is non-null.
     *
     * @param set       the set
     * @param attribute the attribute. May be {@code null}
     */
    private void addAttribute(PrintRequestAttributeSet set, PrintRequestAttribute attribute) {
        if (attribute != null) {
            set.add(attribute);
        }
    }

    /**
     * Determines if a print service supports the requested number of copies.
     *
     * @param printService the print service
     * @param copies       the copies
     * @return {@code true} if the print service supports the requested number of copies, otherwise {@code false}
     */
    private boolean supportsCopies(PrintService printService, int copies) {
        CopiesSupported supported = (CopiesSupported) printService.getSupportedAttributeValues(
                Copies.class, null, null);
        return (supported != null && supported.contains(copies));
    }

    /**
     * Initialises a JDBC data source, if required.
     *
     * @param params  the report parameters
     * @param fields  additional fields available to the report. May be {@code null}
     * @param report  the report
     * @param context the jasper reports context
     * @throws JRException if the data source cannot be created
     */
    private JRQueryExecuter initDataSource(Map<String, Object> params, Map<String, Object> fields, JasperReport report,
                                           SimpleJasperReportsContext context)
            throws JRException {
        JRQueryExecuter executor = null;
        Connection connection = (Connection) params.get(JRParameter.REPORT_CONNECTION);
        if (connection != null) {
            factory.setFields(fields);
            factory.setReportName(getName());
            executor = factory.createQueryExecuter(context, report, params);
            JRDataSource dataSource = executor.createDatasource();
            params.put(JRParameter.REPORT_DATA_SOURCE, dataSource);
        }
        return executor;
    }

    /**
     * Runs a function invoking JasperReports, rethrowing any exception as a {@link ReportException}.
     *
     * @param reporter the reporting function
     * @return the result of the function
     * @throws ReportException for any error
     */
    private <R> R runProtectedReport(Callable<R> reporter) {
        R result;
        try {
            result = reporter.call();
        } catch (ReportException exception) {
            throw exception;
        } catch (MaxPagesGovernorException exception) {
            throw new ReportException(ReportMessages.maxPagesExceeded(getName(), maxPages), exception);
        } catch (JRRuntimeException exception) {
            throw adaptJRException(exception, () -> ReportMessages.failedToGenerateReport(
                    getName(), exception.getMessage()));
        } catch (JRException exception) {
            throw adaptJRException(exception, () -> ReportMessages.failedToGenerateReport(
                    getName(), exception.getMessage()));
        } catch (Throwable exception) {
            throw new ReportException(ReportMessages.failedToGenerateReport(getName(), exception.getMessage()),
                                      exception);
        }
        return result;
    }

    /**
     * Runs a JasperReports print function, rethrowing any exception as a {@link ReportException}.
     *
     * @param printer the printing function
     * @throws ReportException for any error
     */
    private void printProtectedReport(PrintProperties properties, Callable<Void> printer) {
        try {
            printer.call();
        } catch (ReportException exception) {
            throw exception;
        } catch (MaxPagesGovernorException exception) {
            throw new ReportException(ReportMessages.maxPagesExceeded(getName(), maxPages), exception);
        } catch (JRException exception) {
            throw adaptJRException(exception, () -> ReportMessages.failedToPrintReport(
                    getName(), properties.getPrinterName(), exception.getMessage()));
        } catch (Throwable exception) {
            throw new ReportException(ReportMessages.failedToPrintReport(getName(), properties.getPrinterName(),
                                                                         exception.getMessage()), exception);
        }
    }

    /**
     * Attempts to adapt {@link JRException} to a {@link ReportException}, if it indicates that a file or URL
     * used by a JasperReport cannot be accessed. If not, returns an exception using a fallback message.
     *
     * @param exception the exception to adapt. A {@link JRException} or {@link JRRuntimeException}
     * @param fallback  the fallback message, if the exception cannot be adapted
     * @return the new exception
     */
    private ReportException adaptJRException(Exception exception, Supplier<Message> fallback) {
        ReportException result = null;
        for (Throwable cause : ExceptionUtils.getThrowableList(exception)) {
            String resource = getResourceNotFound(cause);
            if (resource != null) {
                result = new ReportException(ReportMessages.reportResourceNotFound(getName(), resource), exception);
                break;
            }
        }
        if (result == null) {
            result = new ReportException(fallback.get(), exception);
        }
        return result;
    }

    /**
     * Determines if an exception is a {@link JRException} indicating that a resource couldn't be found/read.
     *
     * @param throwable the exception
     * @return the resource path/URL, or {@code null} if the exception doesn't refer to a missing resource
     */
    private String getResourceNotFound(Throwable throwable) {
        String result = null;
        if (throwable instanceof JRException) {
            JRException exception = (JRException) throwable;
            String key = exception.getMessageKey();
            if ((EXCEPTION_MESSAGE_KEY_INPUT_STREAM_FROM_URL_OPEN_ERROR.equals(key) ||
                 EXCEPTION_MESSAGE_KEY_BYTE_DATA_NOT_FOUND.equals(key)) && exception.getArgs() != null
                && exception.getArgs().length == 1) {
                result = exception.getArgs()[0].toString();
            }
        }
        return result;
    }
}
