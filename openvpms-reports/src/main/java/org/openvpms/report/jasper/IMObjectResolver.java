/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import org.apache.commons.collections4.ComparatorUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.comparators.ComparatorChain;
import org.apache.commons.collections4.comparators.TransformingComparator;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.service.archetype.helper.BasePropertyResolver;
import org.openvpms.component.business.service.archetype.helper.NodeResolver;
import org.openvpms.component.business.service.archetype.helper.PropertyResolverException;
import org.openvpms.component.business.service.archetype.helper.sort.IMObjectSorter;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.util.PropertyState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.openvpms.component.business.service.archetype.helper.PropertyResolverException.ErrorCode.InvalidProperty;

/**
 * Resolves collections of {@link IMObject}s from a root {@link IMObject}, using the composite node syntax.
 * <p/>
 * TODO - should be rolled into {@link NodeResolver}.
 *
 * @author Tim Anderson
 */
class IMObjectResolver extends BasePropertyResolver {

    /**
     * The root object.
     */
    private final IMObject root;

    /**
     * The default comparator. This treats nulls as low, and uses a natural comparator.
     */
    private static final Comparator<Object> DEFAULT_COMPARATOR = ComparatorUtils.nullLowComparator(null);

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IMObjectResolver.class);

    /**
     * Constructs an {@link IMObjectResolver}.
     *
     * @param root    the root object
     * @param service the archetype service
     * @param lookups the lookup service
     */
    public IMObjectResolver(IMObject root, ArchetypeService service, LookupService lookups) {
        super(service, lookups);
        this.root = root;
    }

    /**
     * Resolves the named property.
     *
     * @param name the property name
     * @return the corresponding property
     * @throws PropertyResolverException if the name is invalid
     */
    @Override
    public Object getObject(String name) {
        return new NodeResolver(root, getService(), getLookups()).getObject(name);
    }

    /**
     * Returns all objects matching the named property.
     * <p/>
     * Unlike {@link #getObject(String)}, this method handles collections of arbitrary size.
     *
     * @param name the property name
     * @return the corresponding property values
     * @throws PropertyResolverException if the name is invalid
     */
    @Override
    public List<Object> getObjects(String name) {
        return new NodeResolver(root, getService(), getLookups()).getObjects(name);
    }

    /**
     * Resolves the state corresponding to a property.
     *
     * @param name the property name
     * @return the resolved state
     * @throws PropertyResolverException if the name is invalid
     */
    @Override
    public PropertyState resolve(String name) {
        return new NodeResolver(root, getService(), getLookups()).resolve(name);
    }

    /**
     * Returns all objects matching the named property.
     * <p/>
     * This may be a composite node name.
     *
     * @param name           the node name
     * @param relativeToRoot if {@code true}, the sort nodes are relative to the root of the collection node,
     *                       otherwise they are relative to the retrieved objects
     * @param sortNodes      the sort nodes. These may be composite node names
     * @return the matching objects
     */
    public List<IMObject> getObjects(String name, boolean relativeToRoot, String... sortNodes) {
        List<IMObject> result;
        if (sortNodes.length == 0 || !relativeToRoot) {
            // if no sort node is specified, getObjectsAndSortRelativeToLeaf() will sort on id
            result = getObjectsAndSortRelativeToLeaf(name, sortNodes);
        } else {
            result = getObjectsAndSortRelativeToRoot(name, sortNodes);
        }
        return result;
    }

    /**
     * Retrieves the objects identified by the composite node name, and sorts them according to the sort nodes,
     * if any. The sort nodes are relative to the retrieved objects.
     * <p/>
     * A final implicit sort on the object id is added to guarantee reproducibility (where there are no duplicates).
     *
     * @param name      the composite node name
     * @param sortNodes the nodes to sort on
     */
    @SuppressWarnings("unchecked")
    private List<IMObject> getObjectsAndSortRelativeToLeaf(String name, String... sortNodes) {
        List<IMObject> result = (List<IMObject>) (List<?>) getObjects(name);

        ComparatorChain<IMObject> chain = new ComparatorChain<>();
        for (String sortNode : sortNodes) {
            RelativeNodeTransformer transformer = new RelativeNodeTransformer(sortNode);
            TransformingComparator<IMObject, Object> transComparator
                    = new TransformingComparator<>(transformer, DEFAULT_COMPARATOR);
            chain.addComparator(transComparator);
        }
        chain.addComparator(IMObjectSorter.getIdComparator());
        result.sort(chain);
        return result;
    }

    /**
     * Retrieves the objects identified by the composite node name, and sorts them according to the sort nodes.
     * The sort nodes are relative to the root object.
     * <p/>
     * This requires an object graph to be built, in order to sort on objects retrieved prior to the leaf node of
     * the specified composite node name.
     * <p/>
     * A final implicit sort on the object id is added to guarantee reproducibility (where there are no duplicates).
     *
     * @param name      the composite node name
     * @param sortNodes the nodes to sort on
     */
    private List<IMObject> getObjectsAndSortRelativeToRoot(String name, String[] sortNodes) {
        List<IMObject> result = new ArrayList<>();
        List<NodeObject> leafNodes = new ArrayList<>();
        leafNodes.add(new NodeObject(null, root));
        String[] nodes = StringUtils.split(name, '.');
        if (nodes.length == 0) {
            throw new PropertyResolverException(PropertyResolverException.ErrorCode.InvalidProperty, name);
        }

        // Build the graph. After iterating through the nodes, leafNodes holds the leaf objects that need to be sorted
        for (String node : nodes) {
            leafNodes = getChildNodes(leafNodes, node, name);
        }

        // To sort, convert the node graph to lists of lists. Each list represents the object paths to the leaf object.
        List<List<IMObject>> lists = new ArrayList<>();
        for (NodeObject object : leafNodes) {
            lists.add(object.getPathAfter(root));
        }

        // build the sort criteria
        ComparatorChain<List<IMObject>> chain = new ComparatorChain<>();
        for (String sortNode : sortNodes) {
            Comparator<List<IMObject>> comparator = getListNodeComparator(nodes, sortNode);
            if (comparator != null) {
                chain.addComparator(comparator);
            }
        }

        // add a final sort on leaf object id
        Transformer<List<IMObject>, Object> transformer = objects -> objects.get(objects.size() - 1).getId();
        chain.addComparator(new TransformingComparator<>(transformer));

        // sort, and get the now sorted leaf objects
        lists.sort(chain);
        for (List<IMObject> objects : lists) {
            result.add(objects.get(objects.size() - 1));
        }
        return result;
    }

    /**
     * Returns a comparator to support lists of {@link IMObject}s, representing the retrieval path of leaf objects.
     *
     * @param nodes    the nodes representing the composite node that the leaf objects were retrieved using
     * @param sortNode the sort node, relative to the root of the composite node
     * @return a comparator, or {@code null} if {@code sortNode} is invalid
     */
    private Comparator<List<IMObject>> getListNodeComparator(String[] nodes, String sortNode) {
        Comparator<List<IMObject>> result = null;
        String[] sortNodes = StringUtils.split(sortNode, '.');
        int lastMatch = -1;
        // find the common node between the composite node and sort node
        for (int i = 0; i < nodes.length && i < sortNodes.length && nodes[i].equals(sortNodes[i]); ++i) {
            lastMatch = i;
        }
        if (lastMatch >= 0 && lastMatch + 1 < sortNodes.length) {
            String relativeSortNode = StringUtils.join(sortNodes, '.', lastMatch + 1, sortNodes.length);
            result = new ListNodeComparator(lastMatch, relativeSortNode);
        } else {
            log.warn("Invalid absolute sort node: {} given for node: {}", sortNode, StringUtils.join(nodes, '.'));
        }
        return result;
    }

    /**
     * Adds the immediate children of the supplied parent nodes, and returns them.
     *
     * @param parents the parent nodes
     * @param node    the node
     * @param name    the composite node name for error reporting
     * @return the immediate children of the parents
     */
    private List<NodeObject> getChildNodes(List<NodeObject> parents, String node, String name) {
        List<NodeObject> result = new ArrayList<>();
        ArchetypeDescriptor archetype = null;
        for (NodeObject parent : parents) {
            IMObject object = parent.object;
            archetype = getArchetype(object, archetype);
            for (IMObject child : getChildren(object, node, name, archetype)) {
                result.add(parent.addChild(child));
            }
        }
        return result;
    }

    /**
     * Returns the child objects of the supplied parent.
     *
     * @param parent    the parent object
     * @param node      the node
     * @param name      the composite node name, for error reporting purposes
     * @param archetype the parent object archetype
     * @return the child objects
     */
    private List<IMObject> getChildren(IMObject parent, String node, String name, ArchetypeDescriptor archetype) {
        List<IMObject> result = Collections.emptyList();
        NodeDescriptor descriptor = getNode(archetype, node);
        if (descriptor.isCollection()) {
            result = descriptor.getValues(parent);
        } else {
            Object child = getValue(parent, descriptor, true);
            if (child != null) {
                if (child instanceof IMObject) {
                    result = Collections.singletonList((IMObject) child);
                } else {
                    // need an IMObject
                    throw new PropertyResolverException(InvalidProperty, name);
                }
            }
        }
        return result;
    }

    /**
     * Returns the object at the named node, if it is a {@link Comparable}.
     *
     * @param object the object to resolve from
     * @param node   the composite node name
     * @return the corresponding object. May be {@code null}
     */
    @SuppressWarnings("unchecked")
    private Comparable<Object> getComparable(IMObject object, String node) {
        Object result = null;
        try {
            NodeResolver resolver = new NodeResolver(object, getService(), getLookups());
            result = resolver.getObject(node);
        } catch (PropertyResolverException ignore) {
            // do nothing
        }
        return result instanceof Comparable ? (Comparable<Object>) result : null;
    }

    private class RelativeNodeTransformer implements Transformer<IMObject, Comparable<Object>> {

        /**
         * The field name.
         */
        private final String name;

        /**
         * Constructs a {@link RelativeNodeTransformer}.
         *
         * @param name the field name
         */
        public RelativeNodeTransformer(String name) {
            this.name = name;
        }

        /**
         * Transforms the input object (leaving it unchanged) into some output
         * object.
         *
         * @param input the object to be transformed, should be left unchanged
         */
        public Comparable<Object> transform(IMObject input) {
            return getComparable(input, name);
        }
    }

    /**
     * A comparator used to extract the relevant sort node from a list of objects representing the leaf object retrieval
     * path. This allows leaf objects to be sorted on nodes from their parents.
     */
    private class ListNodeComparator implements Comparator<List<IMObject>> {

        /**
         * The index into the list.
         */
        private final int index;

        /**
         * The sort node, relative to the index.
         */
        private final String sortNode;

        /**
         * Constructs an {@link ListNodeComparator}.
         *
         * @param index    the index into the list
         * @param sortNode the sort node, relative to index
         */
        public ListNodeComparator(int index, String sortNode) {
            this.index = index;
            this.sortNode = sortNode;
        }

        @Override
        public int compare(List<IMObject> o1, List<IMObject> o2) {
            int result = 0;
            if (index < o1.size() && index < o2.size()) {
                Object v1 = getComparable(o1.get(index), sortNode);
                Object v2 = getComparable(o2.get(index), sortNode);
                result = DEFAULT_COMPARATOR.compare(v1, v2);
            }
            return result;
        }
    }

    /**
     * Helper to build an object graph of retrieved objects, in order to support sorting on parent objects.
     */
    private static class NodeObject {

        /**
         * The parent node.
         */
        private final NodeObject parent;

        /**
         * The object.
         */
        private final IMObject object;

        /**
         * Constructs a {@link NodeObject}.
         *
         * @param parent the parent node
         * @param object the object
         */
        public NodeObject(NodeObject parent, IMObject object) {
            this.parent = parent;
            this.object = object;
        }

        /**
         * Adds a child object of this node.
         *
         * @param object the child object
         * @return the node containing the child object
         */
        public NodeObject addChild(IMObject object) {
            return new NodeObject(this, object);
        }

        /**
         * Returns the object path after the specified root, up to and including this node's object.
         *
         * @param root the root
         * @return the object path
         */
        public List<IMObject> getPathAfter(IMObject root) {
            ArrayList<IMObject> result = new ArrayList<>();
            NodeObject node = this;
            while (node != null && node.object != root) {
                result.add(node.object);
                node = node.parent;
            }
            Collections.reverse(result);
            return result;
        }
    }
}