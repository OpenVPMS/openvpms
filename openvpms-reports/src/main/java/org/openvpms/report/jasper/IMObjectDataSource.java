/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRRewindableDataSource;
import org.apache.commons.jxpath.Functions;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.helper.NodeResolver;
import org.openvpms.component.business.service.archetype.helper.ResolvingPropertySet;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.ExpressionEvaluator;
import org.openvpms.report.IMObjectExpressionEvaluator;
import org.openvpms.report.Parameters;

import java.util.List;
import java.util.Map;


/**
 * Implementation of the {@code JRDataSource} interface, for a single {@link IMObject}.
 *
 * @author Tim Anderson
 */
public class IMObjectDataSource extends AbstractDataSource {

    /**
     * The source object.
     */
    private final IMObject object;

    /**
     * The expression evaluator.
     */
    private final ExpressionEvaluator evaluator;

    /**
     * Determines if there is another record.
     */
    private boolean next = true;

    /**
     * Constructs an {@link IMObjectDataSource}.
     *
     * @param object     the source object
     * @param parameters the report parameters
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param reportName the report name, for error reporting purposes
     * @param service    the archetype service
     * @param handlers   the document handlers
     * @param functions  the JXPath extension functions
     */
    public IMObjectDataSource(IMObject object, Parameters parameters, Map<String, Object> fields,
                              String reportName, ArchetypeService service, LookupService lookups,
                              DocumentHandlers handlers, Functions functions) {
        this(object, new ReportContext(parameters,
                                       fields != null ? new ResolvingPropertySet(fields, service, lookups) : null,
                                       reportName, service, lookups, handlers, functions));
    }

    /**
     * Constructs an {@link IMObjectDataSource}.
     *
     * @param object  the source object
     * @param context the report context
     */
    protected IMObjectDataSource(IMObject object, ReportContext context) {
        super(context);
        this.object = object;
        NodeResolver resolver = new NodeResolver(object, context.getArchetypeService(), context.getLookupService());
        evaluator = new IMObjectExpressionEvaluator(object, resolver, context);
    }

    /**
     * Tries to position the cursor on the next element in the data source.
     *
     * @return {@code true} if there is a next record, {@code false} otherwise
     */
    public boolean next() {
        boolean result = next;
        next = false;
        return result;
    }

    /**
     * Returns a data source for a collection node.
     *
     * @param name the collection node name. This may be a composite node name
     * @param sortNodes the list of nodes to sort on. Nodes names are relative to the retrieved objects
     * @return the data source
     * @throws JRException for any error
     */
    @Override
    public JRRewindableDataSource getDataSource(String name, String... sortNodes) throws JRException {
        return getDataSource(name, false, sortNodes);
    }

    /**
     * Returns a data source for a collection node.
     *
     * @param name the collection node name. This may be a composite node name
     * @param relativeToRoot if {@code true}, the sort nodes are relative to the root of the collection node,
     *                       otherwise they are relative to the retrieved objects
     * @param sortNodes      the list of nodes to sort on
     * @return the data source
     * @throws JRException for any error
     */
    public JRRewindableDataSource getDataSource(String name, boolean relativeToRoot, String... sortNodes)
            throws JRException {
        IMObjectResolver resolver = new IMObjectResolver(object, getContext().getArchetypeService(),
                                                         getContext().getLookupService());
        List<IMObject> values = resolver.getObjects(name, relativeToRoot, sortNodes);
        return new IMObjectCollectionDataSource(values, getContext());
    }

    /**
     * Returns a data source for the given jxpath expression.
     *
     * @param expression the expression. Must return an {@code Iterable} or {@code Iterator} returning {@code IMObjects}
     * @return the data source
     * @throws JRException for any error
     */
    @Override
    public JRRewindableDataSource getExpressionDataSource(String expression) throws JRException {
        return getExpressionDataSource(object, expression);
    }

    /**
     * Gets the field value for the current position.
     *
     * @param field the field
     * @return an object containing the field value. The object type must be the field object type.
     */
    public Object getFieldValue(JRField field) throws JRException {
        return getFieldValue(field, object);
    }

    /**
     * Evaluates an xpath expression.
     *
     * @param expression the expression
     * @return the result of the expression. May be {@code null}
     */
    public Object evaluate(String expression) {
        return evaluate(expression, evaluator, object);
    }

    /**
     * Evaluates an xpath expression against an object.
     *
     * @param object     the object
     * @param expression the expression
     * @return the result of the expression. May be {@code null}
     */
    @Override
    public Object evaluate(Object object, String expression) {
        return evaluate(object, expression, evaluator, this.object);
    }

    /**
     * Gets the field value for the current position.
     * <p/>
     * Implementers of this method aren't required to do any exception handling or verifying that the value is of
     * the correct type.
     *
     * @param field the field
     * @return an object containing the field value
     */
    @Override
    protected Object getValue(JRField field) {
        Object value = evaluator.getValue(field.getName());
        if (value instanceof Document) {
            Document doc = (Document) value;
            if (doc.getContents() != null && doc.getContents().length != 0) {
                DocumentHandlers handlers = getContext().getDocumentHandlers();
                if (handlers != null) {
                    DocumentHandler handler = handlers.get(doc);
                    value = handler.getContent(doc);
                } else {
                    value = null;
                }
            } else {
                value = null;
            }
        }
        return value;
    }
}