/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRRewindableDataSource;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.report.ExpressionEvaluator;
import org.openvpms.report.util.ProtectedExpressionEvaluator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 * Base class for JasperReports data sources.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDataSource implements DataSource {

    /**
     * The report context.
     */
    private final ReportContext context;

    /**
     * The expression evaluator.
     */
    private final ProtectedExpressionEvaluator expressionEvaluator;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractDataSource.class);

    /**
     * Constructs an {@link AbstractDataSource}.
     *
     * @param context the report context
     */
    public AbstractDataSource(ReportContext context) {
        this.context = context;
        this.expressionEvaluator = new ProtectedExpressionEvaluator(context.getReportName(), log);
    }

    /**
     * Returns a data source for a collection node.
     *
     * @param name the collection node name. This may be a composite node name
     * @return the data source
     * @throws JRException for any error
     */
    public JRRewindableDataSource getDataSource(String name) throws JRException {
        return getDataSource(name, false);
    }

    /**
     * Returns a data source for a collection node.
     *
     * @param name      the collection node name. This may be a composite node name
     * @param sortNodes the list of nodes to sort on. Nodes names are relative to the retrieved objects
     * @return the data source
     * @throws JRException for any error
     */
    @Override
    public JRRewindableDataSource getDataSource(String name, String... sortNodes) throws JRException {
        return getDataSource(name, false, sortNodes);
    }

    /**
     * Gets the field value for the current position.
     * <p/>
     * If the value is not of the expected type, this will throw an {@link JRException}, unless the field type is
     * {@code String}, in which case a {@link Object#toString()} conversion will be used.
     * <p/>
     * For historical reasons, this is lenient in what it will allow:
     * <ul>
     *     <li>fields that refer to non-existent nodes will return null<br>
     *         This is because some archetypes share the same templates, despite having different nodes.</li>
     *     <li>any value to be converted to a String, if the field is a string. In all other cases, mismatched types
     *     throw JRException</li>
     *     <li>any expressions that fail to evaluate will return null, unless they throw JRException, which will be
     *     rethrown</li>
     * </ul>
     *
     * @param field   the field
     * @param current the current object. May be {@code null}
     * @return the field value. May be {@code null}
     * @throws JRException for any error
     */
    protected Object getFieldValue(JRField field, IMObject current) throws JRException {
        Object value = null;
        try {
            value = getValue(field);
            if (value != null && !field.getValueClass().isInstance(value)) {
                if (field.getValueClass() == String.class) {
                    value = value.toString();
                } else {
                    throw new JRException("Failed to evaluate '" + field.getName() + "' in report=" + getReportName() +
                                          ". Expected class=" + field.getValueClass()
                                          + " but got " + value.getClass().getName());
                }
            }
        } catch (JRException exception) {
            throw exception;
        } catch (Throwable exception) {
            // For historical reasons, any other failure is caught and null returned
            if (log.isDebugEnabled()) {
                expressionEvaluator.log(field.getName(), current, null, exception);
            }
        }
        return value;
    }

    /**
     * Gets the field value for the current position.
     * <p/>
     * Implementers of this method aren't required to do any exception handling or verifying that the value is of
     * the correct type.
     *
     * @param field the field
     * @return an object containing the field value
     * @throws JRException for any error
     */
    protected abstract Object getValue(JRField field) throws JRException;

    /**
     * Evaluates an xpath expression, catching any exception.
     *
     * @param expression the expression
     * @param evaluator  the expression evaluator
     * @param current    the current object. May be {@code null}
     * @return the result of the expression. May be {@code null}
     */
    protected Object evaluate(String expression, ExpressionEvaluator evaluator, IMObject current) {
        return expressionEvaluator.evaluate(expression, evaluator, current);
    }

    /**
     * Evaluates an xpath expression, catching any exception.
     *
     * @param object     the object
     * @param expression the expression
     * @param current    the current object
     * @return the result of the expression. May be {@code null}
     */
    protected Object evaluate(Object object, String expression, ExpressionEvaluator evaluator, IMObject current) {
        return expressionEvaluator.evaluate(object, expression, evaluator, current);
    }

    /**
     * Returns a data source for the given jxpath expression.
     *
     * @param object     the object
     * @param expression the expression. Must return a single {@link IMObject}, or an {@code Iterable} returning
     *                   {@link IMObject}s
     * @return the data source
     * @throws JRException for any error
     */
    protected JRRewindableDataSource getExpressionDataSource(Object object, String expression) throws JRException {
        Object value = evaluate(object, expression);
        return getCollectionDataSource(value, expression);
    }

    /**
     * Returns a collection data source for the supplied value.
     *
     * @param value      the value. Must be a single {@link IMObject} or an {@link Iterable} returning
     *                   {@link IMObject}s.
     * @param expression the expression that generated the value
     * @return the data source
     * @throws JRException if the value is not supported
     */
    @SuppressWarnings("unchecked")
    protected JRRewindableDataSource getCollectionDataSource(Object value, String expression) throws JRException {
        Iterable<IMObject> iterable;
        if (value instanceof Iterable) {
            iterable = (Iterable<IMObject>) value;
        } else if (value instanceof IMObject) {
            iterable = Collections.singletonList((IMObject) value);
        } else {
            throw new JRException("Unsupported value type=" + ((value != null) ? value.getClass() : null)
                                  + " returned by expression=" + expression);
        }
        return new IMObjectCollectionDataSource(iterable, context);
    }

    /**
     * Returns the report context.
     *
     * @return the report context
     */
    protected ReportContext getContext() {
        return context;
    }

    /**
     * Returns the report name.
     *
     * @return the report name
     */
    protected String getReportName() {
        return context.getReportName();
    }

}
