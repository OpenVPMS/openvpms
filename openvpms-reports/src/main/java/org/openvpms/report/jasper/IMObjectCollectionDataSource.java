/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRRewindableDataSource;
import org.apache.commons.jxpath.Functions;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.report.Parameters;

import java.util.Iterator;
import java.util.List;


/**
 * Implementation of the {@code JRDataSource} interface, for collections of {@link IMObject}s.
 *
 * @author Tim Anderson
 */
public class IMObjectCollectionDataSource extends AbstractDataSource implements JRRewindableDataSource {

    /**
     * The collection.
     */
    private final Iterable<IMObject> collection;

    /**
     * The collection iterator.
     */
    private Iterator<IMObject> iterator;

    /**
     * The current object.
     */
    private IMObjectDataSource current;


    /**
     * Constructs a {@link IMObjectCollectionDataSource} for a collection of objects.
     *
     * @param objects    the objects
     * @param parameters the report parameters. May be {@code null}
     * @param fields     additional report fields. These override any in the report. May be {@code null}
     * @param reportName the report name, for error reporting purposes
     * @param service    the archetype service
     * @param lookups    the lookup service
     * @param handlers   the document handlers
     * @param functions  the JXPath extension functions
     */
    public IMObjectCollectionDataSource(Iterable<IMObject> objects, Parameters parameters, PropertySet fields,
                                        String reportName, ArchetypeService service, LookupService lookups,
                                        DocumentHandlers handlers, Functions functions) {
        this(objects, new ReportContext(parameters, fields, reportName, service, lookups, handlers, functions));
    }

    /**
     * Constructs a {@link IMObjectCollectionDataSource} for a collection of objects.
     *
     * @param objects the objects
     * @param context the report context
     */
    public IMObjectCollectionDataSource(Iterable<IMObject> objects, ReportContext context) {
        super(context);
        collection = objects;
        iterator = collection.iterator();
    }

    /**
     * Constructs a {@link IMObjectCollectionDataSource} for a collection node.
     *
     * @param values  the collection values
     * @param context the report context
     */
    protected IMObjectCollectionDataSource(List<IMObject> values, ReportContext context) {
        super(context);
        collection = values;
        iterator = collection.iterator();
    }

    /**
     * Tries to position the cursor on the next element in the data source.
     *
     * @return true if there is a next record, false otherwise
     */
    public boolean next() {
        boolean result = iterator.hasNext();
        if (result) {
            current = new IMObjectDataSource(iterator.next(), getContext());
        }
        return result;
    }

    /**
     * Returns a data source for a collection node.
     *
     * @param name the collection node name. This may be a composite node name
     * @param relativeToRoot if {@code true}, the sort nodes are relative to the root of the collection node,
     *                       otherwise they are relative to the retrieved objects
     * @param sortNodes      the list of nodes to sort on
     * @return the data source
     * @throws JRException for any error
     */
    @Override
    public JRRewindableDataSource getDataSource(String name, boolean relativeToRoot, String... sortNodes)
            throws JRException {
        return current.getDataSource(name, relativeToRoot, sortNodes);
    }

    /**
     * Returns a data source for the given jxpath expression.
     *
     * @param expression the expression. Must return an {@code Iterable} or {@code Iterator} returning {@code IMObjects}
     * @return the data source
     * @throws JRException for any error
     */
    @Override
    public JRRewindableDataSource getExpressionDataSource(String expression) throws JRException {
        return current.getExpressionDataSource(expression);
    }

    /**
     * Gets the field value for the current position.
     *
     * @return an object containing the field value. The object type must be the field object type.
     * @throws JRException for any error
     */
    public Object getFieldValue(JRField field) throws JRException {
        return getValue(field);
    }

    /**
     * Evaluates an xpath expression.
     *
     * @param expression the expression
     * @return the result of the expression. May be {@code null}
     */
    public Object evaluate(String expression) {
        return current != null ? current.evaluate(expression) : null;
    }

    /**
     * Evaluates an xpath expression against an object.
     *
     * @param object     the object
     * @param expression the expression
     * @return the result of the expression. May be {@code null}
     */
    @Override
    public Object evaluate(Object object, String expression) {
        return current != null ? current.evaluate(object, expression) : null;
    }

    /**
     * Moves back to the first element in the data source.
     */
    @Override
    public void moveFirst() {
        iterator = collection.iterator();
    }

    /**
     * Gets the field value for the current position.
     * <p/>
     * Implementers of this method aren't required to do any exception handling or verifying that the value is of
     * the correct type.
     *
     * @param field the field
     * @return an object containing the field value
     * @throws JRException for any error
     */
    @Override
    protected Object getValue(JRField field) throws JRException {
        return (current != null) ? current.getFieldValue(field) : null;
    }
}
