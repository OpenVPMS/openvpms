/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.tools;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentException;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentHelper;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.version.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * Report template loader.
 *
 * @author Tim Anderson
 */
public class TemplateLoader {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The default application context.
     */
    private static final String APPLICATION_CONTEXT = "applicationContext.xml";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TemplateLoader.class);


    /**
     * Constructs a {@link TemplateLoader}.
     *
     * @param service            the archetype service
     * @param handlers           the document handlers
     * @param transactionManager the transaction manager
     * @param lookups            the lookup service
     */
    public TemplateLoader(IArchetypeService service, DocumentHandlers handlers, PlatformTransactionManager
            transactionManager, LookupService lookups) {
        this.service = service;
        this.handlers = handlers;
        this.transactionManager = transactionManager;
        this.lookups = lookups;
    }

    /**
     * Loads all templates from a file.
     *
     * @param path the file path
     * @throws ArchetypeServiceException for any archetype service error
     * @throws JAXBException             for any JAXB error
     */
    public void load(String path) throws JAXBException {
        load(path, new HashMap<>());
    }

    /**
     * Loads all templates from a file.
     *
     * @param path           the file path
     * @param emailTemplates the loaded email templates
     * @throws ArchetypeServiceException for any archetype service error
     * @throws JAXBException             for any JAXB error
     */
    public void load(String path, Map<String, Entity> emailTemplates) throws JAXBException {
        File file = new File(path);
        Templates templates = getTemplates(file);
        File dir = file.getParentFile();

        // first pass loads all email templates
        for (BaseTemplate template : templates.getTemplateOrEmailTemplate()) {
            if (template instanceof EmailTemplate) {
                Entity entity = loadEmailTemplate((EmailTemplate) template, dir);
                emailTemplates.put(entity.getName(), entity);
            }
        }

        // second pass loads all document templates
        for (BaseTemplate template : templates.getTemplateOrEmailTemplate()) {
            if (template instanceof Template) {
                loadTemplate((Template) template, dir, emailTemplates);
            }
        }
    }

    /**
     * Loads the named template.
     *
     * @param name           the template name
     * @param templates      the available templates
     * @param dir            the directory to base relative files on
     * @param emailTemplates the email templates keyed on name, used to resolve references to email templates
     * @return {@code true} if the template was loaded, or {@code false} it couldn't be found
     */
    public boolean load(String name, Templates templates, File dir, Map<String, Entity> emailTemplates) {
        boolean found = false;
        for (BaseTemplate template : templates.getTemplateOrEmailTemplate()) {
            if (template.getName().equals(name)) {
                if (template instanceof EmailTemplate) {
                    Entity emailTemplate = loadEmailTemplate((EmailTemplate) template, dir);
                    emailTemplates.put(template.getName(), emailTemplate);
                } else {
                    loadTemplate((Template) template, dir, emailTemplates);
                }
                found = true;
                break;
            }
        }
        return found;
    }

    /**
     * Loads the template descriptors from file.
     *
     * @param file the file
     * @return the template descriptors
     * @throws JAXBException for any JAXB error
     */
    public Templates getTemplates(File file) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Templates.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (Templates) unmarshaller.unmarshal(file);
    }

    /**
     * Main line.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        try {
            JSAP parser = createParser();
            JSAPResult config = parser.parse(args);
            if (!config.success()) {
                displayUsage(parser);
            } else {
                String contextPath = config.getString("context");
                String file = config.getString("file");

                ApplicationContext context;
                if (!new File(contextPath).exists()) {
                    context = new ClassPathXmlApplicationContext(contextPath);
                } else {
                    context = new FileSystemXmlApplicationContext(contextPath);
                }

                if (file != null) {
                    IArchetypeService service = context.getBean(IArchetypeRuleService.class);
                    DocumentHandlers handlers = context.getBean(DocumentHandlers.class);
                    PlatformTransactionManager transactionManager = context.getBean(PlatformTransactionManager.class);
                    LookupService lookups = context.getBean(LookupService.class);
                    TemplateLoader loader = new TemplateLoader(service, handlers, transactionManager, lookups);
                    loader.load(file);
                } else {
                    displayUsage(parser);
                }
            }
        } catch (Throwable exception) {
            log.error(exception.getMessage(), exception);
        }
    }

    /**
     * Load a document template.
     * <br/>
     * NOTE: email templates must have already been loaded prior to this being invoked.
     *
     * @param template       the report template to load
     * @param dir            the parent directory for resolving relative paths
     * @param emailTemplates the email templates
     * @throws DocumentException         if the document cannot be created
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void loadTemplate(Template template, File dir, Map<String, Entity> emailTemplates) {
        DocLoader loader = new DocLoader(emailTemplates);
        loader.load(template, dir);
    }

    /**
     * Loads an email template.
     *
     * @param template the template
     * @param dir      the parent directory for resolving relative paths
     * @return the template entity
     */
    private Entity loadEmailTemplate(EmailTemplate template, File dir) {
        EmailLoader loader = new EmailLoader(template.isSystem());
        return loader.load(template, dir);
    }


    /**
     * Creates a new command line parser.
     *
     * @return a new parser
     * @throws JSAPException if the parser can't be created
     */
    private static JSAP createParser() throws JSAPException {
        JSAP parser = new JSAP();
        parser.registerParameter(new FlaggedOption("context").setShortFlag('c')
                                         .setLongFlag("context")
                                         .setDefault(APPLICATION_CONTEXT)
                                         .setHelp("Application context path"));
        parser.registerParameter(new FlaggedOption("file").setShortFlag('f')
                                         .setLongFlag("file").setHelp(
                        "The template configuration file to load."));
        return parser;
    }

    /**
     * Prints usage information.
     */
    private static void displayUsage(JSAP parser) {
        System.err.println();
        System.err
                .println("Usage: java " + TemplateLoader.class.getName());
        System.err.println("                " + parser.getUsage());
        System.err.println();
        System.err.println(parser.getHelp());
        System.exit(1);
    }

    /**
     * Loads a template.
     */
    private abstract class Loader<T extends BaseTemplate> {

        private final String archetype;

        private Entity entity;

        private final Set<IMObject> toSave = new HashSet<>();

        /**
         * Constructs a {@link Loader}.
         *
         * @param archetype the template archetype
         */
        public Loader(String archetype) {
            this.archetype = archetype;
        }

        /**
         * Loads a the template.
         *
         * @param template the template descriptor
         * @param dir      the directory to locate relative paths
         * @return the template entity
         */
        public Entity load(T template, File dir) {
            TransactionTemplate txnTemplate = new TransactionTemplate(transactionManager);
            Entity entity = txnTemplate.execute(status -> {
                Entity result = prepare(template, dir);
                service.save(toSave);
                return result;
            });
            log.info("Loaded '" + entity.getName() + "'");
            return entity;
        }

        /**
         * Prepares the template.
         *
         * @param template the template descriptor
         * @param dir      the directory to locate relative paths
         * @return the template entity
         */
        protected Entity prepare(T template, File dir) {
            Document document = getDocument(dir, template.getPath(), template.getDocType(), template.getMimeType());
            DocumentAct act = getAct(document.getName());
            boolean updateDocument = true;
            Document existing = null;
            if (act != null) {
                IMObjectBean bean = service.getBean(act);
                entity = bean.getTarget("template", Entity.class);
                existing = (act.getDocument() != null) ? (Document) service.get(act.getDocument()) : null;
                if (existing != null) {
                    // if the document hasn't changed, don't load it
                    if (existing.getSize() == document.getSize()
                        && existing.getChecksum() == document.getChecksum()
                        && Objects.equals(existing.getName(), document.getName())
                        && Objects.equals(existing.getMimeType(), document.getMimeType())) {
                        updateDocument = false;
                    }
                }

            } else {
                act = service.create(DocumentArchetypes.DOCUMENT_TEMPLATE_ACT, DocumentAct.class);
            }
            if (updateDocument) {
                act.setFileName(document.getName());
                act.setMimeType(document.getMimeType());
                act.setDescription(DescriptorHelper.getDisplayName(document, service));
                toSave.add(act);
                toSave.add(document);
            } else {
                document = null;
            }

            if (entity == null) {
                entity = (Entity) service.create(archetype);
                if (entity == null) {
                    throw new IllegalStateException("Failed to create " + archetype + ": archetype not found");
                }
                IMObjectBean bean = service.getBean(act);
                bean.setTarget("template", entity);
                toSave.add(act);
            }

            if (updateDocument) {
                if (!act.isNew()) {
                    if (existing != null) {
                        act.setDocument(null);
                        service.save(act);
                        service.remove(existing);
                    }
                }
                act.setDocument(document.getObjectReference());
            }

            entity.setName(template.getName());
            entity.setDescription(template.getDescription());
            IMObjectBean bean = service.getBean(entity);
            bean.setValue("revision", Version.VERSION);
            toSave.add(entity);
            return entity;
        }

        /**
         * Creates a new {@link Document} from a template.
         *
         * @param dir      the directory to locate relative paths
         * @param path     the template path
         * @param docType  the document archetype. May be {@code null}
         * @param mimeType the mime type
         * @return a new document containing the serialized template
         * @throws DocumentException for any error
         */
        protected Document getDocument(File dir, String path, String docType, String mimeType) {
            File file = new File(path);
            if (!file.isAbsolute()) {
                file = new File(dir, path);
            }
            if (docType == null) {
                docType = DocumentArchetypes.DEFAULT_DOCUMENT;
            }
            return DocumentHelper.create(file, docType, mimeType, handlers);
        }

        /**
         * Returns the <em>act.documentTemplate</em> with the specified name.
         *
         * @param name the name
         * @return the corresponding act, or {@code null} if none is found
         */
        private DocumentAct getAct(String name) {
            ArchetypeQuery query = new ArchetypeQuery(DocumentArchetypes.DOCUMENT_TEMPLATE_ACT, false, true);
            query.add(Constraints.eq("name", name));
            query.add(Constraints.join("template").add(Constraints.isA("entity", archetype)));
            query.add(Constraints.sort("id"));
            query.setFirstResult(0);
            IMObjectQueryIterator<DocumentAct> iterator = new IMObjectQueryIterator<>(service, query);
            return iterator.hasNext() ? iterator.next() : null;
        }
    }

    /**
     * Loads document templates.
     */
    private class DocLoader extends Loader<Template> {

        /**
         * The email templates, keyed on name.
         */
        private final Map<String, Entity> emailTemplates;

        /**
         * Constructs a {@link DocLoader}.
         *
         * @param emailTemplates the email templates, keyed on name
         */
        public DocLoader(Map<String, Entity> emailTemplates) {
            super(DocumentArchetypes.DOCUMENT_TEMPLATE);
            this.emailTemplates = emailTemplates;
        }

        /**
         * Prepares the template.
         *
         * @param template the template descriptor
         * @param dir      the directory to locate relative paths
         * @return the template entity
         */
        @Override
        protected Entity prepare(Template template, File dir) {
            Entity entity = super.prepare(template, dir);
            IMObjectBean bean = service.getBean(entity);
            Lookup type = lookups.getLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE, template.getArchetype());
            if (type == null) {
                throw new IllegalStateException("Template '" + template.getName()
                                                + "' references non-existent document template type: "
                                                + template.getArchetype());
            }
            Lookup existing = bean.getObject("type", Lookup.class);
            if (existing == null) {
                entity.addClassification(type);
            } else if (!existing.equals(type)) {
                entity.removeClassification(existing);
                entity.addClassification(type);
            }
            bean.setValue("reportType", template.getReportType());
            if (template.getOrientation() != null) {
                bean.setValue("orientation", template.getOrientation().value());
            }
            Template.EmailTemplate email = template.getEmailTemplate();
            if (email != null) {
                Entity emailTemplate = emailTemplates.get(email.getName());
                if (emailTemplate == null) {
                    throw new IllegalStateException("Template '" + template.getName()
                                                    + "' references non-existent email template: " + email.getName());
                }
                bean.setTarget("email", emailTemplate);
            }
            return entity;
        }
    }

    /**
     * Loads email templates.
     */
    private class EmailLoader extends Loader<EmailTemplate> {

        /**
         * Constructs a {@link EmailLoader}.
         *
         * @param useSystem if {@code true} use <em>entity.documentTemplateEmailSystem</em>,
         *                  otherwise use <em>entity.documentTemplateEmailUser</em>
         */
        public EmailLoader(boolean useSystem) {
            super(useSystem ? DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE : DocumentArchetypes.USER_EMAIL_TEMPLATE);
        }

        /**
         * Prepares the template.
         *
         * @param template the template descriptor
         * @param dir      the directory to locate relative paths
         * @return the template entity
         */
        @Override
        protected Entity prepare(EmailTemplate template, File dir) {
            Entity entity = super.prepare(template, dir);
            IMObjectBean bean = service.getBean(entity);
            bean.setValue("subject", template.getSubject());
            bean.setValue("subjectType", template.getSubjectType());
            bean.setValue("subjectSource", template.getSubjectSource());
            bean.setValue("contentType", "DOCUMENT");
            bean.setValue("contentSource", template.getContentSource());
            bean.setValue("defaultEmailAddress", template.getDefaultEmailAddress());
            return entity;
        }
    }
}
