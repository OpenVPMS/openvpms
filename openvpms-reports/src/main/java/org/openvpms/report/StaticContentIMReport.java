/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.apache.commons.io.IOUtils;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.document.Document;
import org.openvpms.report.i18n.ReportMessages;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * An {@link IMReport} that supports templates containing static content.
 * <p/>
 * This returns the template content unmerged.
 * <p/>
 * Note that a copy of the template content is returned to prevent the original being modified.
 *
 * @author Tim Anderson
 */
public class StaticContentIMReport<T> implements IMReport<T> {

    /**
     * The template document.
     */
    private final Document document;

    /**
     * The document rules.
     */
    private final DocumentRules rules;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The document converter.
     */
    private final DocumentConverter converter;

    /**
     * Constructs a {@link StaticContentIMReport}.
     *
     * @param document  the document
     * @param rules     the document rules
     * @param handlers  the document handlers
     * @param converter the document converter
     */
    public StaticContentIMReport(Document document, DocumentRules rules, DocumentHandlers handlers,
                                 DocumentConverter converter) {
        this.document = document;
        this.rules = rules;
        this.handlers = handlers;
        this.converter = converter;
    }

    /**
     * Generates a report for a collection of objects.
     * <p/>
     * The default mime type will be used to select the output format.
     *
     * @param objects the objects to report on
     * @return a document containing the report
     */
    @Override
    public Document generate(Iterable<T> objects) {
        return copy();
    }

    /**
     * Generates a report for a collection of objects.
     *
     * @param objects  the objects to report on
     * @param mimeType the output format of the report
     * @return a document containing the report
     */
    @Override
    public Document generate(Iterable<T> objects, String mimeType) {
        return generate(mimeType);
    }

    /**
     * Generates a report for a collection of objects.
     * <p/>
     * The default mime type will be used to select the output format.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @return a document containing the report
     */
    @Override
    public Document generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields) {
        return copy();
    }

    /**
     * Generates a report for a collection of objects.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param mimeType   the output format of the report
     * @return a document containing the report
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public Document generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                             String mimeType) {
        return generate(mimeType);
    }

    /**
     * Generates a report for a collection of objects to the specified stream.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param mimeType   the output format of the report
     * @param stream     the stream to write to
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public void generate(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                         String mimeType, OutputStream stream) {
        Document result = convert(mimeType);
        DocumentHandler handler = handlers.get(result);
        try (InputStream input = handler.getContent(result)) {
            IOUtils.copy(input, stream);
        } catch (IOException exception) {
            throw new ReportException(ReportMessages.failedToGenerateReport(getName(), exception.getMessage()),
                                      exception);
        }
    }

    /**
     * Prints a report directly to a printer.
     *
     * @param objects    the objects to report on
     * @param properties the print properties
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public void print(Iterable<T> objects, PrintProperties properties) {
        throw new UnsupportedOperationException();
    }

    /**
     * Prints a report directly to a printer.
     *
     * @param objects    the objects to report on
     * @param parameters a map of parameter names and their values, to pass to the report. May be {@code null}
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param properties the print properties
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public void print(Iterable<T> objects, Map<String, Object> parameters, Map<String, Object> fields,
                      PrintProperties properties) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns the report name.
     *
     * @return the report name.
     */
    @Override
    public String getName() {
        return document.getName();
    }

    /**
     * Returns the set of parameter types that may be supplied to the report.
     *
     * @return the parameter types
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public Set<ParameterType> getParameterTypes() {
        return Collections.emptySet();
    }

    /**
     * Determines if the report accepts the named parameter.
     *
     * @param name the parameter name
     * @return {@code true} if the report accepts the parameter, otherwise {@code false}
     */
    @Override
    public boolean hasParameter(String name) {
        return false;
    }

    /**
     * Returns the default mime type for report documents.
     *
     * @return the default mime type
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public String getDefaultMimeType() {
        return document.getMimeType();
    }

    /**
     * Returns the supported mime types for report documents.
     *
     * @return the supported mime types
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public String[] getMimeTypes() {
        return new String[]{document.getMimeType()};
    }

    /**
     * Generates a report.
     * <p/>
     * The default mime type will be used to select the output format.
     *
     * @param parameters a map of parameter names and their values, to pass to the report
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @return a document containing the report
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public Document generate(Map<String, Object> parameters, Map<String, Object> fields) {
        return copy();
    }

    /**
     * Generates a report.
     *
     * @param parameters a map of parameter names and their values, to pass to the report
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param mimeType   the output format of the report
     * @return a document containing the report
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public Document generate(Map<String, Object> parameters, Map<String, Object> fields, String mimeType) {
        return generate(mimeType);
    }

    /**
     * Determines if printing via the Java Print API is supported.
     *
     * @return {@code true} if printing is supported, otherwise {@code false}
     */
    @Override
    public boolean canPrint() {
        return false;
    }

    /**
     * Prints a report directly to a printer.
     *
     * @param parameters a map of parameter names and their values, to pass to the report
     * @param fields     a map of additional field names and their values, to pass to the report. May be {@code null}
     * @param properties the print properties
     * @throws ReportException               for any report error
     * @throws ArchetypeServiceException     for any archetype service error
     * @throws UnsupportedOperationException if this operation is not supported
     */
    @Override
    public void print(Map<String, Object> parameters, Map<String, Object> fields, PrintProperties properties) {
        throw new UnsupportedOperationException();
    }

    /**
     * Generates the document.
     *
     * @param mimeType the mime-type to convert to, if possible
     * @return the document
     */
    private Document generate(String mimeType) {
        Document result = convert(mimeType);
        if (result == document) {
            result = copy();
        }
        return result;
    }

    /**
     * Copies the document.
     *
     * @return a copy of the document
     */
    private Document copy() {
        return rules.copy(document);
    }

    /**
     * Converts the document to the specified mime type, if possible, else returns it unchanged.
     *
     * @param mimeType the mime type
     * @return a converted document, or the original if conversion isn't supported
     */
    private Document convert(String mimeType) {
        Document result = document;
        if (document.getMimeType() != null && !document.getMimeType().equalsIgnoreCase(mimeType)
            && converter.canConvert(document, mimeType)) {
            result = converter.convert(document, mimeType);
        }
        return result;
    }
}
