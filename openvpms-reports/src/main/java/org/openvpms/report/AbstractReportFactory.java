/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.jxpath.Functions;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.function.factory.ArchetypeFunctionsFactory;
import org.openvpms.archetype.rules.doc.BaseDocumentTemplate;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.ReadOnlyArchetypeService;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.report.i18n.ReportMessages;
import org.openvpms.report.jasper.TemplatedJasperIMObjectReport;
import org.openvpms.report.jasper.TemplatedJasperObjectSetReport;

/**
 * Abstract implementation of {@link ReportFactory}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractReportFactory implements ReportFactory {

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The JXPath extension function factory.
     */
    private final ArchetypeFunctionsFactory factory;

    /**
     * The document rules.
     */
    private final DocumentRules documentRules;

    /**
     * The document converter.
     */
    private final DocumentConverter documentConverter;

    /**
     * The settings.
     */
    private final Settings settings;

    /**
     * Constructs an {@link AbstractReportFactory}.
     *
     * @param service           the archetype service
     * @param lookups           the lookup service
     * @param handlers          the document handlers
     * @param factory           the factory for JXPath extension functions
     * @param documentRules     the document rules
     * @param documentConverter the document converter
     * @param settings          the settings
     */
    public AbstractReportFactory(IArchetypeService service, LookupService lookups, DocumentHandlers handlers,
                                 ArchetypeFunctionsFactory factory, DocumentRules documentRules,
                                 DocumentConverter documentConverter, Settings settings) {
        this.service = new ReadOnlyArchetypeService(service);
        this.lookups = lookups;
        this.handlers = handlers;
        this.factory = factory;
        this.documentRules = documentRules;
        this.documentConverter = documentConverter;
        this.settings = settings;
    }

    /**
     * Determines if a document template contains mergeable content.
     *
     * @param template the template
     * @return {@code true} if the template contains mergeable content, otherwise {@code false}
     */
    @Override
    public boolean hasMergeableContent(BaseDocumentTemplate template) {
        return hasMergeableContent(template.getFileName());
    }

    /**
     * Creates a new report.
     *
     * @param template the document template
     * @return a new report
     * @throws ReportException for any report error
     */
    @Override
    public Report createReport(DocumentTemplate template) {
        Document document = getDocument(template);
        String name = document.getName();
        Report report;
        if (DocFormats.hasExtension(document, DocFormats.JRXML_EXT)) {
            IArchetypeService serviceProxy = proxy(service);
            Functions functions = factory.create(serviceProxy, true);
            report = new TemplatedJasperIMObjectReport(name, document, serviceProxy, lookups, handlers, functions, settings);
        } else {
            throw new ReportException(ReportMessages.unsupportedTemplate(name));
        }
        return report;
    }

    /**
     * Determines if a template can be used to create a report via {@link #createIMObjectReport(BaseDocumentTemplate)}.
     *
     * @param template the template
     * @return {@code true} if the template can be used to create a report
     */
    @Override
    public boolean isIMObjectReport(BaseDocumentTemplate template) {
        return hasMergeableContent(template);
    }

    /**
     * Creates a new report for a collection of {@link IMObject}s.
     *
     * @param template the document template
     * @return a new report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public IMReport<IMObject> createIMObjectReport(BaseDocumentTemplate template) {
        return createIMObjectReport(getDocument(template), template);
    }

    /**
     * Determines if a template can be used to create a report via {@link #createObjectSetReport(BaseDocumentTemplate)}.
     *
     * @param template    the template
     * @param cardinality the no. of objects being reported on. OpenOffice/Word templates only support a single object
     * @return {@code true} if the template can be used to create a report
     */
    @Override
    public boolean isObjectSetReport(BaseDocumentTemplate template, int cardinality) {
        boolean result = false;
        String ext = FilenameUtils.getExtension(template.getFileName());
        if (ext != null) {
            if (isJRXML(ext) || (cardinality == 1 && isDocument(ext))) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Creates a new report for a collection of {@link ObjectSet}s.
     *
     * @param template the document template
     * @return a new report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public IMReport<ObjectSet> createObjectSetReport(BaseDocumentTemplate template) {
        return createObjectSetReport(getDocument(template), template);
    }

    /**
     * Returns a report to process static content.
     *
     * @param template the template
     * @return a new report
     */
    @Override
    public IMReport<IMObject> createStaticContentReport(DocumentTemplate template) {
        Document document = getDocument(template);
        return createStaticContentReport(document);
    }

    /**
     * Returns a report to process static content.
     *
     * @param document the document
     * @return a new report
     */
    @Override
    public IMReport<IMObject> createStaticContentReport(Document document) {
        return new StaticContentIMReport<>(document, documentRules, handlers, documentConverter);
    }

    /**
     * Creates a new report for a collection of {@link IMObject}s.
     *
     * @param document the template document
     * @param template the template
     * @return a new report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    protected IMReport<IMObject> createIMObjectReport(Document document, BaseDocumentTemplate template) {
        IMReport<IMObject> report;
        if (hasMergeableContent(document.getName())) {
            IArchetypeService serviceProxy = proxy(service);
            Functions functions = factory.create(serviceProxy, true);
            String ext = FilenameUtils.getExtension(document.getName());
            if (isJRXML(ext)) {
                report = new TemplatedJasperIMObjectReport(template.getName(), document, serviceProxy, lookups,
                                                           handlers, functions, settings);
            } else if (isDocument(ext)) {
                report = createDocumentReport(document, template, serviceProxy, functions);
            } else {
                throw new ReportException(ReportMessages.unsupportedTemplate(document.getName()));
            }
        } else {
            throw new ReportException(ReportMessages.unsupportedTemplate(document.getName()));
        }
        return report;
    }

    /**
     * Creates a new report for a collection of {@link ObjectSet}s.
     *
     * @param document the template document
     * @param template the template
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    protected IMReport<ObjectSet> createObjectSetReport(Document document, BaseDocumentTemplate template) {
        IMReport<ObjectSet> report;
        String ext = FilenameUtils.getExtension(template.getFileName());
        if (ext != null) {
            IArchetypeService serviceProxy = proxy(service);
            Functions functions = factory.create(serviceProxy, true);
            if (isJRXML(ext)) {
                report = new TemplatedJasperObjectSetReport(template.getName(), document, serviceProxy, lookups,
                                                            handlers, functions, settings);
            } else if (isODT(ext)) {
                report = createDocumentObjectSetReport(document, template, serviceProxy, functions);
            } else {
                throw new ReportException(ReportMessages.unsupportedTemplate(document.getName()));
            }
        } else {
            throw new ReportException(ReportMessages.unsupportedTemplate(document.getName()));
        }
        return report;
    }

    /**
     * Creates a report where the template is an OpenOffice or Word document.
     *
     * @param document  the template document
     * @param template  the template. May be {@code null}
     * @param service   the archetype service. This is a read-only proxy
     * @param functions the JXPath extension functions
     * @return a new report
     */
    protected abstract IMReport<IMObject> createDocumentReport(Document document, BaseDocumentTemplate template,
                                                               ArchetypeService service, Functions functions);

    /**
     * Creates a new report for a collection of {@link ObjectSet}s where the template is an OpenOffice or Word document.
     *
     * @param document  the template document
     * @param template  the template. May be {@code null}
     * @param service   the archetype service. This is a read-only proxy
     * @param functions the JXPath extension functions
     * @return a new report
     */
    protected abstract IMReport<ObjectSet> createDocumentObjectSetReport(Document document,
                                                                         BaseDocumentTemplate template,
                                                                         ArchetypeService service,
                                                                         Functions functions);

    /**
     * Creates a read-only proxy for the archetype service.
     *
     * @param service service
     * @return the proxied service
     */
    protected IArchetypeService proxy(IArchetypeService service) {
        return new ReadOnlyArchetypeService(service);
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return service;
    }

    /**
     * Returns the document handlers.
     *
     * @return the document handlers
     */
    protected DocumentHandlers getHandlers() {
        return handlers;
    }

    /**
     * Returns the lookup service.
     *
     * @return the lookup service
     */
    protected LookupService getLookups() {
        return lookups;
    }

    /**
     * Determines if an extension is a JasperReports .jrxml.
     *
     * @param ext the extension
     * @return {@code true} if the extension is a .jrxml
     */
    protected boolean isJRXML(String ext) {
        return ext.equalsIgnoreCase(DocFormats.JRXML_EXT);
    }

    /**
     * Determines if an extension is a document.
     *
     * @param ext the extension
     * @return {@code true} if the extension is an OpenOffice .odt or Word .doc
     */
    protected boolean isDocument(String ext) {
        return isODT(ext) || isDOC(ext);
    }

    /**
     * Determines if an extension is an OpenOffice .odt.
     *
     * @param ext the extension
     * @return {@code true} if the extension is a .odt
     */
    protected boolean isODT(String ext) {
        return StringUtils.equalsIgnoreCase(ext, DocFormats.ODT_EXT);
    }

    /**
     * Determines if an extension is a Microsoft Word .doc.
     *
     * @param ext the extension
     * @return {@code true} if the extension is a .doc
     */
    protected boolean isDOC(String ext) {
        return StringUtils.equalsIgnoreCase(ext, DocFormats.DOC_EXT);
    }

    /**
     * Determines if a file has mergeable content.
     *
     * @param fileName the file name
     * @return {@code true} if the file contains mergeable content, otherwise {@code false}
     */
    protected boolean hasMergeableContent(String fileName) {
        String ext = FilenameUtils.getExtension(fileName);
        return ext != null && (isDocument(ext) || isJRXML(ext));
    }

    /**
     * Returns the document associated with a template.
     *
     * @param template the template
     * @return the document
     * @throws ReportException if the document cannot be retrieved
     */
    private Document getDocument(BaseDocumentTemplate template) {
        Document document = template.getDocument();
        if (document == null) {
            throw new ReportException(ReportMessages.templateHasNoDocument(template.getName()));
        }
        return document;
    }
}