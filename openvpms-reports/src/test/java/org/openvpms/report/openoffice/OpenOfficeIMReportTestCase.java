/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.apache.commons.jxpath.Functions;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.report.DocFormats;
import org.openvpms.report.IMReport;
import org.openvpms.report.ParameterType;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * {@link OpenOfficeIMReport} test case.
 *
 * @author Tim Anderson
 */
public class OpenOfficeIMReportTestCase extends AbstractOpenOfficeDocumentTest {

    /**
     * JXPath functions.
     */
    @Autowired
    private Functions functions;

    /**
     * Tests reporting.
     */
    @Test
    public void testReport() {
        Document doc = getDocument("src/test/reports/act.customerEstimation.odt", DocFormats.ODT_TYPE);

        IMReport<IMObject> report = new OpenOfficeIMReport<>(doc, getArchetypeService(), getLookupService(),
                                                             getHandlers(), functions, officeService);
        Map<String, Object> fields = new HashMap<>();
        Party practice = create(PracticeArchetypes.PRACTICE, Party.class);
        practice.setName("Vets R Us");
        fields.put("OpenVPMS.practice", practice);

        Party party = createCustomer();
        IMObjectBean act = createBean(EstimateArchetypes.ESTIMATE);
        Date startTime = TestHelper.getDate("2006-08-04");
        act.setValue("startTime", startTime);
        act.setValue("lowTotal", new BigDecimal("100"));
        act.setTarget("customer", party);

        List<IMObject> objects = Collections.singletonList(act.getObject());
        Document result = report.generate(objects, null, fields, DocFormats.ODT_TYPE);
        Map<String, String> userFields = getUserFields(result);
        String expectedStartTime = DateFormat.getDateInstance(DateFormat.MEDIUM).format(startTime);
        assertEquals(expectedStartTime, userFields.get("startTime"));
        assertEquals("$100.00", userFields.get("lowTotal"));
        assertEquals("J", userFields.get("firstName"));
        assertEquals("Zoo", userFields.get("lastName"));
        assertEquals("2.00", userFields.get("expression"));
        assertEquals("1234 Foo St\nMelbourne Victoria 3001",
                     userFields.get("address"));
        assertEquals("Vets R Us", userFields.get("practiceName"));
        assertEquals("Invalid property name: invalid", userFields.get("invalid"));
    }

    /**
     * Verifies that input fields are returned as parameters, and that
     * by specifying it as a parameter updates the corresponding input field.
     */
    @Test
    public void testParameters() {
        Document doc = getDocument("src/test/reports/act.customerEstimation.odt", DocFormats.ODT_TYPE);

        IMReport<IMObject> report = new OpenOfficeIMReport<>(doc, getArchetypeService(), getLookupService(),
                                                             getHandlers(), functions, officeService);

        Set<ParameterType> parameterTypes = report.getParameterTypes();
        Map<String, ParameterType> types = new HashMap<>();
        for (ParameterType type : parameterTypes) {
            types.put(type.getName(), type);
        }
        assertTrue(types.containsKey("inputField1"));
        assertTrue(types.containsKey("IsEmail"));
        assertTrue(report.hasParameter("inputField1"));
        assertTrue(report.hasParameter("IsEmail"));

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("inputField1", "the input value");
        parameters.put("IsEmail", "true");

        Party party = createCustomer();
        IMObjectBean act = createBean(EstimateArchetypes.ESTIMATE);
        act.setTarget("customer", party);

        List<IMObject> objects = Collections.singletonList(act.getObject());
        Document result = report.generate(objects, parameters, null, DocFormats.ODT_TYPE);

        Map<String, String> inputFields = getInputFields(result);
        assertEquals("the input value", inputFields.get("inputField1"));

        Map<String, String> userFields = getUserFields(result);
        assertEquals("true", userFields.get("IsEmail"));
    }

}
