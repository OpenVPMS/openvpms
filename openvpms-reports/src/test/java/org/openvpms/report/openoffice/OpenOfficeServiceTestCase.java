/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.junit.After;
import org.junit.Test;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.user.User;
import org.openvpms.report.AbstractReportTest;
import org.openvpms.report.ReportException;
import org.openvpms.report.i18n.ReportMessages;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link OpenOfficeService}.
 *
 * @author Tim Anderson
 */
public class OpenOfficeServiceTestCase extends AbstractReportTest {

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The OpenOffice service.
     */
    private OpenOfficeService officeService;

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        if (officeService != null) {
            officeService.destroy();
        }
    }

    /**
     * Verifies an {@link OpenOfficeConnectionException} is thrown if OpenOffice cannot be started due to an
     * invalid home directory.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testInvalidOpenOfficeHome() throws IOException {
        Path path = Files.createTempDirectory("openoffice");
        try {
            initService(path.toString(), null);
            officeService.restart();
            fail("Expected service creation to fail");
        } catch (OpenOfficeConnectionException expected) {
            assertEquals("REPORT-0080: Failed to connect to OpenOffice: Invalid officeHome: it doesn't contain " +
                         "soffice.bin: " + path, expected.getMessage());
        } catch (Exception exception) {
            fail("Expected OpenOfficeConnectionException but got: " + exception.getMessage());
        } finally {
            Files.delete(path);
        }
    }

    /**
     * Verifies that if a task throws an {@link OpenVPMSException}, it is propagated to the caller.
     */
    @Test
    public void testExceptionPropagation() {
        initService(null, "8102");
        try {
            officeService.run(context -> {
                throw new ReportException(ReportMessages.failedToGenerateReport("foo", "bar"));
            });
            fail("Expected ReportException");
        } catch (ReportException exception) {
            assertEquals("REPORT-0001: Failed to generate report 'foo': bar", exception.getMessage());
        } catch (Throwable exception) {
            fail("Expected ReportException but got: " + exception.getMessage());
        }
    }

    /**
     * Verifies that OpenOffice can be configured to run on multiple ports.
     *
     * @throws InterruptedException if interrupted waiting for tasks to complete
     */
    @Test
    public void testMultiplePorts() throws InterruptedException {
        initService(null, "8102, 8103");

        AtomicInteger count = new AtomicInteger();

        // create a thread pool to submit tasks concurrently
        ExecutorService threadPool = Executors.newFixedThreadPool(5);

        int tasks = 10;
        for (int i = 0; i < tasks; ++i) {
            threadPool.submit(() -> officeService.run(context -> {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ignore) {
                    // no-op
                }
                count.incrementAndGet();
            }));
        }
        threadPool.shutdown();
        assertTrue(threadPool.awaitTermination(30, TimeUnit.SECONDS));

        // verify all tasks ran
        assertEquals(10, count.get());

        // verify the ports used
        List<Integer> ports = officeService.getConfig().getPorts();
        assertEquals(2, ports.size());
        assertTrue(ports.contains(8102));
        assertTrue(ports.contains(8103));
    }

    /**
     * Verifies an {@link OpenOfficeException} with {@link ReportMessages#openOfficeTaskTimedOut()}  message is
     * thrown if a task times out.
     */
    @Test
    public void testTimeout() {
        initService(null, "8102");
        officeService.setTaskExecutionTimeout(100L); // 100ms
        try {
            officeService.run(context -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignore) {
                    // do nothing
                }
            });
            fail("Expected OpenOfficeException");
        } catch (OpenOfficeException expected) {
            assertEquals("REPORT-0084: This OpenOffice operation timed out.", expected.getMessage());
        }
    }

    /**
     * Verifies that tasks run by {@link OpenOfficeService#run(Consumer)} inherits the security context of the parent
     * thread.
     */
    @Test
    public void testSecurityContextInherited() {
        initService(null, "8102");
        AuthenticationContextImpl context = new AuthenticationContextImpl();
        Thread parent = Thread.currentThread();
        User user = userFactory.createUser();
        context.setUser(user);

        officeService.run(c -> {
            assertNotEquals(parent, Thread.currentThread());
            assertEquals(user, new AuthenticationContextImpl().getUser());
        });
        context.setUser(null);
    }

    /**
     * Initialises the {@link OpenOfficeService}.
     *
     * @param home  the OpenOffice home directory. May be {@code null}
     * @param ports the port string. May be {@code null}
     */
    private void initService(String home, String ports) {
        Settings settings = mock(Settings.class);
        when(settings.getString(eq(SettingsArchetypes.OPEN_OFFICE), eq(OpenOfficeConfig.PATH), any())).thenReturn(home);
        when(settings.getString(eq(SettingsArchetypes.OPEN_OFFICE), eq(OpenOfficeConfig.PORTS), any()))
                .thenReturn(ports);
        when(settings.getInt(eq(SettingsArchetypes.OPEN_OFFICE), eq(OpenOfficeConfig.MAX_TASKS), anyInt()))
                .thenReturn(10);
        officeService = new OpenOfficeService(settings);
    }
}
