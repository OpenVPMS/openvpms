/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.jodconverter.core.office.OfficeException;
import org.jodconverter.core.office.OfficeManager;
import org.jodconverter.core.task.OfficeTask;
import org.jodconverter.local.office.LocalOfficeContext;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.report.ReportException;
import org.openvpms.report.i18n.ReportMessages;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests the {@link OfficeTaskRunner}.
 *
 * @author Tim Anderson
 */
public class OfficeTaskRunnerTestCase {

    /**
     * Verifies that {@link OpenVPMSException} is propagated rather than being wrapped in an {@link OfficeException}.
     */
    @Test
    public void testExceptionPropagation() {
        TestOfficeManager manager = new TestOfficeManager();
        try {
            OfficeTaskRunner.run(() -> manager, context -> {
                throw new ReportException(ReportMessages.failedToGenerateReport("foo", "bar"));
            });
            fail("Expected ReportException");
        } catch (ReportException expected) {
            assertEquals("REPORT-0001: Failed to generate report 'foo': bar", expected.getMessage());
        }
    }

    /**
     * Verifies that {@link OfficeException} with an {@link InterruptedException} as the cause are rethrown
     * as an {@link OpenOfficeException} with an {@link ReportMessages#openOfficeTaskInterrupted()} message.
     */
    @Test
    public void testInterrupted() {
        TestOfficeManager manager = new TestOfficeManager();
        try {
            OfficeTaskRunner.run(() -> manager, context -> {
                throw new OfficeException("Interrupted", new InterruptedException());
            });
            fail("Expected OpenOfficeException");
        } catch (OpenOfficeException expected) {
            assertEquals("REPORT-0082: This OpenOffice operation was interrupted.", expected.getMessage());
        }
    }

    /**
     * Verifies that {@link OfficeException} with an {@link CancellationException} as the cause are rethrown
     * as an {@link OpenOfficeException} with {@link ReportMessages#openOfficeTaskCancelled()} message.
     */
    @Test
    public void testCancelled() {
        TestOfficeManager manager = new TestOfficeManager();
        try {
            OfficeTaskRunner.run(() -> manager, context -> {
                throw new OfficeException("Cancelled", new CancellationException());
            });
            fail("Expected OpenOfficeException");
        } catch (OpenOfficeException expected) {
            assertEquals("REPORT-0083: This OpenOffice operation was cancelled.", expected.getMessage());
        }
    }

    /**
     * Verifies that {@link OfficeException} with an {@link CancellationException} as the cause are rethrown
     * as an {@link OpenOfficeException} with {@link ReportMessages#openOfficeTaskTimedOut()} message.
     */
    @Test
    public void testTimeout() {
        TestOfficeManager manager = new TestOfficeManager();
        try {
            OfficeTaskRunner.run(() -> manager, context -> {
                throw new OfficeException("Timeout", new TimeoutException());
            });
            fail("Expected OpenOfficeException");
        } catch (OpenOfficeException expected) {
            assertEquals("REPORT-0084: This OpenOffice operation timed out.", expected.getMessage());
        }
    }

    /**
     * Verifies that {@link OfficeException} is rethrown as {@link OpenOfficeException} with a
     * {@link ReportMessages#openOfficeError()} message when the underlying cause is not recognised.
     */
    @Test
    public void testOfficeException() {
        TestOfficeManager manager = new TestOfficeManager();
        try {
            OfficeTaskRunner.run(() -> manager, context -> {
                throw new OfficeException("Random error");
            });
            fail("Expected OpenOfficeException");
        } catch (OpenOfficeException expected) {
            assertEquals("REPORT-0081: An OpenOffice error has been encountered.", expected.getMessage());
        }
    }

    /**
     * Simulate an {@link OfficeManager}.
     */
    private static class TestOfficeManager implements OfficeManager {

        /**
         * Executes the specified task and blocks until the task terminates.
         *
         * @param task The task to execute.
         * @throws OfficeException If an error occurs.
         */
        @Override
        public void execute(@NonNull OfficeTask task) throws OfficeException {
            try {
                task.execute(Mockito.mock(LocalOfficeContext.class));
            } catch (OfficeException exception) {
                throw exception;
            } catch (Throwable exception) {
                throw new OfficeException("Rethrown error", exception);
            }
        }

        /**
         * Gets whether the manager is running.
         *
         * @return {@code true} if the manager is running, {@code false} otherwise.
         */
        @Override
        public boolean isRunning() {
            return false;
        }

        /**
         * Starts the manager.
         */
        @Override
        public void start() {
        }

        /**
         * Stops the manager.
         */
        @Override
        public void stop() {
        }
    }
}
