/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.junit.Test;
import org.openvpms.archetype.function.factory.ArchetypeFunctionsFactory;
import org.openvpms.archetype.function.factory.DefaultArchetypeFunctionsFactory;
import org.openvpms.archetype.rules.contact.BasicAddressFormatter;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.report.DocFormats;
import org.openvpms.report.IMReport;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


/**
 * Load tests the OpenOffice interface.
 *
 * @author Tim Anderson
 */
public class OpenOfficeLoadTestCase extends AbstractOpenOfficeDocumentTest {

    /**
     * Load tests the OpenOffice interface.
     *
     * @throws Exception for any error
     */
    @Test
    public void test() throws Exception {
        createCustomer(); // hack to force init of jxpath function cache to
        // avoid ConcurrentModificationException

        Thread[] threads = new Thread[10];
        Reporter[] reporters = new Reporter[threads.length];
        int count = 1000;
        for (int i = 0; i < threads.length; ++i) {
            reporters[i] = new Reporter(count);
            threads[i] = new Thread(reporters[i]);
        }
        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
        for (Reporter reporter : reporters) {
            assertFalse(reporter.failed());
        }
    }

    private class Reporter implements Runnable {

        boolean failed = false;

        private final int count;

        public Reporter(int count) {
            this.count = count;
        }

        public void run() {
            try {
                for (int i = 0; i < count; ++i) {
                    System.out.println(Thread.currentThread().getName()
                                       + " iteration " + (i + 1));
                    test();
                }
            } catch (Throwable exception) {
                exception.printStackTrace();
                synchronized (this) {
                    failed = true;
                }
            }
        }

        public synchronized boolean failed() {
            return failed;
        }

        @Test
        public void test() {
            Document doc = getDocument("src/test/reports/act.customerEstimation.odt", DocFormats.ODT_TYPE);

            BasicAddressFormatter formatter = new BasicAddressFormatter(getArchetypeService(), getLookupService());
            ArchetypeFunctionsFactory factory = new DefaultArchetypeFunctionsFactory(
                    getArchetypeService(), getLookupService(), null, null, formatter, null, null, null, null);
            IMReport<IMObject> report = new OpenOfficeIMReport<>(doc, getArchetypeService(), getLookupService(),
                                                                 getHandlers(), factory.create(), officeService);

            Party party = createCustomer();
            IMObjectBean act = createBean(EstimateArchetypes.ESTIMATE);
            act.setValue("startTime", TestHelper.getDate("2006-08-04"));
            act.setValue("lowTotal", new BigDecimal("100"));
            act.setTarget("customer", party);

            List<IMObject> objects = Collections.singletonList(act.getObject());
            Document result = report.generate(objects, DocFormats.ODT_TYPE);
            Map<String, String> fields = getUserFields(result);
            assertEquals("04/08/2006", fields.get("startTime"));  // @todo localise
            assertEquals("$100.00", fields.get("lowTotal"));
            assertEquals("J", fields.get("firstName"));
            assertEquals("Zoo", fields.get("lastName"));
            assertEquals("2.00", fields.get("expression"));
            assertEquals("1234 Foo St\nMelbourne Victoria 3001", fields.get("address"));
            assertEquals("Invalid property name: invalid", fields.get("invalid"));

        }
    }

}
