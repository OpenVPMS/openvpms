/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.jodconverter.local.office.LocalOfficeContext;
import org.openvpms.component.model.document.Document;
import org.openvpms.report.AbstractReportTest;
import org.openvpms.report.ParameterType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Base class for {@link OpenOfficeDocument} tests.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public abstract class AbstractOpenOfficeDocumentTest extends AbstractReportTest {

    /**
     * The OpenOffice service.
     */
    @Autowired
    protected OpenOfficeService officeService;

    /**
     * Returns the user fields in a document.
     *
     * @param document an OpenOffice document
     * @return a map of user field names and their corresponding values
     */
    protected Map<String, String> getUserFields(Document document) {
        return officeService.call(context -> {
            Map<String, String> fields = new HashMap<>();
            try (OpenOfficeDocument doc = getDocument(document, context)) {
                for (String name : doc.getUserFieldNames()) {
                    fields.put(name, doc.getUserField(name));
                }
            }
            return fields;
        });
    }

    /**
     * Returns the input fields in a document.
     *
     * @param document an OpenOffice document
     * @return a map of user field names and their corresponding values
     */
    protected Map<String, String> getInputFields(Document document) {
        return officeService.call(context -> {
            Map<String, String> fields = new HashMap<>();
            try (OpenOfficeDocument doc = getDocument(document, context)) {
                for (String name : doc.getInputFields().keySet()) {
                    fields.put(name, doc.getInputField(name));
                }
            }
            return fields;
        });
    }

    /**
     * Creates a new {@link OpenOfficeDocument} wrapping a {@link Document}.
     *
     * @param document the document
     * @param context  the OpenOffice context
     * @return a new OpenOffice document
     */
    protected OpenOfficeDocument getDocument(Document document, LocalOfficeContext context) {
        return new OpenOfficeDocument(document, context, getHandlers());
    }

    /**
     * Verifies that a user field can be updated.
     *
     * @param doc   the document
     * @param name  the user field name
     * @param value the value
     */
    protected void checkUpdateUserField(OpenOfficeDocument doc, String name, String value) {
        doc.setUserField(name, value);
        assertEquals(value, doc.getUserField(name));
    }

    /**
     * Verifies that an input field can be updated.
     *
     * @param doc   the document
     * @param name  the input field name
     * @param value the value
     */
    protected void checkUpdateInputField(OpenOfficeDocument doc, String name, String value) {
        doc.setInputField(name, value);
        assertEquals(value, doc.getInputField(name));
    }

    /**
     * Verifies that there is a parameter with the expected description.
     *
     * @param description  the expected description
     * @param defaultValue the expected default value
     * @param input        the input parameters
     * @return the parameter name
     */
    protected String checkParameter(String description, String defaultValue,
                                    Map<String, ParameterType> input) {
        for (ParameterType param : input.values()) {
            if (description.equals(param.getDescription())) {
                assertEquals(String.class, param.getType());
                assertEquals(defaultValue, param.getDefaultValue());
                assertEquals(String.class, param.getType());
                return param.getName();
            }
        }
        fail("Parameter not found with description=" + description);
        return null;
    }

}
