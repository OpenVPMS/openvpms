/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.i18n;

import org.junit.Test;
import org.openvpms.component.i18n.Message;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link ReportMessages} class.
 *
 * @author Tim Anderson
 */
public class ReportMessagesTestCase {

    /**
     * Verifies there is a test case for each message.
     */
    @Test
    public void testCoverage() {
        Set<String> messages = new HashSet<>();
        Set<String> tests = new HashSet<>();
        for (Method method : ReportMessages.class.getMethods()) {
            if (method.getReturnType() == Message.class) {
                messages.add(method.getName());
            }
        }
        for (Method method : getClass().getMethods()) {
            String name = method.getName();
            if (name.startsWith("test") && !name.equals("testCoverage")) {
                tests.add(name);
            }
        }
        for (String message : messages) {
            String test = "test" + message.substring(0, 1).toUpperCase() + message.substring(1);
            assertTrue("No test for: " + message, tests.contains(test));
        }
        assertEquals(messages.size(), tests.size());
    }

    /**
     * Tests the {@link ReportMessages#failedToGenerateReport(String, String)} method.
     */
    @Test
    public void testFailedToGenerateReport() {
        checkMessage("Failed to generate report 'foo': bar", 1, ReportMessages.failedToGenerateReport("foo", "bar"));
    }

    /**
     * Tests the {@link ReportMessages#failedToPrintReport(String, String, String)} method.
     */
    @Test
    public void testFailedToPrintReport() {
        checkMessage("Failed to print report 'foo' on printer 'bar': gum", 2,
                     ReportMessages.failedToPrintReport("foo", "bar", "gum"));
    }

    /**
     * Tests the {@link ReportMessages#cannotConvertJasperReport(String, String)} method.
     */
    @Test
    public void testCannotConvertJasperReport() {
        checkMessage("Cannot convert 'foo' to mime type 'bar'", 10,
                     ReportMessages.cannotConvertJasperReport("foo", "bar"));
    }

    /**
     * Tests the {@link ReportMessages#failedToGetParameters(String, String)} method.
     */
    @Test
    public void testFailedToGetParameters() {
        checkMessage("Failed to get report parameters for 'foo': bar", 11,
                     ReportMessages.failedToGetParameters("foo", "bar"));
    }

    /**
     * Tests the {@link ReportMessages#noPagesToPrint(String)} method.
     */
    @Test
    public void testNoPagesToPrint() {
        checkMessage("Report 'foo' has no pages to print", 12, ReportMessages.noPagesToPrint("foo"));
    }

    /**
     * Tests the {@link ReportMessages#maxPagesExceeded(String, int)} method.
     */
    @Test
    public void testMaxPagesExceeded() {
        checkMessage("Report 'foo' has been terminated as it exceeded the maximum limit of 10 pages", 13,
                     ReportMessages.maxPagesExceeded("foo", 10));
    }

    /**
     * Tests the {@link ReportMessages#reportResourceNotFound(String, String)} method.
     */
    @Test
    public void testReportResourceNotFound() {
        checkMessage("Report 'Foo' uses a resource that cannot be found: bar.png", 14,
                     ReportMessages.reportResourceNotFound("Foo", "bar.png"));
    }

    /**
     * Tests the {@link ReportMessages#printerNotFound(String)} method.
     */
    @Test
    public void testPrinterNotFound() {
        checkMessage("Printer not found: Foo", 15, ReportMessages.printerNotFound("Foo"));
    }

    /**
     * Tests the {@link ReportMessages#failedToCreateReport(String, String)} method.
     */
    @Test
    public void testFailedToCreateReport() {
        checkMessage("Failed to create report 'foo': bar", 20, ReportMessages.failedToCreateReport("foo", "bar"));
    }

    /**
     * Tests the {@link ReportMessages#failedToFindSubReport(String, String)} method.
     */
    @Test
    public void testFailedToFindSubReport() {
        checkMessage("There is no sub-report named 'foo'.\nThis is needed by the report 'bar'.", 21,
                     ReportMessages.failedToFindSubReport("foo", "bar"));
    }

    /**
     * Tests the {@link ReportMessages#noTemplateForType(String)} method.
     */
    @Test
    public void testNoTemplateForType() {
        checkMessage("There is no document template available for report type: foo", 30,
                     ReportMessages.noTemplateForType("foo"));
    }

    /**
     * Tests the {@link ReportMessages#unsupportedTemplate(String)} method.
     */
    @Test
    public void testUnsupportedTemplate() {
        checkMessage("Unsupported document template: foo", 31, ReportMessages.unsupportedTemplate("foo"));
    }

    /**
     * Tests the {@link ReportMessages#templateHasNoDocument(String)} method.
     */
    @Test
    public void testTemplateHasNoDocument() {
        checkMessage("Template has no document: foo", 32, ReportMessages.templateHasNoDocument("foo"));
    }

    /**
     * Tests the {@link ReportMessages#failedToReadJasperReport(String)} method.
     */
    @Test
    public void testFailedToReadJasperReport() {
        checkMessage("Failed to read 'foo'. Please ensure it is saved to be compatible with JasperReports 6.16.0", 40,
                     ReportMessages.failedToReadJasperReport("foo"));
    }

    /**
     * Tests the {@link ReportMessages#noExpressionEvaluatorForType(Class)} method.
     */
    @Test
    public void testNoExpressionEvaluatorForType() {
        checkMessage("No ExpressionEvaluator for type: java.lang.Object", 50,
                     ReportMessages.noExpressionEvaluatorForType(Object.class));
    }

    /**
     * Tests the {@link ReportMessages#failedToLoadOpenOfficeDocument(String)} method.
     */
    @Test
    public void testFailedToLoadOpenOfficeDocument() {
        checkMessage("Failed to load 'foo.doc' into OpenOffice", 60,
                     ReportMessages.failedToLoadOpenOfficeDocument("foo.doc"));
    }

    /**
     * Tests the {@link ReportMessages#failedToGetOpenOfficeUserFields(String)} method.
     */
    @Test
    public void testFailedToGetOpenOfficeUserFields() {
        checkMessage("Failed to get user fields from 'foo.doc'", 61,
                     ReportMessages.failedToGetOpenOfficeUserFields("foo.doc"));
    }

    /**
     * Tests the {@link ReportMessages#failedToGetOpenOfficeField(String, String)} method.
     */
    @Test
    public void testFailedToGetOpenOfficeField() {
        checkMessage("Failed to get field 'foo' from 'bar.doc'", 62,
                     ReportMessages.failedToGetOpenOfficeField("foo", "bar.doc"));
    }

    /**
     * Tests the {@link ReportMessages#failedToGetOpenOfficeInputFields(String)} method.
     */
    @Test
    public void testFailedToGetOpenOfficeInputFields() {
        checkMessage("Failed to get input fields from 'foo.doc'", 63,
                     ReportMessages.failedToGetOpenOfficeInputFields("foo.doc"));
    }

    /**
     * Tests the {@link ReportMessages#failedToSetOpenOfficeField(String, String)} method.
     */
    @Test
    public void testFailedToSetOpenOfficeField() {
        checkMessage("Failed to update field 'foo' in 'bar.doc'", 64,
                     ReportMessages.failedToSetOpenOfficeField("foo", "bar.doc"));
    }

    /**
     * Tests the {@link ReportMessages#failedToPrintOpenOfficeDocument(String)} method.
     */
    @Test
    public void testFailedToPrintOpenOfficeDocument() {
        checkMessage("Failed to print 'foo.doc'", 65, ReportMessages.failedToPrintOpenOfficeDocument("foo.doc"));
    }

    /**
     * Tests the {@link ReportMessages#failedToExportOpenOfficeDocument(String)} method.
     */
    @Test
    public void testFailedToExportOpenOfficeDocument() {
        checkMessage("Failed to export 'foo'", 66, ReportMessages.failedToExportOpenOfficeDocument("foo"));
    }

    /**
     * Tests the {@link ReportMessages#unsupportedConversion(String, String, String)} method.
     */
    @Test
    public void testUnsupportedConversion() {
        checkMessage("Cannot convert 'foo.html' from 'text/html' to 'application/pdf'", 70,
                     ReportMessages.unsupportedConversion("foo.html", "text/html", "application/pdf"));
    }

    /**
     * Tests the {@link ReportMessages#conversionFailed(String, String)} method.
     */
    @Test
    public void testConversionFailed() {
        checkMessage("Failed to convert 'foo' to type 'application/pdf'", 71,
                     ReportMessages.conversionFailed("foo", "application/pdf"));
    }

    /**
     * Tests the {@link ReportMessages#failedToConnectToOpenOffice(String)} method.
     */
    @Test
    public void testFailedToConnectToOpenOffice() {
        checkMessage("Failed to connect to OpenOffice: foo", 80, ReportMessages.failedToConnectToOpenOffice("foo"));
    }

    /**
     * Tests the {@link ReportMessages#openOfficeError()} method.
     */
    @Test
    public void testOpenOfficeError() {
        checkMessage("An OpenOffice error has been encountered.", 81, ReportMessages.openOfficeError());
    }

    /**
     * Tests the {@link ReportMessages#openOfficeTaskInterrupted()} method.
     */
    @Test
    public void testOpenOfficeTaskInterrupted() {
        checkMessage("This OpenOffice operation was interrupted.", 82, ReportMessages.openOfficeTaskInterrupted());
    }

    /**
     * Tests the {@link ReportMessages#openOfficeTaskCancelled()} method.
     */
    @Test
    public void testOpenOfficeTaskCancelled() {
        checkMessage("This OpenOffice operation was cancelled.", 83, ReportMessages.openOfficeTaskCancelled());
    }

    /**
     * Tests the {@link ReportMessages#openOfficeTaskTimedOut()} method.
     */
    @Test
    public void testOpenOfficeTaskTimedOut() {
        checkMessage("This OpenOffice operation timed out.", 84, ReportMessages.openOfficeTaskTimedOut());
    }

    /**
     * Tests the {@link ReportMessages#noDocument(String)} method.
     */
    @Test
    public void testNoDocument() {
        checkMessage("foo has no document to print.", 100, ReportMessages.noDocument("foo"));
    }

    /**
     * Verifies a message matches that expected.
     *
     * @param expectedMessage the expected message
     * @param expectedCode    the expected code
     * @param actual          the actual message
     */
    private void checkMessage(String expectedMessage, int expectedCode, Message actual) {
        assertEquals(expectedMessage, actual.getMessage());
        assertEquals(expectedCode, actual.getCode());
    }
}
