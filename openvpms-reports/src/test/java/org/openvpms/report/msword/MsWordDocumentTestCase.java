/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.msword;

import org.jodconverter.local.office.LocalOfficeContext;
import org.junit.Test;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.report.DocFormats;
import org.openvpms.report.ParameterType;
import org.openvpms.report.openoffice.AbstractOpenOfficeDocumentTest;
import org.openvpms.report.openoffice.OpenOfficeDocument;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link MsWordDocument} class.
 *
 * @author Tim Anderson
 */
public class MsWordDocumentTestCase extends AbstractOpenOfficeDocumentTest {

    /**
     * Tests the {@link MsWordDocument#getUserFieldNames()}, {@link MsWordDocument#getUserField(String)}
     * and {@link MsWordDocument#setUserField(String, String)} methods.
     */
    @Test
    public void testUserFields() {
        officeService.run(context -> {
            OpenOfficeDocument doc = getDocument(context);
            Set<String> fields = new HashSet<>();
            for (String name : doc.getUserFieldNames()) {
                fields.add(doc.getUserField(name));
            }
            String firstName = "customer.entity.firstName";
            String lastName = "customer.entity.lastName";
            String lowTotal = "lowTotal";
            String startTime = "startTime";
            String sum = "[1 + 1]";
            String billing = "[party:getBillingAddress(openvpms:get(., 'customer.entity'))]";
            String invalid = "invalid";
            assertTrue(fields.contains(firstName));
            assertTrue(fields.contains(lastName));
            assertTrue(fields.contains(lowTotal));
            assertTrue(fields.contains(startTime));
            assertTrue(fields.contains(sum));
            assertTrue(fields.contains(billing));
            assertTrue(fields.contains(invalid));

            checkUpdateUserField(doc, firstName, "Foo");
            checkUpdateUserField(doc, lastName, "Bar");
            checkUpdateUserField(doc, lowTotal, "1.0");
            checkUpdateUserField(doc, startTime, "1/1/2008");
            checkUpdateUserField(doc, sum, "2");
            checkUpdateUserField(doc, billing, "1000 Settlement Road Cowes");
            checkUpdateUserField(doc, invalid, "Still invalid");
        });
    }

    /**
     * Tests the {@link MsWordDocument#getInputFields()}, {@link MsWordDocument#getInputField(String)} and
     * {@link MsWordDocument#setInputField(String, String)} methods.
     */
    @Test
    public void testInputFields() {
        officeService.run(context -> {
            OpenOfficeDocument doc = getDocument(context);
            Map<String, ParameterType> input = doc.getInputFields();
            assertEquals(3, input.size());
            String input1 = checkParameter("Enter Field 1", null, input);
            String input2 = checkParameter("Enter Field 2", null, input);
            String input3 = checkParameter("Enter Field 3", null, input);

            checkUpdateInputField(doc, input1, "input1 new value");
            checkUpdateInputField(doc, input2, "input2 new value");
            checkUpdateInputField(doc, input3, "input3 new value");
        });
    }

    /**
     * Loads the document to test against.
     *
     * @param context the OpenOffice context
     * @return the document
     */
    private OpenOfficeDocument getDocument(LocalOfficeContext context) {
        Document document = getDocument("src/test/reports/act.customerEstimation.doc", DocFormats.DOC_TYPE);
        return new MsWordDocument(document, context, getHandlers());
    }

}
