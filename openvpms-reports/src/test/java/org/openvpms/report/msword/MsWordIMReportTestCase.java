/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.msword;

import org.jodconverter.local.office.LocalOfficeContext;
import org.junit.Test;
import org.openvpms.archetype.function.factory.DefaultArchetypeFunctionsFactory;
import org.openvpms.archetype.rules.contact.BasicAddressFormatter;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.jxpath.FunctionsFactory;
import org.openvpms.report.DocFormats;
import org.openvpms.report.IMReport;
import org.openvpms.report.ParameterType;
import org.openvpms.report.openoffice.AbstractOpenOfficeDocumentTest;
import org.openvpms.report.openoffice.OpenOfficeDocument;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * {@link MsWordIMReport} test case.
 *
 * @author Tim Anderson
 */
public class MsWordIMReportTestCase extends AbstractOpenOfficeDocumentTest {

    /**
     * Tests reporting.
     */
    @Test
    public void testReport() {
        Document doc = getDocument("src/test/reports/act.customerEstimation.doc", DocFormats.DOC_TYPE);

        IArchetypeService service = getArchetypeService();
        LookupService lookups = getLookupService();
        BasicAddressFormatter formatter = new BasicAddressFormatter(service, lookups);
        FunctionsFactory factory = new DefaultArchetypeFunctionsFactory(service, lookups, null, null, formatter, null,
                                                                        null, null, null);
        IMReport<IMObject> report = new MsWordIMReport<>(doc, service, lookups, getHandlers(), factory.create(),
                                                         officeService);

        Map<String, Object> fields = new HashMap<>();
        Party practice = create(PracticeArchetypes.PRACTICE, Party.class);
        practice.setName("Vets R Us");
        fields.put("OpenVPMS.practice", practice);

        Party party = createCustomer();
        IMObjectBean act = createBean(EstimateArchetypes.ESTIMATE);
        act.setValue("startTime", TestHelper.getDate("2006-08-04"));
        act.setValue("lowTotal", new BigDecimal("100"));
        act.setTarget("customer", party);

        List<IMObject> objects = Collections.singletonList(act.getObject());

        // TODO:  Currently just do generate to make sure no exception.
        // result not used for merge testing until we can figure out how to
        // maintain fields in generated document.

        report.generate(objects, null, fields, DocFormats.DOC_TYPE);
    }

    /**
     * Verifies that input fields are returned as parameters, and that by specifying it as a parameter updates the
     * corresponding input field.
     */
    @Test
    public void testParameters() {
        Document doc = getDocument("src/test/reports/act.customerEstimation.doc", DocFormats.DOC_TYPE);

        IArchetypeService service = getArchetypeService();
        LookupService lookups = getLookupService();
        BasicAddressFormatter formatter = new BasicAddressFormatter(service, lookups);
        FunctionsFactory factory = new DefaultArchetypeFunctionsFactory(service, lookups, null, null, formatter, null,
                                                                        null, null, null);
        IMReport<IMObject> report = new MsWordIMReport<>(doc, service, lookups, getHandlers(), factory.create(),
                                                         officeService);

        Set<ParameterType> parameterTypes = report.getParameterTypes();
        Map<String, ParameterType> types = new HashMap<>();
        for (ParameterType type : parameterTypes) {
            types.put(type.getName(), type);
        }
        assertTrue(types.containsKey("inputField1"));

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("inputField1", "the input value");

        Party party = createCustomer();
        IMObjectBean act = createBean(EstimateArchetypes.ESTIMATE);
        act.setTarget("customer", party);

        List<IMObject> objects = Collections.singletonList(act.getObject());
        Document result = report.generate(objects, parameters, null, DocFormats.ODT_TYPE);

        Map<String, String> inputFields = getInputFields(result);
        assertEquals("the input value", inputFields.get("inputField1"));
    }

    /**
     * Creates a new {@link OpenOfficeDocument} wrapping a {@link Document}.
     *
     * @param document the document
     * @param context  the OpenOffice context
     * @return a new OpenOffice document
     */
    @Override
    protected OpenOfficeDocument getDocument(Document document, LocalOfficeContext context) {
        return new MsWordDocument(document, context, getHandlers());
    }
}
