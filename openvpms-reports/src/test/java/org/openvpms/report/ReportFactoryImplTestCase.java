/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.openvpms.archetype.function.factory.ArchetypeFunctionsFactory;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayOutputStream;
import java.util.Collections;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link ReportFactoryImpl} class.
 *
 * @author Tim Anderson
 */
public class ReportFactoryImplTestCase extends AbstractReportTest {

    /**
     * The document rules.
     */
    @Autowired
    DocumentRules documentRules;

    /**
     * The JXPath functions.
     */
    @Autowired
    private ArchetypeFunctionsFactory functions;

    /**
     * The settings.
     */
    @Autowired
    private Settings settings;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Verifies that static content can only be used via the
     * {@link ReportFactoryImpl#createStaticContentReport(DocumentTemplate)} method.
     *
     * @throws Exception for any error
     */
    @Test
    public void testStaticContent() throws Exception {
        byte[] content = RandomUtils.nextBytes(100);
        Document pdf = documentFactory.newDocument()
                .name("test.pdf")
                .mimeType(DocFormats.PDF_TYPE)
                .content(content)
                .build();
        Entity template = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(pdf)
                .build();
        IArchetypeService service = getArchetypeService();
        ReportFactory factory = new ReportFactoryImpl(service, getLookupService(),
                                                      getHandlers(), functions, documentRules, null, null, settings);

        DocumentTemplate documentTemplate = new DocumentTemplate(template, service);
        assertFalse(factory.hasMergeableContent(documentTemplate));
        IMReport<IMObject> report = factory.createStaticContentReport(documentTemplate);
        assertNotNull(report);
        Document document = report.generate(Collections.singletonList(patientFactory.newPatient().build(false)));
        assertEquals("test.pdf", document.getName());
        assertEquals(DocFormats.PDF_TYPE, document.getMimeType());

        // verify the document is a copy rather than the original
        assertNotEquals(document.getId(), pdf.getId());

        // verify the content matches that of the original document
        ByteArrayOutputStream actual = new ByteArrayOutputStream();
        IOUtils.copy(handlers.get(document).getContent(document), actual);
        assertArrayEquals(content, actual.toByteArray());

        assertFalse(factory.isIMObjectReport(documentTemplate));

        try {
            factory.createIMObjectReport(documentTemplate);
        } catch (ReportException exception) {
            assertEquals("REPORT-0031: Unsupported document template: test.pdf", exception.getMessage());
        }

        try {
            factory.createIMObjectReport(documentTemplate);
        } catch (ReportException exception) {
            assertEquals("REPORT-0031: Unsupported document template: test.pdf", exception.getMessage());
        }
    }
}