/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.commons.io.IOUtils;
import org.apache.commons.jxpath.Functions;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentTemplateBuilder;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.report.AbstractReportTest;
import org.openvpms.report.DocFormats;
import org.openvpms.report.ReportException;
import org.openvpms.report.jasper.function.EvaluateFunction;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.io.File;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.openvpms.archetype.rules.doc.DocumentTemplate.REPORT;
import static org.openvpms.archetype.rules.doc.DocumentTemplate.SUBREPORT;

/**
 * Tests the {@link TemplatedJasperIMObjectReport} class.
 *
 * @author Tim Anderson
 */
public class TemplatedJasperIMObjectReportTestCase extends AbstractReportTest {

    /**
     * The functions.
     */
    @Autowired
    private Functions functions;

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The settings.
     */
    @Autowired
    private Settings settings;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The reminder factory.
     */
    @Autowired
    private TestReminderFactory reminderFactory;

    /**
     * Tests the {@link TemplatedJasperIMObjectReport#generate(Iterable, Map, Map, String)} method.
     * <p>
     * This verifies that a parameter may be passed and accessed using the {@link EvaluateFunction#EVALUATE(String)}
     * method.
     *
     * @throws Exception for any error
     */
    @Test
    public void testGenerate() throws Exception {
        Party location = practiceFactory.newLocation().name("Main Clinic").build(false);
        Document document = getDocument("src/test/reports/party.customerperson.jrxml", DocFormats.XML_TYPE);
        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport(
                "Customer", document, getArchetypeService(), getLookupService(), getHandlers(),
                functions, settings);
        Party customer = createCustomer("Foo", "Bar");
        List<IMObject> list = Collections.singletonList(customer);

        // verify a field can be supplied
        Map<String, Object> fields = new HashMap<>();
        fields.put("Globals.Location", location);

        // generate the report as a CSV to allow comparison
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("param1", "Hello"); // this is passed to the report, and accessed using EVALUATE()
        Document csv = report.generate(list, parameters, fields, DocFormats.CSV_TYPE);
        String string = IOUtils.toString(getHandlers().get(document).getContent(csv), StandardCharsets.UTF_8);
        assertEquals("Foo,Bar,Main Clinic,Hello", string.trim());
    }

    /**
     * Verifies that SQL reports are supported.
     */
    @Test
    public void testGenerateForSQLQuery() throws Exception {
        Party location = practiceFactory.newLocation().name("Branch Clinic").build(false);
        Document document = getDocument("src/test/reports/sqlreport.jrxml", DocFormats.XML_TYPE);
        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport(
                "SQL Report", document, getArchetypeService(), getLookupService(), getHandlers(), functions, settings);
        Party customer = createCustomer("Foo", "Bar");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("customerId", customer.getId());
        Connection connection = dataSource.getConnection();
        parameters.put(JRParameter.REPORT_CONNECTION, connection);

        // verify a field can be supplied
        Map<String, Object> fields = new HashMap<>();
        fields.put("Globals.Location", location);

        // generate the report as a CSV to allow comparison
        Document csv = report.generate(parameters, fields, DocFormats.CSV_TYPE);
        String string = IOUtils.toString(getHandlers().get(document).getContent(csv), StandardCharsets.UTF_8);
        assertEquals("Foo,Bar,Branch Clinic", string.trim());
        connection.close();
    }

    /**
     * Verifies that sub-reports can be loaded at evaluation time.
     */
    @Test
    public void testReportWithSubReports() {
        DocumentTemplate template = loadReport(REPORT, "src/test/reports/Test Invoice.jrxml");
        // Note: incorrect archetype, but means that the test invoice won't clash with other test data
        loadReport(SUBREPORT, "src/test/reports/Test Title.jrxml");
        loadReport(SUBREPORT, "src/test/reports/Test Invoice Items.jrxml");
        loadReport(SUBREPORT, "src/test/reports/Test Invoice Reminders.jrxml");
        loadReport(SUBREPORT, "src/test/reports/Test Invoice Notes.jrxml");
        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport("Test Invoice", template.getDocument(),
                                                                                 getArchetypeService(),
                                                                                 getLookupService(), getHandlers(),
                                                                                 functions, settings);
        JasperTemplateLoader loader = report.getLoader();
        assertEquals(0, loader.getSubReports().size());

        List<FinancialAct> invoice = FinancialTestHelper.createChargesInvoice(
                BigDecimal.TEN, customerFactory.createCustomer(), patientFactory.createPatient(),
                productFactory.createMedication(), ActStatus.POSTED);
        save(invoice);
        Document document = report.generate(Collections.singletonList(invoice.get(0)));
        assertNotNull(document);

        Map<String, JasperReport> subReports = loader.getSubReports();
        assertEquals(4, subReports.size());
        assertTrue(subReports.containsKey("Test Title.jrxml"));
        assertTrue(subReports.containsKey("Test Invoice Items.jrxml"));
        assertTrue(subReports.containsKey("Test Invoice Reminders.jrxml"));
        assertTrue(subReports.containsKey("Test Invoice Notes.jrxml"));
    }

    /**
     * Verifies that an exception is thrown if a sub-report cannot be found.
     */
    @Test
    public void testMissingSubReport() {
        DocumentTemplate template = loadReport(REPORT, "src/test/reports/missingsubreport.jrxml");
        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport(
                "missingsubreport", template.getDocument(), getArchetypeService(), getLookupService(), getHandlers(),
                functions, settings);
        List<FinancialAct> invoice = FinancialTestHelper.createChargesInvoice(
                BigDecimal.TEN, customerFactory.createCustomer(), patientFactory.createPatient(),
                productFactory.createMedication(), ActStatus.POSTED);
        save(invoice);
        try {
            report.generate(Collections.singletonList(invoice.get(0)));
            fail("Expected report generation to fail");
        } catch (ReportException expected) {
            assertEquals("REPORT-0021: There is no sub-report named 'No Such Subreport.jrxml'.\n" +
                         "This is needed by the report 'missingsubreport.jrxml'.", expected.getMessage());
        }
    }

    /**
     * Verifies that a letterhead sub-report can be loaded based on fields.
     */
    @Test
    public void testLetterhead() {
        Document template = getDocument("src/test/reports/letterheadtest.jrxml", DocFormats.XML_TYPE);
        loadReport(SUBREPORT, "src/test/reports/letterhead-A4.jrxml");
        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport(
                "Letterhead", template, getArchetypeService(), getLookupService(), getHandlers(), functions, settings);
        JasperTemplateLoader loader = report.getLoader();
        assertEquals(0, loader.getSubReports().size());

        Entity letterhead = documentFactory.newLetterhead()
                .logoFile("logo.png")
                .subreport("letterhead-A4.jrxml")
                .build();
        Party location = practiceFactory.newLocation()
                .letterhead(letterhead)
                .build();

        Map<String, Object> fields = new HashMap<>();
        fields.put("OpenVPMS.location", location);

        Party customer = customerFactory.newCustomer().build(false);
        Document document = report.generate(Collections.singletonList(customer), Collections.emptyMap(), fields);
        assertNotNull(document);

        Map<String, JasperReport> subReports = loader.getSubReports();
        assertEquals(1, subReports.size());
        assertTrue(subReports.containsKey("letterhead-A4.jrxml"));
    }

    /**
     * Verify that when a report has an unsupported font, an exception is thrown that includes the template name.
     */
    @Test
    public void testInvalidFont() {
        DocumentTemplate template = loadReport(REPORT, "src/test/reports/invalidfont.jrxml");
        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport(
                "Invalid Font", template.getDocument(), getArchetypeService(), getLookupService(), getHandlers(),
                functions, settings);
        Party patient = patientFactory.newPatient().build(false);
        try {
            report.generate(Collections.singletonList(patient));
            fail("Expected report generation to fail");
        } catch (ReportException expected) {
            assertEquals("REPORT-0001: Failed to generate report 'Invalid Font': Font \"BadFont\" is not " +
                         "available to the JVM. See the Javadoc for more details.", expected.getMessage());
        }
    }

    /**
     * Verify that when a report has an invalid image path, an exception is thrown that includes the template name.
     */
    @Test
    public void testInvalidImagePath() {
        Document template = getDocument("src/test/reports/invalidimage.jrxml", DocFormats.XML_TYPE);
        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport("Invalid Image Path", template,
                                                                                 getArchetypeService(),
                                                                                 getLookupService(), getHandlers(),
                                                                                 functions, settings);
        Party customer = customerFactory.newCustomer().build(false);
        try {
            report.generate(Collections.singletonList(customer), Collections.emptyMap(), null);
            fail("Expected report generation to fail");
        } catch (ReportException expected) {
            assertEquals("REPORT-0014: Report 'Invalid Image Path' uses a resource that cannot be found: foo.png",
                         expected.getMessage());
        }
    }

    /**
     * Verify that when a report has an invalid image URL, an exception is thrown that includes the template name.
     */
    @Test
    public void testInvalidImageURL() {
        Document template = getDocument("src/test/reports/invalidimageurl.jrxml", DocFormats.XML_TYPE);
        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport("Invalid Image URL", template,
                                                                                 getArchetypeService(),
                                                                                 getLookupService(), getHandlers(),
                                                                                 functions, settings);
        Party customer = customerFactory.newCustomer().build(false);
        try {
            report.generate(Collections.singletonList(customer), Collections.emptyMap(), null);
            fail("Expected report generation to fail");
        } catch (ReportException expected) {
            assertEquals("REPORT-0014: Report 'Invalid Image URL' uses a resource that cannot be found: " +
                         "http://localhost:8080/foo.png", expected.getMessage());
        }
    }

    /**
     * Verifies that reports are terminated if they generate too many pages.
     * <br/>
     * This can occur if their content that is designed to fit on one page spills over to another, and JasperReports
     * goes into an infinite loop.
     * <p/>
     * The limit is determined by the maxPages setting of <em>entity.globalSettingsReport</em>.
     */
    @Test
    public void testInfiniteLoopTerminated() {
        // the following template will get into an infinite loop if the number of patient reminders forces it to
        // spill over to a new page. It can be corrected by moving the reminders subreport into the details band.
        DocumentTemplate template = loadReport(PatientArchetypes.DOCUMENT_FORM,
                                               "src/test/reports/Test Vaccination Certificate with reminders.jrxml");
        loadReport(SUBREPORT, "src/test/reports/Test Letterhead-A5.jrxml");
        loadReport(SUBREPORT, "src/test/reports/Test Vaccination Reminders.jrxml");

        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        DocumentAct form = patientFactory.newForm()
                .patient(patient)
                .template(template.getEntity())
                .product(productFactory.newMedication().name("A Vaccination").build())
                .build();
        for (int i = 0; i < 6; ++i) {
            Entity reminderType = reminderFactory.newReminderType()
                    .name("Reminder " + i)
                    .defaultInterval(3, DateUnits.MONTHS)
                    .build();
            reminderFactory.createReminder(patient, reminderType);
        }
        Settings settings = Mockito.mock(Settings.class);
        when(settings.getInt(eq(SettingsArchetypes.REPORT_SETTINGS), eq("maxPages"), anyInt())).thenReturn(10);

        TemplatedJasperIMObjectReport report = new TemplatedJasperIMObjectReport(
                "Test Vaccination Certificate with reminders", template.getDocument(), getArchetypeService(),
                getLookupService(), getHandlers(), functions, settings);
        try {
            Map<String, Object> fields = new HashMap<>();
            Party location = practiceFactory.newLocation()
                    .name("Vets R Us")
                    .addAddress("123 Main St", "NORTHCOTE", "VIC", "30  70")
                    .addPhone("123456")
                    .addEmail("info@vetsrus.biz")
                    .build();
            fields.put("OpenVPMS.location", location);

            report.generate(Collections.singletonList(form), null, fields);
            fail("Expected report generation to fail");
        } catch (Exception expected) {
            assertEquals("REPORT-0013: Report 'Test Vaccination Certificate with reminders' has been " +
                         "terminated as it exceeded the maximum limit of 10 pages", expected.getMessage());
        }
    }

    /**
     * Loads a JasperReport.
     * <p/>
     * This reuses an existing <em>act.documentTemplate</em> with the same name, as JasperReports sub-reports are
     * loaded using their file name via {@link JasperTemplateLoader}.
     *
     * @param type the report type
     * @param path the report path
     * @return the report, as a document template
     */
    private DocumentTemplate loadReport(String type, String path) {
        File file = new File(path);
        TestDocumentTemplateBuilder builder = documentFactory.newTemplate()
                .type(type)
                .singletonByContent(file.getName())
                .document(file, "text/xml");
        return new DocumentTemplate(builder.build(), getArchetypeService());
    }
}
