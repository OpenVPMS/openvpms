/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.SimpleJasperReportsContext;
import net.sf.jasperreports.repo.ReportResource;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentTemplateBuilder;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.report.AbstractReportTest;
import org.openvpms.report.ReportException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.doc.DocumentTemplate.REPORT;
import static org.openvpms.archetype.rules.doc.DocumentTemplate.SUBREPORT;

/**
 * Tests the {@link JasperTemplateLoader}.
 *
 * @author Tim Anderson
 */
public class JasperTemplateLoaderTestCase extends AbstractReportTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Tests the loader.
     */
    @Test
    public void testLoad() {
        DocumentTemplate template = loadReport(REPORT, "src/test/reports/Test Invoice.jrxml");
        // Note: incorrect archetype, but means that the test invoice won't clash with other test data
        loadReport(SUBREPORT, "src/test/reports/Test Title.jrxml");
        loadReport(SUBREPORT, "src/test/reports/Test Invoice Items.jrxml");
        loadReport(SUBREPORT, "src/test/reports/Test Invoice Reminders.jrxml");
        loadReport(SUBREPORT, "src/test/reports/Test Invoice Notes.jrxml");

        TestJasperTemplateLoader loader = new TestJasperTemplateLoader(template.getDocument(), getArchetypeService(),
                                                                       getHandlers(), new SimpleJasperReportsContext());
        loader.getResource("Test Title.jrxml", ReportResource.class);
        loader.getResource("Test Invoice Items.jrxml", ReportResource.class);
        loader.getResource("Test Invoice Reminders.jrxml", ReportResource.class);
        loader.getResource("Test Invoice Notes.jrxml", ReportResource.class);
        assertEquals(4, loader.getSubReports().size());
        assertEquals(4, loader.documents.size());

        // verify documents are only loaded once - subsequent access uses a cached version
        loader.getResource("Test Invoice Notes.jrxml", ReportResource.class);
        assertEquals(4, loader.getSubReports().size());
        assertEquals(4, loader.documents.size());
    }

    /**
     * Verifies a {@link ReportException} is thrown if a report template is invalid.
     */
    @Test
    public void testInvalidReport() {
        DocumentTemplate template = new DocumentTemplate(createBrokenTemplate("Invalid.jrxml"), getArchetypeService());
        try {
            new JasperTemplateLoader(template.getDocument(), getArchetypeService(), getHandlers(),
                                     new SimpleJasperReportsContext());
            fail("Expected JasperTemplateLoader to throw an exception");
        } catch (ReportException exception) {
            assertEquals("REPORT-0020: Failed to create report 'Invalid.jrxml': org.xml.sax.SAXParseException; " +
                         "Premature end of file.", exception.getMessage());
        }
    }

    /**
     * Verifies a {@link ReportException} is thrown if a sub-report is missing.
     */
    @Test
    public void testMissingSubReport() {
        DocumentTemplate template = loadReport(REPORT, "src/test/reports/Test Invoice.jrxml");
        JasperTemplateLoader loader = new JasperTemplateLoader(template.getDocument(), getArchetypeService(),
                                                               getHandlers(), new SimpleJasperReportsContext());
        try {
            loader.getResource("Missing.jrxml", ReportResource.class);
            fail("Expected getResource() to fail");
        } catch (ReportException expected) {
            assertEquals("REPORT-0021: There is no sub-report named 'Missing.jrxml'.\n" +
                         "This is needed by the report 'Test Invoice.jrxml'.", expected.getMessage());
        }
    }

    /**
     * Verifies a {@link ReportException} is thrown if a sub-report cannot be compiled.
     */
    @Test
    public void testInvalidSubReport() {
        DocumentTemplate template = loadReport(REPORT, "src/test/reports/Test Invoice.jrxml");
        JasperTemplateLoader loader = new JasperTemplateLoader(template.getDocument(), getArchetypeService(),
                                                               getHandlers(), new SimpleJasperReportsContext());
        createBrokenTemplate("Bad.jrxml");
        try {
            loader.getResource("Bad.jrxml", ReportResource.class);
            fail("Expected getResource() to fail");
        } catch (ReportException expected) {
            assertEquals("REPORT-0021: There is no sub-report named 'Bad.jrxml'.\n" +
                         "This is needed by the report 'Test Invoice.jrxml'.", expected.getMessage());
        }
    }

    /**
     * Verifies that if there are two sub-report templates with the same name, the one with the lowest id is used.
     * If one is inactive, it is ignored.
     */
    @Test
    public void testDuplicateSubReportTemplate() {
        DocumentTemplate template = loadReport(REPORT, "src/test/reports/Test Invoice.jrxml");
        DocumentTemplate subReport1 = loadReport(SUBREPORT, "src/test/reports/Test Title.jrxml", true);
        DocumentTemplate subReport2 = loadReport(SUBREPORT, "src/test/reports/Test Title.jrxml", false);
        Document doc1 = subReport1.getDocument();
        Document doc2 = subReport2.getDocument();

        SimpleJasperReportsContext context = new SimpleJasperReportsContext();
        TestJasperTemplateLoader loader1 = new TestJasperTemplateLoader(template.getDocument(), getArchetypeService(),
                                                                        getHandlers(), context);

        loader1.getResource("Test Title.jrxml", ReportResource.class);
        assertEquals(1, loader1.documents.size());
        assertEquals(doc1.getObjectReference(), loader1.documents.get(0)); // verify doc1 loaded as it has the lowest id

        // now mark subReport1 inactive. It should no longer be used
        Entity entity = subReport1.getEntity();
        entity.setActive(false);
        save(entity);

        TestJasperTemplateLoader loader2 = new TestJasperTemplateLoader(template.getDocument(), getArchetypeService(),
                                                                        getHandlers(), context);

        // now verify subReport2 is loaded
        loader2.getResource("Test Title.jrxml", ReportResource.class);
        assertEquals(1, loader2.documents.size());
        assertEquals(doc2.getObjectReference(), loader2.documents.get(0));
    }

    /**
     * Creates a dummy jasper report template that can't be compiled.
     *
     * @param name the template name
     * @return the template
     */
    private Entity createBrokenTemplate(String name) {
        Document document = documentFactory.newDocument()
                .name(name)
                .content("")
                .build();
        return documentFactory.newTemplate()
                .singletonByContent(name)
                .document(document)
                .build();
    }

    /**
     * Loads a JasperReport.
     * <p/>
     * This reuses an existing <em>act.documentTemplate</em> with the same name, as JasperReports sub-reports are
     * loaded using their file name via {@link JasperTemplateLoader}.
     *
     * @param type the report type
     * @param path the report path
     * @return the report, as a document template
     */
    private DocumentTemplate loadReport(String type, String path) {
        return loadReport(type, path, true);
    }

    /**
     * Loads a JasperReport.
     *
     * @param type      the report type
     * @param path      the report path
     * @param singleton if {@code true}, re-use existing <em>act.documentTemplate</em> with the same name
     * @return the report, as a document template
     */
    private DocumentTemplate loadReport(String type, String path, boolean singleton) {
        File file = new File(path);
        TestDocumentTemplateBuilder builder = documentFactory.newTemplate()
                .type(type)
                .document(file, "text/xml");
        if (singleton) {
            builder.singletonByContent(file.getName());
        } else {
            builder.name(file.getName());
        }
        return new DocumentTemplate(builder.build(), getArchetypeService());
    }

    private static class TestJasperTemplateLoader extends JasperTemplateLoader {

        /**
         * Tracks the loaded documents.
         */
        private final List<Reference> documents = new ArrayList<>();

        /**
         * Constructs a {@link TestJasperTemplateLoader}.
         *
         * @param template the document template
         * @param service  the archetype service
         * @param handlers the document handlers
         * @param context  the jasper reports context
         * @throws ReportException if the report cannot be created
         */
        public TestJasperTemplateLoader(Document template, ArchetypeService service, DocumentHandlers handlers,
                                        JasperReportsContext context) {
            super(template, service, handlers, context);
        }


        /**
         * Returns the named document.
         *
         * @param name the document name
         * @return the document
         * @throws ReportException           if the document cannot be located
         * @throws ArchetypeServiceException for any archetype service error
         */
        @Override
        protected Document getDocument(String name) {
            Document document = super.getDocument(name);
            documents.add(document.getObjectReference());
            return document;
        }
    }
}
