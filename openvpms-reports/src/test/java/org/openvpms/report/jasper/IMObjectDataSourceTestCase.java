/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import org.apache.commons.jxpath.Functions;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentHelper;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceBuilder;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.report.Parameters;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link IMObjectDataSource} class.
 *
 * @author Tim Anderson
 */
public class IMObjectDataSourceTestCase extends AbstractDataSourceTest<IMObject> {

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The JXPath functions.
     */
    @Autowired
    private Functions functions;

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Tests basic data source functionality.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDataSource() throws Exception {
        IMObject object = createCustomer("Foo", "Bar");
        IMObjectDataSource ds = createDataSource(object);
        JRField firstName = createField("firstName", String.class);
        JRField lastName = createField("lastName", String.class);

        assertTrue(ds.next());
        assertEquals("Foo", ds.getFieldValue(firstName));
        assertEquals("Bar", ds.getFieldValue(lastName));
        assertFalse(ds.next());
    }

    /**
     * Tests handling of documents.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDocument() throws Exception {
        // create a new act and associate an image with it
        IMObjectBean act = createBean(PatientArchetypes.DOCUMENT_IMAGE);
        File file = new File("src/test/images/openvpms.gif");
        Document doc = DocumentHelper.create(file, "image/gif", handlers);
        getArchetypeService().save(doc);
        act.setValue("document", doc.getObjectReference());
        IMObjectDataSource ds = createDataSource(act.getObject());

        // get the image field
        JRField document = createField("document", InputStream.class);
        InputStream stream = (InputStream) ds.getFieldValue(document);

        // verify the image contents match that expected
        FileInputStream expectedStream = new FileInputStream(file);
        int expected;
        while ((expected = expectedStream.read()) != -1) {
            assertEquals(expected, stream.read());
        }
        assertEquals(-1, stream.read());
        expectedStream.close();
        stream.close();
    }

    /**
     * Tests fields.
     *
     * @throws Exception for any error
     */
    @Test
    public void testFields() throws Exception {
        IMObject object = createCustomer("Foo", "Bar");
        Map<String, Object> fields = new HashMap<>();
        fields.put("fieldA", "A");
        fields.put("field.B", "B");
        fields.put("field1", 1);
        fields.put("OpenVPMS.customer", object);
        fields.put("title", "OVERRIDE");

        IMObjectDataSource ds = createDataSource(object, fields);
        JRField firstName = createField("firstName", String.class);
        JRField lastName = createField("lastName", String.class);
        JRField fieldA = createField("fieldA", String.class);
        JRField fieldB = createField("field.B", String.class);
        JRField field1 = createField("field1", Integer.class);
        JRField customerFirstName = createField("OpenVPMS.customer.firstName", String.class);
        JRField title = createField("title", String.class); // overrides the default field

        // verify that fields can also be accessed as variables.
        JRField exprCustomerFirstName = createField("[openvpms:get($OpenVPMS.customer, 'firstName')]", String.class);
        JRField exprCustomerLastName = createField("[openvpms:get($OpenVPMS.customer, 'lastName')]", String.class);

        assertTrue(ds.next());
        assertEquals("Foo", ds.getFieldValue(firstName));
        assertEquals("Bar", ds.getFieldValue(lastName));
        assertEquals("A", ds.getFieldValue(fieldA));
        assertEquals("B", ds.getFieldValue(fieldB));
        assertEquals(1, ds.getFieldValue(field1));
        assertEquals("Foo", ds.getFieldValue(customerFirstName));
        assertEquals("OVERRIDE", ds.getFieldValue(title));

        assertEquals("Foo", ds.getFieldValue(exprCustomerFirstName));
        assertEquals("Bar", ds.getFieldValue(exprCustomerLastName));

        assertFalse(ds.next());
    }

    /**
     * Tests the {@link IMObjectDataSource#getDataSource(String)} method.
     *
     * @throws JRException for any JasperReports error
     */
    @Test
    public void testGetDataSource() throws JRException {
        Party customer = customerFactory.createCustomer();
        patientFactory.newPatient()
                .name("Fido")
                .owner(customer)
                .build();

        IMObjectDataSource ds = createDataSource(customer, new HashMap<>());
        DataSource patients = (DataSource) ds.getDataSource("patients");
        assertTrue(patients.next());

        assertEquals("Fido", patients.getFieldValue(new TestField("target.name", String.class)));
        assertFalse(patients.next());
    }

    /**
     * Verifies that the {@link IMObjectDataSource#getDataSource(String)} returns items in ascending id order.
     *
     * @throws JRException for any JasperReports error
     */
    @Test
    public void testGetDataSourceNaturalSort() throws JRException {
        Party customer = customerFactory.createCustomer();
        Party patient1 = patientFactory.createPatient(customer);
        Party patient2 = patientFactory.createPatient(customer);
        Party patient3 = patientFactory.createPatient(customer);
        IMObjectDataSource ds = createDataSource(customer, new HashMap<>());
        DataSource patients = (DataSource) ds.getDataSource("patients");
        assertTrue(patients.next());

        TestField field = new TestField("target.id", Long.class);
        assertEquals(patient1.getId(), patients.getFieldValue(field));
        assertTrue(patients.next());

        assertEquals(patient2.getId(), patients.getFieldValue(field));
        assertTrue(patients.next());

        assertEquals(patient3.getId(), patients.getFieldValue(field));
        assertFalse(patients.next());
    }

    /**
     * Tests the {@link IMObjectDataSource#getDataSource(String, String[])} method.
     *
     * @throws JRException for any JasperReports error
     */
    @Test
    public void testGetDataSourceWithSort() throws JRException {
        Party patient1 = patientFactory.createPatient();
        Product productZ = productFactory.newMedication().name("productZ").build();
        Product productX = productFactory.newMedication().name("productX").build();
        Product productY = productFactory.newMedication().name("productY").build();
        Date yesterday = DateRules.getYesterday();
        Date today = DateRules.getToday();
        TestInvoiceBuilder builder = accountFactory.newInvoice()
                .customer(createCustomer())
                .item()
                .startTime(today)
                .patient(patient1)
                .product(productY)
                .add()
                .item()
                .startTime(today)
                .patient(patient1)
                .product(productX)
                .add()
                .item()
                .startTime(yesterday)
                .patient(patient1)
                .product(productX)
                .add()
                .item()
                .startTime(yesterday)
                .patient(patient1)
                .product(productZ)
                .add();
        FinancialAct invoice = builder.build();

        IMObjectDataSource ds = createDataSource(invoice, new HashMap<>());

        TestField id = new TestField("id", Long.class);
        TestField productId = new TestField("target.product.entity.id", Long.class);
        TestField startTime = new TestField("target.startTime", Date.class);

        // sort items on product name. Objects will have an implicit sort on relationship id as well.
        DataSource items1 = (DataSource) ds.getDataSource("items", "target.product.entity.name");
        assertTrue(items1.next());
        assertEquals(productX.getId(), items1.getFieldValue(productId));
        Long first = (Long) items1.getFieldValue(id);

        assertTrue(items1.next());
        assertEquals(productX.getId(), items1.getFieldValue(productId));
        Long second = (Long) items1.getFieldValue(id);
        assertTrue(first < second);
        // first relationship id should be lower than second, when the products are the same

        assertTrue(items1.next());
        assertEquals(productY.getId(), items1.getFieldValue(productId));

        assertTrue(items1.next());
        assertEquals(productZ.getId(), items1.getFieldValue(productId));

        // now sort on product name and time
        DataSource items2 = (DataSource) ds.getDataSource("items", "target.product.entity.name", "target.startTime");
        assertTrue(items2.next());
        assertEquals(productX.getId(), items2.getFieldValue(productId));
        assertEquals(yesterday, items2.getFieldValue(startTime));

        assertTrue(items2.next());
        assertEquals(productX.getId(), items2.getFieldValue(productId));
        assertEquals(today, items2.getFieldValue(startTime));

        assertTrue(items2.next());
        assertEquals(productY.getId(), items2.getFieldValue(productId));
        assertEquals(today, items2.getFieldValue(startTime));
        assertTrue(items2.next());
        assertEquals(productZ.getId(), items2.getFieldValue(productId));
        assertEquals(yesterday, items2.getFieldValue(startTime));
        assertFalse(items2.next());
    }

    /**
     * Tests the {@link IMObjectDataSource#getDataSource(String, boolean, String...)} method.
     *
     * @throws JRException for any JasperReports error
     */
    @Test
    public void testGetDataSourceWithCompositeNodeNames() throws JRException {
        Party patient1 = patientFactory.createPatient();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        Entity test1 = laboratoryFactory.newTest().name("AAA").investigationType(investigationType).build();
        Entity test2 = laboratoryFactory.newTest().name("BBB").investigationType(investigationType).build();
        Entity test3 = laboratoryFactory.newTest().name("CCC").investigationType(investigationType).build();
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient1)
                .investigationType(investigationType)
                .laboratory(laboratoryFactory.createLaboratory())
                .results()
                .resultsId("r1")
                .test(test1)
                .categoryName("Haematology")
                .result().resultId("X1").name("Haem analyte 2").result("1").add()
                .result().resultId("Y2").name("Haem analyte 1").result("2").add().add()
                .results()
                .resultsId("r2")
                .test(test2)
                .categoryName("Chemistry")
                .result().resultId("A1").name("Chem analyte 1").result("3").add()
                .result().resultId("C2").name("Chem analyte 2").result("4").add().add()
                .results()
                .resultsId("r3")
                .test(test3)
                .categoryName("Microbiology")
                .result().resultId("Y1").name("Micro analyte 3").result("5").add()
                .result().resultId("M2").name("Micro analyte 1").result("6").add().add()
                .build();

        IMObjectDataSource ds = createDataSource(investigation, new HashMap<>());

        TestField categoryName = new TestField("categoryName", String.class);
        TestField testId = new TestField("test.entity.id", Long.class);

        // access the target acts of the results node, and sort results on category name
        DataSource results1 = (DataSource) ds.getDataSource("results.target", false, "categoryName");
        assertTrue(results1.next());
        assertEquals("Chemistry", results1.getFieldValue(categoryName));
        assertEquals(test2.getId(), results1.getFieldValue(testId));

        assertTrue(results1.next());
        assertEquals("Haematology", results1.getFieldValue(categoryName));
        assertEquals(test1.getId(), results1.getFieldValue(testId));

        assertTrue(results1.next());
        assertEquals("Microbiology", results1.getFieldValue(categoryName));
        assertEquals(test3.getId(), results1.getFieldValue(testId));

        // now sort on results sequence. This needs absolute sort nodes
        DataSource results2 = (DataSource) ds.getDataSource("results.target", true, "results.sequence");
        assertTrue(results2.next());
        assertEquals("Haematology", results2.getFieldValue(categoryName));
        assertEquals(test1.getId(), results2.getFieldValue(testId));

        assertTrue(results2.next());
        assertEquals("Chemistry", results2.getFieldValue(categoryName));
        assertEquals(test2.getId(), results2.getFieldValue(testId));

        assertTrue(results2.next());
        assertEquals("Microbiology", results2.getFieldValue(categoryName));
        assertEquals(test3.getId(), results2.getFieldValue(testId));

        // Now create a data source that retrieves result items ordered on result sequence then name
        // Need to use absolute node names in order to sort on sequence
        TestField name = new TestField("name", String.class);
        DataSource items1 = (DataSource) ds.getDataSource("results.target.items.target", true,
                                                            "results.sequence", "results.target.items.target.name");
        assertTrue(items1.next());
        assertEquals("Haem analyte 1", items1.getFieldValue(name));
        assertTrue(items1.next());
        assertEquals("Haem analyte 2", items1.getFieldValue(name));
        assertTrue(items1.next());
        assertEquals("Chem analyte 1", items1.getFieldValue(name));
        assertTrue(items1.next());
        assertEquals("Chem analyte 2", items1.getFieldValue(name));
        assertTrue(items1.next());
        assertEquals("Micro analyte 1", items1.getFieldValue(name));
        assertTrue(items1.next());
        assertEquals("Micro analyte 3", items1.getFieldValue(name));
        assertFalse(items1.next());
    }

    /**
     * Tests the {@link IMObjectDataSource#getDataSource(String, String[])} method sorts on ascending id when there
     * are duplicates in the node being sorted on.
     *
     * @throws JRException for any JasperReports error
     */
    @Test
    public void testGetDataSourceWithSortDuplicates() throws JRException {
        Party customer = customerFactory.newCustomer()
                .addWorkPhone("9")
                .addWorkPhone("1")
                .addWorkPhone("2")
                .build();
        IMObjectDataSource ds = createDataSource(customer, new HashMap<>());

        // sort on contact name. As these have not been changed, they will all be "Phone Number"
        DataSource contacts = (DataSource) ds.getDataSource("contacts", "name");

        TestField id = new TestField("id", Long.class);
        TestField name = new TestField("name", String.class);

        assertTrue(contacts.next());
        assertEquals("Phone Number", contacts.getFieldValue(name));
        Long first = (Long) contacts.getFieldValue(id);
        assertTrue(contacts.next());
        assertEquals("Phone Number", contacts.getFieldValue(name));
        Long second = (Long) contacts.getFieldValue(id);
        assertTrue(contacts.next());
        assertEquals("Phone Number", contacts.getFieldValue(name));
        Long third = (Long) contacts.getFieldValue(id);
        assertFalse(contacts.next());

        // verify the contacts have been returned in ascending id order
        assertTrue(first < second);
        assertTrue(second < third);
    }

    /**
     * Creates a new data source.
     *
     * @param objects    the objects
     * @param parameters the parameters
     * @param fields     the fields
     * @param handlers   the document handlers
     * @param functions  the functions
     * @return a new data source
     */
    @Override
    protected DataSource createDataSource(List<IMObject> objects, Parameters parameters, PropertySet fields,
                                          DocumentHandlers handlers, Functions functions) {
        if (objects.size() != 1) {
            throw new IllegalArgumentException("Argument 'objects' must contain a single element");
        }
        ReportContext context = new ReportContext(parameters, fields, "test", getArchetypeService(), getLookupService(),
                                                  handlers, functions);
        return new IMObjectDataSource(objects.get(0), context);
    }

    /**
     * Creates a collection of customers to pass to the data source.
     *
     * @param customers the customers
     * @return a collection to pass to the data source
     */
    @Override
    protected List<IMObject> createCollection(Party... customers) {
        return Arrays.asList(customers);
    }

    /**
     * Helper to create a new data source.
     *
     * @param object the object
     * @return a new data source
     */
    private IMObjectDataSource createDataSource(IMObject object) {
        return createDataSource(object, null);
    }

    /**
     * Helper to create a new data source.
     *
     * @param object the object
     * @param fields a map of additional field names and their values, to pass to the report. May be {@code null}
     * @return a new data source
     */
    private IMObjectDataSource createDataSource(IMObject object, Map<String, Object> fields) {
        return new IMObjectDataSource(object, null, fields, "test", getArchetypeService(), getLookupService(), handlers,
                                      functions);
    }

}
