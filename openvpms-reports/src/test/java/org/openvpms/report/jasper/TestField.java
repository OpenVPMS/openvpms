/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRPropertiesHolder;
import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.JRPropertyExpression;

/**
 * Test implementation of {@link JRField}.
 *
 * @author Tim Anderson
 */
class TestField implements JRField {

    private final String name;

    private final Class<?> type;

    public TestField(String name) {
        this(name, String.class);
    }

    public TestField(String name, Class<?> type) {
        this.name = name;
        this.type = type;
    }

    /**
     * Gets the field unique name.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Gets the field optional description.
     */
    @Override
    public String getDescription() {
        return null;
    }

    /**
     * Sets the field description.
     *
     * @param description the description
     */
    @Override
    public void setDescription(String description) {

    }

    /**
     * Gets the field value class. Field types cannot be primitives.
     */
    @Override
    public Class<?> getValueClass() {
        return type;
    }

    /**
     * Gets the field value class name.
     */
    @Override
    public String getValueClassName() {
        return type.getName();
    }

    /**
     * Checks whether the object has any properties.
     *
     * @return whether the object has any properties
     */
    @Override
    public boolean hasProperties() {
        return false;
    }

    /**
     * Returns this object's properties map.
     *
     * @return this object's properties map
     */
    @Override
    public JRPropertiesMap getPropertiesMap() {
        return null;
    }

    /**
     * Returns the parent properties holder, whose properties are used as defaults
     * for this object.
     *
     * @return the parent properties holder, or <code>null</code> if no parent
     */
    @Override
    public JRPropertiesHolder getParentProperties() {
        return null;
    }

    /**
     * Returns the list of dynamic/expression-based properties for this field.
     *
     * @return an array containing the expression-based properties of this field
     */
    @Override
    public JRPropertyExpression[] getPropertyExpressions() {
        return null;
    }

    @Override
    public Object clone() {
        return new TestField(name, type);
    }
}
