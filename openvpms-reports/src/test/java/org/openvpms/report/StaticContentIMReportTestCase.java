/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link StaticContentIMReport}.
 *
 * @author Tim Anderson
 */
public class StaticContentIMReportTestCase extends AbstractReportTest {

    /**
     * The document rules.
     */
    @Autowired
    DocumentRules documentRules;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Tests report generation.
     *
     * @throws Exception for any error
     */
    @Test
    public void testReport() throws Exception {
        byte[] content = RandomUtils.nextBytes(100);
        Document pdf = documentFactory.newDocument()
                .name("test.pdf")
                .mimeType(DocFormats.PDF_TYPE)
                .content(content)
                .build();

        DocumentConverter converter = Mockito.mock(DocumentConverter.class);
        StaticContentIMReport<IMObject> report = new StaticContentIMReport<>(pdf, documentRules, handlers,
                                                                             converter);

        Party patient = patientFactory.newPatient().build(false);
        assertEquals(DocFormats.PDF_TYPE, report.getDefaultMimeType());
        String[] mimeTypes = report.getMimeTypes();
        assertEquals(1, mimeTypes.length);
        assertEquals(DocFormats.PDF_TYPE, mimeTypes[0]);

        assertFalse(report.canPrint());
        assertTrue(report.getParameterTypes().isEmpty());

        List<IMObject> objects = Collections.singletonList(patient);
        Document doc1 = report.generate(objects);
        Document doc2 = report.generate(objects, DocFormats.PDF_TYPE);
        Document doc3 = report.generate(Collections.emptyMap(), Collections.emptyMap());
        Document doc4 = report.generate(Collections.emptyMap(), Collections.emptyMap(), DocFormats.PDF_TYPE);
        Document doc5 = report.generate(objects, Collections.emptyMap(), Collections.emptyMap());
        Document doc6 = report.generate(objects, Collections.emptyMap(), Collections.emptyMap(), DocFormats.PDF_TYPE);
        ByteArrayOutputStream doc7 = new ByteArrayOutputStream();
        report.generate(objects, Collections.emptyMap(), Collections.emptyMap(), DocFormats.PDF_TYPE, doc7);

        checkContent(content, pdf, doc1);
        checkContent(content, pdf, doc2);
        checkContent(content, pdf, doc3);
        checkContent(content, pdf, doc4);
        checkContent(content, pdf, doc5);
        checkContent(content, pdf, doc6);

        assertArrayEquals(content, doc7.toByteArray());
    }

    /**
     * Verifies the generated document matches that expected.
     *
     * @param expected the expected content
     * @param original the original document
     * @param actual   the generated document
     * @throws IOException for any I/O error
     */
    private void checkContent(byte[] expected, Document original, Document actual)
            throws IOException {
        assertNotSame(original.getId(), actual.getId());
        // must be a copy, as we don't want the caller to change the original document

        assertEquals(original.getName(), actual.getName());
        assertEquals(original.getMimeType(), actual.getMimeType());
        assertEquals(original.getSize(), actual.getSize());
        assertEquals(original.getChecksum(), actual.getChecksum());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        IOUtils.copy(handlers.get(actual).getContent(actual), stream);
        assertArrayEquals(expected, stream.toByteArray());
    }
}