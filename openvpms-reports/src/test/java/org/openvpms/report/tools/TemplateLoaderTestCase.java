/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.tools;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.report.AbstractReportTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.doc.DocumentTemplate.SUBREPORT;

/**
 * Tests the {@link TemplateLoader}.
 *
 * @author Tim Anderson
 */
public class TemplateLoaderTestCase extends AbstractReportTest {

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        removeDocumentTemplate("Test Invoice");
        removeDocumentTemplate("Test Invoice Items");
        removeDocumentTemplate("Test Invoice B");

        // remove by act, just in case a prior failure has done a partial deletion
        removeTemplateByAct("Test Invoice.jrxml");
        removeTemplateByAct("Test Invoice Items.jrxml");
        removeTemplateByAct("Test Invoice Notes.jrxml");
        removeTemplateByAct("Test Invoice Reminders.jrxml");
        removeTemplateByAct("Test Invoice B.jrxml");
        removeTemplateByAct("Test Title.jrxml");
        lookupFactory.getLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE, CustomerAccountArchetypes.INVOICE);
        lookupFactory.getLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE, SUBREPORT);
    }

    /**
     * Tests loading templates.
     *
     * @throws Exception for any error
     */
    @Test
    public void testLoadDocumentTemplate() throws Exception {
        TemplateLoader loader = new TemplateLoader(getArchetypeService(), handlers, transactionManager,
                                                   getLookupService());
        loader.load("src/test/reports/templates.xml");

        Entity template1 = getTemplate("Test Invoice", DocumentArchetypes.DOCUMENT_TEMPLATE);
        Entity template2 = getTemplate("Test Invoice Items", DocumentArchetypes.DOCUMENT_TEMPLATE);
        DocumentAct act1 = checkTemplate(template1, "Test Invoice", "Invoice template",
                                         "act.customerAccountChargesInvoice",
                                         "Test Invoice.jrxml", "text/xml", DocumentArchetypes.DEFAULT_DOCUMENT, null);
        DocumentAct act2 = checkTemplate(template2, "Test Invoice Items", "Invoice Items template", SUBREPORT,
                                         "Test Invoice Items.jrxml", "text/xml", DocumentArchetypes.DEFAULT_DOCUMENT,
                                         null);

        Document doc1 = (Document) get(act1.getDocument());
        Document doc2 = (Document) get(act2.getDocument());

        // run the loader again and verify nothing updates
        loader.load("src/test/reports/templates.xml");

        checkTemplateNotUpdated(template1);
        checkTemplateNotUpdated(template2);

        checkActNotUpdated(act1, template1);
        checkActNotUpdated(act2, template2);

        checkDocumentNotUpdated(doc1);
        checkDocumentNotUpdated(doc2);

        // now load templates2.xml which has a different name for the entity, but points to the same file
        // The entity will updated but the act and document won't be
        loader.load("src/test/reports/templates2.xml");
        assertNull(getTemplate("Test Invoice", DocumentArchetypes.DOCUMENT_TEMPLATE)); // entity has been renamed
        Entity template1Updated = getTemplate("Test Invoice B", DocumentArchetypes.DOCUMENT_TEMPLATE);
        DocumentAct act1again = checkTemplate(template1Updated, "Test Invoice B", "Invoice template B",
                                              "act.customerAccountChargesInvoice",
                                              "Test Invoice.jrxml", "text/xml", DocumentArchetypes.DEFAULT_DOCUMENT,
                                              null);
        assertEquals(template1, template1Updated);

        assertEquals(act1, act1again);
        checkActNotUpdated(act1, template1Updated);

        checkDocumentNotUpdated(doc1);

        // now load templates3.xml which has the same name, Test Invoice B, but a different file.
        // This should create a new act and entity. There will be 2 entities named Test Invoice B
        TemplateHelper helper = new TemplateHelper(getArchetypeService());
        assertNull(helper.getDocumentAct("Test Invoice B.jrxml"));

        loader.load("src/test/reports/templates3.xml");
        DocumentAct newInvoiceB = helper.getDocumentAct("Test Invoice B.jrxml");
        assertNotNull(newInvoiceB);
        IMObjectBean bean = getBean(newInvoiceB);
        Entity newTemplateB = bean.getTarget("template", Entity.class);
        DocumentAct invoiceBAgain = checkTemplate(newTemplateB, "Test Invoice B", "Invoice template B",
                                                  "act.customerAccountChargesInvoice", "Test Invoice B.jrxml",
                                                  "text/xml", DocumentArchetypes.DEFAULT_DOCUMENT, null);
        assertEquals(newInvoiceB, invoiceBAgain);
    }

    /**
     * Verifies that if a file changes content, the original document is removed.
     *
     * @throws Exception for any error
     */
    @Test
    public void testOldDocumentRemoved() throws Exception {
        TemplateLoader loader = new TemplateLoader(getArchetypeService(), handlers, transactionManager,
                                                   getLookupService());
        loader.load("src/test/reports/templates.xml");

        Entity template1 = getTemplate("Test Invoice", DocumentArchetypes.DOCUMENT_TEMPLATE);
        DocumentAct act1 = checkTemplate(template1, "Test Invoice", "Invoice template",
                                         "act.customerAccountChargesInvoice",
                                         "Test Invoice.jrxml", "text/xml", DocumentArchetypes.DEFAULT_DOCUMENT, null);
        Document doc1 = (Document) get(act1.getDocument());

        doc1.setChecksum(-doc1.getChecksum());  // simulate content change.
        save(doc1);

        // reload and verify doc1 deleted
        loader.load("src/test/reports/templates.xml");

        Entity template2 = getTemplate("Test Invoice", DocumentArchetypes.DOCUMENT_TEMPLATE);
        assertEquals(template1, template2);
        checkTemplateNotUpdated(template1);

        DocumentAct act2 = checkTemplate(template2, "Test Invoice", "Invoice template",
                                         "act.customerAccountChargesInvoice",
                                         "Test Invoice.jrxml", "text/xml", DocumentArchetypes.DEFAULT_DOCUMENT, null);
        assertEquals(act1, act2);

        Document doc2 = (Document) get(act2.getDocument());
        assertNotEquals(doc1, doc2);   // should be a different document
        assertNull(get(doc1));         // doc1 should be removed
    }

    /**
     * Tests loading email templates.
     *
     * @throws Exception for any error
     */
    @Test
    public void testLoadEmailTemplates() throws Exception {
        removeDocumentTemplate("Test template with email 1");
        removeDocumentTemplate("Test template with email 2");
        removeEmailTemplate("Test email 1", DocumentArchetypes.USER_EMAIL_TEMPLATE);
        removeEmailTemplate("Test email 2", DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE);

        TemplateLoader loader = new TemplateLoader(getArchetypeService(), handlers, transactionManager,
                                                   getLookupService());
        loader.load("src/test/reports/email-templates.xml");

        Entity email1 = getTemplate("Test email 1", DocumentArchetypes.USER_EMAIL_TEMPLATE);
        Entity email2 = getTemplate("Test email 2", DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE);

        DocumentAct act1 = checkEmailTemplate(email1, "Test email 1", "Email template",
                                              "concat('Invoice from ', $OpenVPMS.location.name)", "XPATH",
                                              ".", ".", null, "Test Invoice Notes.jrxml", "text/xml");
        DocumentAct act2 = checkEmailTemplate(email2, "Test email 2", "Email template", "Email subject 2", "TEXT",
                                              null, null, "\"Foo Bar\" <foo@bar.com>", "Test Title.jrxml", "text/xml");

        Document doc1 = get(act1.getDocument(), Document.class);
        Document doc2 = get(act2.getDocument(), Document.class);

        checkTemplate("Test template with email 1", "Invoice template", "act.customerAccountChargesInvoice",
                      "Test Invoice.jrxml", "text/xml", DocumentArchetypes.DEFAULT_DOCUMENT, email1);
        checkTemplate("Test template with email 2", "Invoice template", "act.customerAccountChargesInvoice",
                      "Test Invoice Reminders.jrxml", "text/xml", DocumentArchetypes.DEFAULT_DOCUMENT, email2);


        // run the loader again and verify the template entities haven't changed
        loader.load("src/test/reports/email-templates.xml");

        checkTemplateNotUpdated(email1);
        checkTemplateNotUpdated(email2);

        // NOTE: the documents probably have changed as JRXMLDocumentHandlers reserializes.
        DocumentAct act1Reloaded = get(act1);
        assertNotNull(act1Reloaded);
        DocumentAct act2Reloaded = get(act2);
        assertNotNull(act2Reloaded);

        if (!Objects.equals(act1.getDocument(), act1Reloaded.getDocument())) {
            // document was reloaded - verify doc1 has been removed
            assertNull(get(doc1));
        }

        if (!Objects.equals(act2.getDocument(), act2Reloaded.getDocument())) {
            // document was reloaded - verify doc2 has been removed
            assertNull(get(doc2));
        }
    }

    /**
     * Verifies there is only a single instance of a template, and it matches that in the database.
     *
     * @param template the template
     */
    private void checkTemplateNotUpdated(Entity template) {
        Entity other = getTemplate(template.getName(), template.getArchetype());
        assertNotNull(other);
        assertEquals(template, other);
        assertEquals(template.getVersion(), other.getVersion());
    }

    /**
     * Verifies there is only a single <em>act.documentTemplate</em> associated with an <em>entity.documentTemplate</em>
     *
     * @param act      the act
     * @param template the template
     */
    private void checkActNotUpdated(DocumentAct act, Entity template) {
        DocumentAct current = get(act);
        assertNotNull(current);
        assertEquals(act.getVersion(), current.getVersion());

        // verify there is only act linked to the template
        ArchetypeQuery query = new ArchetypeQuery(DocumentArchetypes.DOCUMENT_TEMPLATE_ACT);
        query.add(Constraints.join("template").add(Constraints.eq("entity", template)));
        query.setMaxResults(2);
        assertEquals(1, getArchetypeService().get(query).getResults().size());
    }

    /**
     * Verifies an <em>act.documentTemplate</em> matches that in the database.
     *
     * @param document the document
     */
    private void checkDocumentNotUpdated(Document document) {
        Document current = get(document);
        assertNotNull(current);
        assertEquals(document.getVersion(), current.getVersion());
    }

    /**
     * Verifies an entity.documentTemplate exists and matches that expected.
     *
     * @param name          the template name
     * @param description   the expected template description
     * @param archetype     the expected 'archetype' (see entity.documentTemplate/archetype)
     * @param fileName      the expected template file name
     * @param mimeType      the expected template mime type
     * @param docType       the expected template document archetype
     * @param emailTemplate the expected email template. May be {@code null}
     */
    private DocumentAct checkTemplate(String name, String description, String archetype, String fileName,
                                      String mimeType, String docType, Entity emailTemplate) {
        Entity template = getTemplate(name, DocumentArchetypes.DOCUMENT_TEMPLATE);
        return checkTemplate(template, name, description, archetype, fileName, mimeType, docType, emailTemplate);
    }

    /**
     * Verifies an entity.documentTemplate matches that expected.
     *
     * @param entity        the template to check
     * @param name          the expected template name
     * @param description   the expected template description
     * @param type          the expected type
     * @param fileName      the expected template file name
     * @param mimeType      the expected template mime type
     * @param docType       the expected template document archetype
     * @param emailTemplate the expected email template. May be {@code null}
     */
    private DocumentAct checkTemplate(Entity entity, String name, String description, String type,
                                      String fileName, String mimeType, String docType, Entity emailTemplate) {
        assertNotNull(entity);
        DocumentTemplate template = new DocumentTemplate(entity, getArchetypeService());
        assertEquals(name, template.getName());
        assertEquals(description, template.getDescription());
        assertEquals(type, template.getType());
        org.openvpms.archetype.rules.doc.EmailTemplate actual = template.getEmailTemplate();
        if (actual != null) {
            assertEquals(emailTemplate, actual.getEntity());
        } else {
            assertNull(emailTemplate);
        }
        return checkDocument(entity, fileName, mimeType, docType);
    }

    /**
     * Verifies an email template exists and matches that expected.
     *
     * @param name                the template name
     * @param description         the expected description
     * @param subject             the expected subject
     * @param subjectType         the expected subject     *
     * @param subjectSource       the expected subject source
     * @param contentSource       the expected content source
     * @param defaultEmailAddress the expected default email address
     * @param fileName            the expected file name
     * @param mimeType            the expected mime type
     * @return the document
     */
    private DocumentAct checkEmailTemplate(Entity template, String name, String description, String subject,
                                           String subjectType, String subjectSource, String contentSource,
                                           String defaultEmailAddress, String fileName, String mimeType) {
        assertNotNull(template);
        assertEquals(name, template.getName());
        assertEquals(description, template.getDescription());
        IMObjectBean bean = getBean(template);
        assertEquals(subject, bean.getString("subject"));
        assertEquals(subjectType, bean.getString("subjectType"));
        assertEquals(subjectSource, bean.getString("subjectSource"));
        assertNull(bean.getString("content"));
        assertEquals(contentSource, bean.getString("contentSource"));
        assertEquals(defaultEmailAddress, bean.getString("defaultEmailAddress"));
        return checkDocument(template, fileName, mimeType, DocumentArchetypes.DEFAULT_DOCUMENT);
    }

    /**
     * Verifies a document matches that expected.
     *
     * @param template the template
     * @param fileName the expected file name
     * @param mimeType the expected mime type
     * @param docType  the expected doc type
     * @return the document act
     */
    private DocumentAct checkDocument(Entity template, String fileName, String mimeType, String docType) {
        TemplateHelper helper = new TemplateHelper(getArchetypeService());
        DocumentAct act = helper.getDocumentAct(template);
        assertNotNull(act);
        assertEquals(fileName, act.getName());
        assertEquals(mimeType, act.getMimeType());
        assertNotNull(act.getDocument());
        Document document = (Document) get(act.getDocument());
        assertNotNull(document);
        assertEquals(docType, document.getArchetype());
        assertEquals(fileName, document.getName());
        assertEquals(mimeType, document.getMimeType());
        return act;
    }

    /**
     * Returns the document template with the given name and type, and verifies there is only a single instance.
     *
     * @param name the name
     * @param type the type
     * @return the document template or {@code null} if none is found
     */
    private Entity getTemplate(String name, String type) {
        Entity result = null;
        ArchetypeQuery query = new ArchetypeQuery(type, false);
        query.add(Constraints.eq("name", name));
        query.add(Constraints.sort("id", false));
        IMObjectQueryIterator<Entity> iterator = new IMObjectQueryIterator<>(query);
        if (iterator.hasNext()) {
            result = iterator.next();
            if (iterator.hasNext()) {
                fail("There are multiple instances of " + name);
            }
        }
        return result;
    }

    /**
     * Removes an <em>act.documentTemplate</em> by file name.
     *
     * @param name the file name
     */
    private void removeTemplateByAct(String name) {
        ArchetypeQuery query = new ArchetypeQuery(DocumentArchetypes.DOCUMENT_TEMPLATE_ACT, false);
        query.add(Constraints.eq("fileName", name));
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        IMObjectQueryIterator<DocumentAct> iterator = new IMObjectQueryIterator<>(query);
        while (iterator.hasNext()) {
            DocumentAct act = iterator.next();
            IMObjectBean bean = getBean(act);
            Entity template = bean.getTarget("template", Entity.class);
            Document document = (Document) get(act.getDocument());
            remove(act);
            if (document != null) {
                remove(document);
            }
            remove(template);
        }
    }

    /**
     * Removes all <em>entity.documentTemplate</em> instances by name.
     *
     * @param name the name
     */
    private void removeDocumentTemplate(String name) {
        ArchetypeQuery query = new ArchetypeQuery(DocumentArchetypes.DOCUMENT_TEMPLATE, false);
        query.add(Constraints.eq("name", name));
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        IMObjectQueryIterator<Entity> iterator = new IMObjectQueryIterator<>(query);
        while (iterator.hasNext()) {
            Entity entity = iterator.next();
            removeDocumentTemplateAct(entity);
            remove(entity);
            IMObjectBean bean = getBean(entity);
            Entity emailTemplate = bean.getTarget("email", Entity.class);
            if (emailTemplate != null) {
                removeDocumentTemplateAct(emailTemplate);
                remove(emailTemplate);
            }
        }
    }

    /**
     * Removes all <em>act.documentTemplate</em> acts associated with a template.
     *
     * @param template the template
     */
    private void removeDocumentTemplateAct(Entity template) {
        ArchetypeQuery query = new ArchetypeQuery(DocumentArchetypes.DOCUMENT_TEMPLATE_ACT, false);
        query.add(Constraints.join("template").add(Constraints.eq("entity", template)));
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        IMObjectQueryIterator<DocumentAct> iterator = new IMObjectQueryIterator<>(query);
        while (iterator.hasNext()) {
            DocumentAct act = iterator.next();
            remove(act);
            if (act.getDocument() != null) {
                Document document = (Document) get(act.getDocument());
                if (document != null) {
                    remove(document);
                }
            }
        }
    }

    /**
     * Removes all email templates by name.
     *
     * @param name      the template name
     * @param archetype the template archetype
     */
    private void removeEmailTemplate(String name, String archetype) {
        ArchetypeQuery query = new ArchetypeQuery(archetype, false);
        query.add(Constraints.eq("name", name));
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        IMObjectQueryIterator<Entity> iterator = new IMObjectQueryIterator<>(query);
        while (iterator.hasNext()) {
            Entity template = iterator.next();
            removeDocumentTemplateAct(template);
            remove(template);
        }
    }
}
