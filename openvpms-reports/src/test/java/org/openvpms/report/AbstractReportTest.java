/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;


/**
 * Abstract base class for report tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractReportTest extends ArchetypeServiceTest {

    /**
     * The document handlers.
     */
    @Autowired
    protected DocumentHandlers handlers;

    /**
     * The customer factory.
     */
    @Autowired
    protected TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    protected TestPatientFactory patientFactory;

    /**
     * Helper to load a document from a file.
     *
     * @param path     the file path
     * @param mimeType the mime type
     * @return a new document
     */
    protected Document getDocument(String path, String mimeType) {
        File file = new File(path);
        return DocumentHelper.create(file, mimeType, handlers);
    }

    /**
     * Returns the document handlers.
     *
     * @return the document handlers
     */
    protected DocumentHandlers getHandlers() {
        return handlers;
    }

    /**
     * Helper to create a new object wrapped in a bean.
     *
     * @param shortName the archetype short name
     * @return the new object
     */
    protected IMObjectBean createBean(String shortName) {
        return getBean(create(shortName));
    }

    /**
     * Helper to create and save a new customer with firstName 'J', lastName
     * 'Zoo', address '1234 Foo St', suburb 'Melbourne' and postcode '3001'.
     *
     * @return a new customer
     */
    protected Party createCustomer() {
        return createCustomer("J", "Zoo");
    }

    /**
     * Helper to create and save a new customer with the specified names,
     * address '1234 Foo St', suburb 'Melbourne' and postcode '3001'.
     *
     * @param firstName the first name
     * @param lastName  the last name
     * @return a new customer
     */
    protected Party createCustomer(String firstName, String lastName) {
        return customerFactory.newCustomer()
                .firstName(firstName)
                .lastName(lastName)
                .newLocation()
                .address("1234 Foo St")
                .state("VIC", "Victoria")
                .suburb("MELBOURNE", "Melbourne")
                .postcode("3001")
                .preferred()
                .add()
                .build();
    }

}
